﻿/*=================================================================*
 * Interface: <IStatPageService>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::StatPage:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IStatPageService
    {
        StatPage GetStatPage(int id);
        List<StatPage> GetStatPages();
    }
}