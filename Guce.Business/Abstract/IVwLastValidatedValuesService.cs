﻿using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Business.Abstract
{
    public interface IVwLastValidatedValuesService
    {
        List<VwLastValidatedValues> GetLastValidatedValues(int entityId);
    }
}
