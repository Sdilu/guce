﻿/*=================================================================*
 * Interface: <ISiteService>
 * Version/date: <2.0.0> <2016.09.21>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::Site:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface ISiteService
    {
        Site GetSite(int id);
        Site GetSite(string code);
        List<Site> GetSites();
    }
}