﻿/*=================================================================*
 * Interface: <IDicVersionCodeService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::DicVersionCode:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IDicVersionCodeService
    {
        List<DicVersionCode> GetDicVersionCodes();
    }
}