﻿/*=================================================================*
 * Interface: <IDicFieldService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::DicField:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Abstract
{
    public interface IDicFieldService
    {
        DicField GetDicField(int id);
        List<DicField> GetDicFields();
        bool IsDate(int fieldId);
        List<DicField> GetDicFields(int dictionaryId);
        List<DicField> GetRequiredAndWarningFields(int dictionaryId);
        List<DicField> GetSearchableDicFields(int referenceId);
        List<IGrouping<string, DicField>> GetFields(int referenceId);
        List<DicField> GetRccmFields();
        List<FieldProperty> GetFieldProperties();
    }
}