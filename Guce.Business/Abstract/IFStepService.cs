﻿/*=================================================================*
 * Interface: <IFStepService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::FStep:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IFStepService
    {
        int AddReturnKey(FStep step);
        WfTransition GetTransition(int id);
        bool IsReadyToBeActivated(List<WfTransition> transitions, int flowId);
        List<WfTransition> GetFromTransitionsByAction(int actionId);
        List<WfTransition> GetToTransitionsByAction(int actionId);
        FStep GetFStep(int id);
        List<FStep> GetFSteps(int flowId);
        void Add(FStep fStep);
        void Update(FStep fStep);
        void Delete(FStep fStep);
    }
}