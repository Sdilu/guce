﻿/*=================================================================*
 * Interface: <IVwAllValueService>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette interface represente l'abstraction de la vue ::vw_All_values:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Business.Abstract
{
    public interface IVwAllValueService
    {
        //List<VwAllValue> GetAllValues();
        List<VwAllValue> GetAllValues(int entityId);
    }
}