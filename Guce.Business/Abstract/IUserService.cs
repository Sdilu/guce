﻿/*=================================================================*
 * Interface: <IUserService>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::User:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using User = Guce.Entities.Concrete.User;

namespace Guce.Business.Abstract
{
    public interface IUserService
    {
        List<User> GetUsers();
        List<User> GetUsersWithoutGlobal();
        User GetUser(string username);
        void Add(User user);
        void Update(User user);
        void Delete(User user);
    }
}