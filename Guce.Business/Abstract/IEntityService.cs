﻿/*=================================================================*
 * Interface: <IEntityService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::Entity:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Abstract
{
    public interface IEntityService
    {
        Entity GetEntity(int id);
        List<Entity> GetEntities(int dicId);
        //List<Entity> GetEntities(int? dicId);
        List<Entity> GetEntities(int dicId, int entityId, int flowId, int actionId, string username, List<EntityIdDicId> entitiesDicIds);
        List<Entity> GetEntities(int? dicId, string keyword);
        List<string> GetReferences(int dicId, string keyword);
        void Add(Entity entity);
        void Update(Entity entity);
        void Delete(Entity entity);
    }
}