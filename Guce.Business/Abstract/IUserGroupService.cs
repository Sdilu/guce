﻿/*=================================================================*
 * Interface: <IUserGroupService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::UserGroup:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IUserGroupService
    {
        List<UserGroup> GetUserGroups();
        bool IsAllowExecuteAction(string username, int grpId);
        List<Group> GetGroups(string username);
        bool InGroup(int groupId, string username);
        string Groups(string username);
        string Roles(string username);
        void SetGroupRole(User user);
        void Add(UserGroup usergroup);
        void Update(UserGroup usergroup);
        void Delete(UserGroup usergroup);
    }
}