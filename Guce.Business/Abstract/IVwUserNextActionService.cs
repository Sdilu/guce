﻿/*=================================================================*
 * Interface: <IVwUserNextActionService>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette interface represente l'abstraction de la vue ::vw_user_next_actions:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Business.Abstract
{
    public interface IVwUserNextActionService
    {
        VwUserNextAction GetUserNextAction(int actionId, int flowId);
        int GetUserNextActionsHomePageCount();
        int GetActiveNextActionsHomePageCount();
        List<VwActiveNextAction> GetActiveNextActionsHomePage();
        List<VwUserNextAction> GetUserNextActionsHomePage();
        List<VwUserNextAction> GetVwUserNextActions();
        List<VwUserNextAction> GetVwUserNextActions(int flowId);
        List<VwActiveNextAction> GetVwActiveNextActions(int flowId);
        bool IsCreateEntity(int actionId, int flowId);
    }
}