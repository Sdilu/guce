﻿/*=================================================================*
 * Interface: <IFVariableValueService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::FVariableValue:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IFVariableValueService
    {
        FVariableValue GetFVariableValue(int id);
        FVariableValue GetVariableValue(int statusId, int wvaId);
        List<FVariableValue> GetFVariableValues(int statusId);
        bool IsDate(int wvaId);
        bool IsMultiValued(int wvaId);
        void Add(FVariableValue fVariableValue);
        void Update(FVariableValue fVariableValue);
        void Delete(FVariableValue fVariableValue);
    }
}