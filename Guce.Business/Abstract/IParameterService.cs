﻿/*=================================================================*
 * Interface: <IParameterService>
 * Version/date: <2.0.0> <2016.08.30>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::Parameter:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Abstract
{
    public interface IParameterService
    {
        Parameter GetParameter(string id, string username);
        string GetValue(string id, string username);
        List<Parameter> GetParameters(string username);
        List<Parameter> GetParameters(string app, string username);
        List<Parameter> GetParameters();
        void Add(Parameter parameter);
        void Update(Parameter parameter);
    }
}