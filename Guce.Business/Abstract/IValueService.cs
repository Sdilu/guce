﻿/*=================================================================*
 * Interface: <IValueService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::Value:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IValueService
    {
        Value GetValue(int entityId, int dflId);
        bool IsDate(int fieldId);
        bool IsMultivalued(int fieldId);
        VwFormField GetFormField(int fieldId);
        VwFormField GetFormFieldByField(int dflId, int actionId);
        VwFormField GetFormFieldByVariable(int wvaId, int actionId);
        List<VwFormField> GetFormFieldsByAction(int actionId);
        List<IGrouping<string, VwFormField>> GetFormFieldsByAction(int actionId, bool adminOnly);
        List<VwFormField> GetFormFieldsByAction(int actionId, int parentId, int dfl);
        Value GetValue(int id);
        List<Value> GetValues(int entityId);
        //List<Value> GetValues();
        void Add(Value value);
        void Update(Value value);
        void Delete(Value value);
    }
}