﻿/*=================================================================*
 * Interface: <IVwCurrentValueService>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette interface represente l'abstraction de la vue ::vw_current_values:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Business.Abstract
{
    public interface IVwCurrentValueService
    {
        VwCurrentValue GetCurrentValue(int entityId, int dflId);
        List<VwCurrentValue> GetMultiValues(int entityId, int dflId);
        List<VwCurrentValue> GetMultiValues(int entityId, int dflId, int valId);
        VwCurrentValue GetCurrentValue(int valId);
        //List<VwCurrentValue> GetCurrentValues();
        List<VwCurrentValue> GetCurrentValues(int entityId);
        bool RccmNotExist(int entityId);
        string GetLastRccm(string entityCategory);
        string GetEntityCategory(int? formeJuridiqueId);
        int GetFlow(int valId);
    }
}