﻿/*=================================================================*
 * Interface: <IVwFormFieldService>
 * Version/date: <2.0.0> <2016.08.05>
 *
 * Description: <” Cette interface represente l'abstraction de la vue ::vw_form_fields:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Business.Abstract
{
    public interface IVwFormFieldService
    {
        VwFormField GetFormField(int dflId, int wvaId);
        List<VwFormField> GetVwFormFields();
        List<VwFormField> GetVwFormFields(int actionId);
    }
}