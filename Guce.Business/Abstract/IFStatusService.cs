﻿/*=================================================================*
 * Interface: <IFStatusService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::FStatus:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IFStatusService
    {
        int CreateStatus(int flowId, int actionId, string username);
        FStatus GetFStatus(int actionId, int flowId);
        FStatus GetFStatus(int id);
        List<FStatus> GetFStatuses(int flowId);
        void Add(FStatus fStatus);
        void Update(FStatus fStatus);
        void Delete(FStatus fStatus);
    }
}