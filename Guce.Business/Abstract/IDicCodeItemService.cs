﻿/*=================================================================*
 * Interface: <IDicCodeItemService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::DicCodeItem:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.Business.Abstract
{
    public interface IDicCodeItemService
    {
        DicCodeItem GetDicCodeItem(int id);
        List<DicCodeItem> GetDicCodeItems();
        List<DicCodeItem> GetDicCodeItems(int? dcdId);
        List<DicCodeItem> GetCodeItems(int dcdId);
        List<DicCodeItem> GetDicCodeItemsByDependend(int dependDciId);
        List<DicCodeItem> GetDicCodeItems(int dcdId, int dependDciId);
        List<DicCodeItem> GetCodeInlineItems(string items);
        string GetParentCodeInline(int childId, int? end);
        int CountDependencies(int begin, int end, int times);
        int? GetDependentId(int end, int valueId);
    }
}