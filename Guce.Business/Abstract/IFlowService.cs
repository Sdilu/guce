﻿/*=================================================================*
 * Interface: <IFlowService>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface represente l'abstraction de l'entité ::Flow:: dans 
 * la couche business logic ”>
 * Specificities: <“ Cette classe est une classe d'abstraction ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Abstract
{
    public interface IFlowService
    {
        string GetLastFlowReference();
        int AddReturnKey(Flow flow);
        int AddEntity(Entity entity);
        Entity GetEntity(int entityId);
        void UpdateEntity(Entity entity);
        FlowEntity StartWorkflow(int wflId, int wfvId, int flowId, int entityId, string username);
        Flow GetFlow(int id);
        List<Flow> GetFlows(int entityId);
        void Add(Flow flow);
        void Update(Flow flow);
        void Delete(Flow flow);
    }
}