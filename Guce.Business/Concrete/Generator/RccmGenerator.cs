﻿/*=================================================================*
 * Classe: <RCCM Generator>
 * Version/date: <2.0.0> <2016.07.19>
 *
 * Description: <” Classe de génération de numéro RCCM. ”>
 * Specificities: <“- ”>
 *
 * Authors: <Vonvon> <AKELEBA>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using static Guce.Entities.Custom.Configuration;
using static Guce.Entities.Custom.ParameterId;

namespace Guce.Business.Concrete.Generator
{
    public class RccmGenerator
    {
        
        //public string LastRccm { get; set; }
        public string JuridictionCode { get; set; }
        public string EntityCategory { get; set; }
        public int Seq { get; set; }

        public string Generate()
        {
            var evalYear = Convert.ToByte(DateTime.Now.ToString("yy"));
            var prefix = "CD/"+ JuridictionCode + "/RCCM/" + evalYear + "-" + EntityCategory + "-";
            //if (string.IsNullOrWhiteSpace(LastRccm))
            //    return $"{prefix}{Convert.ToInt32(Parameters[StartRccmNumber]).ToString("D5")}".ToUpper();

            //var tab = LastRccm.Split('-');
            //return Convert.ToInt32(tab[0].Substring(tab[0].Length - 2)) == evalYear
            //    ? $"{prefix}{(Convert.ToInt32(tab[2]) + 1).ToString("D5")}".ToUpper()
            //    : $"{prefix}{Begin.ToString("D5")}".ToUpper();
            return $"{prefix}{Seq.ToString("D5")}".ToUpper();
        }
    }
}