﻿namespace Guce.Business.Concrete.Generator
{
    public interface IRccmGenerable
    {
        /// <summary>
        /// The code of entity for which RCCM number should be generated (Must be : A, B, C, E or K)
        /// </summary>
        char EntityCategory { get; set; }
        /// <summary>
        /// The code of the juridiction where the RCCM is handled
        /// </summary>
        string JuridictionCode { get; set; }
    }
}