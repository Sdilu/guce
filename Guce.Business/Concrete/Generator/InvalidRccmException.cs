﻿using System;

namespace Guce.Business.Concrete.Generator
{
    public class InvalidRccmException : Exception
    {
        private readonly string _rccm;
        public override string Message => $"Numéro RCCM fourni {this._rccm} invalide!";
        public InvalidRccmException(string rccm) { this._rccm = rccm; }
    }
}