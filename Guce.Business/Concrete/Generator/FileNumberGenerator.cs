﻿/*=================================================================*
 * Classe: <FileNumberGenerator>
 * Version/date: <2.0.0> <2016.07.14>
 *
 * Description: <” Cette classe est repository pattern pour l'ORM entityframework. ”>
 * Specificities: <“ Cette classe permet de générer le numéro Administration Guce ”>
 *
 * Authors: <Vonvon> <AKELEBA>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;

namespace Guce.Business.Concrete.Generator
{
    public class FileNumberGenerator
    {
        private readonly IFlowDal _flowDal;
        private readonly IFlowService _flowService;
        private readonly int _defaultFileNumber;
        private readonly string _juridictionCode;
        private const int Begin = 1;

        public FileNumberGenerator(IFlowDal flowDal, int defaultFileNumber, string juridictionCode)
        {
            _flowDal = flowDal;
            _defaultFileNumber = defaultFileNumber;
            _juridictionCode = juridictionCode;
        }
        public FileNumberGenerator(IFlowService flowService, int defaultFileNumber, string juridictionCode)
        {
            _flowService = flowService;
            _defaultFileNumber = defaultFileNumber;
            _juridictionCode = juridictionCode;
        }

        public string Generate()
        {
            byte currentYear = Convert.ToByte(DateTime.Now.ToString("yy"));
            var lastFileNumber = _flowDal != null ? _flowDal.GetLastFlowReference() : _flowService.GetLastFlowReference();
            if (string.IsNullOrWhiteSpace(lastFileNumber))
                return $"{currentYear}/{_juridictionCode.ToUpper()}/{Convert.ToInt32(_defaultFileNumber).ToString("D6")}";

            var fileNumbIndex = lastFileNumber.Split('/');
            return Convert.ToByte(fileNumbIndex[0]) == currentYear
                ? $"{currentYear}/{_juridictionCode.ToUpper()}/{(Convert.ToInt32(fileNumbIndex[2]) + 1).ToString("D6")}"
                : $"{currentYear}/{_juridictionCode.ToUpper()}/{Begin.ToString("D6")}";
        }
    }
}
