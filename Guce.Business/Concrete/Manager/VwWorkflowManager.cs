﻿/*=================================================================*
 * Classe: <VwWorkflowManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwWorkflow ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwWorkflowService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwWorkflowManager: IVwWorkflowService
    {
        private readonly IVwWorkflowDal _vwWorkflowDal;

        public VwWorkflowManager(IVwWorkflowDal vwWorkflowDal)
        {
            _vwWorkflowDal = vwWorkflowDal;
        }

        public VwWorkflow GetVwWorkflow(int wfvId)
        {
            return _vwWorkflowDal.Get(w => w.WfvId == wfvId);
        }

        public List<VwWorkflow> GetWorkflows()
        {
            return _vwWorkflowDal.GetList();
        }
    }
}