﻿/*=================================================================*
 * Classe: <VwValidatedValueManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwValidatedValue ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwValidatedValueService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwValidatedValueManager: IVwValidatedValueService
    {
        private readonly IVwValidatedValueDal _validatedValue;

        public VwValidatedValueManager(IVwValidatedValueDal validatedValue)
        {
            _validatedValue = validatedValue;
        }

        //public List<VwValidatedValue> GetValidatedValues()
        //{
        //    return _validatedValue.GetList();
        //}

        public List<VwValidatedValue> GetValidatedValues(int entityId)
        {
            return _validatedValue.GetList(v => v.EntId == entityId);
        }
    }
}