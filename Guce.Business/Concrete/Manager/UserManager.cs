﻿/*=================================================================*
 * Classe: <UserManager>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: User :: dans 
 * la couche business logic. Elle implémente l'interface :: IUserService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Custom;
using User = Guce.Entities.Concrete.User;

namespace Guce.Business.Concrete.Manager
{
    public class UserManager : IUserService
    {
        private readonly IUserDal _userDal;

        public UserManager(IUserDal userDal)
        {
            _userDal = userDal;
        }

        public List<User> GetUsers()
        {
            return _userDal.GetList();
        }

        public List<User> GetUsersWithoutGlobal()
        {
            return _userDal.GetList(u => !u.Username.Equals(ParameterId.App));
        }

        public User GetUser(string username)
        {
            return _userDal.Get(u => u.Username.Equals(username));
        }

        public void Add(User user)
        {
            _userDal.Add(user);
        }

        public void Update(User user)
        {
            _userDal.Update(user);
        }

        public void Delete(User user)
        {
            _userDal.Delete(user);
        }
    }
}