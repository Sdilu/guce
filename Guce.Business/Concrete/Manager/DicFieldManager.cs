﻿/*=================================================================*
 * Classe: <DicFieldManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: DicField :: dans 
 * la couche business logic. Elle implémente l'interface :: IDicFieldService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class DicFieldManager : IDicFieldService
    {
        private readonly IDicFieldDal _dicFieldDal;

        public DicFieldManager(IDicFieldDal dicFieldDal)
        {
            _dicFieldDal = dicFieldDal;
        }

        public DicField GetDicField(int id)
        {
            return _dicFieldDal.Get(d => d.Id == id);
        }

        public bool IsDate(int fieldId)
        {
            return _dicFieldDal.Get(f => f.Id == fieldId).Type.Equals(FieldType.Date);
        }

        public List<DicField> GetDicFields()
        {
            return _dicFieldDal.GetList().OrderBy(d => d.Order).ToList();
        }

        public List<DicField> GetDicFields(int dictionaryId)
        {
            return _dicFieldDal.GetList(f => f.DicId == dictionaryId).OrderBy(d => d.Order).ToList();
        }

        public List<DicField> GetRequiredAndWarningFields(int dictionaryId)
        {
            return _dicFieldDal.GetList(f => f.DicId == dictionaryId && (f.Empty.Equals(EmptyType.Error)
                                             || f.Empty.Equals(EmptyType.Warning))).OrderBy(d => d.Order).ToList();
        }

        public List<DicField> GetSearchableDicFields(int referenceId)
        {
            return _dicFieldDal.GetList(f => f.DicId == referenceId && f.SearchCriteria > 0)
                    .OrderBy(a => a.Order).ToList();
        }

        public List<IGrouping<string, DicField>> GetFields(int referenceId)
        {
            return _dicFieldDal.GetList(f => f.DicId == referenceId)
                    .OrderBy(a => a.Order)
                    .GroupBy(f => f.Group).ToList();
        }

        public List<DicField> GetRccmFields()
        {
            return _dicFieldDal.GetList(f => f.Name.Equals(TechnicalName.Rccm) || f.Name.Equals(TechnicalName.DateImmatriculationRccm));
        }

        public List<FieldProperty> GetFieldProperties()
        {
            return GetDicFields().Select(field => new FieldProperty
            {
                Name = field.Name,
                Field = field.DisplayName,
                Type = field.Type,
                Multivalued = field.MultiValued
            }).ToList();
        }
    }
}