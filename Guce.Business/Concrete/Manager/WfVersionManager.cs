﻿/*=================================================================*
 * Classe: <WfVersionManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: WfVersion :: dans 
 * la couche business logic. Elle implémente l'interface :: IWfVersionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class WfVersionManager : IWfVersionService
    {
        private readonly IWfVersionDal _wfVersionDal;

        public WfVersionManager(IWfVersionDal wfVersionDal)
        {
            _wfVersionDal = wfVersionDal;
        }

        public WfVersion GetWfVersion(int id)
        {
            return _wfVersionDal.Get(d => d.Id == id);
        }

        public List<WfVersion> GetWfVersions()
        {
            return _wfVersionDal.GetList();
        }
    }
}