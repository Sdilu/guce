﻿/*=================================================================*
 * Classe: <EntityManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Entity :: dans 
 * la couche business logic. Elle implémente l'interface :: IEntityService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class EntityManager : IEntityService
    {
        private readonly IEntityDal _entityDal;
        private readonly IValueService _valueService;
        private readonly IFStatusService _statusService;
        private readonly IDictionaryService _dictionaryService;

        public EntityManager(IEntityDal entityDal)
        {
            _entityDal = entityDal;
        }

        public EntityManager(IEntityDal entityDal, IValueService valueService, 
            IFStatusService statusService, IDictionaryService dictionaryService)
        {
            _entityDal = entityDal;
            _valueService = valueService;
            _statusService = statusService;
            _dictionaryService = dictionaryService;
        }

        public Entity GetEntity(int id)
        {
            return _entityDal.Get(d => d.Id == id);
        }

        public List<Entity> GetEntities(int dicId)
        {
            return _entityDal.GetList();
        }

        //public List<Entity> GetEntities(int? dicId)
        //{
        //    return dicId != null ? GetEntities(dicId) : GetEntities(dicId);
        //}

        public List<Entity> GetEntities(int dicId, int entityId, int flowId, int actionId, string username, List<EntityIdDicId> entitiesDicIds)
        {
            var listCreate = _dictionaryService.GetDictionary(dicId).ListCreate;
            if (!listCreate) return new List<Entity>();
            var entityIds = EfDatabaseFunction.GetAllDicFromEntity(entityId, dicId).Distinct().ToList();
            var statusId = _statusService.CreateStatus(flowId, actionId, username);
            var entityIds2 = (from entity in GetEntities(dicId)
                join value in _valueService.GetValues(entityId) on entity.Id equals value.EntId
                where entity.DicId == dicId && value.FstId == statusId
                select entity.Id).ToList();
            foreach (var id in entityIds2.Where(id => !entityIds.Contains(id)))
            {
                entityIds.Add(id);
            }
            if (entitiesDicIds != null && entitiesDicIds.Count > 0)
            {
                foreach (var entityIdDicId in entitiesDicIds.Where(ed => ed.DicId == dicId && !entityIds.Contains(ed.EntityId)))
                {
                    entityIds.Add(entityIdDicId.EntityId);
                }
            }
            return entityIds.Select(id => _entityDal.Get(e => e.Id == id)).ToList();
        }

        public List<Entity> GetEntities(int? dicId, string keyword)
        {
            if (keyword == null && dicId != null)
            {
                return GetEntities((int)dicId);
            }
            return dicId != null ? GetEntities((int)dicId).Where(e => e.DicId == dicId && e.Name.ToUpper().Contains(keyword.ToUpper())).ToList() : null;
        }

        public List<string> GetReferences(int dicId, string keyword)
        {

            var value = (from e in GetEntities(dicId) where e.DicId == dicId select e.Name).ToList();
            return value;
        }

        public void Add(Entity entity)
        {
            _entityDal.Add(entity);
        }

        public void Update(Entity entity)
        {
            _entityDal.Update(entity);
        }

        public void Delete(Entity entity)
        {
            _entityDal.Delete(entity);
        }
    }
}