﻿/*=================================================================*
 * Classe: <SiteManager>
 * Version/date: <2.0.0> <2016.09.21>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Site :: dans 
 * la couche business logic. Elle implémente l'interface :: ISiteService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class SiteManager : ISiteService
    {
        private readonly ISiteDal _siteDal;

        public SiteManager(ISiteDal siteDal)
        {
            _siteDal = siteDal;
        }

        public Site GetSite(int id)
        {
            return _siteDal.Get(s => s.Id == id);
        }

        public Site GetSite(string code)
        {
            return _siteDal.Get(s => s.Code.Equals(code));
        }

        public List<Site> GetSites()
        {
            return _siteDal.GetList();
        }
    }
}