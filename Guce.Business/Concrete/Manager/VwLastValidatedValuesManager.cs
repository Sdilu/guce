﻿using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwLastValidatedValuesManager : IVwLastValidatedValuesService
    {
        private readonly IVwLastValidatedValuesDal _vwLastValidatedValuesDal;

        public VwLastValidatedValuesManager(IVwLastValidatedValuesDal vwLastValidatedValuesDal)
        {
            _vwLastValidatedValuesDal = vwLastValidatedValuesDal;
        }

        public List<VwLastValidatedValues> GetLastValidatedValues(int entityId)
        {
            return _vwLastValidatedValuesDal.GetList(e => e.ent_id == entityId).OrderBy(w => w.val_id).ToList();
        }
    }
}
