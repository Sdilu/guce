﻿/*=================================================================*
 * Classe: <IndexManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Index :: dans 
 * la couche business logic. Elle implémente l'interface :: IIndexService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class IndexManager : IIndexService
    {
        private readonly IIndexDal _indexDal;

        public IndexManager(IIndexDal indexDal)
        {
            _indexDal = indexDal;
        }
        
        public Index GetIndex(int id)
        {
            return _indexDal.Get(d => d.Id == id);
        }

        public List<Index> GetIndexes()
        {
            return _indexDal.GetList();
        }
    }
}