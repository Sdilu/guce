﻿/*=================================================================*
 * Classe: <VwCurrentVariableValueManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwCurrentVariableValue ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwActiveWorkflowService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwCurrentVariableValueManager: IVwCurrentVariableValueService
    {
        private readonly IVwCurrentVariableValueDal _currentVariable;
        private readonly IWfVariableDal _variableDal;
        
        public VwCurrentVariableValueManager(IVwCurrentVariableValueDal currentVariable, IWfVariableDal variableDal)
        {
            _currentVariable = currentVariable;
            _variableDal = variableDal;
        }

        public VwCurrentVariableValue GetCurrentVariableValue(int wvaId, int flowId)
        {
            return _variableDal.Get(v => v.Id == wvaId).MultiValued ? GetCurrentVariableValues(flowId).OrderBy(v => v.FvvId).FirstOrDefault(v => v.WvaId == wvaId && v.FlwId == flowId) 
                : GetCurrentVariableValues(flowId).Where(v => v.WvaId == wvaId && v.FlwId == flowId).OrderByDescending(v => v.Pos).FirstOrDefault();
        }

        //public List<VwCurrentVariableValue> GetCurrentVariableValues()
        //{
        //    return _currentVariable.GetList();
        //}

        public List<VwCurrentVariableValue> GetMultiVariables(int wvaId, int flowId, int valId)
        {
            return GetCurrentVariableValues(flowId).Where(v => v.WvaId == wvaId && v.FlwId == flowId && v.FvvId != valId).ToList();
        }

        public List<VwCurrentVariableValue> GetCurrentVariableValues(int flowId)
        {
            return _currentVariable.GetList(v => v.FlwId == flowId).OrderBy(v => v.Order).ToList();
        }

        public int GetFlow(int variableId)
        {
            return _currentVariable.Get(v => v.FvvId == variableId).FlwId;
        }
    }
}