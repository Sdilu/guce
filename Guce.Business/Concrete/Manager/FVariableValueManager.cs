﻿/*=================================================================*
 * Classe: <FVariableValueManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: FVariableValue :: dans 
 * la couche business logic. Elle implémente l'interface :: IFVariableValueService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class FVariableValueManager : IFVariableValueService
    {
        private readonly IFVariableValueDal _fVariableValueDal;
        private readonly IWfVariableDal _variableDal;

        public FVariableValueManager(IFVariableValueDal fVariableValueDal, IWfVariableDal variableDal)
        {
            _fVariableValueDal = fVariableValueDal;
            _variableDal = variableDal;
        }

        public FVariableValue GetFVariableValue(int id)
        {
            return _fVariableValueDal.Get(d => d.Id == id);
        }

        public FVariableValue GetVariableValue(int statusId, int wvaId)
        {
            return GetFVariableValues(statusId).LastOrDefault(v =>v.FstId == statusId && v.WvaId == wvaId);
        }

        public List<FVariableValue> GetFVariableValues(int statusId)
        {
            return _fVariableValueDal.GetList();
        }

        public bool IsDate(int wvaId)
        {
            return _variableDal.Get(f => f.Id == wvaId).Type.Equals(FieldType.Date);
        }

        public bool IsMultiValued(int wvaId)
        {
            return _variableDal.Get(f => f.Id == wvaId).MultiValued;
        }

        public void Add(FVariableValue fVariableValue)
        {
            _fVariableValueDal.Add(fVariableValue);
        }

        public void Update(FVariableValue fVariableValue)
        {
            _fVariableValueDal.Update(fVariableValue);
        }

        public void Delete(FVariableValue fVariableValue)
        {
            _fVariableValueDal.Delete(fVariableValue);
        }
    }
}