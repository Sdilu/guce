﻿/*=================================================================*
 * Classe: <GroupManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Group :: dans 
 * la couche business logic. Elle implémente l'interface :: IGroupService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class GroupManager : IGroupService
    {
        private readonly IGroupDal _groupDal;

        public GroupManager(IGroupDal groupDal)
        {
            _groupDal = groupDal;
        }

        public Group GetGroup(int id)
        {
            return _groupDal.Get(d => d.Id == id);
        }

        public List<Group> GetGroups()
        {
            return _groupDal.GetList();
        }
    }
}