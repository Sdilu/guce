﻿/*=================================================================*
 * Classe: <FStepManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: FStep :: dans 
 * la couche business logic. Elle implémente l'interface :: IFStepService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class FStepManager : IFStepService
    {
        private readonly IFStepDal _fStepDal;
        private readonly IWfTransitionDal _transitionDal;

        public FStepManager(IFStepDal fStepDal, IWfTransitionDal transitionDal)
        {
            _fStepDal = fStepDal;
            _transitionDal = transitionDal;
        }

        public int AddReturnKey(FStep step)
        {
            return _fStepDal.AddReturnKey(step);
        }

        public WfTransition GetTransition(int id)
        {
            return _transitionDal.Get(t => t.Id == id);
        }

        public bool IsReadyToBeActivated(List<WfTransition> transitions, int flowId)
        {
            var count = GetFSteps(flowId).Sum(step => transitions.Count(tr => step.FlwId == flowId && step.WtrId == tr.Id));
            return transitions.Count == count;
        }

        public List<WfTransition> GetFromTransitionsByAction(int actionId)
        {
            return _transitionDal.GetFromTransitionsByAction(actionId);
        }

        public List<WfTransition> GetToTransitionsByAction(int actionId)
        {
            return _transitionDal.GetToTransitionsByAction(actionId);
        }

        public FStep GetFStep(int id)
        {
            return _fStepDal.Get(d => d.Id == id);
        }

        public List<FStep> GetFSteps(int flowId)
        {
            return _fStepDal.GetList(s => s.FlwId == flowId);
        }
        
        public void Add(FStep fStep)
        {
            _fStepDal.Add(fStep);
        }

        public void Update(FStep fStep)
        {
            _fStepDal.Update(fStep);
        }

        public void Delete(FStep fStep)
        {
            _fStepDal.Delete(fStep);
        }
    }
}