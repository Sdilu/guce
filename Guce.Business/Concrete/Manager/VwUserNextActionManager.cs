﻿/*=================================================================*
 * Classe: <VwUserNextActionManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwUserNextAction ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwUserNextActionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class VwUserNextActionManager: IVwUserNextActionService
    {
        private readonly IVwUserNextActionDal _vwUserNextActionDal;
        private readonly IVwActiveNextActionDal _activeNextActionDal;
        private readonly IWfActionDal _actionDal;

        public VwUserNextActionManager(IVwUserNextActionDal vwUserNextActionDal, IWfActionDal actionDal, 
            IVwActiveNextActionDal activeNextActionDal)
        {
            _vwUserNextActionDal = vwUserNextActionDal;
            _actionDal = actionDal;
            _activeNextActionDal = activeNextActionDal;
        }

        public VwUserNextAction GetUserNextAction(int actionId, int flowId)
        {
            return _vwUserNextActionDal.GetList(a => a.WacId == actionId && a.FlwId == flowId).FirstOrDefault();
        }

        public int GetUserNextActionsHomePageCount()
        {
            return GetUserNextActionsHomePage().Count;
        }

        public int GetActiveNextActionsHomePageCount()
        {
            return GetActiveNextActionsHomePage().Count;
        }

        public List<VwUserNextAction> GetUserNextActionsHomePage()
        {
            return _vwUserNextActionDal.GetList().Where(a => !a.WacAction.Equals(ActionType.Null)).OrderByDescending(a => a.FlwId)
                .Distinct(new VwUserNextActionComparer()).ToList();
        }

        public List<VwActiveNextAction> GetActiveNextActionsHomePage()
        {
            return _activeNextActionDal.GetList().Where(a => !a.WacAction.Equals(ActionType.Null))
                .Distinct(new VwActiveNextActionComparer()).ToList();
        }

        public List<VwUserNextAction> GetVwUserNextActions()
        {
            return _vwUserNextActionDal.GetList();
        }

        public List<VwUserNextAction> GetVwUserNextActions(int flowId)
        {
            return _vwUserNextActionDal.GetList(a => a.FlwId == flowId);
        }

        public List<VwActiveNextAction> GetVwActiveNextActions(int flowId)
        {
            return _activeNextActionDal.GetList(a => a.FlwId == flowId);
        }

        public bool IsCreateEntity(int actionId, int flowId)
        {
            var wfvId = _vwUserNextActionDal.Get(a => a.WacId == actionId && a.FlwId == flowId).WfvId;
            var beginAction = _actionDal.GetList(a => a.WfvId == wfvId && a.WfBegin).First();
            return beginAction.Action.Equals(ActionType.CreateEntity);
        }
    }
}