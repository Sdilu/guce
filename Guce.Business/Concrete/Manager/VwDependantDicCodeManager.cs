﻿/*=================================================================*
 * Classe: <VwDependantDicCodeManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwDependantDicCode ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwDependantDicCodeService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwDependantDicCodeManager: IVwDependantDicCodeService
    {
        private readonly IVwDependantDicCodeDal _vwDependantDicCodeDal;

        public VwDependantDicCodeManager(IVwDependantDicCodeDal vwDependantDicCodeDal)
        {
            _vwDependantDicCodeDal = vwDependantDicCodeDal;
        }
        
        public List<VwDependantDicCode> GetVwDependantDicCodes()
        {
            return _vwDependantDicCodeDal.GetList();
        }
    }
}