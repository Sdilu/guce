﻿/*=================================================================*
 * Classe: <DicVersionManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: DicVersion :: dans 
 * la couche business logic. Elle implémente l'interface :: IDicVersionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class DicVersionManager : IDicVersionService
    {
        private readonly IDicVersionDal _dicVersionDal;

        public DicVersionManager(IDicVersionDal dicVersionDal)
        {
            _dicVersionDal = dicVersionDal;
        }

        public DicVersion GetDicVersion(int id)
        {
            return _dicVersionDal.Get(d => d.Id == id);
        }

        public List<DicVersion> GetDicVersions()
        {
            return _dicVersionDal.GetList();
        }
    }
}