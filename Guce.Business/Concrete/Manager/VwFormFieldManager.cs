﻿/*=================================================================*
 * Classe: <VwFormFieldManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwFormField ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwActiveWorkflowService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwFormFieldManager: IVwFormFieldService
    {
        private readonly IVwFormFieldDal _vwFormFieldDal;

        public VwFormFieldManager(IVwFormFieldDal vwFormFieldDal)
        {
            _vwFormFieldDal = vwFormFieldDal;
        }

        public VwFormField GetFormField(int dflId, int wvaId)
        {
            return dflId != 0 ? _vwFormFieldDal.GetList(f => f.DflId != null && f.DflId == dflId).FirstOrDefault() 
                : _vwFormFieldDal.GetList(f => f.WvaId != null && f.WvaId == wvaId).FirstOrDefault();
        }

        public List<VwFormField> GetVwFormFields()
        {
            return _vwFormFieldDal.GetList().OrderBy(a => a.Order).ToList();
        }

        public List<VwFormField> GetVwFormFields(int actionId)
        {
            return _vwFormFieldDal.GetList(a => a.WacId == actionId).OrderBy(a => a.Order).ToList();
        }
        
    }
}