﻿/*=================================================================*
 * Classe: <UserGroupManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: UserGroup :: dans 
 * la couche business logic. Elle implémente l'interface :: IUserGroupService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class UserGroupManager : IUserGroupService
    {
        private readonly IUserGroupDal _userGroupDal;
        private readonly IGroupDal _groupDal;

        public UserGroupManager(IUserGroupDal userGroupDal, IGroupDal groupDal)
        {
            _userGroupDal = userGroupDal;
            _groupDal = groupDal;
        }

        public List<UserGroup> GetUserGroups()
        {
            return _userGroupDal.GetList();
        }

        public bool IsAllowExecuteAction(string username, int actionGrpId)
        {
            return GetUserGroups().Any(ug => ug.GrpId == actionGrpId && ug.Username.Equals(username));
        }

        public List<Group> GetGroups(string username)
        {
            return (
                from g in _groupDal.GetList()
                from ug in GetUserGroups()
                where ug.GrpId == g.Id && ug.Username.Equals(username)
                select g)
                .ToList();
        }

        public bool InGroup(int groupId, string username)
        {
            return GetGroups(username).Any(@group => @group.Id == groupId);
        }

        public string Groups(string username)
        {
            var groups = GetGroups(username).Aggregate("", (current, ug) => current + ug.Name + ", ");
            if (groups.Length > 2)
            {
                groups = groups.Remove(groups.Length - 2);
            }
            return groups;
        }

        public string Roles(string username)
        {
            return EfDatabaseFunction.GetUserRoles(username).FirstOrDefault();
        }

        public void SetGroupRole(User user)
        {
            user.Group = Groups(user.Username);
            user.Role = Roles(user.Username);
        }

        public void Add(UserGroup usergroup)
        {
            _userGroupDal.Add(usergroup);
        }

        public void Update(UserGroup usergroup)
        {
            _userGroupDal.Update(usergroup);
        }

        public void Delete(UserGroup usergroup)
        {
            _userGroupDal.Delete(usergroup);
        }
    }
}