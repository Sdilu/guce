﻿/*=================================================================*
 * Classe: <FStatusManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: FStatus :: dans 
 * la couche business logic. Elle implémente l'interface :: IFStatusService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class FStatusManager : IFStatusService
    {
        private readonly IFStatusDal _fStatusDal;
        private readonly IFStepDal _fStepDal;

        public FStatusManager(IFStatusDal fStatusDal)
        {
            _fStatusDal = fStatusDal;
        }

        public FStatusManager(IFStatusDal fStatusDal, IFStepDal fStepDal)
        {
            _fStatusDal = fStatusDal;
            _fStepDal = fStepDal;
        }

        public int CreateStatus(int flowId, int actionId, string username)
        {
            var status = FindStatus(actionId, flowId);
            if (status != null)
            {
                return status.Id;
            }
            return _fStatusDal.AddReturnKey(new FStatus
            {
                FlwId = flowId,
                WacId = actionId,
                CreationTime = DateTime.Now,
                CreatedBy = username
            });
        }

        private FStatus FindStatus(int actionId, int flowId)
        {
            var statuses = GetFStatuses(flowId).OrderByDescending(s => s.Id).Where(s => s.FlwId == flowId && s.WacId == actionId).ToList();
            var steps = _fStepDal.GetList(t => t.FlwId == flowId).ToList();
            return statuses.FirstOrDefault(status => steps.All(step => status.Id != step.FromFstId));
        }

        public FStatus GetFStatus(int actionId, int flowId)
        {
            return GetFStatuses(flowId).FindLast(s => s.WacId == actionId && s.FlwId == flowId);
        }

        public FStatus GetFStatus(int id)
        {
            return _fStatusDal.Get(d => d.Id == id);
        }

        public List<FStatus> GetFStatuses(int flowId)
        {
            return _fStatusDal.GetList(f => f.FlwId == flowId);
        }

        public void Add(FStatus fStatus)
        {
            _fStatusDal.Add(fStatus);
        }

        public void Update(FStatus fStatus)
        {
            _fStatusDal.Update(fStatus);
        }

        public void Delete(FStatus fStatus)
        {
            _fStatusDal.Delete(fStatus);
        }
    }
}