﻿/*=================================================================*
 * Classe: <VwAllVariableValueManager>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwAllVariableValue ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwAllVariableValueService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwAllVariableValueManager: IVwAllVariableValueService
    {
        private readonly IVwAllVariableValueDal _allVariableValue;

        public VwAllVariableValueManager(IVwAllVariableValueDal allVariableValue)
        {
            _allVariableValue = allVariableValue;
        }

        public VwAllVariableValue GetVwAllVariableValue(int wvaId, int flowId)
        {
            var variables = GetAllVariableValues(flowId).Where(v => v.WvaId == wvaId && v.FlwId == flowId);
            return variables.OrderByDescending(v => v.Pos).FirstOrDefault();
        }

        public List<VwAllVariableValue> GetMultiVariables(int wvaId, int flowId, int valId)
        {
            return GetAllVariableValues(flowId).Where(v => v.WvaId == wvaId && v.FlwId == flowId && v.FvvId != valId).ToList();
        }

        public List<VwAllVariableValue> GetAllVariableValues(int flowId)
        {
            return _allVariableValue.GetList(v => v.FlwId == flowId);
        }
    }
}