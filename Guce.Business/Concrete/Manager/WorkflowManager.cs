﻿/*=================================================================*
 * Classe: <WorkflowManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Workflow :: dans 
 * la couche business logic. Elle implémente l'interface :: IWorkflowService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class WorkflowManager : IWorkflowService
    {
        private readonly IWorkflowDal _workflowDal;

        public WorkflowManager(IWorkflowDal workflowDal)
        {
            _workflowDal = workflowDal;
        }

        public Workflow GetWorkflow(int id)
        {
            return _workflowDal.Get(d => d.Id == id);
        }

        public List<Workflow> GetWorkflows()
        {
            return _workflowDal.GetList();
        }
        public List<Workflow> GetActiveWorkflows()
        {
            return _workflowDal.GetList(w => w.Active);
        }
    }

}