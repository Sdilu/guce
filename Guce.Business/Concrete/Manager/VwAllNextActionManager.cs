﻿/*=================================================================*
 * Classe: <VwAllNextActionManager>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwAllNextAction ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwAllNextActionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwAllNextActionManager: IVwAllNextActionService
    {
        private readonly IVwAllNextActionDal _vwNextActionDal;

        public VwAllNextActionManager(IVwAllNextActionDal vwNextActionDal)
        {
            _vwNextActionDal = vwNextActionDal;
        }

        public List<VwAllNextAction> GetVwNextActions()
        {
            return _vwNextActionDal.GetList();
        }
    }
}