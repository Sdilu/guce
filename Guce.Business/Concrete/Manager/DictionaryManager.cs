﻿/*=================================================================*
 * Classe: <DictionaryManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Dictionary :: dans 
 * la couche business logic. Elle implémente l'interface :: IDictionaryService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class DictionaryManager : IDictionaryService
    {
        private readonly IDictionaryDal _dictionaryDal;

        public DictionaryManager(IDictionaryDal dictionaryDal)
        {
            _dictionaryDal = dictionaryDal;
        }

        public Dictionary GetDictionary(int id)
        {
            return _dictionaryDal.Get(d => d.Id == id);
        }

        public List<Dictionary> GetDictionaries()
        {
            return _dictionaryDal.GetList();
        }
    }
}