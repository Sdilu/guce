﻿/*=================================================================*
 * Classe: <FlowManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Flow :: dans 
 * la couche business logic. Elle implémente l'interface :: IFlowService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.Business.Concrete.Generator;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class FlowManager : IFlowService
    {
        private readonly IFlowDal _flowDal;
        private readonly IWorkflowDal _workflowDal;
        private readonly IEntityDal _entityDal;
        private readonly IParameterDal _parameterDal;

        public FlowManager(IFlowDal flowDal, IWorkflowDal workflowDal, 
            IEntityDal entityDal, IParameterDal parameterDal)
        {
            _flowDal = flowDal;
            _workflowDal = workflowDal;
            _entityDal = entityDal;
            _parameterDal = parameterDal;
        }

        public string GetLastFlowReference()
        {
            return _flowDal.GetLastFlowReference();
        }

        public int AddReturnKey(Flow flow)
        {
            return _flowDal.AddReturnKey(flow);
        }

        public int AddEntity(Entity entity)
        {
            return _entityDal.AddReturnKey(entity);
        }

        public Entity GetEntity(int entityId)
        {
            return _entityDal.Get(e => e.Id == entityId);
        }

        public void UpdateEntity(Entity entity)
        {
            _entityDal.Update(entity);
        }

        public FlowEntity StartWorkflow(int wflId, int wfvId, int flowId, int entityId, string username)
        {
            var reference = new FileNumberGenerator(_flowDal, int.Parse(Configuration.Parameters[ParameterId.StartFileNumber].ToString()), _parameterDal.GetValue(ParameterId.SiteId, ParameterId.App)).Generate();
            var workflow = _workflowDal.Get(w => w.Id == wflId);
            var flow = new Flow
            {
                WfvId = wfvId,
                Reference = reference,
                CreationTime = DateTime.Now,
                CreatedBy = username
            };
            if (flowId == 0)
            {
                flow.Id = AddReturnKey(flow);
            }
            else
            {
                flow = GetFlow(flowId);
                flow.Reference = reference;
            }
            if (entityId == 0)
            {
                if (workflow.DicId != null)
                {
                    var entity = new Entity
                    {
                        CurrentTxId = 0,
                        CreationTime = DateTime.Now,
                        CreatedBy = username,
                        DicId = (int)workflow.DicId
                    };
                    entityId = _entityDal.AddReturnKey(entity);
                }
                flow.EntId = entityId;
            }
            else
            {
                flow.EntId = entityId;
            }
            Update(flow);
            return new FlowEntity
            {
                FlowId = flow.Id,
                EntityId = entityId
            };
        }

        public Flow GetFlow(int id)
        {
            return _flowDal.Get(d => d.Id == id);
        }

        public List<Flow> GetFlows(int entityId)
        {
            return _flowDal.GetList(e => e.EntId == entityId);
        }

        public void Add(Flow flow)
        {
            _flowDal.Add(flow);
        }

        public void Update(Flow flow)
        {
            _flowDal.Update(flow);
        }

        public void Delete(Flow flow)
        {
            _flowDal.Delete(flow);
        }
    }
}