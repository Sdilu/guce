﻿/*=================================================================*
 * Classe: <BusinessRuleManager>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: BusinessRule :: dans 
 * la couche business logic. Elle implémente l'interface :: IBusinessRuleService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class BusinessRuleManager : IBusinessRuleService
    {
        private readonly IBusinessRuleDal _businessRuleDal;

        public BusinessRuleManager(IBusinessRuleDal businessRuleDal)
        {
            _businessRuleDal = businessRuleDal;
        }

        public BusinessRule GetBusinessRule(int id)
        {
            return _businessRuleDal.Get(d => d.Id == id);
        }

        public List<BusinessRule> GetBusinessRules()
        {
            return _businessRuleDal.GetList();
        }
    }
}