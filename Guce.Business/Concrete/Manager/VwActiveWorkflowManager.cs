﻿/*=================================================================*
 * Classe: <VwActiveWorkflowManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwActiveWorkflow ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwActiveWorkflowService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwActiveWorkflowManager: IVwActiveWorkflowService
    {
        private readonly IVwActiveWorkflowDal _vwActiveWorkflowDal;

        public VwActiveWorkflowManager(IVwActiveWorkflowDal vwActiveWorkflowDal)
        {
            _vwActiveWorkflowDal = vwActiveWorkflowDal;
        }

        public List<VwActiveWorkflow> GetActiveWorkflows()
        {
            return _vwActiveWorkflowDal.GetList().OrderBy(w => w.WflId).ToList();
        }
    }
}