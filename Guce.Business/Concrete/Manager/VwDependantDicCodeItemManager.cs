﻿/*=================================================================*
 * Classe: <VwDependantDicCodeItemManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwDependantDicCodeItem ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwDependantDicCodeItemService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwDependantDicCodeItemManager: IVwDependantDicCodeItemService
    {
        private readonly IVwDependantDicCodeItemDal _vwDependantDicCodeItemDal;

        public VwDependantDicCodeItemManager(IVwDependantDicCodeItemDal vwDependantDicCodeItemDal)
        {
            _vwDependantDicCodeItemDal = vwDependantDicCodeItemDal;
        }
        
        public List<VwDependantDicCodeItem> GetVwDependantDicCodeItems()
        {
            return _vwDependantDicCodeItemDal.GetList();
        }
    }
}