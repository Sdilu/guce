﻿/*=================================================================*
 * Classe: <DicCodeManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: DicCode :: dans 
 * la couche business logic. Elle implémente l'interface :: IDicCodeService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class DicCodeManager : IDicCodeService
    {
        private readonly IDicCodeDal _dicCodeDal;

        public DicCodeManager(IDicCodeDal dicCodeDal)
        {
            _dicCodeDal = dicCodeDal;
        }

        public DicCode GetDicCode(int id)
        {
            return _dicCodeDal.Get(d => d.Id == id);
        }

        public List<DicCode> GetDicCodes()
        {
            return _dicCodeDal.GetList();
        }
    }
}