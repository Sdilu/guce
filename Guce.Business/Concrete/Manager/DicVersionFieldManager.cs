﻿/*=================================================================*
 * Classe: <DicVersionFieldManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: DicVersionField :: dans 
 * la couche business logic. Elle implémente l'interface :: IDicVersionFieldService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class DicVersionFieldManager : IDicVersionFieldService
    {
        private readonly IDicVersionFieldDal _dicVersionFieldDal;

        public DicVersionFieldManager(IDicVersionFieldDal dicVersionFieldDal)
        {
            _dicVersionFieldDal = dicVersionFieldDal;
        }

        public List<DicVersionField> GetDicVersionFields()
        {
            return _dicVersionFieldDal.GetList();
        }
    }
}