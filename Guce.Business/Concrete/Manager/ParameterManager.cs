﻿/*=================================================================*
 * Classe: <ParameterManager>
 * Version/date: <2.0.0> <2016.08.30>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Parameter :: dans 
 * la couche business logic. Elle implémente l'interface :: IParameterService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class ParameterManager : IParameterService
    {
        private readonly IParameterDal _parameterDal;

        public ParameterManager(IParameterDal parameterDal)
        {
            _parameterDal = parameterDal;
        }
        
        public Parameter GetParameter(string id, string username)
        {
            return _parameterDal.GetParameter(id, username);
        }

        public string GetValue(string id, string username)
        {
            return _parameterDal.GetValue(id, username);
        }

        public List<Parameter> GetParameters(string username)
        {
            return _parameterDal.GetParameters(username);
        }

        public List<Parameter> GetParameters(string app, string username)
        {
            return _parameterDal.GetList(p => p.Username.Equals(app) || p.Username.Equals(username));
        }

        public List<Parameter> GetParameters()
        {
            return _parameterDal.GetList();
        }

        public void Add(Parameter parameter)
        {
            _parameterDal.Add(parameter);
        }

        public void Update(Parameter parameter)
        {
            _parameterDal.Update(parameter);
        }
    }
}