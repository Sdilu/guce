﻿/*=================================================================*
 * Classe: <WfTransitionManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: WfTransition :: dans 
 * la couche business logic. Elle implémente l'interface :: IWfTransitionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class WfTransitionManager : IWfTransitionService
    {
        private readonly IWfTransitionDal _wfTransitionDal;

        public WfTransitionManager(IWfTransitionDal wfTransitionDal)
        {
            _wfTransitionDal = wfTransitionDal;
        }

        public List<WfTransition> GetFromTransitionsByAction(int actionId)
        {
            return _wfTransitionDal.GetFromTransitionsByAction(actionId);
        }

        public List<WfTransition> GetToTransitionsByAction(int actionId)
        {
            return _wfTransitionDal.GetToTransitionsByAction(actionId);
        }

        public WfTransition GetWfTransition(int id)
        {
            return _wfTransitionDal.Get(d => d.Id == id);
        }

        public List<WfTransition> GetWfTransitions()
        {
            return _wfTransitionDal.GetList();
        }
    }
}