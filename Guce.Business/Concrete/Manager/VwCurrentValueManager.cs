﻿/*=================================================================*
 * Classe: <VwCurrentValueManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwCurrentValue ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwCurrentValueService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class VwCurrentValueManager: IVwCurrentValueService
    {
        private readonly IVwCurrentValueDal _currentValue;
        private readonly IDicCodeItemDal _codeItemDal;
        private readonly IDicFieldDal _dicField;

        public VwCurrentValueManager(IVwCurrentValueDal currentValue, IDicFieldDal dicField)
        {
            _currentValue = currentValue;
            _dicField = dicField;
        }

        public VwCurrentValueManager(IVwCurrentValueDal currentValue, IDicCodeItemDal codeItemDal, IDicFieldDal dicField)
        {
            _currentValue = currentValue;
            _codeItemDal = codeItemDal;
            _dicField = dicField;
        }

        public VwCurrentValue GetCurrentValue(int entityId, int dflId)
        {
            return _dicField.Get(d => d.Id == dflId).MultiValued ? GetCurrentValues(entityId).OrderBy(v => v.ValId).FirstOrDefault(v => v.EntId == entityId && v.DflId == dflId) 
                : GetCurrentValues(entityId).LastOrDefault(v => v.EntId == entityId && v.DflId == dflId);
        }

        public List<VwCurrentValue> GetMultiValues(int entityId, int dflId)
        {
            return GetCurrentValues(entityId).Where(c => c.EntId == entityId && c.DflId == dflId).ToList();
        }

        public List<VwCurrentValue> GetMultiValues(int entityId, int dflId, int valId)
        {
            return GetCurrentValues(entityId).Where(c => c.EntId == entityId && c.DflId == dflId && c.ValId != valId).ToList();
        }

        public VwCurrentValue GetCurrentValue(int valId)
        {
            return _currentValue.Get(v => v.ValId == valId);
        }

        //public List<VwCurrentValue> GetCurrentValues()
        //{
        //    return _currentValue.GetList();
        //}
        
        public List<VwCurrentValue> GetCurrentValues(int entityId)
        {
            return _currentValue.GetList(v => v.EntId == entityId).OrderBy(v => v.Order).ThenBy(v => v.ValId).Distinct(new VwCurrentValueComparer()).ToList();
        }

        public bool RccmNotExist(int entityId)
        {
            return GetCurrentValues(entityId).FirstOrDefault(v => v.FieldName.Equals(TechnicalName.Rccm)) == null;
        }
        
        public string GetLastRccm(string entityCategory)
        {
            var currentValues = _currentValue.GetList(v => v.FieldName.Equals(TechnicalName.Rccm)).OrderByDescending(v => int.Parse(v.Value.Split('-')[2]));
            var values = (from value in currentValues
                let tab = value.Value.Split('-')
                where
                    Convert.ToInt32(tab[0].Substring(tab[0].Length - 2)) == Convert.ToByte(DateTime.Now.ToString("yy")) &&
                    tab[1].Equals(entityCategory)
                select value).OrderByDescending(v => int.Parse(v.Value.Split('-')[2])).ToList();
            return (from value in values select value.Value).FirstOrDefault();
        }

        public string GetEntityCategory(int? formeJuridiqueId)
        {
            return _codeItemDal.Get(c => c.Id == formeJuridiqueId).Code;
        }

        public int GetFlow(int valId)
        {
            return _currentValue.Get(c => c.ValId == valId).FlwId;
        }
    }
}