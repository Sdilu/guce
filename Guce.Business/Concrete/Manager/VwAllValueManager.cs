﻿/*=================================================================*
 * Classe: <VwAllValueManager>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwAllValue ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwAllValueService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class VwAllValueManager: IVwAllValueService
    {
        private readonly IVwAllValueDal _allValue;

        public VwAllValueManager(IVwAllValueDal allValue)
        {
            _allValue = allValue;
        }

        //public List<VwAllValue> GetAllValues()
        //{
        //    return _allValue.GetList();
        //}

        public List<VwAllValue> GetAllValues(int entityId)
        {
            return _allValue.GetList(v => v.EntId == entityId);
        }
    }
}