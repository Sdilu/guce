﻿/*=================================================================*
 * Classe: <DicCodeItemManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: DicCodeItem :: dans 
 * la couche business logic. Elle implémente l'interface :: IDicCodeItemService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class DicCodeItemManager: IDicCodeItemService
    {
        private readonly IDicCodeItemDal _dicCodeItemDal;

        public DicCodeItemManager(IDicCodeItemDal dicCodeItemDal)
        {
            _dicCodeItemDal = dicCodeItemDal;
        }

        public DicCodeItem GetDicCodeItem(int id)
        {
            return _dicCodeItemDal.Get(d => d.Id == id);
        }

        public List<DicCodeItem> GetDicCodeItems()
        {
            return _dicCodeItemDal.GetList().OrderBy(i => i.Id).ToList();
        }

        public List<DicCodeItem> GetDicCodeItems(int? dcdId)
        {
            return _dicCodeItemDal.GetList(c => c.DcdId == dcdId).OrderBy(i => i.Id).ToList();
        }

        public List<DicCodeItem> GetCodeItems(int dcdId)
        {
            return _dicCodeItemDal.GetList(c => c.DcdId == dcdId).OrderBy(i => i.Id).ToList();
        }

        public List<DicCodeItem> GetDicCodeItemsByDependend(int dependDciId)
        {
            return _dicCodeItemDal.GetList(c => c.DependDciId == dependDciId).OrderBy(i => i.Id).ToList();
        }

        public List<DicCodeItem> GetDicCodeItems(int dcdId, int dependDciId)
        {
            return _dicCodeItemDal.GetList(c => c.DcdId == dcdId && c.DependDciId == dependDciId).OrderBy(i => i.Id).ToList();
        }

        public List<DicCodeItem> GetCodeInlineItems(string items)
        {
            var tables = items.Split('|');
            return tables.Select(tab => GetDicCodeItem(int.Parse(tab))).SelectMany(item => GetDicCodeItemsByDependend(item.Id)).OrderBy(i => i.Id).ToList();
        }

        public string GetParentCodeInline(int childId, int? end)
        {
            var value = childId + "|";
            while (true)
            {
                var item = GetDicCodeItem(childId);
                if (item.DependDciId != end)
                {
                    if (item.DependDciId != null) childId = (int) item.DependDciId;
                    value += childId + "|";
                }
                else
                {
                    return value;
                }
            }
        }
        
        public int CountDependencies(int begin, int end, int times)
        {
            while (true)
            {
                ++times;
                var itemId = _dicCodeItemDal.GetList(i => i.DcdId == begin).First().Id;
                var item = _dicCodeItemDal.GetList(i => i.DependDciId == itemId).First();
                if (item.DcdId == end) return times;
                begin = item.DcdId;
            }
        }

        public int? GetDependentId(int end, int valueId)
        {
            while (true)
            {
                var dependentId = GetDicCodeItem(valueId).DependDciId;
                if (dependentId != null)
                {
                    var parentCodeItem = GetDicCodeItem((int) dependentId);
                    if (parentCodeItem.DcdId == end)
                    {
                        return parentCodeItem.DependDciId;
                    }
                    if (parentCodeItem.DependDciId != null) valueId = (int) parentCodeItem.DependDciId;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}