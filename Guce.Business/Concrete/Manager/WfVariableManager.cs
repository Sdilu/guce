﻿/*=================================================================*
 * Classe: <WfVariableManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: WfVariable :: dans 
 * la couche business logic. Elle implémente l'interface :: IWfVariableService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class WfVariableManager : IWfVariableService
    {
        private readonly IWfVariableDal _wfVariableDal;

        public WfVariableManager(IWfVariableDal wfVariableDal)
        {
            _wfVariableDal = wfVariableDal;
        }

        public WfVariable GetWfVariable(int id, int wfvId)
        {
            return wfvId == 0 ? GetWfVariable(id) : _wfVariableDal.Get(d => d.Id == id && d.WfvId == wfvId);
        }

        public WfVariable GetWfVariable(int id)
        {
            return _wfVariableDal.Get(d => d.Id == id);
        }

        public List<WfVariable> GetWfVariables()
        {
            return _wfVariableDal.GetList().OrderBy(d => d.Order).ToList();
        }
    }
}