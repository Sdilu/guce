﻿/*=================================================================*
 * Classe: <DicVersionCodeManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: DicVersionCode :: dans 
 * la couche business logic. Elle implémente l'interface :: IDicVersionCodeService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class DicVersionCodeManager : IDicVersionCodeService
    {
        private readonly IDicVersionCodeDal _dicVersionCodeDal;

        public DicVersionCodeManager(IDicVersionCodeDal dicVersionCodeDal)
        {
            _dicVersionCodeDal = dicVersionCodeDal;
        }

        public List<DicVersionCode> GetDicVersionCodes()
        {
            return _dicVersionCodeDal.GetList();
        }
    }
}