﻿/*=================================================================*
 * Classe: <ValueManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: Value :: dans 
 * la couche business logic. Elle implémente l'interface :: IValueService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.Business.Concrete.Manager
{
    public class ValueManager : IValueService
    {
        private readonly IVwFormFieldDal _formFieldDal;
        private readonly IDicFieldDal _dicFieldDal;
        private readonly IValueDal _valueDal;

        public ValueManager(IValueDal valueDal, IDicFieldDal dicFieldDal)
        {
            _valueDal = valueDal;
            _dicFieldDal = dicFieldDal;
        }

        public ValueManager(IValueDal valueDal, IVwFormFieldDal formFieldDal, IDicFieldDal dicFieldDal)
        {
            _valueDal = valueDal;
            _formFieldDal = formFieldDal;
            _dicFieldDal = dicFieldDal;
        }

        public Value GetValue(int entityId, int dflId)
        {
            return GetValues(entityId).LastOrDefault(value => value.EntId == entityId && value.DflId == dflId);
        }

        public bool IsDate(int fieldId)
        {
            return _dicFieldDal.Get(f => f.Id == fieldId).Type.Equals(FieldType.Date);
        }

        public bool IsMultivalued(int fieldId)
        {
            return _dicFieldDal.Get(f => f.Id == fieldId).MultiValued;
        }

        public VwFormField GetFormField(int fieldId)
        {
            return _formFieldDal.Get(a => a.WafId == fieldId);
        }

        public VwFormField GetFormFieldByField(int dflId, int actionId)
        {
            return _formFieldDal.Get(f => f.DflId == dflId && f.WacId == actionId);
        }

        public VwFormField GetFormFieldByVariable(int wvaId, int actionId)
        {
            return _formFieldDal.Get(f => f.WvaId == wvaId && f.WacId == actionId);
        }

        public List<IGrouping<string, VwFormField>> GetFormFieldsByAction(int actionId, bool adminOnly)
        {
            if (adminOnly)
            {
                return _formFieldDal.GetList(a => a.WacId == actionId)
                  .Where(f => f.ParentDflId == null && f.ParentWvaId == null)
                  .OrderBy(a => a.Order)
                  .GroupBy(f => f.Group).ToList();
            }
            return _formFieldDal.GetList(a => a.WacId == actionId && a.AdminOnly == false)
                .Where(f => f.ParentDflId == null && f.ParentWvaId == null)
                .OrderBy(a => a.Order)
                .GroupBy(f => f.Group).ToList();
        }

        public List<VwFormField> GetFormFieldsByAction(int actionId, int parentId, int dfl)
        {
            var adminOnly = EfDatabaseFunction.IsAdmin();
            if (dfl == 1)
            {
                return _formFieldDal.GetList(a => a.WacId == actionId && a.AdminOnly == adminOnly)
                    .Where(f => f.ParentDflId == parentId)
                    .OrderBy(a => a.Order).ToList();
            }
            return _formFieldDal.GetList(a => a.WacId == actionId && a.AdminOnly == adminOnly)
                .Where(f => f.ParentWvaId == parentId)
                .OrderBy(a => a.Order).ToList();
        }

        public List<VwFormField> GetFormFieldsByAction(int actionId)
        {
            return _formFieldDal.GetList(a => a.WacId == actionId).OrderBy(a => a.Order).ToList();
        }

        public Value GetValue(int id)
        {
            return _valueDal.Get(d => d.Id == id);
        }

        public List<Value> GetValues(int entityId)
        {
            return _valueDal.GetList(e => e.EntId == entityId);
        }

        //public List<Value> GetValues()
        //{
        //    return _valueDal.GetList();
        //}

        public void Add(Value value)
        {
            _valueDal.Add(value);
        }

        public void Update(Value value)
        {
            _valueDal.Update(value);
        }

        public void Delete(Value value)
        {
            _valueDal.Delete(value);
        }
    }
}