﻿using System.IO;
using System.Text.RegularExpressions;
using static System.IO.Path;

namespace Guce.Business.Concrete.Manager
{
    public class FileManager
    {
        public static string GetUniqueFilePathPdf(string filepath)
        {
            if (!File.Exists(filepath)) return filepath;
            var folder = GetDirectoryName(filepath);
            var filename = GetFileNameWithoutExtension(filepath);
            var extension = GetExtension(filepath);
            var number = 0;
            var regex = Regex.Match(filepath, @"(.+) \((\d+)\)\.\w+");
            if (regex.Success)
            {
                filename = regex.Groups[1].Value;
                number = int.Parse(regex.Groups[2].Value);
            }
            do
            {
                number++;
                if (folder != null)
                    filepath = Combine(folder, $"{filename.Substring(0, filename.Length - 3)}{number.ToString("D3")}{extension}");
            } while (File.Exists(filepath));
            return filepath;
        }

        public static string GetUniqueFilePath(string filepath)
        {
            if (!File.Exists(filepath)) return filepath;
            var folder = GetDirectoryName(filepath);
            var filename = GetFileNameWithoutExtension(filepath);
            var extension = GetExtension(filepath);
            var number = 1;
            var regex = Regex.Match(filepath, @"(.+) \((\d+)\)\.\w+");
            if (regex.Success)
            {
                filename = regex.Groups[1].Value;
                number = int.Parse(regex.Groups[2].Value);
            }
            do
            {
                number++;
                if (folder != null)
                    filepath = Combine(folder, $"{filename} ({number}){extension}");
            } while (File.Exists(filepath));
            return filepath;
        }
    }
}