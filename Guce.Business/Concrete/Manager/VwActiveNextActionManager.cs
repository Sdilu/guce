﻿/*=================================================================*
 * Classe: <VwActiveNextActionManager>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le gestionnaire du type complexe (complex type) :: VwActiveNextAction ::
 *  dans la couche business logic. Elle implémente l'interface :: IVwActiveNextActionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.Business.Concrete.Manager
{
    public class VwActiveNextActionManager: IVwActiveNextActionService
    {
        private readonly IVwActiveNextActionDal _vwNextActionDal;

        public VwActiveNextActionManager(IVwActiveNextActionDal vwNextActionDal)
        {
            _vwNextActionDal = vwNextActionDal;
        }

        public List<VwActiveNextAction> GetVwNextActions()
        {
            return _vwNextActionDal.GetList();
        }
    }
}