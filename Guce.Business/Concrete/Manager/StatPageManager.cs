﻿/*=================================================================*
 * Classe: <StatPageManager>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: StatPage :: dans 
 * la couche business logic. Elle implémente l'interface :: IStatPageService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class StatPageManager : IStatPageService
    {
        private readonly IStatPageDal _statPageDal;

        public StatPageManager(IStatPageDal statPageDal)
        {
            _statPageDal = statPageDal;
        }

        public StatPage GetStatPage(int id)
        {
            return _statPageDal.Get(s => s.Id == id);
        }

        public List<StatPage> GetStatPages()
        {
            return _statPageDal.GetList();
        }
    }
}