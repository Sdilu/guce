﻿/*=================================================================*
 * Classe: <WfActionManager>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est le gestionnaire de l'entité :: WfAction :: dans 
 * la couche business logic. Elle implémente l'interface :: IWfActionService :: ”>
 * Specificities: <“ Cette classe est un gestionnaire ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using System.Collections.Generic;
using Guce.Business.Abstract;
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.Business.Concrete.Manager
{
    public class WfActionManager : IWfActionService
    {
        private readonly IWfActionDal _wfActionDal;
        

        public WfActionManager(IWfActionDal wfActionDal)
        {
            _wfActionDal = wfActionDal;
        }

        public WfAction GetWfAction(int id)
        {
            return _wfActionDal.Get(d => d.Id == id);
        }

        public List<WfAction> GetWfActions()
        {
            return _wfActionDal.GetList();
        }

        public WfAction GetBeginAction(int wfvId)
        {
            return _wfActionDal.GetList().Find(a => a.WfvId == wfvId && a.WfBegin);
        }
        
    }
}