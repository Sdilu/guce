﻿/*=================================================================*
 * Classe: <ConfigController>
 * Version/date: <2.0.0> <2016.07.18>
 *
 * Description: <” Cette classe est le controleur pour afficher un champ de type reference (reference vers 
 * un autre dictionnaire). ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Web.Mvc;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.Models.Config;
using Newtonsoft.Json;

namespace Guce.MvcWebUI.Controllers
{
    [Authorize]
    public class ConfigController : Controller
    {
        private readonly IParameterService _parameterService;

        public ConfigController(IParameterService parameterService)
        {
            _parameterService = parameterService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var parameters = _parameterService.GetParameters(ParameterId.App);
            return View(new ConfigListViewModel
            {
                Admin = EfDatabaseFunction.IsAdmin(),
                Parameters = parameters
            });
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            return PartialView(new ParameterListViewModel
            {
                Parameter = _parameterService.GetParameter(id, ParameterId.App)
            });
        }

        [HttpPost]
        public ActionResult Edit(string json, string id)
        {
            var parameter = JsonConvert.DeserializeObject<Parameter>(json);
            _parameterService.Update(parameter);
            Configuration.Parameters.Clear();
            var parameters = _parameterService.GetParameters(ParameterId.App);
            foreach (var par in parameters)
            {
                Configuration.Parameters.Add(par.Id, par.Value);
            }
            return Json(Url.Action("Index", "Config"));
        }
    }
}