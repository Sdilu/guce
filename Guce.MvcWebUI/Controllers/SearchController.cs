﻿/*=================================================================*
 * Classe: <Operateurs>
 * Version/date: <2.0.0> <2016.08.22>
 *
 * Description: <” Cette classe est le controlleur qui gère les opérations de recherche ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Business.Abstract;
using Guce.MvcWebUI.Models.Search;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.WebPages;
using Guce.Entities.Custom;
using static Guce.DataAccess.Concrete.EfDatabaseFunction;
using static Guce.DataAccess.Concrete.EfDatabaseFunction.FunGenerateNotValidatedJsonPropertie;

namespace Guce.MvcWebUI.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        private readonly IFlowService _flowService;
        private readonly IEntityService _entityService;
        private readonly IFStatusService _fStatusService;
        private readonly IWfActionService _wfActionService;
        private readonly IVwAllValueService _vwAllValueService;
        private readonly IVwWorkflowService _workflowService;

        public SearchController(IFlowService flowService,
                                IEntityService entityService,
                                IFStatusService fStatusService,
                                IWfActionService wfActionService,
                                IVwAllValueService vwAllValueService,
                                IVwWorkflowService workflowService)
        {
            _flowService = flowService;
            _entityService = entityService;
            _fStatusService = fStatusService;
            _wfActionService = wfActionService;
            _vwAllValueService = vwAllValueService;
            _workflowService = workflowService;
        }

        // GET: Search
        public ActionResult Index(string typeRecherche, string val)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return View(new SearchViewListModel
                {
                    EntityMainSearchs = new List<EntityMainSearch>(),
                    FolderMainSearchs = new List<FolderMainSearch>(),
                    EntityId = 0
                });
            }
            List<EntityMainSearch> resultEntities = null;
            List<FolderMainSearch> resultFolder = null;
            int entityId = 0;
            if (typeRecherche == "ENT")
            {
                int resultOfTryingParse;
                if (int.TryParse(val, out resultOfTryingParse))
                {
                    entityId = int.Parse(val);
                }
                resultEntities = fun_search_entity(val.Replace("'", "''"));
            }
            else if (typeRecherche == "PROC")
            {
                resultFolder = fun_search_Folder(val);
            }
            else
            {
                resultEntities = new List<EntityMainSearch>();
                resultFolder = new List<FolderMainSearch>();
            }
            resultEntities = resultEntities?.Where(e => e.json.IsEmpty() == false).ToList();

            resultFolder = resultFolder?.Where(e => e.json.IsEmpty() == false).ToList();
            //for (int i = 0; i < 40; i++)
            //{
            //    resultEntities.Add(new EntityMainSearch() { created_by = "eeee " + i, name = "nameche " + i, creation_time = new DateTime(2015, 5, 5) });
            //}

            return View(new SearchViewListModel
            {
                EntityMainSearchs = resultEntities,
                FolderMainSearchs = resultFolder,
                EntityId = entityId
            });
        }

        [HttpPost]
        public string FindDossierEnCours(int entId)
        {
            List<string> list = new List<string>();
            var flows = _flowService.GetFlows(entId).Where(e => e.EntId == entId).ToList();
            foreach (var flow in flows)
            {
                flow.Procedure = _workflowService.GetVwWorkflow(flow.WfvId).Name;
            }
            var json = new JavaScriptSerializer().Serialize(flows);

            return json;
        }

        [HttpPost]
        public string FluxDeTraitement(int flwId)
        {
            List<string> list = new List<string>();
            var listOfStatus = _fStatusService.GetFStatuses(flwId).Where(e => e.FlwId == flwId).ToList();
            foreach (var status in listOfStatus)
            {
                status.WfAction = _wfActionService.GetWfAction(status.WacId);
            }
            var json = new JavaScriptSerializer().Serialize(listOfStatus);

            return json;
        }

        [HttpPost]
        public string FindCurrentValueNotValidated(int entitiId)
        {
            string json = null;
            var currentUser = (string)Session["username"];
            var values = fun_generate_not_validated_json(entitiId, currentUser, All).ToList();

            var firstOrDefault = values.FirstOrDefault();

            if (firstOrDefault != null)
            {
                json = new JavaScriptSerializer().Serialize(firstOrDefault);
            }
            
            return json;
        }
        [HttpPost]
        public string FindCurrentValue(int entitiId)
        {
            string json = null;
            var values = _entityService.GetEntities(entitiId).Where(e => e.Id == entitiId).ToList();
            var jsonSerialiser = new JavaScriptSerializer();
            var firstOrDefault = values.FirstOrDefault();
            if (firstOrDefault != null)
            {
                json = jsonSerialiser.Serialize(firstOrDefault.Json);
            }
            return json;
        }

        [HttpPost]
        public string FindAllValue(int entitiId)
        {
            List<string> list = new List<string>();
            var listOfFlow = _vwAllValueService.GetAllValues(entitiId).Where(e => e.EntId == entitiId).ToList();
            var jsonSerialiser = new JavaScriptSerializer();
            var json = new JavaScriptSerializer().Serialize(listOfFlow.FirstOrDefault());

            return json;
        }
    }
}