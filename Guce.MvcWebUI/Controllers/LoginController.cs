﻿/*=================================================================*
 * Classe: <LoginController>
 * Version/date: <2.0.0> <2016.07.09>
 *
 * Description: <” Cette classe est le controleur pour la page d'authentification (connexion). ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Linq;
using System.Web.Mvc;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Custom;
using Guce.MvcWebUI.Models.Login;

namespace Guce.MvcWebUI.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserGroupService _userGroupService;
        private readonly IParameterService _parameterService;
        private readonly IStatPageService _statPageService;
        private readonly IUserService _userService;

        public LoginController(IUserGroupService userGroupService, IParameterService parameterService, 
            IUserService userService, IStatPageService statPageService)
        {
            _userGroupService = userGroupService;
            _parameterService = parameterService;
            _userService = userService;
            _statPageService = statPageService;
        }

        public ActionResult Index()
        {
            var user = new User
            {
                Authentifie = HttpContext.User.Identity.IsAuthenticated
            };

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                //return Redirect("Home");
            }
            return View(new LoginViewModel
            {
                User = user
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(User user, string returnUrl)
        {
            if (!ModelState.IsValid) return View(new LoginViewModel
            {
                User = user
            });
            Session["username"] = user.Username;
            Session["password"] = user.Password;
            
            if (GuceContextConnexion.TestContext(user.Username, user.Password))
            {
                Configuration.Parameters.Clear();
                var parameters = _parameterService.GetParameters(ParameterId.App, user.Username);
                foreach (var par in parameters.Where(par => !Configuration.Parameters.Contains(par.Id)))
                {
                    if (par.Username.Equals(ParameterId.App))
                    {
                        Configuration.Parameters.Add(par.Id, par.Value);
                    }
                    else
                    {
                        Session[par.Id + "_" + par.Username] = par.Value;
                    }
                }
                Session["isAdmin"] = EfDatabaseFunction.IsAdmin();
                var groups = (bool)Session["isAdmin"] ? _userGroupService.Roles(user.Username) : _userGroupService.Groups(user.Username);
                Session["userGroup"] = groups;
                user.UserGroup = groups;

                var stat = _statPageService.GetStatPages().FirstOrDefault();
                if (stat != null)
                {
                    Session["statPage"] = _userGroupService.InGroup(stat.GrpId, user.Username);
                }
                else
                {
                    Session["statPage"] = false;
                }

                Session["fullname"] = !(bool)Session["isAdmin"] ? _userService.GetUser(user.Username).Fullname : user.Username;
                
                GuceContextSession.CreateCookies(user);
                if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
                return Redirect("~/Home");
            }
            TempData.Add("Message", "Vos paramètres de connexion sont incorrects");
            return View(new LoginViewModel
            {
                User = user
            });
        }
        
        public ActionResult Logout()
        {
            var username = GuceContextSession.ReadCookie().Username;
            var keys = System.Web.HttpContext.Current.Application.AllKeys;
            foreach (var k in keys.Where(k => System.Web.HttpContext.Current.Application[k] != null 
                                              && System.Web.HttpContext.Current.Application[k].ToString().Equals(username)))
            {
                System.Web.HttpContext.Current.Application[k] = null;
            }
            GuceContextSession.DestroyCookies();
            return Redirect("~/Login");
        }

        [HttpPost]
        public string TimeOut()
        {
            //var username = GuceContextSession.ReadCookie().Username;
            var parameters = _parameterService.GetParameters().FirstOrDefault(e => e.Id == "SESSION_TIMEOUT");
            if (parameters == null) throw new ArgumentNullException(nameof(parameters));
            return parameters.Value;
        }
    }
}