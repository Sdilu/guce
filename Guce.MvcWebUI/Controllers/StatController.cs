﻿/*=================================================================*
 * Classe: <StatController>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette classe est le controleur pour la page des statsistiques. ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.MvcWebUI.Models.Stat;
using Newtonsoft.Json;
using Npgsql;

namespace Guce.MvcWebUI.Controllers
{
    public class StatController : Controller
    {
        private readonly IStatPageService _statPageService;

        public StatController(IStatPageService statPageService)
        {
            _statPageService = statPageService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new StatListViewModel
            {
                StatPages = _statPageService.GetStatPages()
            });
        }

        [HttpPost]
        public string ExecuteStat(int statId)
        {
            var stat = _statPageService.GetStatPage(statId);
            var query = stat.Request;
            var colNames = JsonConvert.DeserializeObject<List<string>>(stat.ColName);
            var values = new List<string>();
            using (var dbConnection = GuceContextConnexion.Initialize())
            {
                using (var connection = new NpgsqlConnection(dbConnection.ConnectionString))
                {
                    var command = new NpgsqlCommand(query, connection);
                    connection.Open();
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            values.AddRange(colNames.Select(col => reader[col].ToString()));
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            return new JavaScriptSerializer().Serialize(colNames) + "=||=" + new JavaScriptSerializer().Serialize(values);
        }
    }
}