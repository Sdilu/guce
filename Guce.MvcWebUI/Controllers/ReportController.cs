﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.CustomClass;

namespace Guce.MvcWebUI.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {

        private readonly IReportService _reportService;
        private readonly IUserService _userService;
        private readonly IDicFieldService _dicFieldService;
        private readonly IVwCurrentValueService _vwCurrentValueService;
        private readonly IVwLastValidatedValuesService _vwLastValidatedValuesService;

        public ReportController(IReportService reportService, IUserService userService,
             IVwLastValidatedValuesService vwLastValidatedValuesService, 
             IVwCurrentValueService vwCurrentValueService, IDicFieldService dicFieldService)
        {
            _reportService = reportService;
            _userService = userService;
            _vwLastValidatedValuesService = vwLastValidatedValuesService;
            _vwCurrentValueService = vwCurrentValueService;
            _dicFieldService = dicFieldService;
        }

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string GetFieldProperties()
        {
            return new JavaScriptSerializer().Serialize(_dicFieldService.GetFieldProperties());
        }

        [HttpPost]
        public string FindInfoToPrint(int reportId)
        {
            var report = EfDatabaseFunction.fun_generate_variables_json(reportId).ToList().FirstOrDefault();// _reportService.GetReports().Where(e => e.Id == reportId).ToList().FirstOrDefault();

            if (report != null) return report;
            return "";
        }

        [HttpPost]
        public string FindNumeroDossier(int entityId)
        {
            var report = EfDatabaseFunction.fun_generate_variables_json(entityId).FirstOrDefault();// _reportService.GetReports().Where(e => e.Id == reportId).ToList().FirstOrDefault();

            if (report != null) return report;
            return "";
        }

        [HttpPost]
        public string FindInfoNotaire(int entityId)
        {
            string currentUser = (string)Session["username"];
            var report = EfDatabaseFunction.fun_generate_not_validated_json(entityId, currentUser,
                EfDatabaseFunction.FunGenerateNotValidatedJsonPropertie.All).FirstOrDefault();// _reportService.GetReports().Where(e => e.Id == reportId).ToList().FirstOrDefault();

            if (report != null) return report;
            return "";
        }

        [HttpPost]
        public string FindMoreInfoNotaire(int entityId)
        {
            string json = null;
            string currentUser = (string)Session["username"];
            var result = _userService.GetUsers().FirstOrDefault(e => e.Username == currentUser);
            //var report = DataAccess.Concrete.EfDatabaseFunction.FindInfoNotaire(entityId,currentUser).FirstOrDefault();// _reportService.GetReports().Where(e => e.Id == reportId).ToList().FirstOrDefault();

            if (result != null)
            {
                json = new JavaScriptSerializer().Serialize(result.Attributes);

                return json;
            }
            return json;
        }

        [HttpPost]
        public string FindFluxValues(int entityId, int flowId)
        {
            string json = null;
            string currentUser = (string)Session["username"];
            //var result = _userService.GetUsers().FirstOrDefault(e => e.Username == currentUser);
            var result = EfDatabaseFunction.FindFluxValues(entityId, flowId, currentUser);// _reportService.GetReports().Where(e => e.Id == reportId).ToList().FirstOrDefault();

            if (result != null)
            {
                json = new JavaScriptSerializer().Serialize(result);

                return json;
            }
            return json;
        }

        [HttpPost]
        public string GetValueModified(int entityId, int txId)
        {
            string json = null;
            var result = new List<ModifiedValues>();
            try
            {
                var newValue = _vwCurrentValueService.GetCurrentValues(entityId).Where(e => e.TxId == txId).ToList();
                var lastValue = _vwLastValidatedValuesService.GetLastValidatedValues(entityId);
                
                for (int i = 0; i < newValue.Count; i++)
                {
                    var vwCurrent = lastValue.FirstOrDefault(e => e.ent_id == newValue[i].EntId && e.dfl_id == newValue[i].DflId);
                    if (vwCurrent == null)
                    {
                        vwCurrent = new Entities.ComplexType.VwLastValidatedValues();
                        vwCurrent.deferenced_value = "";
                    }
                    if (newValue[i].Value != vwCurrent.value)
                        
                        result.Add(new ModifiedValues
                        {
                            currentValue = newValue[i].DeferencedValue,
                            libele = newValue[i].FieldDisplayName,
                            oldValue = vwCurrent.deferenced_value,
                            Name = newValue[i].FieldName
                        });
                }
            }
            catch (Exception ex)
            {
                string ee = ex.Message;
                throw;
            }
            json = new JavaScriptSerializer().Serialize(result);

            return json;
        }

        [HttpPost]
        public string GetHistoriqueEntreprise(int entityId)
        {

            var allValues = _vwLastValidatedValuesService.GetLastValidatedValues(entityId);
            var intervallDate = (from a in allValues orderby a.validation_time descending select a.validation_time).Distinct().ToList();
            var tab = new Dictionary<Dictionary<int,int>,int>();
            List<HistoriqueEntreprise> modif = new List<HistoriqueEntreprise>();

            try
            {
                foreach (var dateTime in intervallDate)
                {
                    if (dateTime != null)
                    {
                        var oneDate = (DateTime) dateTime;

                        var year = oneDate.Year;
                        var mouth = oneDate.Month;
                        var day = oneDate.Day;

                        tab.Add(new Dictionary<int, int> {{year, mouth}}, day);
                    }
                }

                var distinctTabDayMounth = new Dictionary<Dictionary<int, int>, int>();
                foreach (var dic in tab)
                {
                    if (!distinctTabDayMounth.ContainsKey(dic.Key))
                    {
                        distinctTabDayMounth.Add(dic.Key, dic.Value);
                    }
                }

                var distinctTab = new Dictionary<Dictionary<int, int>, int>();
                foreach (var dic in distinctTabDayMounth)
                {
                    if (!distinctTab.ContainsKey(dic.Key) && !distinctTab.ContainsValue(dic.Value))
                    {
                        distinctTab.Add(dic.Key, dic.Value);
                    }
                }

                foreach (var intervalleDate in intervallDate)
                {
                    //int year = intervalleDate.Key.First().Key;
                    //int mounth = intervalleDate.Key.First().Value;
                    //int day = intervalleDate.Value;
                    if (intervalleDate != null)
                    {
                        DateTime dateToCompare = (DateTime) intervalleDate;
                        var valeur = (from a in allValues
                            where a.validation_time != null && (a.validation_time > dateToCompare &&
                                                                a.validation_time < dateToCompare.AddMilliseconds(1))

                            select a).ToList();
                        if (valeur.Count > 0)
                        {
                            modif.Add(new HistoriqueEntreprise {Modificatioins = valeur,DateModification = dateToCompare});
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string zz = ex.Message;
                throw;
            }


            var json = new JavaScriptSerializer().Serialize(modif);
            return json;
        }


        [HttpPost]
        public string GenerateDateToWord(string dateToConvert)
        {
            var tab = dateToConvert.Split('/');
            var dt2 = DateTime.Parse(tab[2] + "-" + tab[1] + "-" + tab[0]);
            var dateConverted = ConvertDate.DateToText(dt2, false, false);

            if (dateConverted != null) return dateConverted;
            return "";
        }

        public ActionResult GetPdf(string code)
        {
            /*
            var unitOfWork = this.ControllerContext.GetUnitOfWork<BlogUnitOfWork>();
            var repository = unitOfWork.GetRepository<PostRepository>();
            var post = repository.GetByHashId(code);
            */
            var builder = new PdfBuilder(/* post, */Server.MapPath("/Views/Amp/Pdf.cshtml"));
            return builder.GetPdf();
        }


    }
}