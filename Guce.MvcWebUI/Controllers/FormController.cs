﻿/*=================================================================*
 * Classe: <FormController>
 * Version/date: <2.0.0> <2016.07.12>
 *
 * Description: <” Cette classe est le controleur pour une action de type form d'un workflow. ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.WebPages;
using Guce.Business.Abstract;
using Guce.Business.Concrete.Generator;
using Guce.DataAccess.Concrete;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.CustomClass;
using Guce.MvcWebUI.Models.Custom;
using Guce.MvcWebUI.Models.Form;
using Newtonsoft.Json;
using static System.Web.Mvc.JsonRequestBehavior;
using static Guce.Entities.Custom.ConvertDate;

namespace Guce.MvcWebUI.Controllers
{
    [Authorize]
    public class FormController : Controller
    {
        private readonly IWfActionService _actionService;
        private readonly IFStatusService _statusService;
        private readonly IFVariableValueService _fVariable;
        private readonly IVwCurrentValueService _currentValue;
        private readonly IDicFieldService _fieldService;
        private readonly IParameterService _parameterService;
        private readonly IFStepService _stepService;
        private readonly IFlowService _flowService;
        private readonly IValueService _valueService;
        private readonly IReportService _reportService;
        private readonly IVwUserNextActionService _nextActionService;
        private readonly IVwCurrentVariableValueService _currentVariableValueService;

        public FormController(IValueService valueService, IWfActionService actionService,
            IFlowService flowService, IFStatusService statusService, IFStepService stepService, 
            IFVariableValueService fVariable, IVwUserNextActionService nextActionService, 
            IVwCurrentValueService currentValue, IVwCurrentVariableValueService currentVariableValueService, 
            IParameterService parameterService, IDicFieldService fieldService, IReportService reportService)
        {
            _actionService = actionService;
            _flowService = flowService;
            _statusService = statusService;
            _stepService = stepService;
            _fVariable = fVariable;
            _nextActionService = nextActionService;
            _currentValue = currentValue;
            _currentVariableValueService = currentVariableValueService;
            _parameterService = parameterService;
            _fieldService = fieldService;
            _reportService = reportService;
            _valueService = valueService;
        }

        [HttpGet]
        public ActionResult Start(int wflId, int wfvId, int actionId, int flowId, int entityId, bool ajax)
        {
            var action = actionId == 0 ? _actionService.GetBeginAction(wfvId) : _actionService.GetWfAction(actionId);
            actionId = action.Id;
            var username = GuceContextSession.ReadCookie().Username;
            if (action.Action.Equals(ActionType.CreateEntity))
            {
                return CreateEntity(wflId, wfvId, actionId, entityId, true);
            }
            if (action.Action.Equals(ActionType.FindAndLockEntity) || action.Action.Equals(ActionType.FindOrCreateEntity))
            {
                if (ajax)
                {
                    return Json(Url.Action("Find", "Form", new { wflId, wfvId, actionId, flowId, entityId }), AllowGet);
                }
                return RedirectToAction("Find", "Form", new { wflId, wfvId, actionId, flowId, entityId });
            }

            System.Web.HttpContext.Current.Application[flowId + "_" + entityId + "_" + actionId] = username;//Lock this action
            if (action.Action.Equals(ActionType.Validate))
            {
                if (ajax)
                {
                    return Json(Url.Action("Validate", "Form", new { wflId, wfvId, actionId, flowId, entityId }), AllowGet);
                }
                return RedirectToAction("Validate", "Form", new { wflId, wfvId, actionId, flowId, entityId });
            }
            if (action.Action.Equals(ActionType.Print))
            {
                if (ajax)
                {
                   return Json(Url.Action("Print", "Form", new { wflId, wfvId, actionId, flowId, entityId }), AllowGet); 
                }
                return RedirectToAction("Print", "Form", new { wflId, wfvId, actionId, flowId, entityId });
            }
            // Le cas restant : action.Action.Equals(ActionType.Form)
            if (ajax)
            {
                return Json(Url.Action("Index", "Form", new {wflId, wfvId, actionId, flowId, entityId}), AllowGet);
            }
            return RedirectToAction("Index", "Form", new { wflId, wfvId, actionId, flowId, entityId });
        }

        [HttpGet]
        public ActionResult Index(int wflId, int wfvId, int actionId, int flowId, int entityId)
        {
            var action = actionId == 0 ? _actionService.GetBeginAction(wfvId) : _actionService.GetWfAction(actionId);
            actionId = action.Id;
            ViewBag.dialogJSON = "pageJSON";
            return View(new FormListViewModel
            {
                ActionId = action.Id,
                WflId = wflId,
                WfvId = wfvId,
                FlowId = flowId,
                EntityId = entityId,
                Action = action,
                StatusId = CreateStatus(actionId, flowId, GuceContextSession.ReadCookie().Username),
                Reference = _flowService.GetFlow(flowId).Reference,
                VwUserNextAction = _nextActionService.GetUserNextAction(actionId, flowId),
                FormFields = _valueService.GetFormFieldsByAction(actionId, EfDatabaseFunction.IsAdmin()),
                Infos = new List<ComponentInfo>()
            });
        }

        [HttpPost]
        public ActionResult Index(string json, int wflId, int wfvId, int actionId, int flowId, int entityId)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var action = _actionService.GetWfAction(actionId);
            var flowEntity = CreateFlowEntity(action.WfBegin, wflId, wfvId, flowId, entityId, username);
            var statusId = CreateStatus(action.Id, flowId, username);

            SaveValueVariableDb(json, flowEntity, statusId, username);
            
            UpdateEntityName(flowEntity.EntityId);
            var nextActions = CrossTransition(action, statusId, flowEntity, username);
            return SwitchNextAction(nextActions, wflId, flowEntity.FlowId, flowEntity.EntityId, true);
        }

        [HttpPost]
        public ActionResult Save(string json, int wflId, int wfvId, int actionId, int flowId, int entityId, int statusId)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var action = _actionService.GetWfAction(actionId);
            var flowEntity = CreateFlowEntity(action.WfBegin, wflId, wfvId, flowId, entityId, username);
            if (statusId == 0)
            {
                statusId = CreateStatus(actionId, flowId, username);
            }
            SaveValueVariableDb(json, flowEntity, statusId, username);

            UpdateEntityName(flowEntity.EntityId);
            TempData.Add("Message", "Les informations ont été sauvegardées avec succès");
            var returnAction = (bool)Session["isAdmin"] ? "All" : "Index";
            return Json(Url.Action(returnAction, "Home"));
        }

        private void SaveValueVariableDb(string json, FlowEntity flowEntity, int statusId, string username)
        {
            var dflVarValues = JsonConvert.DeserializeObject<List<DflVarValue>>(json);
            foreach (var varVal in dflVarValues)
            {
                varVal.Value = varVal.Value.IsEmpty() ? null : varVal.Value;
                if (varVal.Dfl)
                {
                    if (varVal.Id != 0)
                    {
                        var existDb = _valueService.GetValue(varVal.Id);
                        if (existDb.TxId == 0 && existDb.FstId == statusId)
                        {
                            existDb.ValueContent = _valueService.IsDate(varVal.Key) ? DatabaseFormat(varVal.Value) : varVal.Value;
                            _valueService.Update(existDb);
                        }
                        else
                        {
                            varVal.Value = _valueService.IsDate(varVal.Key) ? DatabaseFormat(varVal.Value) : varVal.Value;
                            if (_valueService.IsMultivalued(varVal.Key))
                            {
                                if (existDb.FstId != statusId)
                                {
                                    _valueService.Add(new Value
                                    {
                                        DflId = varVal.Key,
                                        EntId = flowEntity.EntityId,
                                        FstId = statusId,
                                        CreationTime = DateTime.Now,
                                        CreatedBy = username,
                                        TxId = 0,
                                        ValueContent = varVal.Value
                                    });
                                }
                                else
                                {
                                    if (!existDb.ValueContent.Equals(varVal.Value))
                                    {
                                        _valueService.Add(new Value
                                        {
                                            DflId = varVal.Key,
                                            EntId = flowEntity.EntityId,
                                            FstId = statusId,
                                            CreationTime = DateTime.Now,
                                            CreatedBy = username,
                                            TxId = 0,
                                            ValueContent = varVal.Value
                                        });
                                    }
                                }
                            }
                            else
                            {
                                if (!existDb.ValueContent.Equals(varVal.Value))
                                {
                                    _valueService.Add(new Value
                                    {
                                        DflId = varVal.Key,
                                        EntId = flowEntity.EntityId,
                                        FstId = statusId,
                                        CreationTime = DateTime.Now,
                                        CreatedBy = username,
                                        TxId = 0,
                                        ValueContent = varVal.Value
                                    });
                                }
                            }
                        }
                    }
                    else
                    {
                        if (varVal.Value != null)
                        {
                            _valueService.Add(new Value
                            {
                                DflId = varVal.Key,
                                EntId = flowEntity.EntityId,
                                FstId = statusId,
                                CreationTime = DateTime.Now,
                                CreatedBy = username,
                                TxId = 0,
                                ValueContent = _valueService.IsDate(varVal.Key) ? DatabaseFormat(varVal.Value) : varVal.Value
                            });
                        }
                    }
                }
                else
                {
                    if (varVal.Id != 0)
                    {
                        var existDb = _fVariable.GetFVariableValue(varVal.Id);
                        if (statusId == existDb.FstId)
                        {
                            existDb.Value = _fVariable.IsDate(varVal.Key) ? DatabaseFormat(varVal.Value) : varVal.Value;
                            _fVariable.Update(existDb);
                        }
                        else
                        {
                            varVal.Value = _fVariable.IsDate(varVal.Key) ? DatabaseFormat(varVal.Value) : varVal.Value;
                            if (_fVariable.IsMultiValued(varVal.Key))
                            {
                                if (statusId != existDb.FstId)
                                {
                                    _fVariable.Add(new FVariableValue
                                    {
                                        WvaId = varVal.Key,
                                        Value = varVal.Value,
                                        FstId = statusId,
                                        CreationTime = DateTime.Now,
                                        CreatedBy = username
                                    });
                                }
                                else
                                {
                                    if (!existDb.Value.Equals(varVal.Value))
                                    {
                                        _fVariable.Add(new FVariableValue
                                        {
                                            WvaId = varVal.Key,
                                            Value = varVal.Value,
                                            FstId = statusId,
                                            CreationTime = DateTime.Now,
                                            CreatedBy = username
                                        });
                                    }
                                }
                            }
                            else
                            {
                                if (!existDb.Value.Equals(varVal.Value))
                                {
                                    _fVariable.Add(new FVariableValue
                                    {
                                        WvaId = varVal.Key,
                                        Value = varVal.Value,
                                        FstId = statusId,
                                        CreationTime = DateTime.Now,
                                        CreatedBy = username
                                    });
                                }
                            }
                        }
                    }
                    else
                    {
                        if (varVal.Value != null)
                        {
                            _fVariable.Add(new FVariableValue
                            {
                                WvaId = varVal.Key,
                                Value = _fVariable.IsDate(varVal.Key) ? DatabaseFormat(varVal.Value) : varVal.Value,
                                FstId = statusId,
                                CreationTime = DateTime.Now,
                                CreatedBy = username
                            });
                        }
                    }
                }
            }
        }

        [HttpGet]
        public ActionResult Find(int wflId, int wfvId, int actionId, int flowId, int entityId)
        {
            var action = _actionService.GetWfAction(actionId);
            return View(new FindListViewModel
            {
                Action = action,
                WflId = wflId,
                WfvId = wfvId,
                ActionId = actionId,
                FlowId = flowId,
                VwUserNextAction = _nextActionService.GetUserNextAction(actionId, flowId),
                EntityId = entityId
            });
        }

        [HttpPost]
        public string Find(string keyword, string criteria)
        {
            if (int.Parse(criteria) == 1)
            {
                Session["denominationSociale"] = keyword;
            }
            var entities = EfDatabaseFunction.SearchEntities(keyword.Replace("'", "''")).Where(e => e.json != null).ToList();
            return new JavaScriptSerializer().Serialize(entities);
        }

        [HttpPost]
        public ActionResult ExecuteFindEntity(int wflId, int wfvId, int actionId, int entityId)
        {
            var action = _actionService.GetWfAction(actionId);
            var username = GuceContextSession.ReadCookie().Username;
            var flowEntity = CreateFlowEntity(action.WfBegin, wflId, wfvId, 0, entityId, username);
            var statusId = CreateStatus(action.Id, flowEntity.FlowId, username);
            var entity = _flowService.GetEntity(flowEntity.EntityId);
            entity.LockTime = DateTime.Now;
            entity.LockFstId = statusId;
            _flowService.UpdateEntity(entity);
            var nextActions = CrossTransition(action, statusId, flowEntity, username);
            return SwitchNextAction(nextActions, wflId, flowEntity.FlowId, flowEntity.EntityId, true);
        }

        [HttpPost]
        private ActionResult CreateEntity(int wflId, int wfvId, int actionId, int entityId, bool ajax)
        {
            var action = _actionService.GetWfAction(actionId);
            var username = GuceContextSession.ReadCookie().Username;
            var flowEntity = CreateFlowEntity(action.WfBegin, wflId, wfvId, 0, entityId, username);
            var statusId = CreateStatus(action.Id, flowEntity.FlowId, username);
            var entity = _flowService.GetEntity(flowEntity.EntityId);
            entity.LockTime = DateTime.Now;
            entity.LockFstId = statusId;
            _flowService.UpdateEntity(entity);
            var nextActions = CrossTransition(action, statusId, flowEntity, username);
            return SwitchNextAction(nextActions, wflId, flowEntity.FlowId, flowEntity.EntityId, ajax);
        }

        [HttpPost]
        public ActionResult ExecuteCreateEntity(int wflId, int wfvId, int actionId, int entityId, bool ajax)
        {
            return CreateEntity(wflId, wfvId, actionId, entityId, true);
        }

        [HttpGet]
        public ActionResult Validate(int wflId, int wfvId, int actionId, int flowId, int entityId)
        {
            var action = _actionService.GetWfAction(actionId);
            return View(new ValidateListViewModel
            {
                Action = action,
                WflId = wflId,
                WfvId = wfvId,
                ActionId = actionId,
                FlowId = flowId,
                EntityId = entityId,
                Reference = _flowService.GetFlow(flowId).Reference,
                Infos = new List<ComponentInfo>(),
                VwUserNextAction = _nextActionService.GetUserNextAction(actionId, flowId),
                CurrentVariables = _currentVariableValueService.GetCurrentVariableValues(flowId),
                CurrentValues = _currentValue.GetCurrentValues(entityId)
            });
        }

        [HttpPost]
        public ActionResult Validate(int wflId, int actionId, int flowId, int entityId)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var action = _actionService.GetWfAction(actionId);
            var statusId = CreateStatus(action.Id, flowId, username);
            if (_currentValue.RccmNotExist(entityId))
            {
                var rccm = GenerateRccm(entityId);
                var fields = _fieldService.GetRccmFields();
                foreach (var value in fields.Select(field => new Value
                {
                    DflId = field.Id,
                    CreatedBy = username,
                    CreationTime = DateTime.Now,
                    EntId = entityId,
                    FstId = statusId,
                    TxId = 0,
                    ValueContent = field.Name.Equals(TechnicalName.Rccm) ? rccm : DateTime.Now.ToString("yyyy-MM-dd")
                }))
                {
                    _valueService.Add(value);
                }
            }
            EfDatabaseFunction.fun_validate_flow_values(flowId);
            var nextActions = CrossTransition(action, statusId, new FlowEntity
            {
                FlowId = flowId,
                EntityId = entityId
            }, username);
            return SwitchNextAction(nextActions, wflId, flowId, entityId, true);
        }

        [HttpGet]
        public ActionResult Print(int wflId, int wfvId, int actionId, int flowId, int entityId)
        {
            var action = _actionService.GetWfAction(actionId);
            if (action.RptId != null)
                return View(new PrintListViewModel
                {
                    ActionId = actionId,
                    WflId = wflId,
                    WfvId = wfvId,
                    FlowId = flowId,
                    EntityId = entityId,
                    Action = action,
                    Reference = _flowService.GetFlow(flowId).Reference,
                    VwUserNextAction = _nextActionService.GetUserNextAction(actionId, flowId),
                    ReportId = (int) action.RptId,
                    FunctionToPrint = _reportService.GetReport((int)action.RptId).Template
                });
            return RedirectToAction("Start", "Home");
        }

        [HttpPost]
        public ActionResult Print(int wflId, int wfvId, int actionId, int flowId, int entityId, int reportId)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var action = _actionService.GetWfAction(actionId);
            var flowEntity = CreateFlowEntity(action.WfBegin, wflId, wfvId, flowId, entityId, username);
            var statusId = CreateStatus(action.Id, flowEntity.FlowId, username);

            UpdateEntityName(flowEntity.EntityId);
            var nextActions = CrossTransition(action, statusId, flowEntity, username);
            return SwitchNextAction(nextActions, wflId, flowId, entityId, true);
        }
        
        [HttpPost]
        public string SaveReference(string json, int dicId, int actionId, int flowId, int wfvId, int statusId)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var entity = new Entity
            {
                DicId = dicId,
                CurrentTxId = 0,
                CreatedBy = username,
                CreationTime = DateTime.Now
            };
            var entityId = _flowService.AddEntity(entity);
            if (flowId == 0)
            {
                flowId = _flowService.AddReturnKey(new Flow
                {
                    Reference = new FileNumberGenerator(_flowService, int.Parse(Configuration.Parameters[ParameterId.StartFileNumber].ToString()), _parameterService.GetValue(ParameterId.SiteId, ParameterId.App)).Generate(),
                    CreatedBy = username,
                    CreationTime = DateTime.Now,
                    WfvId = wfvId
                });
            }
            if (statusId == 0)
            {
                statusId = CreateStatus(actionId, flowId, username);
            }
            var values = JsonConvert.DeserializeObject<List<DflValue>>(json);
            foreach (var val in values)
            {
                _valueService.Add(new Value
                {
                    DflId = val.DflId,
                    ValueContent = _valueService.IsDate(val.DflId) ? DatabaseFormat(val.Value) : val.Value,
                    CreationTime = DateTime.Now,
                    CreatedBy = username,
                    EntId = entityId,
                    FstId = statusId
                });
            }
            entity.Name = EfDatabaseFunction.fun_get_not_validated_name(entityId);
            _flowService.UpdateEntity(entity);
            return new JavaScriptSerializer().Serialize(new EntityFlowNameStatus
            {Entity = entityId, Flow = flowId, Name = entity.Name, Status = statusId});
        }

        [HttpPost]
        public string EditReference(string json, int actionId, int flowId, int entityId, int statusId)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var values = JsonConvert.DeserializeObject<List<ValDflValue>>(json);
            if (statusId == 0)
            {
                statusId = CreateStatus(actionId, flowId, username);
            }
            foreach (var val in values)
            {
                var value = new Value
                {
                    EntId = entityId,
                    FstId = statusId,
                    DflId = val.DflId,
                    CreationTime = DateTime.Now,
                    CreatedBy = username
                };
                val.Value = val.Value.IsEmpty() ? null : val.Value;
                if (val.ValId != 0)
                {
                    var existDb = _valueService.GetValue(val.ValId);
                    if (existDb.TxId == 0 && existDb.FstId == statusId)
                    {
                        existDb.ValueContent = _valueService.IsDate(val.DflId) ? DatabaseFormat(val.Value) : val.Value;
                        _valueService.Update(existDb);
                    }
                    else
                    {
                        val.Value = _valueService.IsDate(val.DflId) ? DatabaseFormat(val.Value) : val.Value;
                        if (_valueService.IsMultivalued(val.DflId))
                        {
                            if (_currentValue.GetFlow(val.ValId) != flowId)
                            {
                                _valueService.Add(new Value
                                {
                                    DflId = val.DflId,
                                    EntId = entityId,
                                    FstId = statusId,
                                    CreationTime = DateTime.Now,
                                    CreatedBy = username,
                                    TxId = 0,
                                    ValueContent = val.Value
                                });
                            }
                            else
                            {
                                if (!existDb.ValueContent.Equals(val.Value))
                                {
                                    _valueService.Add(new Value
                                    {
                                        DflId = val.DflId,
                                        EntId = entityId,
                                        FstId = statusId,
                                        CreationTime = DateTime.Now,
                                        CreatedBy = username,
                                        TxId = 0,
                                        ValueContent = val.Value
                                    });
                                }
                            }
                        }
                        else
                        {
                            if (!existDb.ValueContent.Equals(val.Value))
                            {
                                _valueService.Add(new Value
                                {
                                    DflId = val.DflId,
                                    EntId = entityId,
                                    FstId = statusId,
                                    CreationTime = DateTime.Now,
                                    CreatedBy = username,
                                    TxId = 0,
                                    ValueContent = val.Value
                                });
                            }
                        }
                    }
                }
                else
                {
                    if (val.Value != null)
                    {
                        value.ValueContent = _valueService.IsDate(val.DflId) ? DatabaseFormat(val.Value) : val.Value;
                        value.TxId = 0;
                        _valueService.Add(value);
                    }
                }
            }
            var name = EfDatabaseFunction.fun_get_not_validated_name(entityId);
            var entity = _flowService.GetEntity(entityId);
            entity.Name = name;
            _flowService.UpdateEntity(entity);
            return new JavaScriptSerializer().Serialize(new StatusName { Name = name, Status = statusId });
        }

        public void DeleteReference(int valueId, int dfl, int statusId)
        {
            if (valueId == 0) return;
            if (dfl == 1)
            {
                var value = _valueService.GetValue(valueId);
                value.ValueContent = null;
                if (value.TxId == 0 && value.FstId == statusId)
                {
                    _valueService.Update(value);
                }
                else
                {
                    _valueService.Add(value);
                }
            }
            else
            {
                var variable = _fVariable.GetFVariableValue(valueId);
                variable.Value = null;
                if (variable.FstId == statusId)
                {
                    _fVariable.Update(variable);
                }
                else
                {
                    _fVariable.Add(variable);
                }
            }
        }

        private ActionResult SwitchNextAction(IReadOnlyCollection<VwUserNextAction> actions, int wflId, int flowId, int entityId, bool ajax)
        {
            if (actions != null && actions.Count > 0)
            {
                var action = actions.First();
                if (action != null)
                {
                    //Execution d'une action de type "null"
                    if (action.WacAction.Equals(ActionType.Null))
                    {
                        var autoAction = _actionService.GetWfAction(action.WacId);
                        var username = GuceContextSession.ReadCookie().Username;
                        CrossTransition(autoAction, CreateStatus(autoAction.Id, flowId, username), new FlowEntity
                        {
                            FlowId = flowId, EntityId = entityId
                        }, username);
                    }
                    else
                    {
                        if (ajax) //Affichage de l'action suivante à l'utilisateur
                        {
                            return Json(Url.Action("Start", "Form", new
                            {
                                wflId, wfvId = action.WfvId, actionId = action.WacId, flowId, entityId, ajax = false }), AllowGet);
                        }
                        return RedirectToAction("Start", "Form", new {
                            wflId, wfvId = action.WfvId, actionId = action.WacId, flowId, entityId, ajax = false
                        });
                    }
                }
            }
            TempData.Add("Message", "Opération effectuée avec succès");
            var returnAction = (bool)Session["isAdmin"] ? "All" : "Index";
            if (ajax)
            {
                return Json(Url.Action(returnAction, "Home"), AllowGet);
            }
            return RedirectToAction(returnAction, "Home");// retour a la page d'acceuil
        }

        private List<VwUserNextAction> CrossTransition(WfAction action, int statusId, FlowEntity flowEntity, string username)
        {
            var isAdmin = (bool) Session["isAdmin"];
            var actions = new List<VwUserNextAction>();
            if (isAdmin)
            {
                var adminActions = _nextActionService.GetVwActiveNextActions(flowEntity.FlowId);
                actions.AddRange(adminActions.Select(adminAction => new VwUserNextAction
                {
                    FlwId = adminAction.FlwId, Lock = adminAction.Lock, EntId = adminAction.EntId, WacId = adminAction.WacId, FlwReference = adminAction.FlwReference, WflId = adminAction.WflId, NeededWtr = adminAction.NeededWtr, GrpId = adminAction.GrpId, WfvVersion = adminAction.WfvVersion, WfvId = adminAction.WfvId, FseId = adminAction.FseId, WacAction = adminAction.WacAction, GrpName = adminAction.GrpName, BranchJoin = adminAction.BranchJoin, WacName = adminAction.WacName, EntName = adminAction.EntName, AvailableWtr = adminAction.AvailableWtr, WflName = adminAction.WflName
                }));
            }
            else
            {
                actions = _nextActionService.GetVwUserNextActions(flowEntity.FlowId);
            }
            if (!action.WfBegin)
            {
                if (action.BranchJoin.Equals(BranchType.And))
                {
                    foreach (var step in actions.Select(una => _stepService.GetFStep(una.FseId)))
                    {
                        step.ToFstId = statusId;
                        _stepService.Update(step);
                    }
                }
                else
                {
                    var fseId = actions.Find(a => a.WacId == _statusService.GetFStatus(statusId).WacId).FseId;
                    var step = _stepService.GetFStep(fseId);
                    step.ToFstId = statusId;
                    _stepService.Update(step);
                }
            }
            var transitions = EfDatabaseFunction.fun_test_transitions(statusId);
            foreach (var tr in transitions)
            {
                _stepService.Add(new FStep
                {
                    FlwId = flowEntity.FlowId,
                    WtrId = tr,
                    CreationTime = DateTime.Now,
                    CreatedBy = username,
                    FromFstId = statusId
                });
            }
            //Destroy session for this action in this entity in this flows 
            System.Web.HttpContext.Current.Application[flowEntity.FlowId + "_" + flowEntity.EntityId + "_" + action.Id] = null;
            if (!isAdmin) return _nextActionService.GetVwUserNextActions(flowEntity.FlowId);
            {
                var adminActions = _nextActionService.GetVwActiveNextActions(flowEntity.FlowId);
                actions.Clear();
                actions.AddRange(adminActions.Select(adminAction => new VwUserNextAction
                {
                    FlwId = adminAction.FlwId,
                    Lock = adminAction.Lock,
                    EntId = adminAction.EntId,
                    WacId = adminAction.WacId,
                    FlwReference = adminAction.FlwReference,
                    WflId = adminAction.WflId,
                    NeededWtr = adminAction.NeededWtr,
                    GrpId = adminAction.GrpId,
                    WfvVersion = adminAction.WfvVersion,
                    WfvId = adminAction.WfvId,
                    FseId = adminAction.FseId,
                    WacAction = adminAction.WacAction,
                    GrpName = adminAction.GrpName,
                    BranchJoin = adminAction.BranchJoin,
                    WacName = adminAction.WacName,
                    EntName = adminAction.EntName,
                    AvailableWtr = adminAction.AvailableWtr,
                    WflName = adminAction.WflName
                }));
                return actions;
            }
        }

        private FlowEntity CreateFlowEntity(bool begin, int wflId, int wfvId, int flowId, int entityId, string username)
        {
            var flowEntity = new FlowEntity {FlowId = flowId, EntityId = entityId};
            if (begin)
            {
                flowEntity = _flowService.StartWorkflow(wflId, wfvId, flowId, entityId, username);
            }
            return flowEntity;
        }

        private int CreateStatus(int actionId, int flowId, string username)
        {
            return _statusService.CreateStatus(flowId, actionId, username);
        }

        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        private string GenerateRccm(int entityId)
        {
            var entityCategory = GetEntityCategory(entityId);
            var par = entityCategory.ToLower().Equals("a") ? ParameterId.NextPhyRccm : ParameterId.NextMorRccm;
            var sequence = _parameterService.GetParameter(par, ParameterId.App);
            var seq = int.Parse(sequence.Value);
            //var lastRccm = _currentValue.GetLastRccm(entityCategory);
            var rccm = new RccmGenerator
            {
                Seq = seq,
                EntityCategory = entityCategory,
                JuridictionCode = _parameterService.GetValue(ParameterId.SiteId, ParameterId.App)
            };
            sequence.Value = (seq + 1).ToString();
            _parameterService.Update(sequence);
            return rccm.Generate();
        }

        private string GetEntityCategory(int entityId)
        {
            var values = _currentValue.GetCurrentValues(entityId);
            var formeJuridiqueId = values.Find(v => v.FieldName.Equals(TechnicalName.FormeJuridiqueN1))?.CodeId;
            return _currentValue.GetEntityCategory(formeJuridiqueId);
        }

        private void UpdateEntityName(int entityId)
        {
            var entity = _flowService.GetEntity(entityId);
            entity.Name = EfDatabaseFunction.fun_get_not_validated_name(entityId);
            _flowService.UpdateEntity(entity);
        }
    }
}