﻿/*=================================================================*
 * Classe: <UserController>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette classe est le controleur pour la gestion des utilisateurs. ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete;
using Guce.Entities.Concrete;
using Guce.MvcWebUI.Models.User;
using User = Guce.Entities.Concrete.User;

namespace Guce.MvcWebUI.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IUserGroupService _userGroupService;

        public UserController(IUserService userService, IUserGroupService userGroupService)
        {
            _userService = userService;
            _userGroupService = userGroupService;
        }

        public ActionResult Index()
        {
            var users = _userService.GetUsersWithoutGlobal();
            foreach (var user in users)
            {
                _userGroupService.SetGroupRole(user);
            }
            var userCount = _userService.GetUsersWithoutGlobal().Count;
            return View(new UserListViewModel
            {
                Users = users, UserCount = userCount
            });
        }

        [HttpPost]
        public int ChangePassword(string username, string password)
        {
            return EfDatabaseFunction.ChangePassword(username, password.Replace("'", "''"));
        }

        [HttpGet]
        public ActionResult Password()
        {
            return PartialView(new PasswordUserListViewModel());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return PartialView(new AddUserListViewModel
            {
                User = new User()
            });
        }

        [HttpGet]
        public ActionResult Edit(string username)
        {
            var user = _userService.GetUser(username);
            _userGroupService.SetGroupRole(user);
            return PartialView(new AddUserListViewModel
            {
                User = user
            });
        }

        [HttpPost]
        public bool CheckUsername(string username)
        {
            return _userService.GetUser(username) == null;
        }

        [HttpPost]
        public ActionResult CreateUser(string fullname, string username, string password, string groups, string attributes)
        {
            var groupTab = groups.Split(',');
            var firstGroup = groupTab.First();
            var group = int.Parse(firstGroup);
            EfDatabaseFunction.CreateUser(username, fullname.Replace("'", "''"), password.Replace("'", "''"), 
                group, attributes.Replace("'", "''"));
            if (groupTab.Length > 1)
            {
                foreach (var g in groupTab.Where(g => !g.Equals(firstGroup)))
                {
                    _userGroupService.Add(new UserGroup
                    {
                        Username = username,
                        GrpId = int.Parse(g)
                    });
                }
            }
            return Json(Url.Action("Index", "User"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditUser(string fullname, string username, string groups, string attributes)
        {
            _userService.Update(new User
            {
                Username = username,
                Fullname = fullname,
                Attributes = attributes.IsEmpty() ? null : attributes
            });
            EfDatabaseFunction.DeleteUserGroups(username);
            foreach (var g in groups.Split(','))
            {
                _userGroupService.Add(new UserGroup
                {
                    Username = username,
                    GrpId = int.Parse(g)
                });
            }
            return Json(Url.Action("Index", "User"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteUser(string username)
        {
            //EfDatabaseFunction.DeleteRole(username); //FIXME guce_admin can not drop role
            EfDatabaseFunction.DeleteUser(username);
            return Json(Url.Action("Index", "User"), JsonRequestBehavior.AllowGet);
        }
    }
}