﻿/*=================================================================*
 * Classe: <ReferenceController>
 * Version/date: <2.0.0> <2016.07.18>
 *
 * Description: <” Cette classe est le controleur pour afficher un champ de type reference (reference vers 
 * un autre dictionnaire). ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.HtmlHelpers;
using Guce.MvcWebUI.Models.Custom;
using Newtonsoft.Json;
using Guce.MvcWebUI.Models.Reference;
using iTextSharp.text;
using iTextSharp.text.pdf;
using static System.Drawing.Image;
using static System.IO.Directory;
using static System.IO.File;
using static System.IO.Path;
using static Guce.Business.Concrete.Manager.FileManager;
using static Guce.Entities.Custom.Configuration;
using static Guce.Entities.Custom.ParameterId;
using PageSize = iTextSharp.text.PageSize;

namespace Guce.MvcWebUI.Controllers
{
    [Authorize]
    public class ReferenceController : Controller
    {
        private readonly IDicFieldService _dicFieldService;
        private readonly IWfVariableService _variableService;
        private readonly IDicCodeItemService _codeItemService;
        private readonly IValueService _valueService;
        private readonly IFVariableValueService _fVariableValue;
        private readonly IEntityService _entityService;
        private readonly IDictionaryService _dictionary;
        private readonly IParameterService _parameterService;
        private readonly IVwCurrentVariableValueService _currentVariableValueService;
        private readonly IIndexService _indexService;
        private readonly string _separator = DirectorySeparatorChar.ToString();

        public ReferenceController(IDicFieldService dicFieldService, IDicCodeItemService codeItemService, 
            IWfVariableService variableService, IValueService valueService, IFVariableValueService fVariableValue, 
            IEntityService entityService, IDictionaryService dictionary, IParameterService parameterService, 
            IIndexService indexService, IVwCurrentVariableValueService currentVariableValueService)
        {
            _dicFieldService = dicFieldService;
            _codeItemService = codeItemService;
            _variableService = variableService;
            _valueService = valueService;
            _fVariableValue = fVariableValue;
            _entityService = entityService;
            _dictionary = dictionary;
            _parameterService = parameterService;
            _indexService = indexService;
            _currentVariableValueService = currentVariableValueService;
        }

        public PartialViewResult Add(int dicId, int parentId, int actionId, int dfl)
        {
            ViewBag.dialogJSON = "dialogJSON" + dicId;
            var dflValues = Session["SearchValues"] != null
                ? JsonConvert.DeserializeObject<List<DflValue>>(Session["SearchValues"].ToString()) : null;
            Session["SearchValues"] = null;
            if (dflValues != null)
            {
                foreach (var dflValue in dflValues.Where(dflValue => _valueService.IsDate(dflValue.DflId)))
                {
                    dflValue.Value = ConvertDate.DatabaseFormat(dflValue.Value);
                }
            }
            var formFields = _valueService.GetFormFieldsByAction(actionId, parentId, dfl);
            if (formFields == null || formFields.Count == 0)
            {
                var fields = _dicFieldService.GetDicFields(dicId);
                formFields = fields.Select(field => new VwFormField
                {
                    Visibility = field.Visibility, Mask = field.Mask, Css = field.Css, ConditionalDisplayName = field.ConditionalDisplayName, Description = field.Description, DisplayName = field.DisplayName, TechnicalName = field.Name, Empty = field.Empty, Order = field.Order, Group = field.Group, Origin = field.Origin, DefaultValue = field.DefaultValue, TextArea = field.TextArea, MultiValued = field.MultiValued, ValidationRegexp = field.ValidationRegexp, Type = field.Type, CodeDcdId = field.CodeDcdId, CodeInlineDcdId = field.CodeInlineDcdId, SearchCriteria = field.SearchCriteria, ReferenceDicId = field.ReferenceDicId, DflId = field.Id, WafId = field.Id
                }).ToList();
            }
            return PartialView(new AddReferenceListViewModel
            {
                DicId = dicId,
                Fields = formFields.GroupBy(f => f.Group).ToList(),
                Infos = new List<ComponentInfo>(),
                DflValues = dflValues
            });
        }
    
        public PartialViewResult Edit(int dicId, int entityId, int parentId, int actionId, int dfl)
        {
            ViewBag.dialogJSON = "dialogJSON" + dicId;
            var formFields = _valueService.GetFormFieldsByAction(actionId, parentId, dfl);
            if (formFields == null || formFields.Count == 0)
            {
                var fields = _dicFieldService.GetDicFields(dicId);
                formFields = fields.Select(field => new VwFormField
                {
                    Visibility = field.Visibility,
                    Mask = field.Mask,
                    Css = field.Css,
                    ConditionalDisplayName = field.ConditionalDisplayName,
                    Description = field.Description,
                    DisplayName = field.DisplayName,
                    TechnicalName = field.Name,
                    Empty = field.Empty,
                    Order = field.Order,
                    Group = field.Group,
                    Origin = field.Origin,
                    DefaultValue = field.DefaultValue,
                    TextArea = field.TextArea,
                    MultiValued = field.MultiValued,
                    ValidationRegexp = field.ValidationRegexp,
                    Type = field.Type,
                    CodeDcdId = field.CodeDcdId,
                    CodeInlineDcdId = field.CodeInlineDcdId,
                    SearchCriteria = field.SearchCriteria,
                    ReferenceDicId = field.ReferenceDicId,
                    DflId = field.Id,
                    WafId = field.Id
                }).ToList();
            }
            return PartialView(new AddReferenceListViewModel
            {
                DicId = dicId,
                EntityId = entityId,
                Fields = formFields.GroupBy(f => f.Group).ToList(),
                Infos = new List<ComponentInfo>()
            });
        }

        public PartialViewResult Info(int dicId, int entityId, int parentId, int actionId, int dfl)
        {
            ViewBag.dialogJSON = "dialogJSON" + dicId;
            var formFields = _valueService.GetFormFieldsByAction(actionId, parentId, dfl);
            if (formFields == null || formFields.Count == 0)
            {
                var fields = _dicFieldService.GetDicFields(dicId);
                formFields = fields.Select(field => new VwFormField
                {
                    Visibility = field.Visibility,
                    Mask = field.Mask,
                    Css = field.Css,
                    ConditionalDisplayName = field.ConditionalDisplayName,
                    Description = field.Description,
                    DisplayName = field.DisplayName,
                    TechnicalName = field.Name,
                    Empty = field.Empty,
                    Order = field.Order,
                    Group = field.Group,
                    Origin = field.Origin,
                    DefaultValue = field.DefaultValue,
                    TextArea = field.TextArea,
                    MultiValued = field.MultiValued,
                    ValidationRegexp = field.ValidationRegexp,
                    Type = field.Type,
                    CodeDcdId = field.CodeDcdId,
                    CodeInlineDcdId = field.CodeInlineDcdId,
                    SearchCriteria = field.SearchCriteria,
                    ReferenceDicId = field.ReferenceDicId,
                    DflId = field.Id,
                    WafId = field.Id
                }).ToList();
            }
            return PartialView(new AddReferenceListViewModel
            {
                DicId = dicId,
                EntityId = entityId,
                Fields = formFields.GroupBy(f => f.Group).ToList(),
                Infos = new List<ComponentInfo>()
            });
        }

        public PartialViewResult Search(int dicId, int entityId, int flowId, int actionId, string id, string entities)
        {
            var dflValues = Session["SearchValues"] != null
                ? JsonConvert.DeserializeObject<List<DflValue>>(Session["SearchValues"].ToString()) : null;
            ViewBag.dialogJSON = "dialogJSON" + dicId;
            var username = GuceContextSession.ReadCookie().Username;
            if (dflValues != null && dflValues.Count > 0)
            {
                foreach (var dv in dflValues.Where(dv => _valueService.IsDate(dv.DflId)))
                {
                    dv.Value = ConvertDate.DatabaseFormat(dv.Value);
                }
            }
            return PartialView(new SearchReferenceListViewModel
            {
                DicId = dicId,
                DicName = _dictionary.GetDictionary(dicId).Name,
                Id = id,
                Fields = _dicFieldService.GetSearchableDicFields(dicId),
                Infos = new List<ComponentInfo>(),
                Entities = _entityService.GetEntities(dicId, entityId, flowId, actionId, username, 
                JsonConvert.DeserializeObject<List<EntityIdDicId>>(entities)), 
                DflValues = dflValues
            });
        }

        [HttpPost]
        public bool CheckRequiredWarningField(int entityId, int dicId)
        {
            var fields = _dicFieldService.GetRequiredAndWarningFields(dicId);
            var values = _valueService.GetValues(entityId);
            return fields.Any(field => values.All(value => value.DflId != field.Id));
        }

        [HttpPost]
        public string CreateComponent(int parentId, string parentProperty, bool dialog, int index, string label)
        {
            ComponentInfo info;
            if (parentProperty.Equals("DflId"))
            {
                var field = _dicFieldService.GetDicField(parentId);
                var dictionary = field.ReferenceDicId != null ? _dictionary.GetDictionary((int)field.ReferenceDicId) : null;
                info = new ComponentInfo
                {
                    Id = field.Id + index.ToString(),
                    DisplayName = label,
                    Type = field.Type,
                    CodeDcdId = field.CodeDcdId,
                    CodeInlineDcdId = field.CodeInlineDcdId,
                    Description = field.Description,
                    Empty = field.Empty,
                    Index = index,
                    ParentId = field.Id,
                    ReferenceId = field.ReferenceDicId,
                    TagName = "Values",
                    ParentProperty = "DflId",
                    ValueProperty = "ValueContent",
                    Visibility = field.Visibility,
                    ConditionalDisplayName = field.ConditionalDisplayName,
                    Name = field.Name,
                    DefaultValue = field.DefaultValue,
                    TextArea = field.TextArea,
                    Css = field.Css,
                    MultiValued = field.MultiValued,
                    ValidationRegexp = field.ValidationRegexp,
                    SearchCriteria = field.SearchCriteria,
                    Group = field.Group,
                    Order = field.Order,
                    Mask = field.Mask,
                    DicName = dictionary?.Name,
                    DicShareable = dictionary?.Shareable,
                    DicListCreate = dictionary?.ListCreate,
                    IdxId = field.IdxId,
                    ReadOnly = false
                };
            }
            else
            {
                var field = _variableService.GetWfVariable(parentId);
                var dictionary = field.ReferenceDicId != null ? _dictionary.GetDictionary((int)field.ReferenceDicId) : null;
                info = new ComponentInfo
                {
                    Id = field.Id + index.ToString(),
                    DisplayName = label,
                    Type = field.Type,
                    CodeDcdId = field.CodeDcdId,
                    CodeInlineDcdId = field.CodeInlineDcdId,
                    Description = field.Description,
                    Empty = field.Empty,
                    Index = index,
                    ParentId = field.Id,
                    ReferenceId = field.ReferenceDicId,
                    TagName = "Variables",
                    ParentProperty = "WvaId",
                    ValueProperty = "Value",
                    Visibility = field.Visibility,
                    ConditionalDisplayName = field.ConditionalDisplayName,
                    Name = field.Name,
                    TextArea = field.TextArea,
                    Css = field.Css,
                    MultiValued = field.MultiValued,
                    ValidationRegexp = field.ValidationRegexp,
                    Group = field.Group,
                    Order = field.Order,
                    Mask = field.Mask,
                    DicName = dictionary?.Name,
                    DicShareable = dictionary?.Shareable,
                    DicListCreate = dictionary?.ListCreate,
                    ReadOnly = false,
                    DefaultValue = field.DefaultValue
                };
            }
            return ComponentHelper.CreateComponent(info, dialog, true, Session["username"].ToString());
        }

        [HttpPost]
        public void RemoveComponent(int valueId, int dfl, int statusId)
        {
            if (dfl == 1)
            {
                if (valueId == 0) return;
                var value = _valueService.GetValue(valueId);
                value.ValueContent = null;
                if (value.TxId == 0 && value.FstId == statusId)
                {
                    _valueService.Update(value);
                }
                else
                {
                    _valueService.Add(value);
                }
            }
            else
            {
                if (valueId == 0) return;
                var variable = _fVariableValue.GetFVariableValue(valueId);
                variable.Value = null;
                if (variable.FstId == statusId)
                {
                    _fVariableValue.Update(variable);
                }
                else
                {
                    _fVariableValue.Add(variable);
                }
            }
        }
        
        [HttpPost]
        public string SearchEntity(string json, int dicId, string jsonData)
        {
            Session["SearchValues"] = jsonData;
            var dflValues = JsonConvert.DeserializeObject<List<DflValue>>(json);
            return new JavaScriptSerializer().Serialize(SearchEntitiesDb(dflValues, dicId));
        }

        private List<DbEntity> SearchEntitiesDb(IEnumerable<DflValue> dflValues, int dicId)
        {
            var where = "";
            foreach (var dflValue in dflValues)
            {
                var val = _valueService.IsDate(dflValue.DflId) ? ConvertDate.DatabaseFormat(dflValue.Value) : dflValue.Value;
                where += EfDatabaseFunction.CanonicalizedSearchCriteria(dflValue.DflId, val.Replace("'", "''")) + " and ";
            }
            if (where.Length > 4)
            {
                where = where.Substring(0, where.Length - 4);
            }
            var query = "select id, name from entities where dic_id = " + dicId + " and " + where;
            return EfDatabaseFunction.ExecuteQuery(query);
        }

        [HttpPost]
        public string GetDependencies(int dependentDciId)
        {
            return new JavaScriptSerializer().Serialize(_codeItemService.GetDicCodeItemsByDependend(dependentDciId));
        }

        [HttpPost]
        public string GetCodeItem(int id)
        {
            return new JavaScriptSerializer().Serialize(_codeItemService.GetDicCodeItem(id));
        }

        [HttpPost]
        public string Upload()
        {
            var file = Request.Files[0];
            var flowId = int.Parse(Request.Form.Get("flowId"));
            var name = Request.Form.Get("name");
            var valueId = int.Parse(Request.Form.Get("valueId"));
            var dfl = int.Parse(Request.Form.Get("dfl"));
            var currentValue = Request.Form.Get("currentValue");
            var fileName = GetFileName(file?.FileName);
            if (fileName == null) return null;

            var docFileName = PdfFileName(fileName, name, flowId, valueId, dfl, currentValue);
            file.SaveAs(docFileName);
            return new JavaScriptSerializer().Serialize(GetFileName(docFileName) + ";" + "md5=" + CheckSumMd5(docFileName));
        }

        private string PdfFileName(string fileName, string name, int flowId, int valueId, int dfl, string currentValue)
        {
            const int seq = 1;
            var now = DateTime.Now;
            var ext = fileName != null ? GetExtension(fileName) : ".pdf";
            var docsDir = AppDomain.CurrentDomain.BaseDirectory + Parameters[DocDir];
            var dir = CreateDirectory(docsDir + _separator + now.ToString("yyyy") + _separator + now.ToString("MM"));

            var multivalued = dfl != 0
                ? _dicFieldService.GetDicFields().First(f => f.Name.Equals(name)).MultiValued
                : _variableService.GetWfVariables().First(v => v.Name.Equals(name)).MultiValued;

            if (valueId == 0)
            {
                if (string.IsNullOrEmpty(currentValue))
                {
                    if (multivalued)
                    {
                        fileName = now.ToString("yyyyMMdd") + "_" + name + "_" + flowId.ToString("D5") + "_" + seq.ToString("D3") + ext;
                        fileName = GetUniqueFilePathPdf(dir.FullName + _separator + fileName);
                    }
                    else
                    {
                        fileName = dir.FullName + _separator + now.ToString("yyyyMMdd") + "_" + name + "_" + flowId.ToString("D5") + "_" + seq.ToString("D3") + ext;
                    }
                }
                else
                {
                    fileName = dir.FullName + _separator + currentValue;
                }
            }
            else
            {
                if (dfl != 0)
                {
                    var value = _valueService.GetValue(valueId);
                    if (value.TxId == 0 && value.ValueContent != null && value.ValueContent.Split(';').Length > 0)
                    {
                        fileName = dir.FullName + _separator + value.ValueContent.Split(';')[0];
                    }
                    else
                    {
                        if (value.ValueContent != null && value.ValueContent.Split(';').Length > 0)
                        {
                            fileName = GetUniqueFilePathPdf(dir.FullName + _separator + value.ValueContent.Split(';')[0]);
                        }
                        else
                        {
                            fileName = now.ToString("yyyyMMdd") + "_" + name + "_" + flowId.ToString("D5") + "_" + seq.ToString("D3") + ext;
                            fileName = GetUniqueFilePathPdf(dir.FullName + _separator + fileName);
                        }
                    }
                }
                else
                {
                    var variable = _fVariableValue.GetFVariableValue(valueId);
                    if (_currentVariableValueService.GetFlow(valueId) == flowId && variable.Value != null && variable.Value.Split(';').Length > 0)
                    {
                        fileName = dir.FullName + _separator + variable.Value.Split(';')[0];
                    }
                    else
                    {
                        if (variable.Value != null && variable.Value.Split(';').Length > 0)
                        {
                            fileName = GetUniqueFilePathPdf(dir.FullName + _separator + variable.Value.Split(';')[0]);
                        }
                        else
                        {
                            fileName = now.ToString("yyyyMMdd") + "_" + name + "_" + flowId.ToString("D5") + "_" + seq.ToString("D3") + ext;
                            fileName = GetUniqueFilePathPdf(dir.FullName + _separator + fileName);
                        }
                    }
                }

            }
            return fileName;
        }

        [HttpPost]
        public void DeleteFile(string filename, int valueId)
        {
            var url = "~/" + Parameters[DocDir] + "/" + filename.Substring(0, 4) + "/" + filename.Substring(4, 2) + "/" + filename;
            var filePath = System.Web.HttpContext.Current.Server.MapPath(url);
            if (valueId == 0)
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            else
            {
                var value = _valueService.GetValue(valueId);
                if (value.TxId > 0) return;
                if (!System.IO.File.Exists(filePath)) return;
                System.IO.File.Delete(filePath);
                value.ValueContent = null;
                _valueService.Update(value);
            }
        }

        [HttpPost]
        public string FileDir()
        {
            return Request?.Url?.OriginalString.Substring(0, Request.Url.OriginalString.IndexOf("/Reference",
                    StringComparison.Ordinal)) + "/" + Parameters[DocDir] + "/";
        }

        [HttpPost]
        public string GetScanners()
        {
            var proxy = new WebClient();
            var responseData = proxy.DownloadString(new Uri("http://" + GetUserIp() + ":4243/agent/Scanners/"));
            if (responseData.Length <= 4) return "NO_SCANNER_FOUND";
            var scanners = JsonConvert.DeserializeObject<List<Scanner>>(responseData);
            foreach (var scanner in scanners)
            {
                scanner.Id = scanner.Id.Substring(scanner.Id.Length - 4);
            }
            return new JavaScriptSerializer().Serialize(scanners);
        }

        [HttpPost]
        public string Scan(string scannerId, string mode)
        {
            var proxy = new WebClient();
            var url = new Uri("http://" + GetUserIp() + ":4243/agent/Scanner/" + scannerId + "?dpi=" 
                + Parameters[ScanDpi] + "&mode=" + mode);
            var responsedata = proxy.DownloadData(url);
            var stream = new MemoryStream(responsedata);
            var image = FromStream(stream);
            CreateDirectory(Parameters[TmpDir].ToString());
            var files = new List<string>();
            var fileName = GetUniqueFilePath(Parameters[TmpDir] + _separator + "file.jpeg");
            image.Save(fileName, ImageFormat.Jpeg);
            files.Add(GetFileName(fileName));
            //SetupUserParameter(ScannerId, scannerId);//setup the favori scanner
            return new JavaScriptSerializer().Serialize(files);
        }

        public void SetupUserParameter(string parameterId, string value)
        {
            var username = GuceContextSession.ReadCookie().Username;
            var parameter = _parameterService.GetParameter(parameterId, username);
            if (parameter == null)
            {
                _parameterService.Add(new Parameter
                {
                    Id = parameterId,
                    Username = username,
                    Value = value,
                    Description = parameterId + " de " + username
                });
                Session[parameterId + "_" + username] = value;
            }
            else
            {
                if (parameter.Value.Equals(value)) return;
                parameter.Value = value;
                _parameterService.Update(parameter);
                Session[parameterId + "_" + username] = value;
            }
        }

        private static string GetUserIp()
        {
            var ip = string.Empty;
            if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.UserHostAddress))
            {
                ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            return ip;
        }

        [HttpPost]
        public string MergeFiles(string json, int flowId, string name, int valueId, int dfl, string currentValue)
        {
            var images = JsonConvert.DeserializeObject<List<DflValue>>(json);
            
            var pdfFileName = PdfFileName(null, name, flowId, valueId, dfl, currentValue);
            var pdfFile = JpgToPdf(images, pdfFileName);

            foreach (var image in images.Where(image => System.IO.File.Exists(Parameters[TmpDir] + _separator + image.Value)))
            {
                System.IO.File.Delete(Parameters[TmpDir] + _separator + image.Value);
            }
            return GetFileName(pdfFileName) + ";" + "md5=" + CheckSumMd5(pdfFile);
        }

        private string JpgToPdf(IEnumerable<DflValue> images, string pdf)
        {
            var document = new Document(PageSize.A4, 5, 5, 5, 5);
            using (var stream = new FileStream(pdf, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                PdfWriter.GetInstance(document, stream);
                document.Open();
                foreach (var img in images)
                {
                    using (var imageStream = new FileStream(Parameters[TmpDir] + _separator + img.Value, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var image = Image.GetInstance(imageStream);
                        if (image.Height > PageSize.A4.Height - 5)
                        {
                            image.ScaleToFit(PageSize.A4.Width - 5, PageSize.A4.Height - 5);
                        }
                        else if (image.Width > PageSize.A4.Width - 5)
                        {
                            image.ScaleToFit(PageSize.A4.Width - 5, PageSize.A4.Height - 5);
                        }
                        image.Alignment = Element.ALIGN_MIDDLE;
                        document.Add(image);
                    }
                }
                document.Close();
            }
            return pdf;
        }

        private static string CheckSumMd5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = OpenRead(filename))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "‌​").ToLower();
                }
            }
        }

        [HttpPost]
        public string CheckIndex(int index, int entityId, string value)
        {
            var canonicalize = EfDatabaseFunction.IdxCanonicalize(index, value.Replace("'", "''"));
            if (_indexService.GetIndex(index).Unique)
            {
                return new JavaScriptSerializer().Serialize(new CheckCanonicalize
                {
                    Check = EfDatabaseFunction.IdxCheck(index, entityId, canonicalize.Replace("'", "''")),
                    Canonicalize = canonicalize
                });
            }
            return new JavaScriptSerializer().Serialize(new CheckCanonicalize
            {
                Canonicalize = canonicalize
            });
        }

        [HttpPost]
        public string ValidateIndex(int entityId, string json)
        {
            var validateIndexes = JsonConvert.DeserializeObject<List<ValidateIndex>>(json);
            var indexResults = (from index in validateIndexes
                let result = EfDatabaseFunction.IdxAddValue(index.IdxId, entityId,
                    EfDatabaseFunction.IdxCanonicalize(index.IdxId, index.Value.Replace("'", "''")).Replace("'", "''"))
                where result == 0
                select new ValidateIndex
                {
                    Id = index.Id
                }).ToList();
            return new JavaScriptSerializer().Serialize(indexResults);
        }
    }
}