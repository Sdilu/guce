﻿/*=================================================================*
 * Classe: <AdminController>
 * Version/date: <2.0.0> <2016.09.23>
 *
 * Description: <” Cette classe est le controleur pour l'administration de l'application. ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.MvcWebUI.Models.Admin;
using System.Web.Mvc;

namespace Guce.MvcWebUI.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            var usernames = new List<string>();
            return View(new AdminListViewModel
            {
                Usernames = usernames
            });
        }
    }
}