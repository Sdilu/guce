﻿/*=================================================================*
 * Classe: <HomeController>
 * Version/date: <2.0.0> <2016.07.08>
 *
 * Description: <” Cette classe est le controleur pour la page d'accueil. ”>
 * Specificities: <“ Cette classe est un controller. ”>
 *
 * Authors:   <Steve dilu>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Web.Mvc;
using Guce.Business.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Custom;
using Guce.MvcWebUI.Models.Home;

namespace Guce.MvcWebUI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IVwUserNextActionService _vwUserNextActionService;
        private readonly IVwActiveWorkflowService _activeWorkflowService;
        private readonly ISiteService _siteService;

        public HomeController(IVwUserNextActionService vwUserNextActionService,
            IVwActiveWorkflowService activeWorkflowService, ISiteService siteService)
        {
            _vwUserNextActionService = vwUserNextActionService;
            _activeWorkflowService = activeWorkflowService;
            _siteService = siteService;
        }

        public ActionResult Index()
        {
            var username = GuceContextSession.ReadCookie().Username;
            var groups = GuceContextSession.ReadCookie().UserGroup;
            var userNextActions = _vwUserNextActionService.GetUserNextActionsHomePage();
            foreach (var action in userNextActions)
            {
                var sessionId = action.FlwId + "_" + action.EntId + "_" + action.WacId;
                if (System.Web.HttpContext.Current.Application[sessionId] != null)
                {
                    action.Lock = !System.Web.HttpContext.Current.Application[sessionId].ToString().Equals(username);
                }
            }
            var actionsCount = _vwUserNextActionService.GetUserNextActionsHomePageCount();
            // all actions for grp_admin
            if ((bool) Session["isAdmin"])
            {
                var allActions = _vwUserNextActionService.GetActiveNextActionsHomePageCount();
                Session["pendingAllActions"] = allActions;
                return RedirectToAction("All", "Home");
            }
            Session["pendingActions"] = actionsCount;
            return View(new HomeViewListModel
            {
                VwUserNextActions = userNextActions,
                Workflows = _activeWorkflowService.GetActiveWorkflows(),
                SiteName = _siteService.GetSite(Configuration.Parameters[ParameterId.SiteId].ToString()).Name,
                Groups = groups
            });
        }

        public ActionResult All()
        {
            var username = GuceContextSession.ReadCookie().Username;
            var groups = GuceContextSession.ReadCookie().UserGroup;
            var activeNextActions = _vwUserNextActionService.GetActiveNextActionsHomePage();
            foreach (var action in activeNextActions)
            {
                var sessionId = action.FlwId + "_" + action.EntId + "_" + action.WacId;
                if (System.Web.HttpContext.Current.Application[sessionId] != null)
                {
                    action.Lock = !System.Web.HttpContext.Current.Application[sessionId].ToString().Equals(username);
                }
            }
            var allActions = _vwUserNextActionService.GetActiveNextActionsHomePageCount();
            Session["pendingAllActions"] = allActions;
            return View(new HomeViewListModel
            {
                VwActiveNextActions = activeNextActions,
                SiteName = _siteService.GetSite(Configuration.Parameters[ParameterId.SiteId].ToString()).Name,
                Groups = groups
            });
        }

        public ActionResult Start(int wflId, int wfvId, int actionId = 0, int flowId = 0, int entityId = 0)
        {
            return RedirectToAction("Start", "Form", new { wflId, wfvId, actionId, flowId, entityId, ajax = true });
        }
    }
}