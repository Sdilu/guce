﻿using System.Collections.Generic;
using System.Linq;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.Reference
{
    public class AddReferenceListViewModel
    {
        public int DicId { get; set; }
        public int EntityId { get; set; }
        public List<IGrouping<string, VwFormField>> Fields { get; set; }
        public List<ComponentInfo> Infos { get; set; }
        public List<DflValue> DflValues { get; set; }
    }
}