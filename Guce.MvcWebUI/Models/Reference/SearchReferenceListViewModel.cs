﻿using System.Collections.Generic;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.Reference
{
    public class SearchReferenceListViewModel
    {
        public int DicId { get; set; }
        public string DicName { get; set; }
        public string Id { get; set; }
        public int EntityId { get; set; }
        public List<DicField> Fields { get; set; }
        public List<ComponentInfo> Infos { get; set; }
        public List<Entity> Entities { get; set; }
        public List<DflValue> DflValues { get; set; }
    }
}