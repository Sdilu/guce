﻿using System.Collections.Generic;
using Guce.Entities.Custom;

namespace Guce.MvcWebUI.Models.Search
{
    public class SearchViewListModel
    {
       
        public int WflId { get; set; }
        public int WfvId { get; set; }
        public int ActionId { get; set; }
        public int EntityId { get; set; }
        public string Keyword { get; set; }
        public int? DicId { get; set; }
        public List<EntityMainSearch> EntityMainSearchs { get; set; }

        public List<Entities.Custom.FolderMainSearch> FolderMainSearchs { get; set; }
    }
}