﻿using System.Collections.Generic;

namespace Guce.MvcWebUI.Models.Admin
{
    public class AdminListViewModel
    {
        public List<string> Usernames { get; set; }
    }
}