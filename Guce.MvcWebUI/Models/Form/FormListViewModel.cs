﻿using System.Collections.Generic;
using System.Linq;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.Form
{
    public class FormListViewModel
    {
        public WfAction Action { get; set; }
        public int ActionId { get; set; }
        public int WflId { get; set; }
        public int WfvId { get; set; }
        public int FlowId { get; set; }
        public int EntityId { get; set; }
        public int StatusId { get; set; }
        public string Reference { get; set; }
        public VwUserNextAction VwUserNextAction { get; set; }
        public List<IGrouping<string, VwFormField>> FormFields { get; set; }
        public List<ComponentInfo> Infos { get; set; }
    }
}