﻿using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;

namespace Guce.MvcWebUI.Models.Form
{
    public class PrintListViewModel
    {
        public int WflId { get; set; }
        public WfAction Action { get; set; }
        public int ActionId { get; set; }
        public int WfvId { get; set; }
        public int FlowId { get; set; }
        public int EntityId { get; set; }
        public int StatusId { get; set; }
        public string Reference { get; set; }
        public VwUserNextAction VwUserNextAction { get; set; }
        public int ReportId { get; set; }
        public string FunctionToPrint { get; set; }
        public object DocumentToPrint { get; set; }
    }
}