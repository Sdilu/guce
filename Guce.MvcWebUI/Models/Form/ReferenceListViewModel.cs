﻿using System.Collections.Generic;
using Guce.Entities.Concrete;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.Form
{
    public class ReferenceListViewModel
    {
        public string IdParent { get; set; }
        public int DicId { get; set; }
        public List<Value> Values { get; set; }
        public List<ComponentInfo> Infos { get; set; }
    }
}