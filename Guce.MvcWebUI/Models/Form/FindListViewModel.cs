﻿using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;

namespace Guce.MvcWebUI.Models.Form
{
    public class FindListViewModel
    {
        public WfAction Action { get; set; }
        public int ActionId { get; set; }
        public int EntityId { get; set; }
        public int FlowId { get; set; }
        public int WflId { get; set; }
        public int WfvId { get; set; }
        public string Keyword { get; set; }
        public string Entities { get; set; }
        public VwUserNextAction VwUserNextAction { get; set; }
    }
}