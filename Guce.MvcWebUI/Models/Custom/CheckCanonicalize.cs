﻿namespace Guce.MvcWebUI.Models.Custom
{
    public class CheckCanonicalize
    {
        public int Check { get; set; }
        public string Canonicalize { get; set; } 
    }
}