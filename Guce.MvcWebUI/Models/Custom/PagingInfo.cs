﻿/*=================================================================*
 * Classe: <PagingInfo>
 * Version/date: <2.0.0> <2016.07.09>
 *
 * Description: <” Cette classe est le modèle pour faire la pagination. ”>
 * Specificities: <“ Cette classe est un pager. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

namespace Guce.MvcWebUI.Models.Custom
{
    public class PagingInfo
    {
        public int CurrentPage { get; set; }
        public int TotalPageCount { get; set; }
        public string BaseUrl { get; set; }
    }
}