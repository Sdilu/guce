﻿namespace Guce.MvcWebUI.Models.Custom
{
    public class ValidateIndex
    {
        public bool IsDfl { get; set; }
        public int ParentId { get; set; }
        public int IdxId { get; set; }
        public string Value { get; set; }
        public string Id { get; set; }

    }
}