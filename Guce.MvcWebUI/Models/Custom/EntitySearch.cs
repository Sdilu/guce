﻿namespace Guce.MvcWebUI.Models.Custom
{
    public class EntitySearch
    {
        public int EntityId { get; set; }
        public string NomCommercial { get; set; }
        public string Enseigne { get; set; }
        public string Sigle { get; set; }
        public string Rccm { get; set; }
        public string NumeroGuce { get; set; }
    }
}