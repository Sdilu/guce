﻿/*=================================================================*
 * Classe: <ComponentInfo>
 * Version/date: <2.0.0> <2016.07.09>
 *
 * Description: <” Cette classe definie les informations composantes un composant. ”>
 * Specificities: <“ -. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Concrete;

namespace Guce.MvcWebUI.Models.Custom
{
    public class ComponentInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string ConditionalDisplayName { get; set; }
        public string Type { get; set; }
        public int Index { get; set; }
        public string Value { get; set; }
        public int ValueId { get; set; }
        public int? ParentId { get; set; }
        public string Visibility { get; set; }
        public string TagName { get; set; }
        public string ValueProperty { get; set; }
        public string ParentProperty { get; set; }
        public string Empty { get; set; }
        public string Description { get; set; }
        public int? CodeDcdId { get; set; }
        public int? CodeInlineDcdId { get; set; }
        public DicCodeItem CodeItem { get; set; }
        public string DefaultValue { get; set; }
        public int? ReferenceId { get; set; }
        public string DicName { get; set; }
        public bool? ReadOnly { get; set; }
        public bool? MultiValued { get; set; }
        public int? SearchCriteria { get; set; }
        public int? Order { get; set; }
        public string Group { get; set; }
        public string Css { get; set; }
        public string Mask { get; set; }
        public bool TextArea { get; set; }
        public string ValidationRegexp { get; set; }
        public string DicColor { get; set; }
        public bool? DicShareable { get; set; }
        public bool? DicListCreate { get; set; }
        public bool InSearchDialog { get; set; }
        public int? IdxId { get; set; }
    }
}