﻿using Guce.Entities.Concrete;

namespace Guce.MvcWebUI.Models.Config
{
    public class ParameterListViewModel
    {
        public Parameter Parameter { get; set; }
    }
}