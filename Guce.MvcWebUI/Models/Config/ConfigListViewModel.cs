﻿using System.Collections.Generic;
using Guce.Entities.Concrete;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.Config
{
    public class ConfigListViewModel
    {
        public bool Admin { get; set; }
        public List<Parameter> Parameters { get; set; }
    }
}