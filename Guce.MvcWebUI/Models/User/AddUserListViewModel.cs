﻿
namespace Guce.MvcWebUI.Models.User
{
    public class AddUserListViewModel
    {
        public Entities.Concrete.User User { get; set; }
        public string Password { get; set; }
    }
}