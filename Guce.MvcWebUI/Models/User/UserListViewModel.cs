﻿using System.Collections.Generic;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.User
{
    public class UserListViewModel
    {
        public List<Entities.Concrete.User> Users { get; set; }
        public int UserCount { get; set; }
    }
}