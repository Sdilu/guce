﻿namespace Guce.MvcWebUI.Models.User
{
    public class PasswordUserListViewModel
    {
        public string NewPassword { get; set; }

        public string RepeatPassword { get; set; }
    }
}