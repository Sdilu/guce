﻿using System.Collections.Generic;
using Guce.Entities.ComplexType;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.Models.Home
{
    public class HomeViewListModel
    {
        public List<VwUserNextAction> VwUserNextActions { get; set; }
        public List<VwActiveNextAction> VwActiveNextActions { get; set; }
        public List<VwActiveWorkflow> Workflows { get; set; }
        public string Groups { get; set; }
        public string SiteName { get; set; }
    }
}