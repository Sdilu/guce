﻿using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.MvcWebUI.Models.Stat
{
    public class StatListViewModel
    {
        public List<StatPage> StatPages { get; set; }
    }
}