﻿/*=================================================================*
 * Classe: <ComponentHelper>
 * Version/date: <2.0.0> <2016.07.09>
 *
 * Description: <” Cette classe permet de créer un composant Html5 en fonction du type fourni. ”>
 * Specificities: <“ Cette classe est un HtmlHelper. ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
 using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.WebPages;
using Guce.Business.Abstract;
using Guce.Business.Concrete.Manager;
using Guce.DataAccess.Concrete;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Guce.MvcWebUI.Models.Custom;

namespace Guce.MvcWebUI.HtmlHelpers
{
    public static class ComponentHelper
    {
        private static readonly IDicCodeItemService ItemService = new DicCodeItemManager(new EfDicCodeItemDal());
        private static readonly IEntityService EntityService = new EntityManager(new EfEntityDal());
        private static readonly IDicFieldService FieldService = new DicFieldManager(new EfDicFieldDal());
        private static readonly IDictionaryService DictionaryService = new DictionaryManager(new EfDictionaryDal());
        private static readonly IVwCurrentValueService CurrentValueService = new VwCurrentValueManager(new EfVwCurrentValueDal(), new EfDicFieldDal());
        private static readonly IVwFormFieldService FormFieldService = new VwFormFieldManager(new EfVwFormFieldDal());
        private static readonly IParameterService ParameterService = new ParameterManager(new EfParameterDal());

        public static MvcHtmlString CreateComponent(this HtmlHelper helper, ComponentInfo info, bool dialog, bool copy, string username)
        {
            var id = info.Id;
            var sb = new StringBuilder();
            var formGroup = dialog ? " form-group-sm" : "";
            sb.Append("<div class='form-group" + formGroup + "'>\n"); //début du form-group
            sb.Append("<label id='label_" + info.Id + "' for='comp_" + info.Id + "' class='col-sm-3 control-label realLabelField'" +
                      ">\n" + info.DisplayName + RequiredStar(info.Empty) + "</label>\n");

            var multivalued = info.Type.Equals(FieldType.Reference) ||
                              (info.MultiValued != null && (bool) info.MultiValued)
                              || info.Type.Equals(FieldType.Pdf) || info.Type.Equals(FieldType.Doc) ||
                              info.Type.Equals(FieldType.Amount) ? "input-group" : "";
            multivalued = (info.Type.Equals(FieldType.Pdf) || info.Type.Equals(FieldType.Doc)) && !multivalued.IsEmpty()
                          && info.Value == null && info.ReadOnly != null && (bool) info.ReadOnly ? "" : multivalued;
            if (info.MultiValued != null && (bool)info.MultiValued)
            {
                multivalued = " input-group";
            }
            multivalued = info.Type.Equals(FieldType.Reference) && info.Value == null && info.ReadOnly != null 
                && (bool)info.ReadOnly && info.MultiValued != null && !(bool)info.MultiValued ? "" : multivalued;
            if (dialog && !multivalued.IsEmpty())
            {
                multivalued += " input-group-sm";
            }
            var formControl = !dialog ? "form-control" : "form-control form-control-sm";
            var readOnly = info.ReadOnly != null && (bool)info.ReadOnly ? " readonly='readonly' " : "";
            var dfl = info.ParentProperty.Equals("DflId") ? 1 : 0;
            var tooltip = info.InSearchDialog ? "" : "data-toggle='tooltip'";

            //debut de creation de la colonne pour le composant ::col-sm-9::
            sb.Append("<div class='col-sm-9'>\n");
            sb.Append(multivalued.IsEmpty() ? "" : "<div class='" + multivalued + "'>\n");
            //debut input-group
            var disabled = info.ReadOnly != null && (bool)info.ReadOnly ? "disabled = 'disabled'" : "";
            var visibility = "";
            if (helper != null)
            {
                visibility = "checkVisibility('"+ helper.ViewBag.PanelId + "', " + helper.ViewBag.dialogJSON + ", " 
                    + helper.ViewBag.MethodeVisibility + ");";
            }
            //var checkChanges = !info.InSearchDialog ? "checkChanges(\"" + info.Id + "\");" : "";
            if (info.Type.Equals(FieldType.Reference))
            {
                info.Value = info.Value == null && info.DefaultValue != null ? info.DefaultValue : info.Value;
                if (info.ReferenceId != null)
                {
                    info.DicShareable = info.DicShareable ?? DictionaryService.GetDictionary((int) info.ReferenceId).Shareable;
                    info.DicName = info.DicName ?? DictionaryService.GetDictionary((int)info.ReferenceId).Name;
                    info.DicListCreate = info.DicListCreate ?? DictionaryService.GetDictionary((int)info.ReferenceId).ListCreate;
                }
                var searchOrAddMethod = info.DicShareable != null && (bool)info.DicShareable ? "searchEntity" : "addEntity";
                var searchOrAddIcon = info.DicShareable != null && (bool)info.DicShareable ? "fa fa-search text-blue" : "fa fa-clone";
                var searchOrAddClass = info.DicShareable != null && (bool)info.DicShareable ? "default" : "primary";
                var referenceName = info.Value != null ? EntityService.GetEntity(int.Parse(info.Value)).Name : "";
                var entityId = info.Value != null ? int.Parse(info.Value) : 0;
                
                var visibilityMethod = helper != null && helper.ViewBag != null ? helper.ViewBag.MethodeVisibility : 0;
                var visibilityJson = helper != null && helper.ViewBag != null ? helper.ViewBag.dialogJSON : 0;
                //champ hidden qui contient la valeur de value pour un champ de type reference
                sb.Append("<input id='comp_" + info.Id + "' type='hidden' class='" + formControl + "' "
                          + CreateTagName(info.TagName, info.Index, info.ValueProperty) + " value=\"" + info.Value 
                          + "\" data-parent-id='" + info.ParentId + "' data-value-id='" + info.ValueId + "' data-dfl='" 
                          + dfl + "' data-readonly='" + info.ReadOnly + "' data-list-create='" + info.DicListCreate 
                          + "' data-empty-type='" + info.Empty + "' data-entity-shareable='"+ info.DicShareable + "' " +
                          "data-field-type='" + info.Type + "' data-technical-name='" + info.Name 
                          + "' data-multivalued='" + info.MultiValued + "'>\n");
                sb.Append("<span class='input-group-btn'>\n");
                //bouton pour rechercher / creer une entité
                if (disabled.IsEmpty())
                {
                    sb.Append("<button " + disabled + " class='btn btn-" + searchOrAddClass + "' type='button' id='search_"
                          + info.Id + "' onclick='" + searchOrAddMethod + "(\"" + info.Id + "\", \"" + info.ReferenceId +
                          "\", \"" + info.DicName?.ToLower() + "\", " + visibilityJson
                          + ", " + visibilityMethod + ");'><i class='" + searchOrAddIcon + "'></i></button>\n");
                    //bouton pour modifier une entité
                    if (info.ReferenceId != null && info.DicListCreate != null && (bool)info.DicListCreate)
                    {
                        sb.Append("<button class='btn btn-warning' type='button' id='edit_" + info.Id +
                              "' onclick='editEntity(\"" + info.Id + "\", \"" + info.ReferenceId + "\", \"" + entityId + "\", \""
                              + info.DisplayName.ToLower().Replace("'", "") + "\", false, " + visibilityJson
                              + ", " + visibilityMethod + ");' " + disabled + " ><i class='fa fa-edit'></i></button>\n");
                    }
                    //bouton pour supprimer une entité
                    sb.Append("<button class='btn btn-danger' type='button' id='delete_" + info.Id +
                              "' onclick='deleteEntity(\"" + info.Id + "\", \"" + referenceName.Replace("'", "")
                              + "\");' " + disabled + "><i class='fa fa-trash-o'></i></button>\n");
                }
                else
                {
                    if (info.Value != null)
                    {
                        sb.Append("<button class='btn bg-navy' type='button' id='info_" + info.Id +
                              "' onclick='infoEntity(\"" + info.Id + "\", \"" + info.ReferenceId + "\", \"" + referenceName.Replace("'", "")
                              + "\");' ><i class='fa fa-info'></i></button>\n");
                    }
                }
                sb.Append("</span>\n");
                sb.Append("<input type ='text' class='" + formControl + "' id='entityName_" + info.Id + "' title=\""
                    + info.Description + "\" " + tooltip + " readonly='readonly' value=\"" + referenceName
                    + "\" data-technical-name='" + info.Name + "' onclick='searchOrAddEntity(\"" + info.Id 
                    + "\");' data-empty-type='" + info.Empty + "' data-readonly='" + info.ReadOnly + "'>\n");
            }
            else if (info.Type.Equals(FieldType.Pdf))
            {
                var value = info.Value != null && info.Value.Split(';').Length > 0 ? info.Value.Split(';')[0] : null;
                var md5 = info.Value != null && info.Value.Split(';').Length > 1 ? info.Value.Split(';')[1] : null;
                sb.Append("<input type ='text' class='" + formControl + "' data-value-id='" + info.ValueId 
                    + "' id='comp_" + info.Id + "' title=\"" + info.Description + "\" " + tooltip + " style='" + info.Css + "' "
                          + CreateTagName(info.TagName, info.Index, info.ValueProperty) + " value=\"" + value + "\" data-technical-name='" 
                          + info.Name + "' data-parent-id='" + info.ParentId + "' readonly='readonly' data-dfl='" 
                          + dfl + "' data-readonly='" + info.ReadOnly + "' data-empty-type='" + info.Empty 
                          + "' data-md5='" + md5 + "' data-field-type='" + info.Type + "'>\n");
            }
            else if (info.Type.Equals(FieldType.Doc))
            {
                var value = info.Value != null && info.Value.Split(';').Length > 0 ? info.Value.Split(';')[0] : null;
                var md5 = info.Value != null && info.Value.Split(';').Length > 1 ? info.Value.Split(';')[1] : null;
                sb.Append("<input accept='.doc, .docx' type='file' data-technical-name='" + info.Name + "' id='chooseDoc_"
                        + info.Id + "' style='display:none;'/>");
                sb.Append("<input type ='text' class='" + formControl + "' data-value-id='" + info.ValueId 
                    + "' id='comp_" + info.Id + "' title=\"" + info.Description + "\" " + tooltip + " style='" + info.Css + "' "
                          + CreateTagName(info.TagName, info.Index, info.ValueProperty) + " value=\"" + value + "\" data-technical-name='"
                          + info.Name + "' data-parent-id='" + info.ParentId + "' readonly='readonly' data-dfl='" 
                          + dfl + "' data-readonly='" + info.ReadOnly + "' data-empty-type='" + info.Empty + "'" +
                          " data-md5='" + md5 + "' data-field-type='" + info.Type + "'>\n");
            }
            else if (info.Type.Equals(FieldType.Code))
            {
                var items = new List<DicCodeItem>();
                var dependentParent = GetDependentParent(info.CodeDcdId);
                if (info.Value == null)
                {
                   items = info.CodeInlineDcdId != null
                    ? ItemService.GetDicCodeItems(info.CodeInlineDcdId).Where(i => i.DependDciId == null).ToList()
                    : ItemService.GetDicCodeItems(info.CodeDcdId);
                }
                else
                {
                    if (info.CodeInlineDcdId != null)
                    {
                        var item = ItemService.GetCodeItems((int)info.CodeInlineDcdId).FirstOrDefault();
                        if (item?.DependDciId == null)
                        {
                            items = ItemService.GetDicCodeItems(info.CodeInlineDcdId).ToList();
                        }
                        else
                        {
                            var dependentDciId = ItemService.GetDependentId((int)info.CodeInlineDcdId, int.Parse(info.Value));
                            if (dependentDciId != null)
                                items = ItemService.GetDicCodeItems((int)info.CodeInlineDcdId, (int)dependentDciId).ToList();
                        }
                    }
                    if (info.CodeInlineDcdId == null)
                    {
                        var codeItem = ItemService.GetDicCodeItem(int.Parse(info.Value));
                        items = codeItem?.DependDciId != null 
                            ? ItemService.GetDicCodeItemsByDependend((int)codeItem.DependDciId) 
                            : ItemService.GetDicCodeItems(info.CodeDcdId).Where(i => i.DependDciId == null).ToList();
                    }
                }
                var inline = info.CodeInlineDcdId != null;
                sb.Append("<select onchange=\"loadDependencies(this, '" + inline + "');" + visibility + "\" class='" + formControl 
                    + "' style='" + info.Css + "' id='comp_" + info.Id + "' title=\"" + info.Description + "\" data-code-id='" 
                    + info.CodeDcdId + "' data-value-id='" + info.ValueId + "' data-code-inline='" + inline + "' " +
                    CreateTagName(info.TagName, info.Index, info.ValueProperty) + " " + tooltip + " data-technical-name='"
                          + info.Name + "' data-parent-id='" + info.ParentId + "' " + readOnly + " " + disabled + " data-dfl='" 
                          + dfl + "' data-old-value=\"" + info.Value + "\" data-dependent-parent='" + dependentParent + "'" +
                          " data-readonly='" + info.ReadOnly + "' data-empty-type='" + info.Empty + "' data-field-type='" + 
                          info.Type + "'>\n");
                foreach (var item in items)
                {
                    var selected = "";
                    if (info.Value != null)
                    {
                        if (item.Id.ToString().Equals(info.Value))
                        {
                            selected = "selected";
                        }
                    }
                    else
                    {
                        if (item.Id.ToString().Equals(info.DefaultValue))
                        {
                            selected = "selected";
                        }
                    }
                    sb.Append("<option id='" + item.Code + "' value=\"" + item.Id
                              + "\" " + selected + ">" + item.Value + "</option>\n");
                }
                sb.Append("</select>\n");
            }
            else if (info.Type.Equals(FieldType.Boolean))
            {
                var oui = (info.Value != null && info.Value.Equals("true")) || (info.Value == null
                    && info.DefaultValue != null && info.DefaultValue.Equals("true")) ? "checked" : "";
                sb.Append("<div class='checkbox checkbox-success checkbox-inline'>\n");
                sb.Append("<input onchange=\"" + visibility + "\" data-empty-type='" + info.Empty + "' data-value-id='"
                    + info.ValueId + "' id='comp_" + info.Id + "' type ='radio' class='form-control' "
                    + CreateTagName(info.TagName, info.Index, info.ValueProperty)
                          + " value='true' data-technical-name='" + info.Name + "' " + oui + " data-parent-id='" + info.ParentId
                          + "' data-old-value=\"" + info.Value + "\" " + disabled + " data-readonly='" + info.ReadOnly 
                          + "' data-field-type='" + info.Type + "'>\n");
                sb.Append("<label for='comp_" + info.Id + "'>Oui</label>\n");
                sb.Append("</div>\n");

                var non = (info.Value != null && info.Value.Equals("false")) || (info.Value == null
                    && info.DefaultValue != null && info.DefaultValue.Equals("false")) ? "checked" : "";
                sb.Append("<div class='checkbox checkbox-danger checkbox-inline'>\n");
                sb.Append("<input onchange=\"" + visibility + "\" data-empty-type='" + info.Empty + "' id='comp_" + info.Id
                          + "_non' type ='radio' class='form-control' " + CreateTagName(info.TagName, info.Index, info.ValueProperty)
                          + " value='false' data-technical-name='" + info.Name + "' " + non + " data-old-value=\"" + info.Value 
                          + "\" " + disabled + " data-readonly='" + info.ReadOnly 
                          + "' data-field-type='" + info.Type + "'>\n");
                sb.Append("<label for='comp_" + info.Id + "_non'>Non</label>\n");
                sb.Append("</div>\n");
            }
            else if (info.Type.Equals(FieldType.Date))
            {
                info.Value = info.Value != null ? ConvertDate.ComponentFormat(info.Value) : "";
                sb.Append("<div class='input-group' id='dateValue_" + info.Id + "' " +
                          "style='" + info.Css + "'>\n" +
                          "<input type ='text' class='" + formControl + "' data-value-id='" + info.ValueId + "' " +
                          "title=\"" + info.Description + "\" " + tooltip + " data-old-value=\"" + info.Value + "\"" +
                          CreateTagName(info.TagName, info.Index, info.ValueProperty) + " id='comp_" + info.Id +
                          "' data-technical-name='" + info.Name + "' data-parent-id='" + info.ParentId + "' value='" 
                          + info.Value + "' " + readOnly + " data-dfl='" + dfl + "' data-readonly='" + info.ReadOnly 
                          + "' data-empty-type='" + info.Empty + "' data-field-type='" + info.Type + "' onblur='checkDate(\"" 
                          + info.Id + "\")'>\n"
                          + "<span class='input-group-addon'>\n" +
                          "<span class='fa fa-calendar'></span>\n </span>\n"
                          + "</div>\n");
            }
            else if (info.Type.Equals(FieldType.Amount))
            {
                info.Value = info.Value == null && info.DefaultValue != null ? info.DefaultValue : info.Value;
                var currencies = Configuration.Parameters[ParameterId.TypeAmountCurrency].ToString().Split(',');
                sb.Append("<div class='input-group-btn'>\n");
                sb.Append("<button " + disabled + " id='currencyBtn_" + info.Id +
                          "' type='button' class='btn btn-primary dropdown-toggle' " +
                          "data-toggle='dropdown' aria-expanded='false'>" + currencies[0] + "\n");
                sb.Append("</button>\n");
                sb.Append("<ul class='dropdown-menu'>\n");
                foreach (var currency in currencies)
                {
                    sb.Append("<li><a onclick='changeCurrency(\"" + info.Id + "\", \"" + currency + "\", \"" +
                              currencies[0] + "\")' style='cursor: pointer;'>"
                              + currency + "</a></li>\n");
                }
                sb.Append("</ul>\n");
                sb.Append("</div>\n");
                sb.Append("<input type ='text' class='" + formControl + " digitOnly' data-amount='true' id='comp_" + info.Id
                          + "' title=\"" + info.Description + "\" data-empty-type='" + info.Empty + "' " + tooltip + " " + readOnly +
                          CreateTagName(info.TagName, info.Index, info.ValueProperty) + " data-readonly='" + info.ReadOnly + "'"
                          + " value=\"" + info.Value + "\" data-technical-name='" + info.Name + "' data-value-id='" + info.ValueId +
                          "' style='" + info.Css + "' data-parent-id='" + info.ParentId + "' data-dfl='" + dfl + "' " +
                          " data-old-value=\"" + info.Value + "\" data-field-type='" + info.Type + "'>\n");
            }
            else
            {
                var regexp = info.ValidationRegexp != null ? "data-regexp=\"" + info.ValidationRegexp + "\"" : "";
                var check = !regexp.IsEmpty() ? "validateRegExp(this);" : "";
                check += info.IdxId != null ? "checkIndex(this, " + info.IdxId + ");" : "";

                info.Value = info.Value == null && info.DefaultValue != null ? info.DefaultValue : info.Value;
                if (info.TextArea && info.Type.Equals(FieldType.String))
                {
                    sb.Append("<textarea class='" + formControl + "' rows='4' data-empty-type='" + info.Empty 
                        + "' id='comp_" + info.Id + "' title=\"" + info.Description + "\" " + tooltip + " " + readOnly +
                              CreateTagName(info.TagName, info.Index, info.ValueProperty) + " " + regexp
                              + " data-technical-name='" + info.Name + "' data-value-id='" + info.ValueId +
                              "' style='" + info.Css + "' data-parent-id='" + info.ParentId + "' data-dfl='" + dfl + "' " +
                              " onblur='" + check + "' data-old-value=\"" + info.Value + "\" data-readonly='" + info.ReadOnly 
                              + "' data-field-type='" + info.Type + "' maxlength='1000'>");
                    sb.Append(info.Value);
                    sb.Append("</textarea>\n");
                }
                else
                {
                    var checkIndex = info.Value != null && info.IdxId != null ? "data-check-index='0'" : "";
                    var digitOnly = info.Type.Equals(FieldType.Integer) ? "digitOnly" : "";
                    info.Value = info.Value == null && info.DefaultValue != null ? info.DefaultValue : info.Value;
                    var mask = info.Mask != null ? "data-inputmask='\"mask\": \"" + info.Mask + "\"' data-mask" : "";
                    sb.Append("<input type ='text' class='" + formControl + " " + digitOnly + "' id='comp_" + info.Id
                              + "' title=\"" + info.Description + "\" " + tooltip + " " + readOnly +
                              CreateTagName(info.TagName, info.Index, info.ValueProperty) + " data-empty-type='" + info.Empty + "' " + regexp
                              + " value=\"" + info.Value + "\" data-technical-name='" + info.Name + "' data-value-id='" + info.ValueId +
                              "' " + mask + " style='" + info.Css + "' data-parent-id='" + info.ParentId + "' data-dfl='" 
                              + dfl + "' data-old-value=\"" + info.Value + "\" onblur='" + check + "' data-readonly='" + info.ReadOnly + "'" +
                              " data-index-id='" + info.IdxId + "' " + checkIndex + " data-field-type='" + info.Type + "'>\n");
                }
            }
            if (info.Type.Equals(FieldType.Doc))
            {
                sb.Append("<span class='input-group-btn'>\n");
                sb.Append("<button onclick='downloadFile(\"" + info.Id + "\");' id='download_" + info.Id
                    + "' class='btn btn-default' type='button'><i class='fa fa-file-word-o'></i> Visualiser</button>\n");
                if (info.ReadOnly != null && !(bool)info.ReadOnly)
                {
                    sb.Append("<button class='btn btn-danger' type='button' id='deleteFile_" + info.Id +
                              "' onclick='deleteFile(\"" + info.Id + "\");'><i class='fa fa-trash-o'></i></button>\n");
                    sb.Append("<button onclick='chooseFile(\"chooseDoc_" + info.Id + "\");' class='btn btn-primary' " +
                          "type='button'>...</button>\n");
                }
                sb.Append("</span>\n");
            }
            if (info.Type.Equals(FieldType.Pdf))
            {
                sb.Append("<span class='input-group-btn'>\n");
                sb.Append("<button onclick='previewFile(\"" + info.Id + "\");' id='preview_" + info.Id
                    + "' class='btn btn-default' type='button'>\n");
                sb.Append("<i class='fa fa-file-pdf-o'></i> Visualiser</button>\n");
                if (info.ReadOnly != null && !(bool) info.ReadOnly)
                {
                    sb.Append("<button class='btn btn-danger' type='button' id='deleteFile_" + id +
                              "' onclick='deleteFile(\"" + id + "\");'><i class='fa fa-trash-o'></i></button>\n");
                    //var favori = ParameterService.GetParameter(ParameterId.ScannerId, helper?.ViewBag?.Username);
                    Parameter favori = null;
                    var btnScannerAction = "chooseScanner(\"" + id + "\", \"" + favori?.Value + "\")";
                    sb.Append("<button id='scan_" + id + "' class='btn btn-info scanBtn' onclick='" + btnScannerAction
                        + "' type='button' " + disabled + ">\n<i class='fa fa-copy'></i> Numériser</button>\n");
                    sb.Append("<button class='btn btn-info dropdown-toggle' type='button' " + disabled 
                        + " data-toggle='dropdown'aria-expanded='false'>\n" +
                        "<span class='caret'></span></button>\n");
                    var scanWay = ParameterService.GetParameter(UserParameterId.ScanningWay, username);
                    var checkScanWay = scanWay != null && int.Parse(scanWay.Value) == 0 ? "checked" : "";
                    sb.Append("<ul class='dropdown-menu dropdown-menu-right'>\n");
                    sb.Append("<li>\n");
                    sb.Append("<a class='small'><input style='cursor: pointer;' type='checkbox' class='scanWay' onchange='changeSanWay();' " + checkScanWay + "/> Chargeur documents</a>\n");
                    sb.Append("</li>\n");
                    sb.Append("</ul>\n");
                    //var btnCss = favori == null ? "style='display: none;'" : string.Empty;
                    //sb.Append("<button id='changeScanner_" + id + "' type='button' onclick='chooseScanner(\"" + id
                    //        + "\", \"" + favori?.Value + "\")' " + disabled + " class='btn btn-info changeScannerBtn' " + btnCss 
                    //        + "><i class='fa fa-th-large'></i></button>\n");
                }
                var choosePdfFile = bool.Parse(Configuration.Parameters[ParameterId.ScanAddFile].ToString());
                if (choosePdfFile && info.ReadOnly != null && !(bool)info.ReadOnly)
                {
                    sb.Append("<input accept='.pdf' type='file' data-technical-name='" + info.Name + "' id='choosePdf_" 
                        + info.Id + "' style='display:none;'/>");
                    sb.Append("<button type='button' class='btn btn-primary' onclick='chooseFile(\"choosePdf_" + info.Id + "\")'>\n");
                    sb.Append("<span >...</span>\n");
                    sb.Append("</button>\n");
                }
                sb.Append("</span>\n");
            }
            if (info.MultiValued != null && (bool) info.MultiValued)
            {
                var icon = !copy ? "plus" : "minus";
                var cssClass = !copy ? "success" : "danger";
                var onclick = !copy
                    ? "onclick=\"addField('" + info.ParentId + "', '" + info.ParentProperty + "');\""
                    : "onclick='removeField();'";
                sb.Append("<span class='input-group-btn'>\n");
                sb.Append("<button id='add_" + info.Id + "' class='btn btn-" + cssClass +
                          "' type='button' " + onclick + " " + disabled + ">\n");
                sb.Append("<i class='fa fa-" + icon + "'></i></button>\n");
                sb.Append("</span>\n");
            }
            sb.Append(multivalued.IsEmpty() ? "" : "</div>\n"); //fin input-group
            sb.Append("</div>\n"); //fin creation colonne pour composant
            sb.Append("</div>\n"); //fin du form-group
            //Ajout des infos dans la liste pour la creation de javascript
            if (copy || (info.MultiValued != null && (bool)info.MultiValued))
            {
                sb.Append(PageJavascript(info));
            }
            if (!copy)
            {
                helper.ViewBag.ComponentInfos?.Add(info);
            }
            return MvcHtmlString.Create(sb.ToString());
        }

        public static string GetValue(List<DflValue> dflValues, int? dfl)
        {
            return (from dflValue in dflValues where dflValue.DflId == dfl select dflValue.Value).FirstOrDefault();
        }

        public static string CreateComponent(ComponentInfo info, bool dialog, bool copy, string username)
        {
            return CreateComponent(null, info, dialog, copy, username).ToHtmlString();
        }

        private static ComponentInfo CreateInfo(VwCurrentValue currentValue, VwCurrentVariableValue currentVariable)
        {
            var field = currentValue != null ? FormFieldService.GetFormField(currentValue.DflId, 0)
                : FormFieldService.GetFormField(0, currentVariable.WvaId);
            string value;
            if (currentValue != null)
            {
                value = currentValue.Type.Equals(FieldType.Code) ? currentValue.CodeId?.ToString() : currentValue.Value;
            }
            else
            {
                value = currentVariable.WvaType.Equals(FieldType.Code) ? currentVariable.DciId?.ToString() : currentVariable.Value;
            }
            if (field != null)
            {
                return new ComponentInfo
                {
                    Id = field.WafId.ToString(),
                    DisplayName = field.DisplayName,
                    Type = field.Type,
                    CodeDcdId = field.CodeDcdId,
                    CodeInlineDcdId = field.CodeInlineDcdId,
                    Description = field.Description,
                    Empty = field.Empty,
                    ParentId = field.DflId ?? field.WvaId,
                    ReferenceId = field.ReferenceDicId,
                    TagName = field.DflId != null ? "Values" : "Variables",
                    ParentProperty = field.DflId != null ? "DflId" : "WvaId",
                    ValueProperty = field.DflId != null ? "ValueContent" : "Value",
                    Visibility = field.Visibility,
                    DicName = field.DicName,
                    Name = field.TechnicalName,
                    DefaultValue = field.DefaultValue,
                    TextArea = field.TextArea,
                    Css = field.Css,
                    ValidationRegexp = field.ValidationRegexp,
                    ReadOnly = field.ReadOnly,
                    SearchCriteria = field.SearchCriteria,
                    Group = field.Group,
                    Order = field.Order,
                    DicColor = field.DicColor,
                    DicShareable = field.DicShareable,
                    Mask = field.Mask,
                    ConditionalDisplayName = field.ConditionalDisplayName,
                    ValueId = currentValue?.ValId ?? currentVariable.FvvId,
                    Value = value
                };
            }
            return null;
        }

        public static MvcHtmlString ValidateValue(this HtmlHelper helper, VwCurrentValue currentValue, VwCurrentVariableValue currentVariable)
        {
            var info = CreateInfo(currentValue, currentVariable);

            var sb = new StringBuilder();
            if (info == null) return MvcHtmlString.Create(sb.ToString());
            sb.Append("<div class='form-group'>\n"); //debut du form-group
            sb.Append("<label id='label_" + info.Id + "' for='comp_" + info.Id + "' "
                      + "class='col-sm-3 control-label'>\n" + info.DisplayName + "</label>\n"); //label du composant
            //debut de creation de la colonne pour le composant ::col-sm-9::
            sb.Append("<div id='value_" + info.Id + "' class='col-sm-9'>\n");
            string value;
            if (info.Type.Equals(FieldType.Reference))
            {
                value = EntityService.GetEntity(int.Parse(info.Value)).Name;
            }
            else if (info.Type.Equals(FieldType.Date))
            {
                value = ConvertDate.UserFormat(info.Value);
            }
            else if (info.Type.Equals(FieldType.Boolean))
            {
                value = bool.Parse(info.Value) ? "Oui" : "Non";
            }
            else if (info.Type.Equals(FieldType.Amount))
            {
                var defalutCurrency = Configuration.Parameters[ParameterId.TypeAmountCurrency].ToString().Split(',');
                if (info.Value.Contains(":"))
                {
                    var currency = info.Value.Split(':');
                    value = currency[0] + " " + defalutCurrency[0] + " => <b>" + currency[2] + " " + currency[1]
                            + "</b> au " + ConvertDate.UserFormat(currency[3]);
                }
                else
                {
                    value = info.Value + " " + defalutCurrency[0];
                }
            }
            else if (info.Type.Equals(FieldType.Code))
            {
                value = currentValue?.Value ?? currentVariable?.Value;
            }
            else if (info.Type.Equals(FieldType.Pdf) || info.Type.Equals(FieldType.Doc))
            {
                value = info.Value.Split(';')[0];
            }
            else
            {
                value = info.Value;
            }
            sb.Append("<div id='comp_" + info.Id + "' class='inputValue'>" + value + "\n"); //div showing value
            if (info.Type.Equals(FieldType.Pdf) || info.Type.Equals(FieldType.Doc))
            {
                var text = info.Type.Equals(FieldType.Doc) ? "download" : "preview";
                sb.Append("<a onclick='" + text + "File(\"" + info.Id + "\");' id='" + text + "_" + info.Id + "'> Visualiser</a>\n");
            }
            sb.Append("</div>\n"); // end div show value

            sb.Append("</div>\n"); //fin creation colonne pour composant
            sb.Append("</div>\n"); //fin form-group
            helper.ViewBag.ComponentInfos?.Add(info);
            return MvcHtmlString.Create(sb.ToString());
        }

        private static int GetDependentParent(int? dcdId)
        {
            if (dcdId == null) return 0;
            var item = ItemService.GetCodeItems((int) dcdId).FirstOrDefault();
            return item?.DependDciId != null ? ItemService.GetDicCodeItem((int)item.DependDciId).DcdId : 0;
        }
        
        private static string VisibilityJs(ComponentInfo info, string json, bool validation)
        {
            var sb = new StringBuilder();
            if (info.Visibility != null && !validation)
            {
                var value = info.Visibility.Replace("ENT", json).Replace("FLW", json);
                var tab = value.Split(' ');
                foreach (var t in tab)
                {
                    var s = t;
                    if (s.Contains(json) && !s.Contains("="))
                    {
                        s = s.Replace("(", "").Replace(")", "");
                        s = s.Contains("!") ? s.Replace("!", "") + "==='false'" : s + "==='true'";
                        value = value.Replace(t, s);
                    }
                }
                
                sb.Append("if (" + value + ") {\n");
                sb.Append(" showComponent('" + info.Id + "');\n");
                sb.Append("} else {\n");
                sb.Append(" hideComponent('" + info.Id + "');\n");
                sb.Append("}\n");
            }
            if (info.ConditionalDisplayName == null) return sb.ToString();
            {
                var value = info.ConditionalDisplayName.Replace("ENT", json).Replace("FLW", json);
                sb.Append("labelText = " + value + ";\n");
                sb.Append("changeLabel('" + info.Id + "', labelText, '" + info.Empty + "', '" + validation + "');\n");
            }
            return sb.ToString();
        }

        private static string ComponentJavascript(ComponentInfo info)
        {
            var sb = new StringBuilder();
            if (info.Type.Equals(FieldType.Reference) && info.ReferenceId != null)
            {
                if (info.Value != null)
                {
                    var entityName = EntityService.GetEntity(int.Parse(info.Value)).Name;
                    sb.Append("chooseEntity(\"" + info.Value + "\", \"" + info.ReferenceId + "\", \"" + info.Id + "\", \"" + entityName + "\", false);\n");
                }
                else
                {
                    sb.Append("$('#edit_" + info.Id + "').hide();\n");
                    sb.Append("$('#delete_" + info.Id + "').hide();\n");
                }
            }
            if (info.Type.Equals(FieldType.Amount) && info.Value != null && info.Value.Contains(":"))
            {
                var currency = info.Value.Split(':');
                var currencies = Configuration.Parameters[ParameterId.TypeAmountCurrency].ToString().Split(',');
                sb.Append("changeCurrencyWithValues(\"" + info.Id + "\", \"" + currency[1] + "\", \"" + currencies[0] + "\");\n");
            }
            if (info.Type.Equals(FieldType.Date))
            {
                sb.Append("$('#comp_" + info.Id + "').inputmask('dd/mm/yyyy', {'placeholder': 'jj/mm/aaaa'});\n");
            }
            if (info.Type.Equals(FieldType.Code) && info.Value == null && info.DefaultValue == null)
            {
                sb.Append("$('#comp_" + info.Id + "').prop('selectedIndex', -1);\n");
                if (info.CodeInlineDcdId == null)
                {
                    sb.Append("loadDependentItems('" + info.Id + "');\n");
                }
                else
                {
                    sb.Append("loadDependentCodeInlineItems('" + info.Id + "');\n");
                }
            }
            else if (info.Type.Equals(FieldType.Code) && info.Value != null && info.CodeInlineDcdId != null 
                && info.MultiValued != null && !(bool)info.MultiValued)
            {
                var dependentDciId = ItemService.GetDependentId((int)info.CodeInlineDcdId, int.Parse(info.Value));
                var dependencies = ItemService.GetParentCodeInline(int.Parse(info.Value), dependentDciId);
                if (dependencies.Length > 0)
                {
                    dependencies = dependencies.Substring(0, dependencies.Length - 1);
                }
                var codeItems = new JavaScriptSerializer().Serialize(ItemService.GetCodeInlineItems(dependencies));
                sb.Append("fillCodeInlinesComponent('" + info.Id + "','" + dependencies + "','" + codeItems + "');\n");
            }

            if (info.Type.Equals(FieldType.Doc))
            {
                sb.Append("$('#chooseDoc_" + info.Id + "').change(function(e){\n");
                sb.Append("docFileChoosen('" + info.Id + "', e);\n");
                sb.Append("});\n");
                if (info.Value != null)
                {
                    sb.Append("$('#download_" + info.Id + "').show();\n");
                    sb.Append("$('#deleteFile_" + info.Id + "').show();\n");
                }
                else
                {
                    sb.Append("$('#download_" + info.Id + "').hide();\n");
                    sb.Append("$('#deleteFile_" + info.Id + "').hide();\n");
                }
            }
            if (!info.Type.Equals(FieldType.Pdf)) return sb.ToString();
            var choosePdfFile = bool.Parse(Configuration.Parameters[ParameterId.ScanAddFile].ToString());
            if (choosePdfFile)
            {
                sb.Append("$('#choosePdf_" + info.Id + "').change(function(e){\n");
                sb.Append("pdfFileChoosen('" + info.Id + "', e);\n");
                sb.Append("});\n");
            }
            if (info.Value != null)
            {
                sb.Append("$('#preview_" + info.Id + "').show();\n");
                sb.Append("$('#deleteFile_" + info.Id + "').show();\n");
            }
            else
            {
                sb.Append("$('#preview_" + info.Id + "').hide();\n");
                sb.Append("$('#deleteFile_" + info.Id + "').hide();\n");
            }
            return sb.ToString();
        }
        
        private static string RequiredStar(string empty)
        {
            if (empty.Equals(EmptyType.Error))
            {
                return "<span style='color: red;'>*</span>";
            }
            return empty.Equals(EmptyType.Warning) ? "<span style='color: #00ACD6;'>~</span>" : "";
        }

        private static string CreateTagName(string tag, int index, string property)
        {
            if (tag != null)
            {
                return "name='" + tag + "[" + index + "]." + property + "'";
            }
            return "";
        }
        
        private static string JsonGenerator(ComponentInfo info)
        {
            var value = info.Value == null && info.DefaultValue != null ? info.DefaultValue : info.Value;
            string jsonProperty;
            if (info.Type.Equals(FieldType.Reference) && info.ReferenceId != null)
            {
                var fields = FieldService.GetDicFields((int)info.ReferenceId);
                var result = "";
                foreach (var field in fields)
                {
                    var currentValue = info.Value != null ? CurrentValueService.GetCurrentValue(int.Parse(info.Value), field.Id) : null;
                    var val = info.Value != null ? currentValue?.Value : null;
                    val = val == null && field.DefaultValue != null ? field.DefaultValue : val;
                    if (field.Type.Equals(FieldType.Code))
                    {
                        val = currentValue != null ? currentValue.CodeId?.ToString() : val;
                        val = val != null ? ItemService.GetDicCodeItem(int.Parse(val)).Code : null;
                        result += "\"" + field.Name + "\":{\"code\":\"" + val + "\"},";
                    }
                    else
                    {
                        result += "\"" + field.Name + "\":\"" + val + "\",";
                    }
                }
                if (result.Length > 0)
                {
                    result = result.Substring(0, result.Length - 1);
                }
                jsonProperty = "{" + result + "}";
            }
            else if (info.Type.Equals(FieldType.Code))
            {
                value = value != null ? ItemService.GetDicCodeItem(int.Parse(value)).Code : "";
                jsonProperty = "{\"code\":\"" + value + "\"}";
            }
            else
            {
                jsonProperty = "\"" + value + "\"";
            }
            return jsonProperty;
        }
        
        public static MvcHtmlString PageJavascript(List<ComponentInfo> infos, string pageJson, 
            string componentVisibilityMethod, bool validation, int entityId)
        {
            var sb = new StringBuilder();
            sb.Append("<script type='text/javascript'>\n");
            sb.Append("var " + pageJson + ";\n");
            sb.Append("function " + componentVisibilityMethod + "{\n");
            //sb.Append("var obj;\n");
            sb.Append("var labelText;\n");
            foreach (var info in infos)
            {
                sb.Append(VisibilityJs(info, pageJson, validation));
            }
            sb.Append("}\n");
            sb.Append("$(document).ready(function() {\n");
            sb.Append("$('[data-mask]').inputmask();\n");
            sb.Append("$('[data-toggle=\"tooltip\"]').tooltip();\n");
            sb.Append("digitsOnly();\n");
            string[] json = {""};
            foreach (var info in infos)
            {
                if (!validation)
                {
                    sb.Append(ComponentJavascript(info));
                }
                json[0] += "\"" + info.Name + "\":" + JsonGenerator(info) + ",";
            }
            if (entityId != 0)
            {
                var currentValues = CurrentValueService.GetCurrentValues(entityId);
                infos = currentValues.Select(currentValue => new ComponentInfo
                {
                    Type = currentValue.Type,
                    Value =
                        currentValue.Type.Equals(FieldType.Code) ? currentValue.CodeId.ToString() : currentValue.Value,
                    ReferenceId = currentValue.ReferenceDicId,
                    DefaultValue = FieldService.GetDicField(currentValue.DflId).DefaultValue,
                    Name = currentValue.FieldName
                }).ToList();
                foreach (var info in infos.Where(info => !json[0].Contains(info.Name)))
                {
                    json[0] += "\"" + info.Name + "\":" + JsonGenerator(info) + ",";
                }
            }
            if (json[0].Length > 0)
            {
                json[0] = json[0].Substring(0, json[0].Length - 1);
            }
            
            sb.Append(pageJson + " = {" + json[0] + "};\n");
            sb.Append(componentVisibilityMethod + ";\n");
            if (!validation)
            {
                sb.Append("boxVisibility();\n");
                sb.Append("loadDefaultData();\n");
            }
            sb.Append("});\n");
            sb.Append("</script>\n");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static string PageJavascript(ComponentInfo info)
        {
            var sb = new StringBuilder();
            sb.Append("<script type='text/javascript'>\n");
            sb.Append("$(document).ready(function() {\n");
            if (info.Type.Equals(FieldType.Code) && info.Value != null && info.CodeInlineDcdId != null
                && info.MultiValued != null && (bool) info.MultiValued)
            {
                var dependentDciId = ItemService.GetDependentId((int)info.CodeInlineDcdId, int.Parse(info.Value));
                var dependencies = ItemService.GetParentCodeInline(int.Parse(info.Value), dependentDciId);
                if (dependencies.Length > 0)
                {
                    dependencies = dependencies.Substring(0, dependencies.Length - 1);
                }
                var codeItems = new JavaScriptSerializer().Serialize(ItemService.GetCodeInlineItems(dependencies));
                sb.Append("fillCodeInlinesComponent('" + info.Id + "','" + dependencies + "','" + codeItems + "');\n");
            }
            else
            {
                sb.Append(ComponentJavascript(info));
            }
            sb.Append("});\n");
            sb.Append("</script>\n");
            return sb.ToString();
        }
        
    }
}