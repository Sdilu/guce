﻿/*=================================================================*
 * Classe: <PagingHelper>
 * Version/date: <2.0.0> <2016.07.09>
 *
 * Description: <” Cette classe permet de créer un composant html pour la pagination. ”>
 * Specificities: <“ Cette classe est un HtmlHelper. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Text;
using System.Web.Mvc;
using Guce.MvcWebUI.Models.Custom;
using static System.Math;

namespace Guce.MvcWebUI.HtmlHelpers
{
    public static class PagingHelper
    {
        public static MvcHtmlString Pager(this HtmlHelper helper, PagingInfo pagingInfo)
        {
            var sb = new StringBuilder();
            if (pagingInfo.TotalPageCount <= 1) return MvcHtmlString.Create(sb.ToString());

            var prevPage = pagingInfo.CurrentPage == 1 ? 1 : pagingInfo.CurrentPage - 1;
            var nextPage = pagingInfo.CurrentPage >= pagingInfo.TotalPageCount
                ? pagingInfo.TotalPageCount
                : pagingInfo.CurrentPage + 1;
            sb.Append("<ul class='pagination pagination-sm inline'>");
            if (pagingInfo.CurrentPage != 1)
            {
                sb.AppendFormat("<li><a href=\"{0}{1}\"><i class='fa fa-angle-double-left'></i></a></li>", pagingInfo.BaseUrl, 1);
                sb.AppendFormat("<li><a href=\"{0}{1}\"><i class='fa fa-angle-left'></i></a></li>", pagingInfo.BaseUrl, prevPage);
            }
            const int numberOfLink = 5;

            var uPage = Ceiling((decimal)pagingInfo.CurrentPage / numberOfLink) * numberOfLink;
            var lPage = Floor((decimal)pagingInfo.CurrentPage / numberOfLink) * numberOfLink;

            lPage = lPage == 0 ? 1 : lPage;
            uPage = lPage == uPage ? uPage + numberOfLink : uPage;

            if (uPage > pagingInfo.TotalPageCount)
            {
                uPage = pagingInfo.TotalPageCount - 1;
            }

            for (var i = lPage; i <= uPage; i++)
            {
                sb.AppendFormat("<li class='{2}'><a  href='{1}{0}'>{0}</a></li>", i, pagingInfo.BaseUrl,
                    pagingInfo.CurrentPage == i ? "active" : string.Empty);
            }

            if (pagingInfo.CurrentPage != pagingInfo.TotalPageCount)
            {
                sb.AppendFormat("<li><a href=\"{0}{1}\"><i class='fa fa-angle-right'></i></a></li>", pagingInfo.BaseUrl, nextPage);
                sb.AppendFormat("<li><a href=\"{0}{1}\"><i class='fa fa-angle-double-right'></i></a></li>", pagingInfo.BaseUrl, pagingInfo.TotalPageCount);
            }
            
            if (pagingInfo.CurrentPage == pagingInfo.TotalPageCount)
            {
                sb.AppendFormat("<li class='active'><a href=\"{0}{1}\">{1}</a></li>", pagingInfo.BaseUrl, pagingInfo.CurrentPage);
            }
            sb.Append("</ul>");
            return MvcHtmlString.Create(sb.ToString());
        }
    }
}