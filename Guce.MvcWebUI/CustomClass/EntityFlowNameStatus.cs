﻿namespace Guce.MvcWebUI.CustomClass
{
    public class EntityFlowNameStatus
    {
        public int Entity { get; set; }
        public int Flow { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
    }
}