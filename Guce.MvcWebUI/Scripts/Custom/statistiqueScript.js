﻿
function showStatistiques() {
    var box = startLoader();
    $("#exportCsvBtn").addClass("disabled");
    $("#statResult").html("");
    var typeStat = $("#typeStat").val();
    if (typeStat !== "" && typeStat !== null) {
        $.ajax({
            type: "POST",
            url: executeStatUrl,
            data: {
                statId: typeStat
            },
            success: function (response) {
                var tab = response.split("=||=");
                var colunms = $.parseJSON(tab[0]);
                var result = "<table id='statPageTable' class='table table-hover table-striped'>";
                result += "<thead><tr>";
                $.each(colunms, function (i, column) {
                    result += "<th>" + column + "</th>";
                });
                result += "</tr></thead>";
                var values = $.parseJSON(tab[1]);
                result += "<tbody>";
                var k = 0;
                $.each(values, function (i, value) {
                    if (k === 0) {
                        result += "<tr>";
                    }
                    result += "<td>" + value + "</td>";
                    if (k === colunms.length - 1) {
                        result += "</tr>";
                    }
                    if (k === colunms.length) {
                        k = 0;
                    }
                    k++;
                });
                result += "</tbody>";
                result += "</table>";
                $("#statResult").html(result);
                if (values.length > 0) {
                    $("#exportCsvBtn").removeClass("disabled");
                    $("#statPageTable").DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": false,
                        "autoWidth": false,
                        "pageLength": 6,
                        "language": {
                            "url": dataTableFrenchJson
                        }
                    });
                }
                stopLoader(box);
            },
            error: function (response) {
                showError(box, response);
            }
        });
    } else {
        stopLoader(box);
        messageAlert("Veuillez choisir une statistique");
    }
}

function exportCsv() {
    $("table").tableToCSV();
}