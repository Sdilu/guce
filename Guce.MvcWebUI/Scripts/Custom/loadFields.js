﻿function load(fieldName,selectedElement) {
    var t = $(this);
    $.ajax({
        type: "POST",
        url: "/Field/GetData",
        data: {
            fieldName: fieldName, selectedElement: selectedElement
        },
        success: function (response) {
            var json = $.parseJSON(response); // create an object with the key of the array
            var associated = findField(fieldName);
            $.each(json, function (i) {
                var mySelect = "<option id='\" " + json[i].Code + "' value='" + json[i].Id + "'>" + json[i].Value + "</option>";
                alert(mySelect);
            });
        },  
        error: function () {
            messageAlert("Erreur pendant l'enregistrement ...");
        }
    });
}

function findField(fieldName) {

    
    var result = "";
    $.ajax({
        type: "POST",
        url: "/Field/FindField",
        data: {
            fieldName: fieldName
        },
        success: function (response) {
            result = response;
        },
        error: function () {
            messageAlert("Erreur pendant l'enregistrement ...");
        }
    });
    return result;
}

function getValue() {
    var value = event.target;
    var result;
    $(value).children().each(function () {
        if ($(this).is(":selected")) {
            result = $(this).text();
        }
    });
   
    return result;
}

function fff(dic_id) {
    $.ajax({
        type: "POST",
        url: "/Search/FindTypeSearch",
        data: {
            dic_id: dic_id
        },
        success: function (response) {
            result = response;
        },
        error: function () {
            messageAlert("Erreur pendant l'enregistrement ...");
        }
    });
}

/*
 Revoie la valeur actuelle d'un champ a partir de son nom générique
 
  Parametre :
    dictionnaire -> Le conteneur qui contient la liste des champs
    genericNameToFind -> le nom générique du champs
*/
function getCurrentValueFromChamp(dictionnaire, genericNameToFind) {
    var value = "null"; //La valeur a retourner
    var childrenDictionnaire = $(dictionnaire).children();

    childrenDictionnaire.each(function () {
        var divTag = $(this).children();
        var elementTag = $(divTag[1]).children();

        // Recuperation du nom générique
        var detailTag = $(elementTag[0]);
        var childrenDetailTag = $(detailTag).children();
        var genericNameFinded = $(childrenDetailTag[2]).text();

        // Compare les élémentstrouvé
        if (genericNameFinded.substring(6, genericNameFinded.length) == genericNameToFind) {
            if ($(elementTag[1]).is("input")) {
                value = $(elementTag[1]).text();
            } else if ($(elementTag[1]).is("select")) {
                var selectElement = $(elementTag[1]).children();
                $(selectElement).each(function () {
                    if ($(this).is(":selected")) {
                        value = $(this).attr("id");
                    }
                });
            }
        }

    });

    return value;
}
