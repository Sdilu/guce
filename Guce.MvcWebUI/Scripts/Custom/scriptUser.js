﻿/*=================================================================*
* Classe: <Operateurs>
* Version/date: <2.0.0> <2016.08.23>
*
* Description: <” Ce fichier contient les scripts nécessaire au fonctionnnement du module de recherche ”>
* Specificities: <“ - Elle ermet de gérer l'affichage des panels, 
                    - de pouvoir rechercher dans les résultats un mot spécifique ...
                       ”>
*
* Authors:  <Steve> <DILU>
* Copyright: GUCE/ESSOR, all rights reserved
*
*================================================================*/

/*
 Masque ou affiche les Panels qui contient les détails du choix de l'utilisateur apres séléction du résultats de recherche. 

 Parametre : 
    obj -> la div principale qui contient le champ. Elle a la classe "form-group"
*/
function collapsePanel() {
    if ($("#main1").is(":visible")) {
        $("#main1").hide();
        var currentElement = event.target;

        var firstTD = $(currentElement).closest("td");
        var acteurTD = $(firstTD).prev();
        var dateTD = $(acteurTD).prev();
        var nomTD = $(dateTD).prev();

        $("#titre").text($(nomTD).text());
        $("#detailTabDiv").show();
        $("#folderTabDiv").show();
        $("#AllContent").show();
        var ee = $(firstTD).next();
        var selectedID = $(ee).text();
        getCurrentValueForSearchFolder(selectedID);
    } else {
        $("#main1").show();
        $("#titre").text("Résultats recherche");
        $("#detailTabDiv").hide();
        $("#detailsContent").html("");

        $("#folderTabDiv").hide();
        $("#folderContent").html("");

        $("#fluxTabDiv").hide();
        $("#fluxContent").html("");

        $("#AllContent").hide();
        $("#AllContent").html("");

        $("#titreDetail").text("");
    }
}

/*
 Recherche les détails pour une entité donné  

 Parametre : 
    entitiId -> L'ID de l'entité dans la base de donnée
*/
function getCurrentValueForSearchFolder(entitiId) {
    $.ajax({
        type: "POST",
        url: findCurrentValuetUrl,
        data: { entitiId: entitiId },
        success: function (data) {
            var json1 = $.parseJSON(data);
            var json = $.parseJSON(json1);
            var resultData = "";
            
            
            if (!$.isEmptyObject(json)) {
               
                //resultData += "<thead><tr><th>Libélé</th><th>Valeur</th></tr></thead>";
                resultData += "<tr><td style='font-weight:bold;'>Dénomination sociale </td><td>" + json.nomCommercial + "</td></tr>";
                resultData += "<tr><td style='font-weight:bold;'>Numéro RCCM </td><td><span class='label label-info' style='font-size: 13px;'>"
                    + json.rccm + "</span></td></tr>";
                var tabDate = json.dateImmatriculationRCCM.split("-");
                var dateRccm = tabDate[2] + "/" + tabDate[1] + "/" + tabDate[0];
                resultData += "<tr><td style='font-weight:bold;'>Date immatriculation RCCM </td><td>" + dateRccm + "</td></tr>";
                
                resultData += "<tr><td style='font-weight:bold;'>Forme juridique </td><td>" + json.formeJuridiqueN3.value + "</td></tr>";
                resultData += "<tr><td style='font-weight:bold;'>Adresse siège sociale </td><td>" + json.adresseSiegeSocial._name + "</td></tr>";

                var creationTime = $.format.date(new Date(json._creation_time), "ddd, d MMMM yyyy HH:mm:ss");
                var validationTime = $.format.date(new Date(json.dateImmatriculationRCCM), "ddd, d MMMM yyyy HH:mm:ss");

                resultData += "<tr><td style='font-weight:bold;'>Date création </td><td>" + creationTime + "</td></tr>";
                resultData += "<tr><td style='font-weight:bold;'>Date validation </td><td>" + validationTime + "</td></tr>";
            }
            $("#detailsContent").html(resultData);

            if (resultData != "") {
                
            
            $.ajax({
                type: "POST",
                url: FindDossierEnCoursUrl,
                data: { EntId: entitiId },
                success: function (data) {
                    var json1 = $.parseJSON(data);
                    var resultData = "";
                    resultData += "<thead><tr><th> Date création</th><th>Procédure</th><th>Numéro dossier</th>" +
                        "<th style='width: 322px;'><i class='fa fa-gears'></i> Action</th>" +
                        "<th style='display: none;'>id</th></tr></thead>";
                    if (!$.isEmptyObject(json1)) {
                        $.each(json1, function (i, obj) {
                            var creationTime = $.format.date(new Date(parseInt(obj.CreationTime.substr(6, 13))), "ddd, d MMMM yyyy HH:mm:ss");
                            resultData += "<tr><td>" + creationTime + "</td><td>" + obj.Procedure + "</td><td style='width: 125px;'>" +
                                "<span class='label label-danger' style='font-size: 12px;'>" +
                                obj.Reference + "</span></td><td><button type='submit' class='btn btn-warning' onclick='getFluxTraiement(" +
                                obj.Id + ")' style='margin-right: 5px;'><i class='fa fa-server'></i> Flux de traitement</button><button type='submit'" +
                                " class='btn btn-success' onclick='getAllValue(" + entitiId +
                                ")' style='margin-right: 5px;'><i class='fa fa-info'></i> Plus de détails</button></td><td style='display: none;'>" +
                                obj.Id + "</td></tr>";
                        });
                    }
                    $("#folderContent").html(resultData);
                },
                error: function () {
                    messageAlert("Erreur pendant la recherche ...");
                }
            });
            }
        },
        error: function () {
            messageAlert("Erreur pendant la recherche ...");
        }
    });
}

/*
 Recherche la liste des flux pour une entité donné  

 Parametre : 
    flwId -> L'ID du flux dans la base de donnée
*/
function getFluxTraiement(flwId) {
    $.ajax({
        type: "POST",
        url: searchFluxTraitementUrl,
        data: { flwId: flwId },
        success: function (data) {
            var json1 = $.parseJSON(data);
            var resultData = "";
            resultData += "<thead><tr><th>Date traitement</th><th>Action</th><th>Auteur</th></tr></thead>";
            if (!$.isEmptyObject(json1)) {
                $.each(json1, function (i, obj) {
                    var creationTime = $.format.date(new Date(parseInt(obj.CreationTime.substr(6, 13))), "ddd, d MMMM yyyy HH:mm:ss");
                    var ee = obj.CreationTime.substr(6, 13);
                    resultData += "<tr><td>" + creationTime + "</td><th>" + obj.WfAction.Name + "</th><td>" + obj.CreatedBy + "</td></tr>";
                });
            }

            $("#fluxContent").html(resultData);
            $("#fluxTabDiv").show();
            $("#AllTabDiv").hide();
        },
        error: function () {
            messageAlert("Erreur pendant la recherche ...");
        }
    });

}


function getAllValue(entitiId) {
    Recepisse_demande_immatriculation(entitiId);
    $("#AllTabDiv").show();
    $("#fluxTabDiv").hide();
    //$.ajax({
    //    type: "POST",
    //    url: findEntitieUrl,
    //    data: { entitiId: entitiId },
    //    success: function (data) {
    //        var json1 = $.parseJSON(data);
    //        var js = $.parseJSON(json1);

    //        var ee = GetInfoBaseHtml(js, []) + GetFormeJuridiqueHtml(js) + GetSecteurActivite(js);;
    //        var infoBase = "<div class=\"ml-1 table table-striped table-bordered\">" + ee + "</div>";

    //        // ajout des établissements secondaires
    //        var renderHtml1 = "<div style=\"width: 20px;\"></div><div>" + GetEtablissementSecondaireHtml(js) + "</div>";

    //// ajout des dirigeants
    //        var renderHtml2 = "<div style=\"width: 20px;\"></div><div>" + "<div>" + GetDirigeantsHtml(js) + "</div>";

    //// ajout des accociés
    //        var renderHtml3 = "<div style=\"width: 20px;\"></div><div>" + GetAssociesHtml(js);

    //// ajout des manddaitaires
    //        var renderHtml4 = "<div style=\"width: 20px;\"></div><div>" + GetMandataireHtml(js);

    ////Liste des documents scanné
    //var renderHtml5 = "";//GetListeDocumentScanneHtml(data);

    //var renderHtml6 = GetDocumentForAllNaturalPersonne(js);
    //var renderHtml7 = GetDocumentForEntrepriseHtml(js);

    //$("#AllContent").html(infoBase + renderHtml1 + renderHtml2 + renderHtml3 + renderHtml4 + renderHtml5 + renderHtml6 + renderHtml7);
    //        $("#AllTabDiv").show();
    //        $("#fluxTabDiv").hide();

    //    },
    //    error: function () {
    //        messageAlert("Erreur pendant la recherche ...");
    //    }
    //});
}

/*
 Recherche la liste de toutes les valeurs liée a une entité pour un flux  

 ************** PAS ENCOR TESTE **********************************

 Parametre : 
    flwId -> L'ID du flux dans la base de donnée
*/
function getAllValue1(entitiId) {
    $.ajax({
        type: "POST",
        url: findCurrentValuetUrl,
        data: { entitiId: entitiId },
        success: function (data) {
            var json1 = $.parseJSON(data);

            var js = $.parseJSON(json1);
            var resultData = "";
            resultData += "";
            if (!$.isEmptyObject(json1)) {
                $.each(js, function (key, value) {
                    //var creationTime = $.format.date(new Date(Date(obj.CreationTime)), "ddd, MMMM yyyy HH:mm:ss");
                    if (typeof value == 'object') {
                        var resultD = "<table  class='table table-striped table-bordered'>";
                        $.each(value, function (key1, value1) {
                            if ($.isNumeric(key1) == false) {
                                if (key1.substring(0, 1) !== "_") {
                                    resultD += "<thead><tr><th>" + key1 + "</th><th>" + value1 + "</th></tr></thead>";
                                }
                            }
                        });
                        resultD += "</table>";
                        resultData += "<tr><td>" + key + "</td><th>" + resultD + "</th></tr>";
                    } else {
                        if ($.isNumeric(key) == false) {
                            if (key.substring(0, 1) !== "_") {
                                resultData += "<tr><td>" + key + "</td><th>" + value + "</th></tr>";
                            }
                        }
                    }
                });
            }
            $("#AllContent").html(resultData);
            $("#AllTabDiv").show();
            $("#fluxTabDiv").hide();
        },
        error: function () {
            messageAlert("Erreur pendant la recherche ...");
        }
    });
}

var globalVar = null;

function LoadScan(id) {
    var box = startLoader();
    var dialog = new BootstrapDialog({
        title: "Numérisation ",
        draggable: true,
        size: BootstrapDialog.SIZE_WIDE,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: false,
        onhidden: function () {
            stopLoader(box);
        },
        buttons: [
            {
                label: "Numériser",
                cssClass: "btn-primary",
                icon: "fa fa-check",
                id: "numBtn",
                autospin: true,
                action: function (dialogRef) {
                    dialogRef.getModalBody().html("Veuillez patienter !!! <br/>Cela peut prendre jusqu'a <i style='color:red;'>25 secondes.</i>");
                    dialogRef.enableButtons(false);
                    dialogRef.setClosable(false);
                    globalVar = id;
                    $.ajax({
                        type: "POST",
                        url: "/Scan/Scan",
                        success: function (data) {
                            setTimeout(function () {
                                if (data == "") {
                                    dialog.setMessage("Erreur lors du scan");
                                } else if (data.match("^Erreur :")) {
                                    dialog.setMessage(data);
                                } else {
                                    var resultData;
                                    var data1 = data.substring(31, data.length);
                                    resultData = "<div style='width: 100%; height: 400px;'>";
                                    resultData += ("<embed class='pdfobject' src='" + data1 + "' type='application/pdf' style='overflow: auto; width: 100%; height: 100%;' internalinstanceid='50' title=''>");
                                    resultData += "</div>";
                                    dialog.setMessage(resultData);
                                    var btn = dialogRef.getButton("numBtn");
                                    //btn.text("Valider");
                                }
                            }, 20000);
                        },
                        error: function () {
                            messageAlert("Erreur pendant le scan ...");
                        }

                    });
                    dialogRef.enableButtons(true);
                    dialogRef.setClosable(true);

                }
            }, {
                label: "Fermer",
                cssClass: "btn-default",
                icon: "fa fa-times",
                action: function () {
                    $("#" + id).attr("value", globalVar);
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: function () { return "Clicker sur Numériser pour lancer l'opération de scan" }
    });
    dialog.open();
}
