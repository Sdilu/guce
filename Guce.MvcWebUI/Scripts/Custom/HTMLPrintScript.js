﻿
// ============================================== Nouvelle implementation des rapports ============================================
var inscriptionComplementaire = false;
var fieldPropertiesJson;
var excludedFields = ["dirigeant", "associe", "commissaireAuxComptes", "mandataireSociete", "formeJuridiqueN1", "siegeExploitation", "adressesEtablissementsSecondaires"];
var indentification = ["nomCommercial", "rccm", "dateImmatriculationRCCM", "enseigne", "sigle", "adresseSiegeSocial", "adresseSiegeExploitation", "dateDebutExploitation"];
var renseignementMorale = ["formeJuridiqueN3", "secteurActivite", "activitePrincipale", "duree", "capitalSocial"];
var renseignementPhysique = ["nom", "postNom", "prenom", "dateNaissance", "adresse", "situationMatrimoniale"];
var parent = ["nomPere", "nomMere"];

function todayDate() {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    return day + "/" + (month < 10 ? "0" : "") + month + "/" + (day < 10 ? "0" : "") + d.getFullYear();
}

function getFieldProperty(key, property) {
    var prop = "";
    $.each(fieldPropertiesJson, function (i, obj) {
        if (obj.Name === key) {
            prop = obj[property];
        }
    });
    return prop;
}

function loadReport() {
    var report = $("#FunctionToPrint").attr("value");
    $("#textToPrint").html("<script type='text/javascript'>" + report + "();</script>");
}

function showData(data, type) {
    if (type === "code") {
        return data.value;
    } else if (type === "boolean") {
        return data ? "Oui" : "Non";
    } else if (type === "date") {
        var tab = data.split("-");
        return tab[2] + "/" + tab[1] + "/" + tab[0];
    } else if (type === "amount") {
        if (data + "".indexOf(":") === -1) {
            var tabCurrency = data + "".split(":");
            var dataTab = tabCurrency[3].split("-");
            var dateConversion = dataTab[2] + "/" + dataTab[1] + "/" + dataTab[0];
            return tabCurrency[0] + " CDF => " + tabCurrency[2] + " " + tabCurrency[1] + " au " + dateConversion;
        } else {
            return data + " CDF";
        }
    } else if (type === "reference") {
        return data._name;
    } else {
        return data;
    }
}

function renderValues(renderHtml, key, isPhysique, docs, data) {
    var type = getFieldProperty(key, "Type");
    var fieldEnt = getFieldProperty(key, "Field");
    var multivaluedEnt = getFieldProperty(key, "Multivalued");
    var checkMultivalued = multivaluedEnt ? data[key] !== undefined && data[key].length > 0 : true;
    var check = data[key] !== null && data[key] !== undefined && data[key] !== "" && checkMultivalued;
    if (key === "adresseSiegeExploitation" && !check) {
        var siegeExploitation = data["siegeExploitation"];
        if (!siegeExploitation) {
            key = "adresseSiegeSocial";
            renderHtml += "<tr><td style='width:35%;'>" + fieldEnt + "</td><td style='text-align:justify;'>" + showData(data[key], type) + "</td></tr>";
        }
    } else {
        if (check && !isExcluding(key)) {
            if ((type === "pdf" || type === "doc") && docs !== undefined && docs !== null) {
                docs.push(key);
            } else {
                if (multivaluedEnt) {
                    var tab = data[key];
                    for (var a = 0; a < tab.length; a++) {
                        renderHtml += "<tr><td style='width:35%;'>" + fieldEnt + "</td><td style='text-align:justify;'>" + showData(tab[a], type) + "</td></tr>";
                    }
                } else {
                    renderHtml += "<tr><td style='width:35%;'>" + (key === "formeJuridiqueN2" ? formeJuridiqueN2Exception(isPhysique)
                        : fieldEnt) + "</td><td style='text-align:justify;'>" + showData(data[key], type) + "</td></tr>";
                }
            }
        }
    }
    return renderHtml;
}

function formeJuridiqueN2Exception(isPhysique) {
    return isPhysique ? "Type personne physique" : "Type société";
}

function isExcluding(key) {
    var found = false;
    for (var i = 0; i < excludedFields.length; i++) {
        if (excludedFields[i] === key) {
            found = true;
            break;
        }
    }
    return found;
}

function renderEntity(renderHtml, entityProp, data, personDbName, entitNames) {
    var entityFound = "";
    $.each(fieldPropertiesJson, function(index, obj) {
        var multivaluedEnt = getFieldProperty(obj.Name, "Multivalued");
        var checkMultivalued = multivaluedEnt ? data[obj.Name] !== undefined && data[obj.Name].length > 0 : true;
        if (data[obj.Name] !== null && data[obj.Name] !== undefined && data[obj.Name] !== ""
            && checkMultivalued && obj.Name === entityProp) {
            entityFound = getFieldProperty(entityProp, "Field");
        }
    });
    renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>RENSEIGNEMENTS RELATIFS AUX " + entitNames.toUpperCase() + "</b></div><hr/>";

    if (entityFound !== "") {
        $.each(fieldPropertiesJson, function (index, obj) {
            var multivaluedDir = getFieldProperty(obj.Name, "Multivalued");
            var fieldDir = entitNames.substring(0, entitNames.length - 1);
            var checkMultivalued = multivaluedDir ? data[obj.Name] !== undefined && data[obj.Name].length > 0 : true;
            if (data[obj.Name] !== null && data[obj.Name] !== undefined && data[obj.Name] !== ""
                && checkMultivalued && obj.Name === entityProp) {
                var type = getFieldProperty(obj.Name, "Type");
                if (multivaluedDir) {
                    var tab = data[obj.Name];
                    for (var b = 0; b < tab.length; b++) {
                        renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
                        renderHtml += "<tr>" + (tab.length > 1 ? "<td style='width:35%;'>" + fieldDir + " " + (b + 1) + "</td>" : "")
                            + "<td><b>" + showData(tab[b], type) + "</b></td></tr>";
                        renderHtml += "</table>";
                        if (type === "reference") {
                            var docs = new Array();
                            renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
                            var json = tab[b];
                            $.each(fieldPropertiesJson, function (k, item) {
                                var type2 = getFieldProperty(item.Name, "Type");
                                if (json[item.Name] !== null && json[item.Name] !== undefined && json[item.Name] !== "") {
                                    if (type2 === "pdf") {
                                        docs.push(item.Name);
                                    } else {
                                        var field1 = getFieldProperty(item.Name, "Field");
                                        if (item.Name !== personDbName) {
                                            renderHtml += "<tr><td style='width:35%;'>" + field1 + "</td><td>" + showData(json[item.Name], type2) + "</td></tr>";
                                        }
                                        if (type2 === "reference") {
                                            var json2 = json[item.Name];
                                            $.each(fieldPropertiesJson, function (k2, item2) {
                                                if (json2[item2.Name] !== null && json2[item2.Name] !== undefined && json2[item2.Name] !== "") {
                                                    var field2 = getFieldProperty(item2.Name, "Field");
                                                    var type3 = getFieldProperty(item2.Name, "Type");
                                                    if (type3 === "pdf") {
                                                        docs.push(item2.Name);
                                                    } else {
                                                        renderHtml += "<tr><td style='width:35%;'>" + field2 + "</td><td>" + showData(json2[item2.Name], type3) + "</td></tr>";
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                            renderHtml += "<tr><td style='width:35%;'>Documents fournis</td><td style='text-align: justify;'>";
                            if (docs.length > 0) {
                                for (var z = 0; z < docs.length; z++) {
                                    var field4 = getFieldProperty(docs[z], "Field");
                                    var multivalued4 = getFieldProperty(docs[z], "Multivalued");
                                    if (multivalued4 && data[docs[z]].length > 1) {
                                        renderHtml += "<b>" + field4 + " (" + data[docs[z]].length + ")</b>, ";
                                    } else {
                                        renderHtml += "<b>" + field4 + "</b>, ";
                                    }
                                }
                                renderHtml = renderHtml.substring(0, renderHtml.length - 2);
                            } else {
                                renderHtml += "<i>Aucun document fourni</i>";
                            }
                            renderHtml += "</td></tr>";
                            renderHtml += "</table>";

                        }
                    }
                } else {
                    renderHtml += "<tr><td>" + showData(data[obj.Name], type) + "</td></tr>";
                }
            }
        });
    } else {
        renderHtml += "<div style='text-align: center;'><i> --- NEANT --- </i></div>";
    }
    return renderHtml;
}

function renderEntityLight(renderHtml, entityProp, data, personDbName, entitNames) {
    var entityFound = "";
    $.each(fieldPropertiesJson, function (index, obj) {
        var multivaluedEnt = getFieldProperty(obj.Name, "Multivalued");
        var checkMultivalued = multivaluedEnt ? data[obj.Name] !== undefined && data[obj.Name].length > 0 : true;
        if (data[obj.Name] !== null && data[obj.Name] !== undefined && data[obj.Name] !== ""
            && checkMultivalued && obj.Name === entityProp) {
            entityFound = getFieldProperty(entityProp, "Field");
        }
    });
    renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>RENSEIGNEMENTS RELATIFS AUX " + entitNames.toUpperCase() + "</b></div><hr/>";

    if (entityFound !== "") {
        $.each(fieldPropertiesJson, function (index, obj) {
            var multivaluedDir = getFieldProperty(obj.Name, "Multivalued");
            var fieldDir = entitNames.substring(0, entitNames.length - 1);
            var checkMultivalued = multivaluedDir ? data[obj.Name] !== undefined && data[obj.Name].length > 0 : true;
            if (data[obj.Name] !== null && data[obj.Name] !== undefined && data[obj.Name] !== ""
                && checkMultivalued && obj.Name === entityProp) {
                var type = getFieldProperty(obj.Name, "Type");
                if (multivaluedDir) {
                    var tab = data[obj.Name];
                    renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
                    for (var b = 0; b < tab.length; b++) {
                        renderHtml += "<tr>" + (tab.length > 1 ? "<td style='width:35%;'>" + fieldDir + " " + (b + 1) + "</td>" : "")
                            + "<td><b>" + showData(tab[b], type) + "</b></td></tr>";
                    }
                    renderHtml += "</table>";
                } else {
                    renderHtml += "<tr><td>" + showData(data[obj.Name], type) + "</td></tr>";
                }
            }
        });
    } else {
        renderHtml += "<div style='text-align: center;'><i> --- NEANT --- </i></div>";
    }
    return renderHtml;
}

function renderSuccursale(renderHtml, data) {
    if (data.adressesEtablissementsSecondaires.length > 0) {
        renderHtml += "<hr><div style='text-align: left;font-size: 12px;'><b>RENSEIGNEMENTS RELATIFS AUX ETABLISSEMENTS SECONDAIRES, SUCCURSALES OU FILIALES<hr/></b></div>";
        renderHtml += "<table class='table table-striped'><tbody>";
        for (var i = 0; i < data.adressesEtablissementsSecondaires.length; i++) {
            var type = getFieldProperty("adressesEtablissementsSecondaires", "Type");
            renderHtml += "<tr style='font-size: 12px;'><td style='width:35%;'>" + data.adressesEtablissementsSecondaires[i]._ste_name +
                "</td><td>" + showData(data.adressesEtablissementsSecondaires[i], type) + "</td></tr>";
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function renderFormulaireUniqueHeader(juridiction) {
    return "<div style=\"height:50px;margin:5px;font-size: 12px;font-weight: bold;\"><div style='float:left;width:300px;'>" +
        "<p style='margin-left:50px;'>Antenne de " + juridiction +
        "</p>Registre du Commerce et du Crédit Mobilier (RCCM)</div><div>" +
        "<img style=\"float:right;width:90px;height:90px;margin-top:-8px;margin-right:7px;\" src=\"" + logoUrl + "\"/></div></div>";
}

function renderFormulaireUniqueFooter(renderHtml) {
    renderHtml += "<div style='height:30px;'></div><p style='font-family: Arial, Helvetica, sans-serif;" +
        "width:250px;float:left;font-size: 12px;'>Noms et signature du requérant </p>";
    renderHtml += "<p style='font-family: Arial, Helvetica, sans-serif;width:180px;float:right;" +
        "font-size: 12px;'>Fait à Kinshasa, le  " + todayDate() + "</p><div style='height:30px;'></div>" + legalMention();
    return renderHtml;
}

function renderKbisFooter(renderHtml) {
    //renderHtml += "<div style='height:20px;'><p style='font-family: Arial, Helvetica, sans-serif;" +
    //    "width:250px;float:left;font-size: 12px;'>Greffier RCCM</p></div>";
    renderHtml += "<div style='height:30px;'></div><div style='height:20px;" +
        "float:left;font-size: 12px;'><b>ONEZIME KAUNDA MBIYA</b><br/>Greffier Titulaire du Guichet Unique</div>";
    renderHtml += "<p style='font-family: Arial, Helvetica, sans-serif;width:180px;float:right;" +
        "font-size: 12px;'>Fait à Kinshasa, le  " + todayDate() + "</p><div style='height:30px;'></div>" + legalMention();
    return renderHtml;
}

function legalMention() {
    return "<div style='height:20px;'></div><p style='font-family: Arial, Helvetica, sans-serif;" +
        "font-size: 9px;text-align:justify;'><i> Le présent document a été établi sur base des articles 50 et 66 de" +
        " l’AUDCG prévoyant un contrôle à posteriori à intervenir dans les trois mois à compter de la" +
        " date de l’immatriculation au RCCM. Sous réserve des erreurs à déceler qui pourront faire l’objet" +
        " de modification d’office.</i></div>";
}

function renderInscriptionComplementaires(data) {
    var renderHtml = "";
    if (data.length > 0) {
        renderHtml += "<div style='text-align: left;font-size: 12px;'><b>RENSEIGNEMENTS RELATIFS AUX INSCRIPTIONS COMPLEMENTAIRES, " +
            "AUX MODIFICATIONS, AUX DEPOTS D'ACTE<hr/></b></div>";

        renderHtml += "<table class='table table-striped table-bordered'>" +
            "<thead><tr><th width='30%'></th><th style='width:35%;'>Ancienne valeur</th>" +
            "<th style='width:35%;'>Nouvelle valeur</th></tr></thead><tbody>";

        for (var i = 0; i < data.length; i++) {
            var val1 = testValue(data[i].oldValue);
            var val2 = testValue(data[i].currentValue);
            if (val1 === "NON") {
                val1 = "";
            }
            if (val2 === "NON") {
                val2 = "";
            }
            var type = getFieldProperty(data[i].Name, "Type");
            var field = data[i].libele;
            if (type !== "pdf" && type !== "doc") {
                renderHtml += "<tr><td>" + field + "</td><td>" + val1 + "</td><td>" + val2 + "</td></tr>";
            } else {
                renderHtml += "<tr><td>" + field + "</td><td>" + val1 + "</td><td><i>Nouveau document</i></td></tr>";
            }
            
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function Recepisse_demande_inscription_complementaire() {
    inscriptionComplementaire = true;
    hideOtherReport();
    var entityId = $("#EntityId").val();
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entityId },
        success: function(response) {
            var data = $.parseJSON($.parseJSON(response));
            $.ajax({
                type: "POST",
                url: GetValueModified,
                data: { entityId: entityId, txId: 0 },
                success: function(dataModified) {
                    var stringData = $.parseJSON(dataModified);

                    var isPhysique = data.formeJuridiqueN1.code === "A";
                    var renderHtml = "<div style='font-family: Arial, Helvetica, sans-serif;'>";
                    renderHtml += renderFormulaireUniqueHeader(data._ste_name);
                    renderHtml += "<div style='margin-top:50px;text-align: center;'> <h4><b>ACCUSE DE RECEPTION</b></h4></div>";

                    renderHtml += "<div style='height:12px;text-align: justify;font-size: 12px;'>A la date ci-dessous, " +
                        "le Guichet Unique de Création d'Entreprise (GUCE) accuse bonne réception de votre dossier pour la MODIFICATION " +
                        " de " + (isPhysique ? "l'établissement" : "la société")
                        + " dont la dénomination sociale est :</div><br/>";

                    renderHtml += "<div style='font-family: Arial, Helvetica, sans-serif;text-align: " +
                        "left;font-size: 14px;margin:5px 0;'><b> " + data.nomCommercial + "</b></div>";

                    renderHtml += "<div style='height:20px;text-align: left;font-size: 12px;'>Numéro du dossier : <b>" +
                        $(".label-warning").text() + "</b></div>";

                    renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>IDENTIFICATION DE "
                        + (isPhysique ? "L'ETABLISSEMENT" : "LA SOCIETE") + "</b></div><hr/>";
                    renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
                    if (isPhysique) {
                        for (var i = 0; i < indentification.length; i++) {
                            renderHtml = renderValues(renderHtml, indentification[i], true, null, data);
                        }
                        for (var t = 0; t < renseignementMorale.length; t++) {
                            renderHtml = renderValues(renderHtml, renseignementMorale[t], true, null, data);
                        }
                    } else {
                        for (var j = 0; j < indentification.length; j++) {
                            renderHtml = renderValues(renderHtml, indentification[j], false, null, data);
                        }
                    }
                    renderHtml += "</table>";

                    renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE "
                        + (isPhysique ? "PHYSIQUE" : "MORALE") + "</b></div><hr/>";
                    renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
                    if (isPhysique) {
                        for (var h = 0; h < renseignementPhysique.length; h++) {
                            renderHtml = renderValues(renderHtml, renseignementPhysique[h], false, null, data["dirigeant"][0]["personneDirigeant"]);
                        }
                    } else {
                        for (var k = 0; k < renseignementMorale.length; k++) {
                            renderHtml = renderValues(renderHtml, renseignementMorale[k], false, null, data);
                        }
                    }
                    renderHtml += "<tr><td>Constitution ­dépots de l'acte constitutif</td><td>" + data._ste_name + "</td></tr>";
                    renderHtml += "</table>";

                    renderHtml += renderInscriptionComplementaires(stringData);

                    renderHtml = renderFormulaireUniqueFooter(renderHtml);
                    renderHtml += "</div>";
                    $("#textToPrint").html(renderHtml);
                },
                error: function() {
                    messageAlert("Erreur lors de la génération du rapport...");
                }
            });
        },
        error: function() {
            messageAlert("Erreur lors de la génération du rapport...");
        }
    });
}

function Recepisse_demande_immatriculation(entitiId) {
    hideOtherReport();
    var entityId = entitiId === undefined ? $("#EntityId").val() : entitiId ;

    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entityId },
        success: function (response) {
            var data = $.parseJSON($.parseJSON(response));
            var isPhysique = data.formeJuridiqueN1.code === "A";
            var renderHtml = "<div style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;'>";

            if(entitiId === undefined)
            {
                renderHtml += renderFormulaireUniqueHeader(data._ste_name);
                renderHtml += "<div style='margin-top:50px;text-align: center;'> <h4><b>FORMULAIRE UNIQUE</b></h4></div>";
                renderHtml += "<div style='height:40px;text-align: center;'> (Valant accusé de réception)</div>";

                renderHtml += "<div style='height:12px;text-align: justify;font-size: 12px;'>A la date ci-dessous, " +
                    "le Guichet Unique de Création d'Entreprise (GUCE) accuse bonne réception de votre dossier pour la " +
                    (data.origine.value).toUpperCase() + " de " + (isPhysique ? "l'établissement" : "la société")
                    + " dont la dénomination sociale est :</div><br/>";

                renderHtml += "<div style='font-family: Arial, Helvetica, sans-serif;text-align: " +
                    "left;font-size: 14px;margin:5px 0;'><b> " + data.nomCommercial + "</b></div>";

                renderHtml += "<div style='height:20px;text-align: left;font-size: 12px;'>Numéro du dossier : <b>" +
                    $(".label-warning").text() + "</b></div>";

            }
            
            
            renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE "
                + (isPhysique ? "PHYSIQUE" : "MORALE") + "</b></div><hr/>";

            renderHtml += "<table class='table table-striped' style='font-size: 12px;'>";
            var documentEntreprise = new Array();
            $.each(fieldPropertiesJson, function (index, obj) {
                renderHtml = renderValues(renderHtml, obj.Name, isPhysique, documentEntreprise, data);
            });
            
            renderHtml += "</table>";

            renderHtml = renderSuccursale(renderHtml, data);

            renderHtml = renderEntity(renderHtml, "dirigeant", data, "personneDirigeant", "Dirigeants");
            if (!isPhysique) {
                var entitName = data.formeJuridiqueN3.code === "B12" ? "Associés" : "Actionnaires";
                renderHtml = renderEntity(renderHtml, "associe", data, "personneAssocie", entitName);
                renderHtml = renderEntity(renderHtml, "mandataireSociete", data, "personneMandataire", "Mandataires");
                renderHtml = renderEntity(renderHtml, "commissaireAuxComptes", data, "", "Commissaires aux comptes");
            }

            renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>DOCUMENTS DE  "
                + (isPhysique ? "L'ETABLISSEMENT" : "LA SOCIETE") + " FOURNIS</b></div><hr/>";
            renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
            renderHtml += "<tr><td style='text-align: justify;'>";
            if (documentEntreprise.length > 0) {
                for (var i = 0; i < documentEntreprise.length; i++) {
                    var field = getFieldProperty(documentEntreprise[i], "Field");
                    //var multivalued = getFieldProperty(documentEntreprise[i], "Multivalued");
                    //if (multivalued && data[documentEntreprise[i]].length > 1) {
                    //    renderHtml += "<b>" + field + " (" + data[documentEntreprise[i]].length + ")</b>, ";
                    //} else {
                        renderHtml += "<b>" + field + "</b>, ";
                    //}
                }
                renderHtml = renderHtml.substring(0, renderHtml.length - 2);
            } else {
                renderHtml += "<b><i>Aucun document fourni</i></b>";
            }
            renderHtml += "</td></tr>";
            renderHtml += "</table>";
            if (entitiId === undefined) {
                renderHtml = renderFormulaireUniqueFooter(renderHtml);
            }
            renderHtml += "</div>";
            $("#textToPrint").html(renderHtml);
            $("#AllContent").html(renderHtml);
        },
        error: function () {
            messageAlert("Erreur lors de la génération du rapport...");
        }
    });
}

function Recepisse_correction_erreur_materielle() {
    Recepisse_demande_immatriculation();
}

function Acte_notaire() {
    hideOtherReport();
    var entityId = $("#EntityId").val();
    var flowId = getUrlParameter("flowId");
    $.ajax({
        type: "POST",
        url: findInfoNotaire,
        data: { entityId: entityId },
        success: function (data) {
            var dataNotaire = $.parseJSON(data);
            
            var output = todayDate();

            $.ajax({
                type: "POST",
                url: GenerateDateToWord,
                data: { dateToConvert: output },
                success: function (dateConverted) {

                    $.ajax({
                        type: "POST",
                        url: findMoreInfoNotaire,
                        data: { entityId: entityId },
                        success: function (infoNotaire) {
                            var moreInfo = $.parseJSON(infoNotaire);
                            var moreInfoJson = $.parseJSON(moreInfo);

                            $.ajax({
                                type: "POST",
                                url: FindFluxValues,
                                data: { entityId: entityId, flowId: flowId },
                                success: function (infoFlux) {
                                    var infoFluxString = $.parseJSON(infoFlux);
                                    var formatText = infoFluxString.replace("}{", ",");
                                    var infoFluxJson = $.parseJSON(formatText);
                                    
                                    // Début de la DIV principale
                                    var renderHtml = "<div style=\"font-family: Arial, Helvetica, sans-serif;margin:25px;\">";

                                    // Insertion de l'en-tete
                                    renderHtml += renderHeaderActeNotarie();

                                    // Titre du document
                                    renderHtml += "<div style=\"height:20px;text-align: center;\">" +
                                        "<b><u><p style='margin-top:15px;'>ACTE NOTARIE N° " + $(".label-warning").text()
                                        + "</p></u></b></div>";
                                    renderHtml += "<div  style=\"height:15px;\"></div>";
                                    renderHtml += "<div style=\"text-align: left;font-size: 15px;\"> " + dateConverted + ".***</div>";

                                    // Document a notarier
                                    var documentANotarier = "";
                                    if (infoFluxJson.documentANotarier) {
                                        for (var ii = 0; ii < infoFluxJson.documentANotarier.length; ii++) {
                                            if (infoFluxJson.documentANotarier[ii].code === "S") {
                                                documentANotarier += infoFluxJson.documentANotarier[ii].value + " du " +
                                                    showData(infoFluxJson.dateStatuts, "date") + ", ";
                                            } else {
                                                documentANotarier += infoFluxJson.documentANotarier[ii].value + ", ";
                                            }
                                        }
                                    }

                                    // autres document a notarier .autreDocumentANotarier
                                    if (infoFluxJson.autreDocumentANotarier) {
                                        for (var i = 0; i < infoFluxJson.autreDocumentANotarier.length; i++) {
                                            documentANotarier += infoFluxJson.autreDocumentANotarier[i] + ", ";
                                        }
                                    }

                                    if (documentANotarier.length > 2) {
                                        documentANotarier = documentANotarier.substring(0, documentANotarier.length - 2);
                                    }

                                    // Ajout des comparants
                                    var comparantActeNotarie1 = "";
                                    var adresseComparant1 = "";//getValueFromDataJson(infoFluxJson, ".activitePrincipale.value");
                                    if (infoFluxJson.comparantActeNotarie["0"] !== null &&
                                        infoFluxJson.comparantActeNotarie.length > 0) {
                                        comparantActeNotarie1 = infoFluxJson.comparantActeNotarie["0"].civilite.value + " " +
                                            infoFluxJson.comparantActeNotarie["0"].prenom + " " +
                                                                                        infoFluxJson.comparantActeNotarie["0"].nom;
                                        adresseComparant1 = testValue(infoFluxJson.comparantActeNotarie["0"].adresse._name);
                                    }

                                    var comparantActeNotarie2 = "";
                                    var adresseComparant2 = "";
                                    if (infoFluxJson.comparantActeNotarie["1"] !== null && infoFluxJson.comparantActeNotarie.length > 1) {
                                        comparantActeNotarie2 = testValue(infoFluxJson.comparantActeNotarie["1"].prenom) + " " +
                                                                                        testValue(infoFluxJson.comparantActeNotarie["1"].nom);
                                        adresseComparant2 = testValue(infoFluxJson.comparantActeNotarie["1"].adresse._name);
                                    }

                                    //// Ajout des temoins
                                    var temoinActeNotarie1 = "";
                                    var adresseTemoinActeNotarie1 = "";
                                    var fonctionTemoinActeNotarie1 = "";
                                    if (infoFluxJson.temoinActeNotarie1 !== null) {
                                        temoinActeNotarie1 = infoFluxJson.temoinActeNotarie1.personneTemoin;
                                        adresseTemoinActeNotarie1 = infoFluxJson.temoinActeNotarie1.villeTemoin;
                                        fonctionTemoinActeNotarie1 = infoFluxJson.temoinActeNotarie1.fonctionTemoin;
                                    }

                                    var temoinActeNotarie2 = "";
                                    var adresseTemoinActeNotarie2 = "";
                                    var fonctionTemoinActeNotarie2 = "";
                                    if (infoFluxJson.temoinActeNotarie2 !== null) {
                                        temoinActeNotarie2 = infoFluxJson.temoinActeNotarie2.personneTemoin;
                                        adresseTemoinActeNotarie2 = infoFluxJson.temoinActeNotarie2.villeTemoin;
                                        fonctionTemoinActeNotarie2 = infoFluxJson.temoinActeNotarie2.fonctionTemoin;
                                    }


                                    renderHtml += "<div  style=\"height:20px;\"></div>";
                                    renderHtml += "<div style=\"text-align: justify;font-size: 15px;\"> Nous soussignés, <b>" + moreInfoJson.notaire +
                                        "</b>, " + moreInfoJson.fonctionNotaire + " à l\'Office Notarial du Guichet Unique de Création " +
                                    "d'Entreprise à " + (dataNotaire._ste_name).substring(4, dataNotaire._ste_name.length) + ", agissant conformément aux prescrits des articles 9," +
                                    " 10 et 15 du Décret N⁰ 14/014 du 08 Mai 2014 portant Création, Organisation et Fonctionnement du Guichet Unique de Création d'Entreprise, à l'" +
                                    testValue(moreInfoJson.decretNominationNotaire).charAt(0).toLowerCase() +
                                    testValue(moreInfoJson.decretNominationNotaire).slice(1) +
                                    " portant Nomination des Notaires au Guichet Unique de Création d'Entreprise, ainsi qu'à l'Ordonnance n⁰ 66/344 du 9 juin 1966 relative aux actes notariés ; certifions que les documents ci-après : " +
                                    testValue(documentANotarier) + " de la société " + dataNotaire.nomCommercial + " ayant son" +
                                    " siège  situé sur " + dataNotaire.adresseSiegeSocial._name +
                                    ", dont les clauses ci-dessous insérées nous ont été présentées ce jour, à " + (dataNotaire._ste_name).substring(4, dataNotaire._ste_name.length) +
                                    " par <b>" + comparantActeNotarie1 +
                                    "</b>, dûment mandaté, ayant son adresse professionnelle située sur " + adresseComparant1 + "; comparaissant en personne" +
                                    " en présence de <b>" + temoinActeNotarie1 + "</b>, " + fonctionTemoinActeNotarie1 +
                                    ", résidant à " + adresseTemoinActeNotarie1 + ", et de <b>" + temoinActeNotarie2 + "</b>, " + fonctionTemoinActeNotarie2 +
                                    ", résident à " + adresseTemoinActeNotarie2 + ", témoins instrumentaires à ce requis" +
                                    " réunissant les conditions exigées par la loi en la matière; lecture du contenu de l'acte susmentionné a" +
                                    " été faite par nous, tant au comparant qu'aux témoins " + temoinActeNotarie1 +
                                    ", ci-dessus identifié et " + temoinActeNotarie2 + ", ci-dessus identifié.***</div>";

                                    renderHtml += "<div  style=\"height:20px;\"></div>";
                                    renderHtml += "<div style=\"text-align: justify;font-size: 15px;\">Le comparant pré-qualifié persiste et signe devant témoins et nous que, l\'économie des documents à authentifier renferme " +
                                    "bien l'expression de la volonté des signataires, qu'ils sont seuls responsables de toutes contestations pouvant" +
                                    " naître de l'exécution dudit document sans évoquer la complicité de l'Office Notarial ainsi que du " + moreInfoJson.fonctionNotaire + "." +
                                    " En foi de quoi, le présent acte vient d'être signé par les comparants, témoins et nous, et revêtu" +
                                    " du sceau de l\'Office Notarial du Guichet Unique de Création d\'Entreprises a " + (dataNotaire._ste_name).substring(4, dataNotaire._ste_name.length) + ".***</div>";

                                    renderHtml += "<div  style=\"height:15px;\"></div>";
                                    renderHtml += "<div style=\"height:17px;font-size: 12px;font-weight: bold;\">" +
                                                "<p style=\"width:250px;float:left;font-size: 12px;\"><u>SIGNATURE DU COMPARANT </u></p>" +
                                                "<p style=\"width:180px;float:right;font-size: 12px;\"><u>SIGNATURE DU NOTAIRE</u></p></div>";

                                    renderHtml += "<div  style=\"height:17px;\"></div>";
                                    renderHtml += "<div style=\"height:20px;font-size: 12px;font-weight: bold;\">" +
                                                "<div style=\"width:50%;float:left;font-size: 12px;\"><p style='margin-left:7px;text-transform:uppercase;'>" + comparantActeNotarie1 + "</p></div>" +
                                                "<div style=\"width:50%;float:right;font-size: 12px;\"><p style='text-align:right;margin-right:40px;text-transform:uppercase;'>" + moreInfoJson.notaire + "</p></div></div>";

                                    renderHtml += "<div  style=\"height:15px;\"></div>";
                                    renderHtml += "<div style=\"height:15px;font-size: 12px;font-weight: bold;\">" +
                                                "<p style=\"text-align: center;\"><u>NOM ET SIGNATURE DES TEMOINS</u></p></div>";
                                    renderHtml += "<div style=\"height:25px;margin-top:10px;\">" +
                                                "<p style=\"width:250px;float:left;font-size: 12px;margin-left:20px;text-transform:uppercase;\">1. " + temoinActeNotarie1 + " </p>" +
                                                "<p style=\"width:180px;float:right;font-size: 12px;text-transform:uppercase;\">2. " + temoinActeNotarie2 + "</p></div>";

                                    renderHtml += "<div  style=\"height:30px;\"></div>";
                                    renderHtml += "<div style=\"height:50px;text-align:justify;font-size: 10px;\">Droits perçu : Frais d\'acte de " +
                                            testValue(infoFluxJson.fraisActeNotarie) + " CDF dont " + testValue(infoFluxJson.fraisAuthentification) +
                                            " CDF pour l\'authentification.*** <br/>Suivant la note de perception N° " +
                                            testValue(infoFluxJson.numeroNotePerception) + " et ainsi" +
                                         " que l'attestation de paiement N° " + testValue(infoFluxJson.numeroRecepisseBanque)
                                         + " (" + testValue(infoFluxJson.banque.value).toUpperCase() + ")" +
                                         " de ce jour. Enregistré par nous soussignés, ce <span style='text-transform:lowercase;'>" + dateConverted + "</span> sous le N° " +
                                         $(".label-warning").text() + ".</div>";

                                    renderHtml += "<div style=\"height:15px;font-size: 12px;font-weight: bold;margin-top:1px;\">" +
                                                "<p style=\"width:180px;float:right;font-size: 12px;\"><u>SIGNATURE DU NOTAIRE</u></p></div>";

                                    renderHtml += "<div  style=\"height:25px;\"></div>";
                                    renderHtml += "<div style=\"height:20px;font-size: 12px;font-weight: bold;\">" +
                                                "<p style=\"width:180px;float:right;font-size: 12px;margin-right:-10px;\">" + moreInfoJson.notaire + "</p></div>"
                                                + legalMention();

                                    renderHtml += "</div>";
                                    $("#textToPrint").html(renderHtml);

                                },
                                error: function () {
                                    messageAlert("Erreur lors de la génération du rapport...");
                                }
                            });

                        },
                        error: function () {
                            messageAlert("Erreur lors de la génération du rapport...");
                        }
                    });
                },
                error: function () {
                    messageAlert("Erreur lors de la génération du rapport...");
                }
            });

        },
        error: function () {
            messageAlert("Erreur lors de la génération du rapport ...");
        }
    });
}

function Acte_notaire_modif() {
    inscriptionComplementaire = true;
    Acte_notaire();
}

function Kbis() {
    var entityId = $("#EntityId").val();
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entityId },
        success: function (response) {
            var data = $.parseJSON($.parseJSON(response));
            var isPhysique = data.formeJuridiqueN1.code === "A";
            manageOtherReportInKBis(isPhysique);
            var renderHtml = "<div style='font-family: Arial, Helvetica, sans-serif;'>";
            renderHtml += renderFormulaireUniqueHeader(data._ste_name);
            renderHtml += "<div style='margin-top:50px;text-align: center;'> <h4><b>Extrait du Registre du Commerce et du Crédit Mobilier</b></h4></div>";
            
            renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>IDENTIFICATION DE "
                + (isPhysique ? "L'ETABLISSEMENT" : "LA SOCIETE") + "</b></div><hr/>";
            renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
            if (isPhysique) {
                for (var i = 0; i < indentification.length; i++) {
                    renderHtml = renderValues(renderHtml, indentification[i], true, null, data);
                }
                for (var t = 0; t < renseignementMorale.length; t++) {
                    renderHtml = renderValues(renderHtml, renseignementMorale[t], true, null, data);
                }
            } else {
                for (var j = 0; j < indentification.length; j++) {
                    renderHtml = renderValues(renderHtml, indentification[j], false, null, data);
                }
            }
            renderHtml += "</table>";

            renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE "
                + (isPhysique ? "PHYSIQUE" : "MORALE") + "</b></div><hr/>";
            renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
            if (isPhysique) {
                for (var h = 0; h < renseignementPhysique.length; h++) {
                    renderHtml = renderValues(renderHtml, renseignementPhysique[h], false, null, data["dirigeant"][0]["personneDirigeant"]);
                }
                for (var z = 0; z < parent.length; z++) {
                    renderHtml = renderValues(renderHtml, parent[z], true, null, data);
                }
            } else {
                for (var k = 0; k < renseignementMorale.length; k++) {
                    renderHtml = renderValues(renderHtml, renseignementMorale[k], false, null, data);
                }
            }
            renderHtml += "<tr><td>Constitution ­dépots de l'acte constitutif</td><td>" + data._ste_name + "</td></tr>";
            renderHtml += "</table>";

            renderHtml = renderSuccursale(renderHtml, data);

            renderHtml = renderEntityLight(renderHtml, "dirigeant", data, "personneDirigeant", "Dirigeants");
            if (!isPhysique) {
                var entitName = data.formeJuridiqueN3.code === "B12" ? "Associés" : "Actionnaires";
                renderHtml = renderEntityLight(renderHtml, "associe", data, "personneAssocie", entitName);
            }
            
            renderHtml += "<hr/><div style='text-align: left;font-size: 13px;'><b>RENSEIGNEMENTS RELATIFS AU SURETES</b></div><hr/>";
            renderHtml += "<table class='table table-striped table-bordered' style='font-size: 12px;'>";
            renderHtml += "<tr><td>NEANT (Non déclaré sur le système informatique RCCM au " + todayDate() + ")</td></tr>";
            renderHtml += "</table>";
            
            renderHtml = renderKbisFooter(renderHtml);
            renderHtml += "</div>";
            $("#textToPrint2").html(renderHtml);
            PrintGreffier(!isPhysique, data);
        },
        error: function () {
            messageAlert("Erreur lors de la génération du rapport...");
        }
    });
}

function Kbis_modif() {
    inscriptionComplementaire = true;
    Kbis();
}

function Kbis_erreur_materielle() {
    Kbis();
}

function renderHeaderActeNotarie() {
    return "<div style='height:140px;margin:5px;font-size: 12px;'><div style=\"float:left;width:300px\">" +
        "&nbsp;&nbsp;&nbsp;République Démocratique du Congo <br/>" +
        "Ministère de la Justice et Garde des Sceaux<br/>" +
        "<img style='margin-top: 4px;margin-bottom:4px;float:left;margin-left:80px;width:70px;height:70px;' src='" +
        drapeauUrl + "'/><div style='float:left;'><b>Guichet Unique de Création d'Entreprise</b>" +
        "<br/><u style=\"font-weight:bold;margin-left: 62px;\">OFFICE NOTARIAL</u></div></div>" +
        "<div><img  style=\"float:right;width:120px;height:120px;\" src=\"" + logoUrl + "\"/></div></div>";
}

function tableStyle() {
    var style = "<style>table {border-spacing: 0;border-collapse: collapse;}td,th {padding: 0;}";
    style += " .table {border-collapse: collapse !important;}.table td,.table th {background-color: #fff !important;}.table-bordered th,.table-bordered td {border: 1px solid #ddd !important;}";
    style += "hr {margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;}";
    style += "table {background-color: transparent;}caption {padding-top: 8px;padding-bottom: 8px;color: #777;text-align: left;}th {text-align: left;}.table {width: 100%;max-width: 100%;margin-bottom: 20px;}.table > thead > tr > th,.table > tbody > tr > th,.table > tfoot > tr > th,.table > thead > tr > td,.table > tbody > tr > td,.table > tfoot > tr > td {padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;}.table > thead > tr > th {vertical-align: bottom;border-bottom: 2px solid #ddd;}.table > caption + thead > tr:first-child > th,.table > colgroup + thead > tr:first-child > th,.table > thead:first-child > tr:first-child > th,.table > caption + thead > tr:first-child > td,.table > colgroup + thead > tr:first-child > td,.table > thead:first-child > tr:first-child > td {border-top: 0;}.table > tbody + tbody {border-top: 2px solid #ddd;}.table .table {background-color: #fff;}.table-condensed > thead > tr > th,.table-condensed > tbody > tr > th,.table-condensed > tfoot > tr > th,.table-condensed > thead > tr > td,.table-condensed > tbody > tr > td,.table-condensed > tfoot > tr > td {padding: 5px;}.table-bordered {border: 1px solid #ddd;}.table-bordered > thead > tr > th,.table-bordered > tbody > tr > th,.table-bordered > tfoot > tr > th,.table-bordered > thead > tr > td,.table-bordered > tbody > tr > td,.table-bordered > tfoot > tr > td {border: 1px solid #ddd;}.table-bordered > thead > tr > th,.table-bordered > thead > tr > td {border-bottom-width: 2px;}.table-striped > tbody > tr:nth-of-type(odd) {background-color: #f9f9f9;}.table-hover > tbody > tr:hover {background-color: #f5f5f5;}table col[class*=\"col-\"] {position: static;display: table-column;float: none;}table td[class*=\"col-\"],table th[class*=\"col-\"] {position: static;display: table-cell;float: none;}.table > thead > tr > td.active,.table > tbody > tr > td.active,.table > tfoot > tr > td.active,.table > thead > tr > th.active,.table > tbody > tr > th.active,.table > tfoot > tr > th.active,.table > thead > tr.active > td,.table > tbody > tr.active > td,.table > tfoot > tr.active > td,.table > thead > tr.active > th,.table > tbody > tr.active > th,.table > tfoot > tr.active > th {background-color: #f5f5f5;}.table-hover > tbody > tr > td.active:hover,.table-hover > tbody > tr > th.active:hover,.table-hover > tbody > tr.active:hover > td,.table-hover > tbody > tr:hover > .active,.table-hover > tbody > tr.active:hover > th {background-color: #e8e8e8;}.table > thead > tr > td.success,.table > tbody > tr > td.success,.table > tfoot > tr > td.success,.table > thead > tr > th.success,.table > tbody > tr > th.success,.table > tfoot > tr > th.success,.table > thead > tr.success > td,.table > tbody > tr.success > td,.table > tfoot > tr.success > td,.table > thead > tr.success > th,.table > tbody > tr.success > th,.table > tfoot > tr.success > th {background-color: #dff0d8;}.table-hover > tbody > tr > td.success:hover,.table-hover > tbody > tr > th.success:hover,.table-hover > tbody > tr.success:hover > td,.table-hover > tbody > tr:hover > .success,.table-hover > tbody > tr.success:hover > th {background-color: #d0e9c6;}.table > thead > tr > td.info,.table > tbody > tr > td.info,.table > tfoot > tr > td.info,.table > thead > tr > th.info,.table > tbody > tr > th.info,.table > tfoot > tr > th.info,.table > thead > tr.info > td,.table > tbody > tr.info > td,.table > tfoot > tr.info > td,.table > thead > tr.info > th,.table > tbody > tr.info > th,.table > tfoot > tr.info > th {background-color: #d9edf7;}.table-hover > tbody > tr > td.info:hover,.table-hover > tbody > tr > th.info:hover,.table-hover > tbody > tr.info:hover > td,.table-hover > tbody > tr:hover > .info,.table-hover > tbody > tr.info:hover > th {background-color: #c4e3f3;}.table > thead > tr > td.warning,.table > tbody > tr > td.warning,.table > tfoot > tr > td.warning,.table > thead > tr > th.warning,.table > tbody > tr > th.warning,.table > tfoot > tr > th.warning,.table > thead > tr.warning > td,.table > tbody > tr.warning > td,.table > tfoot > tr.warning > td,.table > thead > tr.warning > th,.table > tbody > tr.warning > th,.table > tfoot > tr.warning > th {background-color: #fcf8e3;}.table-hover > tbody > tr > td.warning:hover,.table-hover > tbody > tr > th.warning:hover,.table-hover > tbody > tr.warning:hover > td,.table-hover > tbody > tr:hover > .warning,.table-hover > tbody > tr.warning:hover > th {background-color: #faf2cc;}.table > thead > tr > td.danger,.table > tbody > tr > td.danger,.table > tfoot > tr > td.danger,.table > thead > tr > th.danger,.table > tbody > tr > th.danger,.table > tfoot > tr > th.danger,.table > thead > tr.danger > td,.table > tbody > tr.danger > td,.table > tfoot > tr.danger > td,.table > thead > tr.danger > th,.table > tbody > tr.danger > th,.table > tfoot > tr.danger > th {background-color: #f2dede;}.table-hover > tbody > tr > td.danger:hover,.table-hover > tbody > tr > th.danger:hover,.table-hover > tbody > tr.danger:hover > td,.table-hover > tbody > tr:hover > .danger,.table-hover > tbody > tr.danger:hover > th {background-color: #ebcccc;}.table-responsive {min-height: .01%;overflow-x: auto;}";
    style += "label {display: inline-block;max-width: 100%;}</style>";
    return style;
}

function printReport(title) {
    printPanel(title, $("#textToPrint"));
    printPanel(title, $("#interCalaireObjetSocial"));
    printPanel(title, $("#interCalaireEntity"));
    printPanel(title, $("#acteDepot"));
    printPanel(title, $("#textToPrint1"));
    printPanel(title, $("#textToPrint2"));
}

function printPanel(title, element) {
    if ($(element).is(":visible")) {
        var mywindow = window.open("", "_blank");
        var header = "<head><title>" + title + "</title> " + tableStyle() + "</head>";
        var content = $(element).html();
        mywindow.document.write(header + content);
        mywindow.document.close();
        mywindow.focus();
        mywindow.print();
    }
}

function printReportIntercalair(title) {
    var mywindow = window.open("", "_blank");
    var currentHeader = $("head").html();
    var header = "<head><title>" + title + "</title> " + tableStyle() + "</head>";
    var content = $("#textToPrint1").html();
    mywindow.document.head.textContent = currentHeader;
    mywindow.document.write(header + content);
    mywindow.document.close();
    mywindow.focus();
    mywindow.print();
    var ee = $("#textToPrint2").text();
    if ($("#textToPrint2").text() !== "") {
        printReportEnd(title);
    }
}

function printReportEnd(title) {
    var mywindow = window.open("", "_blank");
    var currentHeader = $("head").html();
    var header = "<head><title>" + title + "</title> " + tableStyle() + "</head>";
    var content = $("#textToPrint2").html();
    mywindow.document.head.textContent = currentHeader;
    mywindow.document.write(header + content);
    mywindow.document.close();
    mywindow.focus();
    mywindow.print();
}

function hideOtherReport() {
    $("#textToPrint1").hide();
    $("#textToPrint2").hide();
    $("#interCalaireObjetSocial").hide();
    $("#interCalaireEntity").hide();
    $("#acteDepot").hide();
}

function manageOtherReportInKBis(isPhysique) {
    if (isPhysique) {
        $("#interCalaireEntity").hide();
        $("#acteDepot").hide();
    } else {
        $("#textToPrint1").hide();
    }
}

// =================================================== Fin =======================================================================

var police = "font-family: Arial, Helvetica, sans-serif;";

function printHTML() {
    var reportId = $("#FunctionToPrint").attr("value");
    $("#textToPrint").after("<script>" + reportId + "();</script>");

}

//function Recepisse_demande_immatriculation() {
//    var entitiID = getUrlParameter("entityId");
//    $.ajax({
//        type: "POST",
//        url: findEntitieUrl,
//        data: { entitiId: entitiID },
//        success: function (data) {
            //            var stringData = $.parseJSON(data);
//            //console.log(stringData);
//            orderData = $.parseJSON(stringData);
//            if (orderData.formeJuridiqueN1.code == "B") {
//                immatriculation_personne_moral(orderData);
//            } else if (orderData.formeJuridiqueN1.code == "A") {
//                immatriculation_personne_physique(orderData);
//            }
//        },
//        error: function (e, status) {
//            if (e.status == 500) {
//                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
//                window.location.replace(logoutUrl);
//            } else {
//                messageAlert("Erreur lors de la génération du rapport...");
//            }
//        }
//    });
//}

function Recepisse_demande_inscription_complementaire1() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

            if (orderData.formeJuridiqueN1.code == "B") {
                inscription_complementaire_personne_morale(orderData);
            } else if (orderData.formeJuridiqueN1.code == "A") {
                inscription_complementaire_personne_physique(orderData);
            }
        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function inscription_complementaire_personne_physique(data) {
    //GetValueModified
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: GetValueModified,
        data: { entityId: entitiID, txId: 0 },
        success: function (dataModified) {
            var stringData = $.parseJSON(dataModified);


            // Début de la DIV principale
            var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

            // Insertion de l'en-tete
            renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));


            // Titre du document
            renderHTML += "<div style=\"height:30px;text-align: center;\">" +
                "<h2><b>ACCUSE DE RECEPTION</b></h2></div>";
            renderHTML += "<div  style=\"height:20px;\"></div>";

            renderHTML += "<div style=\"height:12px;text-align: justify;font-size: 12px;\">A la date ci-dessous, " +
                "le Guichet Unique de Création d'Entreprises accuse bonne réception de votre dossier pour la MODIFICATION " +
                                        " de votre entreprise dont la dénomination est :</div>";

            renderHTML += "<div style=\"text-align: left;\"><h3> " + testValue(data.nomCommercial)
                + "</h3></div>";

            renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Numéro du dossier : &nbsp;&nbsp;&nbsp;&nbsp; <b>" +
                $(".label-warning").text() + "</b></div>";
            renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Avec les informations suivantes :</div>";

            renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE<hr/></b></div>";

            if (testValue(data.adresseSiegeExploitation) == "") {
                data.adresseSiegeExploitation = testValue(data.adresseSiegeSocial._name);
            }
            renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
                "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
                testValue(data.nomCommercial) + "</td></tr>" +
                "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
                "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>" +
                "</tr><tr><td>Adrese de l'établissement</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
                "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>";
           
            
            //.origine.value
            if (typeof data.objetSocial == "object" && (data.objetSocial == ! null)) {
                renderHTML += "</tr><tr><td>Objet social</td><td>:</td><td>" + testValue(data.objetSocial) + "</td>";
            }

            renderHTML += "</tr><tr><td>Date de d&eacute;but d'exploitation</td><td>:</td><td>" + testValue(data.dateDebutExploitation) + "</td>";
            renderHTML += "</tr></tbody></table>";
            renderHTML += GetFormeJuridiqueHtml(data);
            renderHTML += GetSecteurActivite(data);

            renderHTML += GetInscriptionComplementaires(stringData);
            var renderHTML8 = GetPiedPageAccuseReceptionHtml();
            renderHTML8 += "</div>";

            $("#textToPrint").html(renderHTML + renderHTML8);
        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                //window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function inscription_complementaire_personne_morale(data) {

    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: GetValueModified,
        data: { entityId: entitiID, txId: 0 },
        success: function (dataModified) {
            var stringData = $.parseJSON(dataModified);


            // Début de la DIV principale
            var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

            // Insertion de l'en-tete
            renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));


            // Titre du document
            renderHTML += "<div style=\"height:30px;text-align: center;\">" +
                "<h2><b>ACCUSE DE RECEPTION</b></h2></div>";
            renderHTML += "<div  style=\"height:20px;\"></div>";

            renderHTML += "<div style=\"height:12px;text-align: justify;font-size: 12px;\">A la date ci-dessous, " +
                "le Guichet Unique de Création d'Entreprises accuse bonne réception de votre dossier pour la MODIFICATION " +
                                        " de votre entreprise dont la dénomination sociale est :</div>";

            renderHTML += "<div style=\"text-align: left;\"><h3> " + testValue(data.nomCommercial)
                + "</h3></div>";

            renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Numéro du dossier : &nbsp;&nbsp;&nbsp;&nbsp; <b>" +
                $(".label-warning").text() + "</b></div>";
            renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Avec les informations suivantes :</div>";

            renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE<hr/></b></div>";
            data = testAdresse(data);
            renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
               "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
               testValue(data.nomCommercial) + "</td></tr>" +
               "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
               "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>" +
               "</tr><tr><td>Capital</td><td>:</td><td>" + testValue(data.capitalSocial) + " Fc</td>" +
               "</tr><tr><td>Adrese du si&egrave;ge social</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
               "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>" +
               "</tr><tr><td>Date de d&eacute;but d'exploitation</td><td>:</td><td>" + formatDate(testValue(data.dateDebutExploitation)) + "</td>" +
               "</tr></tbody></table>";

            renderHTML += GetFormeJuridiqueHtml(data);
            renderHTML += GetSecteurActivite(data);
            if (stringData.length > 0) {
                renderHTML += "<div  style=\"height:20px;\"></div>";
                renderHTML += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX INSCRIPTIONS COMPLEMENTAIRES, AUX MODIFICATIONS, AUX DEPOTS D'ACTE<hr/></b></div>";

                renderHTML += "<table width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
                    "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"width:15%;\">Procédure</td><td style=\"width:15%;\">Ancienne valeur</td><td style=\"width:15%;\">Nouvelle valeur</td></tr></thead><tbody>";

                for (var ii = 0; ii < stringData.length; ii++) {

                    renderHTML += "<tr style=\"font-size: 12px;\"><td>" + testValue(stringData[ii].libele) + "</td><td>" +
                    testValue(stringData[ii].currentValue) + "</td><td>" +
                    testValue(stringData[ii].oldValue) + "</td></tr>";
                }
                renderHTML += "</tbody></table>";
            }
            var renderHTML8 = GetPiedPageAccuseReceptionHtml();

            renderHTML8 += "</div>";

            $("#textToPrint").html(renderHTML + renderHTML8);
        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                //window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function immatriculation_personne_physique(data) {
    // Début de la DIV principale
    var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

    // Insertion de l'en-tete
    renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));

    // Titre du document
    renderHTML += "<div style=\"height:50px;text-align: center;\">" +
        "<h2><b>FORMULAIRE UNIQUE</b></h2></div>";
    renderHTML += "<div style=\"height:40px;text-align: left; padding-left:211px;\">" +
      "(Valant accusé de réception)</div>";

    renderHTML += "<div style=\"height:12px;text-align: justify;font-size: 12px;\">A la date ci-dessous, " +
        "le Guichet Unique de Création d'Entreprises accuse bonne réception de votre dossier pour la " +
        (testValue(data.origine.value)).toUpperCase()
        + " d'entreprise dont la dénomination sociale est :</div>";

    renderHTML += "<div style=\"text-align: left;\"><h3> " + testValue(data.nomCommercial)
        + "</h3></div>";

    renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Numéro du dossier : &nbsp;&nbsp;&nbsp;&nbsp; <b>" +
        $(".label-warning").text() + "</b></div>";
    renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Avec les informations suivantes :</div>";

    renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE<hr/></b></div>";

    data = testAdresse(data);
    renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
        "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
        testValue(data.nomCommercial) + "</td></tr>" +
        "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
        "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>" +
        "</tr><tr><td>Adrese de l'établissement</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
        "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>";
   
    //.origine.value
    if (typeof data.objetSocial == "object" && (data.objetSocial !== null)) {
        renderHTML += "</tr><tr><td>Objet social</td><td>:</td><td>" + testValue(data.objetSocial) + "</td>";
    }

    if (data.emailSociete !== "" && data.emailSociete !== null) {
        renderHTML += "</tr><tr><td>Adresse Email</td><td>:</td><td>" + testValue(data.emailSociete) + "</td>";
    }

    if ((testValue(data.nbSalaries) != "" ||
        testValue(data.nbSalaries) != null)) {
        if (testValue(data.nbEnfants) != null || testValue(data.nbEnfants) != "") {
            data.nbEnfants = " avec au total " + testValue(data.nbEnfants) + " enfants";
        } else {
            data.nbSalaries = "";
        }
        renderHTML += "</tr><tr><td>Effectif salarié</td><td>:</td><td>" + testValue(data.nbSalaries) + " salariés " +
                    data.nbEnfants + "</td>";
    }
    renderHTML += "</tr></tbody></table>";
    renderHTML += GetFormeJuridiqueHtml(data);
    renderHTML += GetSecteurActivite(data);
    renderHTML += "<div  style=\"height:20px;\"></div>";
    var arrayToExclude = [
       "Adresse siege exploitation", "Sigle","Email societe",
        "Enseigne", "Duree", "Activite secondaire1","Activite secondaire2",
        "Nom commercial", "Debut exploitation"];
    var infoBase = GetInfoBaseHtml(data, arrayToExclude);
    // ajout des établissements secondaires
    var renderHtml1 = GetEtablissementSecondaireHtml(data);

    // ajout des dirigeants
    var renderHtml2 = GetDirigeantsHtml(data);

    // ajout des accociés
    var renderHtml3 = GetAssociesHtml(data);

    // ajout des manddaitaires
    var renderHtml4 = GetMandataireHtml(data);

    //Liste des documents scanné
    var renderHtml5 = "";//GetListeDocumentScanneHtml(data);

    var renderHtml6 = GetDocumentForAllNaturalPersonne(data);
    var renderHtml7 = GetDocumentForEntrepriseHtml(data);

    var renderHtml8 = GetPiedPageAccuseReceptionHtml();
    renderHtml8 += "</div>";
    $("#textToPrint").html(renderHTML + infoBase + renderHtml1 + renderHtml2 + renderHtml3 +
        renderHtml4 + renderHtml5 + renderHtml6 + renderHtml7 + renderHtml8);
   
    
}

function immatriculation_personne_moral(data) {
   
    // Début de la DIV principale
    var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

    // Insertion de l'en-tete
    renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));

    // Titre du document
    renderHTML += "<div style=\"height:30px;text-align: center;\">" +
        "<h2><b>FORMULAIRE UNIQUE</b></h2></div>";
    renderHTML += "<div style=\"height:40px;text-align: left; padding-left:211px;\">" +
      "(Valant accusé de réception)</div>";

    renderHTML += "<div style=\"height:12px;text-align: justify;font-size: 12px;\">A la date ci-dessous, " +
        "le Guichet Unique de Création d'Entreprises accuse bonne réception de votre dossier pour la " +
        (testValue(data.origine.value)).toUpperCase()
        + " d'entreprise dont la dénomination sociale est :</div>";

    renderHTML += "<div style=\"text-align: left;\"><h3> " + testValue(data.nomCommercial)
        + "</h3></div>";

    renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Numéro du dossier : &nbsp;&nbsp;&nbsp;&nbsp; <b>" +
        $(".label-warning").text() + "</b></div>";
    renderHTML += "<div style=\"height:25px;text-align: left;font-size: 12px;\">Avec les informations suivantes :</div>";

    renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE<hr/></b></div>";

    
    
    data = testAdresse(data);
    renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
        "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
           testValue(data.nomCommercial) + "</td></tr>" +
        "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
        "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>";

    renderHTML += "</tr></tr><tr><td>Capital social</td><td>:</td><td>" + testValue(data.capitalSocial) + " Fc</td>" +
        "</tr><tr><td>Adresse du si&egrave;ge social</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
        "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>";
   
   
    if (data.duree != "" || data.duree == null) {
        renderHTML += "</tr><tr><td>Durée de la societé</td><td>:</td><td>" + testValue(data.duree) + " ans</td>";
    }
    if ((testValue(data.nbSalaries) != "" ||
        testValue(data.nbSalaries) != null) ) {
        if (testValue(data.nbEnfants) != null || testValue(data.nbEnfants) != "") {
            data.nbEnfants = " avec au total " + testValue(data.nbEnfants) + " enfants";
        } else {
            data.nbSalaries = "";
        }
        renderHTML += "</tr><tr><td>Effectif salariés</td><td>:</td><td>" + testValue(data.nbSalaries) + " salariés "+
                    data.nbEnfants + "</td>";
    }
   
    renderHTML += "</tr></tbody></table>";
    renderHTML += GetFormeJuridiqueHtml(data);
    renderHTML += GetSecteurActivite(data);
    
    renderHTML += "<div  style=\"height:20px;\"></div>";
    var arrayToExclude = [
        "Adresse siege exploitation", "Sigle", "Activite secondaire2",
        "Capital social", "Enseigne", "Duree", "Nb enfants",
         "Nom commercial", "Debut exploitation"];
    var infoBase = GetInfoBaseHtml(data, arrayToExclude);
    // ajout des établissements secondaires
    var renderHtml1 = GetEtablissementSecondaireHtml(data);

    // ajout des dirigeants
    var renderHtml2 = GetDirigeantsHtml(data);

    // ajout des accociés
    var renderHtml3 = GetAssociesHtml(data);

    // ajout des manddaitaires
    var renderHtml4 = GetMandataireHtml(data);

    //Liste des documents scanné
    var renderHtml5 = "";//GetListeDocumentScanneHtml(data);

    var renderHtml6 = GetDocumentForAllNaturalPersonne(data);
    var renderHtml7 = GetDocumentForEntrepriseHtml(data);

    var renderHtml8 = GetPiedPageAccuseReceptionHtml();
    renderHtml8 += "</div>";

    $("#textToPrint").html(renderHTML + infoBase + renderHtml1 + renderHtml2 + renderHtml3 +
        renderHtml4 + renderHtml5 + renderHtml6 + renderHtml7 + renderHtml8 );

      
}

//function Acte_notaire() {
//    var entitiID = getUrlParameter("entityId");
//    var flowId = getUrlParameter("flowId");
//    $.ajax({
//        type: "POST",
//        url: findInfoNotaire,
//        data: { entityId: entitiID },
//        success: function (data) {
//            var dataNotaire = $.parseJSON(data);
//            var d = new Date();
//            //Generate current date
//            var month = d.getMonth() + 1;
//            var day = d.getDate();
//            var output = day + "/" +
//                (month < 10 ? "0" : "") + month + "/" +
//                (day < 10 ? "0" : "") + d.getFullYear();
//            $.ajax({
//                type: "POST",
//                url: GenerateDateToWord,
//                data: { dateToConvert: output },
//                success: function (dateConverted) {
//                    $.ajax({
//                        type: "POST",
//                        url: findMoreInfoNotaire,
//                        data: { entityId: entitiID },
//                        success: function (infoNotaire) {
//                            var moreInfo = $.parseJSON(infoNotaire);
//                            var moreInfoJson = $.parseJSON(moreInfo);
//                            $.ajax({
//                                type: "POST",
//                                url: FindFluxValues,
//                                data: { entityId: entitiID, flowId: flowId },
//                                success: function (infoFlux) {
//                                    var infoFluxString = $.parseJSON(infoFlux);
//                                    var formatText = infoFluxString.replace("}{", ",");
//                                    var infoFluxJson = $.parseJSON(formatText);
                                      // Début de la DIV principale
//                                    var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;margin:25px;\">";
//                                    // Insertion de l'en-tete
//                                    renderHTML += GetEnTeteActeNotarieHtml();
//                                    // Titre du document
//                                    renderHTML += "<div  style=\"height:5px;\"></div>";
//                                    renderHTML += "<div style=\"height:20px;text-align: center;\">" +
//                                        "<b><u><h4 style=\"text-align: center;\">ACTE NOTARIE No" + $(".label-warning").text()
//                                        + "</h4></u></b></div>";
//                                    renderHTML += "<div  style=\"height:15px;\"></div>";
//                                    renderHTML += "<div style=\"text-align: left;font-size: 12px;\"> " + dateConverted + " | " + getValueFromDataJson(dataNotaire, "_name") + "</div>";
//                                    // Document a notarier
//                                    var documentANotarier = "";
//                                    if (infoFluxJson.documentANotarier) {
//                                        for (var ii = 0; ii < infoFluxJson.documentANotarier.length; ii++) {
//                                            documentANotarier += infoFluxJson.documentANotarier[ii].value + ", ";
//                                        }
//                                    }
//                                    // autres document a notarier .autreDocumentANotarier
//                                    if (infoFluxJson.autreDocumentANotarier) {
//                                        for (var ii = 0; ii < infoFluxJson.autreDocumentANotarier.length; ii++) {
//                                            documentANotarier += infoFluxJson.autreDocumentANotarier[ii] + ", ";
//                                        }
//                                    }
//                                    // Ajout des comparants
//                                    var comparantActeNotarie1 = "";
//                                    var adresseComparant1 = "";//getValueFromDataJson(infoFluxJson, ".activitePrincipale.value");
//                                    if (infoFluxJson.comparantActeNotarie["0"] !== null  &&
//                                        infoFluxJson.comparantActeNotarie.length > 0) {
//                                        comparantActeNotarie1 = testValue(infoFluxJson.comparantActeNotarie["0"].prenom) + " " +
//                                                                                        testValue(infoFluxJson.comparantActeNotarie["0"].nom);
//                                        adresseComparant1 = testValue(infoFluxJson.comparantActeNotarie["0"].adresse._name);
//                                    }
//                                    var comparantActeNotarie2 = "";
//                                    var adresseComparant2 = "";
//                                    if (infoFluxJson.comparantActeNotarie["1"] !== null  && infoFluxJson.comparantActeNotarie.length > 1) {
//                                        comparantActeNotarie2 = testValue(infoFluxJson.comparantActeNotarie["1"].prenom) + " " +
//                                                                                        testValue(infoFluxJson.comparantActeNotarie["1"].nom);
//                                        adresseComparant2 = testValue(infoFluxJson.comparantActeNotarie["1"].adresse._name);
//                                    }
//                                    //// Ajout des temoins
//                                    var temoinActeNotarie1 = "";
//                                    var adresseTemoinActeNotarie1 = "";
//                                    var fonctionTemoinActeNotarie1 = "";
//                                    if (infoFluxJson.temoinActeNotarie1 !== null  ) {
//                                        temoinActeNotarie1 = testValue(infoFluxJson.temoinActeNotarie1.personneTemoin);
//                                        adresseTemoinActeNotarie1 = testValue(infoFluxJson.temoinActeNotarie1.villeTemoin);
//                                        fonctionTemoinActeNotarie1 = testValue(infoFluxJson.temoinActeNotarie1.fonctionTemoin);
//                                    }
//                                    var temoinActeNotarie2 = "";
//                                    var adresseTemoinActeNotarie2 = "";
//                                    var fonctionTemoinActeNotarie2 = "";
//                                    if (infoFluxJson.temoinActeNotarie2 !== null ) {
//                                        temoinActeNotarie2 = testValue(infoFluxJson.temoinActeNotarie2.personneTemoin);
//                                        adresseTemoinActeNotarie2 = testValue(infoFluxJson.temoinActeNotarie2.villeTemoin);
//                                        fonctionTemoinActeNotarie2 = testValue(infoFluxJson.temoinActeNotarie2.fonctionTemoin);
//                                    }
//                                    renderHTML += "<div  style=\"height:20px;\"></div>";
//                                    renderHTML += "<div style=\"text-align: justify;font-size: 15px;\"> Nous soussignés, " + testValue(moreInfoJson.notaire) +
//                                        ", " + testValue(moreInfoJson.fonctionNotaire) + " à l\'Office Notarial du Guichet Unique de Création " +
//                                    "d'Entreprise à " + (dataNotaire._ste_name).substring(4, dataNotaire._ste_name.length) + ", agissant conformément aux prescrits des articles 9," +
//                                    " 10 et 15 du Décret N⁰ 14/014 du 08 Mai 2014 portant Création, Organisation et Fonctionnement du Guichet Unique de Création d'Entreprise,  à l' " +
//                                    testValue(moreInfoJson.decretNominationNotaire).charAt(0).toLowerCase() +
//                                    testValue(moreInfoJson.decretNominationNotaire).slice(1) +
//                                    " portant Nomination des Notaires au Guichet Unique de Création d'Entreprise, ainsi qu'à l'Ordonnance n⁰ 66/344 du 9 juin 1966 relative aux actes notariés ; certifions que les documents ci-après:" +
//                                    testValue(documentANotarier) + " de la société " + dataNotaire.nomCommercial + " ayant son" +
//                                    " siège  situé sur " + dataNotaire.adresseSiegeSocial._name +
//                                    " , dont les clauses ci-dessous insérées nous ont été présentées ce jour, a " + (dataNotaire._ste_name).substring(4, dataNotaire._ste_name.length) +
//                                    " par " + comparantActeNotarie1 +
//                                    " , dûment mandaté, ayant son adresse professionnelle située sur " + adresseComparant1 + " ; comparaissant en personne" +
//                                    " en présence de " + temoinActeNotarie1 + ", " + fonctionTemoinActeNotarie1 +
//                                    ", résidant à " + adresseTemoinActeNotarie1 + ", et de " + temoinActeNotarie2 + ", " + fonctionTemoinActeNotarie2 +
//                                    ", résident à " + adresseTemoinActeNotarie2 + ", témoins instrumentaires à ce requis" +
//                                    " réunissant les conditions exigées par la loi en la matière; lecture du contenu de l'acte susmentionné a" +
//                                    " été faite par nous, tant au comparant qu'aux témoins " + temoinActeNotarie1 +
//                                    ", ci-dessus identifié et " + temoinActeNotarie2 + ", ci-dessus identifié (1).</div>";
//                                    renderHTML += "<div  style=\"height:20px;\"></div>";
//                                    renderHTML += "<div style=\"text-align: justify;font-size: 15px;\">Le comparant pré-qualifié persiste et signe devant témoins et nous que, l\'économie des documents à authentifier renferme " +
//                                    "bien l'expression de la volonté des signataires, qu'ils sont seuls responsables de toutes contestations pouvant" +
//                                    " naître de l'exécution dudit document sans évoquer la complicité de l'Office Notarial ainsi que du " + testValue(moreInfoJson.fonctionNotaire) + "." +
//                                    " En foi de quoi, le présent acte vient d'être signé par les comparants, témoins et nous, et revêtu" +
//                                    " du sceau de l\'Office Notarial du Guichet Unique de Création d\'Entreprises a " + (dataNotaire._ste_name).substring(4, dataNotaire._ste_name.length) + ".</div>";
//                                    renderHTML += "<div  style=\"height:15px;\"></div>";
//                                    renderHTML += "<div style=\"height:17px;font-size: 12px;font-weight: bold;\">" +
//                                                "<p style=\"width:250px;float:left;font-size: 12px;\"><u>SIGNATURE DU COMPARANT </u></p>" +
//                                                "<p style=\"width:180px;float:right;font-size: 12px;\"><u>SIGNATURE DU NOTAIRE</u></p></div>";
//                                    renderHTML += "<div  style=\"height:17px;\"></div>";
//                                    renderHTML += "<div style=\"height:20px;font-size: 12px;font-weight: bold;\">" +
//                                                "<p style=\"width:250px;float:left;font-size: 12px;margin-left:20px;\">" + comparantActeNotarie1 + "</p>" +
//                                                "<p style=\"width:180px;float:right;font-size: 12px;\">" + testValue(moreInfoJson.notaire) + "</p></div>";
//                                    renderHTML += "<div  style=\"height:15px;\"></div>";
//                                    renderHTML += "<div style=\"height:15px;font-size: 12px;font-weight: bold;\">" +
//                                                "<p style=\"text-align: center;\"><u>NOM ET SIGNATURE DES TEMOINS</u></p></div>";
//                                    renderHTML += "<div style=\"height:25px;\">" +
//                                                "<p style=\"width:250px;float:left;font-size: 12px;margin-left:20px;\">" + temoinActeNotarie1 + " </p>" +
//                                                "<p style=\"width:180px;float:right;font-size: 12px;\">" + temoinActeNotarie2 + "</p></div>";
//                                    renderHTML += "<div  style=\"height:15px;\"></div>";
//                                    renderHTML += "<div style=\"height:50px;text-align:justify;font-size: 11px;\">Droits perçu : Frais d\'acte de " +
//                                            testValue(infoFluxJson.fraisActeNotarie) + " Fc dont " + testValue(infoFluxJson.fraisAuthentification) +
//                                            "Fc pour l\'authentification(1). <br/>Suivant la note de perception no " +
//                                            testValue(infoFluxJson.numeroNotePerception) + " et ainsi" +
//                                         " que l'attestation de paiement no " + testValue(infoFluxJson.numeroOrdonnancement) +
//                                         " de ce jour. Enregistré par nous soussignés, ce " + dateConverted + " sous le " +
//                                         $(".label-warning").text() + "</div>";
//                                    renderHTML += "<div style=\"height:15px;font-size: 12px;font-weight: bold;\">" +
//                                                "<p style=\"width:180px;float:right;font-size: 12px;\"><u>SIGNATURE DU NOTAIRE</u></p></div>";
//                                    renderHTML += "<div  style=\"height:25px;\"></div>";
//                                    renderHTML += "<div style=\"height:20px;font-size: 12px;font-weight: bold;\">" +
//                                                "<p style=\"width:180px;float:right;font-size: 12px;\">" +
//                                                testValue(moreInfoJson.notaire) + "</p></div>" + remarque();
//                                    renderHTML += "</div>";
//                                    $("#textToPrint").html(renderHTML);
//                                },
//                                error: function (e, status) {
//                                    if (e.status == 500) {
//                                        alert("Erreur interne au serveur. ");
//                                    } else {
//                                        messageAlert("Erreur lors de la génération du rapport...");
//                                    }
//                                }
//                            });
//                        },
//                        error: function (e, status) {
//                            if (e.status == 500) {
//                                alert("Erreur interne au serveur. ");
//                            } else {
//                                messageAlert("Erreur lors de la génération du rapport...");
//                            }
//                        }
//                    });
//                },
//                error: function (e, status) {
//                    if (e.status == 500) {
//                        alert("Erreur interne au serveur.");
//                    } else {
//                        messageAlert("Erreur lors de la génération du rapport...");
//                    }
//                }
//            });
//        },
//        error: function (e, status) {
//            if (e.status == 500) {
//                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
//                window.location.replace(logoutUrl);
//            } else {
//                messageAlert("Erreur lors de la génération du rapport...");
//            }
//        }
//    });
//}

function Kbis_personne_physique(data) {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: GetHistoriqueEntreprise,
        data: { entityId: entitiID },
        success: function (data1) {
            var dataJson = $.parseJSON(data1);


            // Début de la DIV principale
            var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

            // Insertion de l'en-tete
            renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));

            // Titre du document
            renderHTML += "<div  style=\"height:30px;\"></div>";
            renderHTML += "<div style=\"height:30px;text-align: center;\">" +
                "<h4><b>Extrait du Registre du Commerce et du Crédit Mobilier</b></h4></div>";

            renderHTML += "<div  style=\"height:20px;\"></div>";
            renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>IDENTIFICATION DE L'ETABLISSEMENT<hr/></b></div>";

            data = testAdresse(data);
            renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
                "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
                testValue(data.nomCommercial) + "</td></tr>" +
                "<tr><td>Numéro RCCM</td><td>:</td><td>" + testValue(data.rccm) + "</td>" +
                "<tr><td>Date d\'\immatriculation RCCM</td><td>:</td><td>" + formatDate(testValue(data.dateImmatriculationRCCM)) + "</td>";
            //
            if (data.duree == null) {
                data.duree = "***";
            }
            renderHTML += "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
            "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>" +
            "</tr><tr><td>Durée</td><td>:</td><td>" + testValue(data.duree) + " ans</td>" +
            "</tr><tr><td>Adrese de l'établissement</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
            "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>" +
            "</tr><tr><td>Date de d&eacute;but d'exploitation</td><td>:</td><td>" + formatDate(testValue(data.dateDebutExploitation)) + "</td>" +
            "</tr></tbody></table>";

            renderHTML += GetFormeJuridiqueHtml(data);
            renderHTML += GetSecteurActivite(data);
            renderHTML += "<div  style=\"height:20px;\"></div>";
            renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE<hr/></b></div>";

            renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
               "<td style=\"width: 200px;\">Nom</td><td style=\"width: 10px;\">:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.nom) + "</td></tr>" +
               "<tr><td>Prenom</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.prenom) + "</td>" +
               "<tr><td>Date de naissance</td><td>:</td><td>" + formatDate(testValue(data.dirigeant["0"].personneDirigeant.dateNaissance)) + "</td>";

            renderHTML += "<tr><td>Adresse</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.adresse._name) + "</td>" +
            "</tr><tr><td>Nationalite</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.nationalite.value) + "</td>" +
            "</tr><tr><td>Situation matrimoniale</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.situationMatrimoniale.value) + "</td>";
            if ((testValue(data.dirigeant["0"].personneDirigeant.telephoneMobiles) != "")) {
                renderHTML += "</tr><tr><td>Numéro de telephone</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.telephoneMobiles) + "</td>";

            }

            renderHTML += "</tr></tbody></table>";

            var renderHTML1 = "";
            // Ajout des adresse pour un etablissement secondaire
            if (orderData.adressesEtablissementsSecondaires.length > 0) {
                renderHTML1 += "<div  style=\"height:20px;\"></div>";
                renderHTML1 += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A L\'ETABLISSEMENT SECONDAIRE OU SUCCURSALE<hr/></b></div>";
                renderHTML1 += "<table class=\"table table-striped table-bordered\"  border=\"1\" style=\"border-collapse: collapse;\"><tbody>";

                for (var ii = 0; ii < orderData.adressesEtablissementsSecondaires.length; ii++) {
                    renderHTML1 += "<tr style=\"font-size: 12px;\"><td>" + testValue(data.adressesEtablissementsSecondaires[ii]._name) + "</td></tr>";

                }
                renderHTML1 += "</tbody></table>";
            }



            // ajout des établissements secondaires
            var renderHTML1 = GetEtablissementSecondaireHtml(data);

            // ajout des dirigeants
            var renderHTML2 = GetDirigeantsHtml(data);

            // ajout des accociés
            var renderHTML3 = GetAssociesHtml(data);

            // ajout des manddaitaires
            var renderHTML4 = GetMandataireHtml(data);

            var renderHTML5 = GetEtablissementSecondaireHtml(data);

            var renderHTML6 = GetHistoriqueEntrepriseHtml(dataJson);

            var renderHTML7 = GetPiedPageExtraitRccmHtml();

            renderHTML1 += "</div>";
            $("#textToPrint").html(renderHTML + renderHTML1 + renderHTML2 + renderHTML3 + renderHTML4 + renderHTML5+ renderHTML6 +
                 renderHTML7);

        }, error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });

}

function Kbis_personne_morale(data) {
    // Début de la DIV principale
    var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

    // Insertion de l'en-tete
    renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));

    // Titre du document
    renderHTML += "<div  style=\"height:30px;\"></div>";
    renderHTML += "<div style=\"height:30px;text-align: center;\">" +
        "<h4><b>Extrait du Registre du Commerce et du Crédit Mobilier</b></h4></div>";

    renderHTML += "<div  style=\"height:20px;\"></div>";
    renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>IDENTIFICATION DE LA SOCIETE<hr/></b></div>";

    data = testAdresse(data);
    renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
        "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
        testValue(data.nomCommercial) + "</td></tr>" +
        "<tr><td>Numéro RCCM</td><td>:</td><td>" + testValue(data.rccm) + "</td>" +
        "<tr><td>Date d\'\immatriculation RCCM</td><td>:</td><td>" + testValue(data.dateImmatriculationRCCM) + "</td>";
    //
    renderHTML += "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
    "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>" +
    "</tr><tr><td>Adrese de l'établissement</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
    "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>" +
    "</tr><tr><td>Date de d&eacute;but d'exploitation</td><td>:</td><td>" + formatDate(testValue(data.dateDebutExploitation)) + "</td>" +
    "</tr></tbody></table>";

    renderHTML += "<div  style=\"height:20px;\"></div>";
    renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE<hr/></b></div>";

    renderHTML += GetFormeJuridiqueHtml(data);
    renderHTML += GetSecteurActivite(data);

    renderHTML += "<table  class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody>" +
        "<tr><td>Capital social</td><td>:</td><td>" + testValue(data.capitalSocial) + " Fc</td></tr>";
    if (data.duree != "" || data.duree == null) {
        renderHTML += "<tr><td>Durée de la societé</td><td>:</td><td>" + testValue(data.duree) + " ans</td></tr>";
    }
    renderHTML += "<tr><td style=\"width: 200px;\">Constitution - dépots de l\'acte constitutif</td><td style=\"width: 10px;\">:</td><td>" +
        testValue(data._ste_name) + "</td></tr>" +
     "</tbody></table>";

   
    // ajout des établissements secondaires
    var renderHTML1 = GetEtablissementSecondaireHtml(data);

    // ajout des dirigeants
    var renderHTML2 = GetDirigeantsHtml(data);

    // ajout des accociés
    var renderHTML3 = GetAssociesHtml(data);

    // ajout des manddaitaires
    var renderHTML4 = GetMandataireHtml(data);


    var renderHTML8 = GetPiedPageExtraitRccmHtml();
    renderHTML8 += "</div>";

    $("#textToPrint").html(renderHTML + renderHTML1 + renderHTML2 + renderHTML3 +
        renderHTML4 + renderHTML8);


}

//function Kbis() {
//    var entitiID = getUrlParameter("entityId");
//    $.ajax({
//        type: "POST",
//        url: findEntitieUrl,
//        data: { entitiId: entitiID },
//        success: function (data) {
//            var stringData = $.parseJSON(data);
//            orderData = $.parseJSON(stringData);
//            if (orderData.formeJuridiqueN1.code == "B") {
//                Kbis_personne_morale(orderData);
//                PrintMo(orderData); 
//            } else if (orderData.formeJuridiqueN1.code == "A") {
//                Kbis_personne_physique(orderData);
//            }
//        },
//        error: function (e, status) {
//            if (e.status == 500) {
//                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
//                window.location.replace(logoutUrl);
//            } else {
//                messageAlert("Erreur lors de la génération du rapport...");
//            }
//        }
//    });
//}

function Kbis_modif_personne_physique(data) {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: GetHistoriqueEntreprise,
        data: { entityId: entitiID },
        success: function (data1) {
            var dataJson = $.parseJSON(data1);

            $.ajax({
                type: "POST",
                url: GetValueModified,
                data: { entityId: entitiID, txId: 0 },
                success: function (dataModified) {
                    var stringData = $.parseJSON(dataModified);


            // Début de la DIV principale
            var renderHTML = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";

            // Insertion de l'en-tete
            renderHTML += GetEnTeteAccuseReceptionHtml(testValue(data._ste_name));

            // Titre du document
            renderHTML += "<div  style=\"height:30px;\"></div>";
            renderHTML += "<div style=\"height:30px;text-align: center;\">" +
                "<h4><b>Extrait du Registre du Commerce et du Crédit Mobilier</b></h4></div>";

            renderHTML += "<div  style=\"height:20px;\"></div>";
            renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>IDENTIFICATION DE L'ETABLISSEMENT<hr/></b></div>";

            data = testAdresse(data);
            renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
                "<td style=\"width: 200px;\">D&eacute;nomination social</td><td style=\"width: 10px;\">:</td><td>" +
                testValue(data.nomCommercial) + "</td></tr>" +
                "<tr><td>Numéro RCCM</td><td>:</td><td>" + testValue(data.rccm) + "</td>" +
                "<tr><td>Date d\'\immatriculation RCCM</td><td>:</td><td>" + formatDate(testValue(data.dateImmatriculationRCCM)) + "</td>";
                    //
            if (data.duree == null) {
                data.duree = "***";
            }
            renderHTML += "<tr><td>Enseigne</td><td>:</td><td>" + testValue(data.enseigne) + "</td>" +
            "</tr><tr><td>Sigle</td><td>:</td><td>" + testValue(data.sigle) + "</td>" +
            "</tr><tr><td>Durée</td><td>:</td><td>" + testValue(data.duree) + " ans</td>" +
            "</tr><tr><td>Adrese de l'établissement</td><td>:</td><td>" + testValue(data.adresseSiegeSocial._name) + "</td>" +
            "</tr><tr><td>Adresse du si&egrave;ge d'exploitation</td><td>:</td><td>" + testValue(data.adresseSiegeExploitation) + "</td>" +
            "</tr><tr><td>Activit&eacute; principale</td><td>:</td><td>" + testValue(data.activitePrincipale.value) + "</td>" +
            "</tr><tr><td>Date de d&eacute;but d'exploitation</td><td>:</td><td>" + testValue(data.dateDebutExploitation) + "</td>" +
            "</tr></tbody></table>";

            renderHTML += "<div  style=\"height:20px;\"></div>";
            renderHTML += "<div style=\"text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE<hr/></b></div>";

            renderHTML += "<table class=\"table table-striped table-bordered\"  style=\"font-size: 12px;\"><tbody><tr>" +
               "<td style=\"width: 200px;\">Nom</td><td style=\"width: 10px;\">:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.nom) + "</td></tr>" +
               "<tr><td>Prenom</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.prenom) + "</td>" +
               "<tr><td>Date de naissance</td><td>:</td><td>" + formatDate(testValue(data.dirigeant["0"].personneDirigeant.dateNaissance)) + "</td>";

            renderHTML += "<tr><td>Adresse</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.adresse._name) + "</td>" +
            "</tr><tr><td>Nationalité</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.nationalite.value) + "</td>" +
            "</tr><tr><td>Situation matrimoniale</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.situationMatrimoniale.value) + "</td>";
            if ((testValue(data.dirigeant["0"].personneDirigeant.telephoneMobiles) != "")) {
                renderHTML += "</tr><tr><td>Numéro de telephone</td><td>:</td><td>" + testValue(data.dirigeant["0"].personneDirigeant.telephoneMobiles) + "</td>";

            }
            renderHTML += "</tr></tbody></table>";

            var renderHTML1 = "";
           

            // ajout des inscriptions complementaires
             renderHTML1 += GetInscriptionComplementaires(stringData);

            // ajout des dirigeants
            var renderHTML2 = GetDirigeantsHtml(data);

            // ajout des accociés
            var renderHTML3 = GetAssociesHtml(data);

            // ajout des manddaitaires
            var renderHTML4 = GetMandataireHtml(data);

            var renderHTML5 = GetEtablissementSecondaireHtml(data);

            var renderHTML6 = GetHistoriqueEntrepriseHtml(dataJson);

            var renderHTML7 = GetPiedPageExtraitRccmHtml();

            renderHTML1 += "</div>";
            $("#textToPrint").html(renderHTML + renderHTML1 + renderHTML2 + renderHTML3 + renderHTML4 + renderHTML5 + renderHTML6 +
                 renderHTML7);

                }, error: function (e, status) {
                    if (e.status == 500) {
                        alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                        window.location.replace(logoutUrl);
                    } else {
                        messageAlert("Erreur lors de la génération du rapport...");
                    }
                }
            });
        }, error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });


}

function Kbis_modif_personne_morale(data) {
    Kbis_personne_morale(data);
}

function GetEnTeteActeNotarieHtml() {
    var enTete = "";

    enTete += "<div style=\"height:140px;margin:5px;font-size: 12px;font-weight: bold;\"><div style=\"float:left;width:300px\">" +
        "&nbsp;&nbsp;&nbsp;République Démocratique du Congo <br/>" +
        "Ministère de la Justice et Droit Humains<br/>" +
        "<img  style=\"float:left;margin-left:80px;width:70px;height:70px;\" src=\"" +
        drapeauUrl + "\"/><br/><br/><br/><br/><br/>Guichet Unique de Création d'Entreprise" +
        "<br/><u style=\"font-weight:bold;margin-left: 62px;\">OFFICE NOTARIAL</u></div>" +
        "<div><img  style=\"float:right;width:120px;height:120px;margin-top:20px;\" src=\"" + logoUrl + "\"/></div></div>";
    return enTete;
}

function GetEnTeteAccuseReceptionHtml(juridiction) {
    var enTete = "";
    enTete += "<div style=\"height:50px;margin:5px;font-size: 12px;font-weight: bold;\"><div style='float:left;width:300px;'>" +
        "<p style='margin-left:50px;'>Antenne de " + juridiction +
        "</p>Registre du Commerce et du Crédit Mobilier (RCCM)</div><div><img style=\"float:right;width:90px;height:90px;margin-top:-8px;\" src=\"" +
        logoUrl + "\"/></div></div>";
    return enTete;
}

/*
 
function GetDirigeantsHtml1(data) {
    // ajout des dirigeants
    var renderHtml = "";

    if (data.dirigeant.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX DIRIGEANTS<hr/></b></div>";
        renderHtml += "<table width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
            "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"width:30%;\">Information principales</td><td style=\"width:70%;\">Autres informations</td></tr></thead><tbody>";

        for (var ii = 0; ii < data.dirigeant.length; ii++) {

            var dataToPrint = "";
            var dataString = GetLibeleValueFromJson(data.dirigeant[ii]);
            for (var i = 0; i < dataString.length; i++) {
                dataToPrint += dataString[i][0] + " : <b>" + dataString[i][1] + "</b> | ";
            }
            renderHtml += "<tr style=\"font-size: 12px;\"><td>" + testValue(data.dirigeant[ii]._name) + "</td><td>" +
             dataToPrint + "</td></tr>";
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}
*/

function GetDirigeantsHtml(data) {
    // ajout des dirigeants
    var renderHtml = "";

    if (data.dirigeant.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX DIRIGEANTS<hr/></b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
            "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\">" +
            "<td style=\"width:30%;\">Dirigeants</td><td style=\"width:70%;\">Autres informations</td></tr></thead><tbody>";

        renderHtml += constructDataTable(data.dirigeant);
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function constructDataTable(data) {
    var renderHtml = "";
    for (var ii = 0; ii < data.length; ii++) {

        var dataString = GetLibeleValueFromJson(data[ii]);
        var eeee = "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police + 
            "border-collapse: collapse;\">" +
      "<tbody>";

        for (var i = 0; i < dataString.length; i++) {
            if (dataString[i][0].indexOf("deja declare") !== -1) {
                dataString[i][0] = dataString[i][0].replace("deja declare", "déjà déclaré");
            }
            if (dataString[i][0].indexOf("adresseRD c") !== -1) {
                dataString[i][0] = dataString[i][0].replace("adresseRD c", "adresse en RDC");
            }
            if (dataString[i][0].indexOf("nationalite") !== -1) {
                dataString[i][0] = dataString[i][0].replace("nationalite", "nationalité");
            }
            if (dataString[i][0].indexOf("no titre sejour") !== -1) {
                dataString[i][0] = dataString[i][0].replace("no titre sejour", "numéro de titre de sejour");
            } 
            if (dataString[i][0].indexOf("telephone") !== -1) {
                dataString[i][0] = dataString[i][0].replace("telephone", "téléphone");
            }
            if (dataString[i][0].indexOf("debut") !== -1) {
                dataString[i][0] = dataString[i][0].replace("debut", "début");
            }
            if (dataString[i][0].indexOf("substitue") !== -1) {
                dataString[i][0] = dataString[i][0].replace("substitue", "substitué");
            }
            if (dataString[i][0].indexOf("role") !== -1) {
                dataString[i][0] = dataString[i][0].replace("role", "rôle");
            }
            if (dataString[i][0].indexOf("prenom") !== -1) {
                dataString[i][0] = dataString[i][0].replace("prenom", "prénom");
            }
            if (dataString[i][0].indexOf("etranger") !== -1) {
                dataString[i][0] = dataString[i][0].replace("etranger", "étranger");
            }
            if (dataString[i][0].indexOf("duree") !== -1) {
                dataString[i][0] = dataString[i][0].replace("duree", "durée");
            }
            if (dataString[i][0].indexOf("validite titre sejour") !== -1) {
                dataString[i][0] = dataString[i][0].replace("validite titre sejour", "validité titre sejour");
            }
            if (dataString[i][0].indexOf("civilite") !== -1) {
                dataString[i][0] = dataString[i][0].replace("civilite", "civilité");
            }
            if (dataString[i][0].indexOf("dirigeant entite representee") !== -1) {
                dataString[i][0] = dataString[i][0].replace("dirigeant entite representee", "dirigeant entité representée");
            }
            if (dataString[i][0].indexOf("déjà déclaré raison") !== -1) {
                dataString[i][0] = dataString[i][0].replace("déjà déclaré raison", "déjà déclaré dénomination");
            } 
            if (dataString[i][0].indexOf("duree mandat dirigeant") !== -1 || dataString[i][0].indexOf("duree manda") !== -1) {
                dataString[i][1] = dataString[i][1] + " ans";
            }
            if (dataString[i][1] == "true") {
                dataString[i][1] = " renseigné, voir document numerisé";
            }
            if (dataString[i][1] == "false") {
                dataString[i][1] = " non numerisé";
            }
            eeee += "<tr style=\"font-size: 12px;width:55%;\"><td style=\"width:40%;\">" + dataString[i][0] + "</td><td>:&nbsp;&nbsp;&nbsp;&nbsp;" +
                          dataString[i][1] + "</td></tr>";
        }
        eeee += "</tbody></table>";
        renderHtml += "<tr style=\"font-size: 12px;\"><td valign=\"top\">" + testValue(data[ii]._name) + "</td><td>" +
         eeee + "</td></tr>";
    }
    return renderHtml;
}

/*
function GetDirigeantsHtml(data) {
    // ajout des dirigeants
    var renderHtml = "";

    if (data.dirigeant.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX DIRIGEANTS<hr/></b></div>";
        renderHtml += "<table width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
            "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"width:15%;\">Nom</td><td style=\"width:15%;\">Postnom</td><td style=\"width:15%;\">Date naissance</td><td style=\"width:15%;\">Fonction</td><td style=\"width:40%;\">Adresse</td></tr></thead><tbody>";

        for (var ii = 0; ii < data.dirigeant.length; ii++) {

            renderHtml += "<tr style=\"font-size: 12px;\"><td>" + testValue(data.dirigeant[ii].personneDirigeant.nom) + "</td><td>" +
            testValue(data.dirigeant[ii].personneDirigeant.prenom) + "</td><td>" +
            formatDate(testValue(data.dirigeant[ii].personneDirigeant.dateNaissance)) + "</td><td>" +
            testValue(data.dirigeant[ii].fonction) + "</td><td>" +
            testValue(data.dirigeant[ii].personneDirigeant.adresse._name) + "</td></tr>";
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}
*/

function GetAssociesHtml(data) {
    var renderHtml = "";
    if (data.associe.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT<hr/></b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
             "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\">" +
            "<td style=\"width:30%;\">Associés</td><td style=\"width:70%;\">Autres informations</td></tr></thead><tbody>";

        renderHtml += constructDataTable(data.associe);
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function GetMandataireHtml(data) {
    // ajout des mandataire
    var renderHtml = "";
    if (data.mandataireSociete.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX MANDATAIRES<hr/></b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
            "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\">" +
            "<td style=\"width:30%;\">Mandataires</td><td style=\"width:70%;\">Autres informations</td></tr></thead><tbody>";

        renderHtml += constructDataTable(data.mandataireSociete);
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function GetHistoriqueEntrepriseHtml(stringData) {
    var renderHTML = "";
    if (stringData.length > 0) {
        renderHTML += "<div  style=\"height:20px;\"></div>";
        renderHTML += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX INSCRIPTIONS COMPLEMENTAIRES, AUX MODIFICATIONS, AUX DEPOTS D'ACTE<hr/></b></div>";
        for (var i = 0; i < stringData.length - 1; i++) {
            var d = new Date(new Date(parseInt(testValue(stringData[i].DateModification).substr(6))));
            var stringaDate = d.getUTCDay() + "-" + d.getUTCMonth() + "-" + d.getFullYear();

            renderHTML += "<div  style=\"height:10px;\"></div>";
            renderHTML += "<div style=\"" + police + "text-align: left;font-size: 13px;\"><b>" + stringaDate + "</b></div>";
            renderHTML += "<table width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
            "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"width:30%;\">Procédure</td><td style=\"width:70%;\">Valeur</td></tr></thead><tbody>";
            
            for (var ii = 0; ii < stringData[i].Modificatioins.length; ii++) {
                var testValuePdf = testValue(stringData[i].Modificatioins[ii].deferenced_value);

                if ((testValuePdf.indexOf(".pdf") !== -1) && (testValuePdf.indexOf("pdf;md5=") !== -1)) {
                    testValuePdf = "Document scanné fournis";
                }

                renderHTML += "<tr style=\"font-size: 12px;\"><td>" + testValue(stringData[i].Modificatioins[ii].field_display_name) + "</td><td>" +
                testValuePdf + "</td></tr>";
            }
            renderHTML += "</tbody></table>";
        }
    }
    return renderHTML;
}

function GetEtablissementSecondaireHtml(data) {
    //..adressesEtablissementsSecondaires .adressesEtablissementsSecondaires["0"]._ste_name
    var renderHtml = "";
    if (data.adressesEtablissementsSecondaires.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX ETABLISSEMENTS SECONDAIRES, SUCCURSALES OU FILIALES<hr/></b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\">" +
            "<thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"width:50%;\">Institutions</td><td style=\"width:50%;\">Adresse</td></tr></thead><tbody>";

        for (var ii = 0; ii < data.adressesEtablissementsSecondaires.length; ii++) {
            renderHtml += "<tr style=\"font-size: 12px;\"><td>" + testValue(data.adressesEtablissementsSecondaires[ii]._ste_name) +
                "</td><td>" + testValue(data.adressesEtablissementsSecondaires[ii]._name) + "</td></tr>";
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function GetCommissaireAuxComptesHtml(data) {
    //.commissaireAuxComptes
    var renderHtml = "";
    if (data.mandataireSociete.length > 0) {
        renderHtml += "<div  style=\"height:20px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AUX COMMISSAIRE AUX COMPTES<hr/></b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police + "border-collapse: collapse;\"><tbody>";

        for (var ii = 0; ii < data.commissaireAuxComptes.length; ii++) {
            renderHtml += "<tr style=\"font-size: 12px;\"><td>" + testValue(data.commissaireAuxComptes[ii]) + "</td><td>";
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function GetFormeJuridiqueHtml(data) {
    // ajout des dirigeants
    var renderHtml = "";

    renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"0\" style=\"" + police + "border-collapse: collapse;font-size: 12px;\">" +
            "<tbody>";
    var textToPrint = "";
    textToPrint = GetFormeJuridique(data);
    renderHtml += "</tr><tr><td style=\"width: 200px;\">Forme Juridique</td><td style=\"width: 10px;\">:</td><td>" + textToPrint + "</td>" + "</td></tr>";
    renderHtml += "</tbody></table>";
    return renderHtml;
}

function GetFormeJuridique(data) {
    var textToPrint = "";
    if (getValueFromDataJson(data, "formeJuridiqueN1") != "***" &&
        getValueFromDataJson(data, "formeJuridiqueN2.value") != "***" &&
        getValueFromDataJson(data, "formeJuridiqueN3.value") != "***") {

        textToPrint += getValueFromDataJson(data, "formeJuridiqueN3.value");

    } else if (getValueFromDataJson(data, "formeJuridiqueN1") != "***" &&
        getValueFromDataJson(data, "formeJuridiqueN2.value") != "***" &&
        getValueFromDataJson(data, "formeJuridiqueN3.value") == "***") {

        textToPrint += getValueFromDataJson(data, "formeJuridiqueN1.value") + ", ";
        textToPrint += getValueFromDataJson(data, "formeJuridiqueN2.value");

    } else {
        textToPrint += getValueFromDataJson(data, "formeJuridiqueN1.value");
    }
    return textToPrint;
}

function GetSecteurActivite(data) {
    // ajout des dirigeants
    var renderHtml = "";
    
    renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"0\" style=\"" + police + "border-collapse: collapse;font-size: 12px;\">" +
            "<tbody>";
    var textToPrint = "";
    if (data.secteurActivite != null) {
        renderHtml += "</tr><tr><td style=\"width: 200px;\">Secteur d'Activité principale</td><td style=\"width: 10px;\">:</td><td>" +
               testValue(data.secteurActivite.value) + "</td>" + "</td></tr>";
    }
    if (data.activitePrincipale != null) {
        renderHtml += "</tr><tr><td style=\"width: 200px;\">Activité 1</td><td style=\"width: 10px;\">:</td><td> " +
               testValue(data.activitePrincipale.value) + "</td>" + "</td></tr>";
    }

    if (data.activiteSecondaire1 != null ) {
        renderHtml += "</tr><tr><td style=\"width: 200px;\">Activité 2</td><td style=\"width: 10px;\">:</td><td>" +
               testValue(data.activiteSecondaire1.value) + "</td>" + "</td></tr>";
    }

    if (data.activiteSecondaire2 != null) {
        renderHtml += "</tr><tr><td style=\"width: 200px;\">Activité accessoires-</td><td style=\"width: 10px;\">:</td><td>" +
               testValue(data.activiteSecondaire2.value) + "</td>" + "</td></tr>";
    }
    renderHtml += "</tbody></table>";
    return renderHtml;
}

function GetInfoBaseHtml(jsonData,elementToExclude) {

    var Document = GetLibeleValueFromJsonFirstLevel(jsonData);
   
    Document.sort();

    var render = "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"0\" style=\"" + police + "border-collapse: collapse;\"><tbody><tr>";
    for (var i = 0; i < Document.length; i++) {
        
        var print = Document[i];
        var libelle = testValue(print[0]).charAt(0).toUpperCase() + testValue(print[0]).slice(1);
        if (testValue(print[1]) != "false" && testValue(print[1]) != "true") {
            var test = true;
            for (var a = 0; a < elementToExclude.length; a++) {
                if (elementToExclude[a] == libelle) {
                    test = false;
                }
            }
            if (test) {
                 render += "</tr><tr style=\"font-size: 12px;\"><td style=\"width: 200px;\">" +
                testValue(print[0]).charAt(0).toUpperCase() + testValue(print[0]).slice(1) +
                "</td><td>:&nbsp;&nbsp;&nbsp;&nbsp;" +
                        testValue(print[1]) + "</td>";
            }
        }
    }
    render += "</tbody></table>";
   return render;
}

function GetDocumentForEntrepriseHtml(jsonData) {

    // Parcours Data
    var listOfProperty = GetListOfPropertyFromJon(jsonData);
    var scannedDocument = [];

    for (var i = 0; i < listOfProperty.length; i++) {
        if (listOfProperty[i].indexOf(".pdf") !== -1) { // Vérifier s'il contient .pdf
            var proprety = listOfProperty[i];
            var tabResult = proprety.split("||");

            var result = tabResult[0];
            var first = result.split(".");
            if (first[1].indexOf("pdf") !== -1) {
                result = result.substring(4, result.length);
                var resultToPrint = result;
                var length = result.length;
                for (var j = 0; j < length; j++) {
                    length = result.length;
                    if (result.charAt(j) !== result.charAt(j).toLowerCase()) {
                        var a = result.charAt(j).toString();
                        var b = " " + result.charAt(j).toLowerCase().toString();
                        result = result.replace(a, b);
                    }
                }
                //r c c m
                if (result.indexOf("r c c m") !== -1) {
                    result = result.replace("r c c m", "RCCM");
                }
                if (result.indexOf("p v c a") !== -1) {
                    result = result.replace("p v c a", "PVCA");
                }
                if (result.indexOf("p v a g e") !== -1) {
                    result = result.replace("p v a g e", "PV AGE");
                }
                if (result.indexOf("p v a g o") !== -1) {
                    result = result.replace("p v a g o", "PV AGO");
                }
                if (result.indexOf("i n s s") !== -1) {
                    result = result.replace("i n s s", "INSS");
                }   
                if (result.indexOf("l iberation") !== -1) {
                    result = result.replace("l iberation", "libération");
                }
                 if (result.indexOf("modifies") !== -1) {
                    result = result.replace("modifies", "modifiés");
                }
                if (result.indexOf("n r c") !== -1) {
                    result = result.replace("n r c", "NRC");
                }
                if (result.indexOf("i n p p") !== -1) {
                    result = result.replace("i n p p", "INPP");
                } 
                if (result.indexOf("creation") !== -1) {
                    result = result.replace("creation", "création");
                }
                if (result.indexOf("impot") !== -1) {
                    result = result.replace("impot", "impôt");
                }
                if (result.indexOf("numero") !== -1) {
                    result = result.replace("numero", "numéro");
                } 
                 if (result.indexOf("acte de depot") !== -1) {
                    result = result.replace("acte de depot", "acte de dépôt");
                }
                if (result.indexOf("piece") !== -1) {
                    result = result.replace("piece", "pièce");
                }
                if (result.indexOf("statuts signes") !== -1) {
                    result = result.replace("statuts signes", "statuts signés");
                }
                if (tabResult[1] == "***") {
                    result += "(non fournis)";
                } else {
                    result = "<b><u>"+result +"</u></b>";
                }
        
                scannedDocument.push(result);
            }
        }
    }

    var renderHtml = "";

    // Document Entreprise

    var renderEntreprisetHtml = "";

    if (scannedDocument.length > 0) {
        renderHtml += "<div  style=\"height:10px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 13px;\"><b>Documents de l'entreprise</b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police +
"border-collapse: collapse;\"><thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td>Documents numérisés</td></tr></thead><tbody>";

        var documents = "";
        for (var v = 0; v < scannedDocument.length; v++) {
            documents += scannedDocument[v] + ", ";
        }
        renderEntreprisetHtml += "<tr style=\"font-size: 12px;\"><td>" + documents + "</td></tr>";
        renderHtml += renderEntreprisetHtml;
        renderHtml += "</tbody></table>";
    } else {
        renderHtml = "";
    }

    return renderHtml;
}

function GetDocumentForAllNaturalPersonne(data) {

    var renderHtml = "";
    renderHtml += "<div  style=\"height:20px;\"></div>";
    renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>LISTE DES DOCUMENTS FOURNIS<hr/></b></div>";

    // Dirigeant
    if (data.dirigeant.length > 0) {
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 13px;\"><b>Dirigeants</b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police +
"border-collapse: collapse;\"><thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"font-size: 12px;width:21%;\">Dirigeant</td><td>Documents numérisés</td></tr></thead><tbody>";

        for (var ii = 0; ii < data.dirigeant.length; ii++) {
            var renderDirigeantHtml = "";
            renderDirigeantHtml += "<tr style=\"font-size: 12px;width:80px;\"><td>" + testValue(data.dirigeant[ii].personneDirigeant.nom) + " " +
                testValue(data.dirigeant[ii].personneDirigeant.prenom) + "</td>";
            var scannedDocument = GetDocumentForEntity(data.dirigeant[ii]);
            if (scannedDocument.length > 0) {
                var documents = "";
                for (var v = 0; v < scannedDocument.length; v++) {
                    documents += scannedDocument[v] + ", ";
                }
                renderDirigeantHtml += "<td>" + documents + "</td></tr>";
            } else {
                renderDirigeantHtml = "";
            }
            renderHtml += renderDirigeantHtml;
        }
        renderHtml += "</tbody></table>";
    }

    // Mandaitaires
    if (data.mandataireSociete.length > 0) {
        renderHtml += "<div  style=\"height:10px;\"></div>";
        renderHtml += "<div style=\"" + police + "text-align: left;font-size: 13px;\"><b>Mandataires</b></div>";
        renderHtml += "<table class=\"table table-striped table-bordered\" width=\"100%\" border=\"1\" style=\"" + police +
"border-collapse: collapse;\"><thead style=\"margin:0;padding:0;\"><tr style=\"font-size: 12px;\"><td style=\"font-size: 12px;width:21%;\">Mandataires</td><td>Documents numérisés</td></tr></thead><tbody>";

        for (var ii = 0; ii < data.mandataireSociete.length; ii++) {
            var rendermandataireSocietetHtml = "";
            rendermandataireSocietetHtml += "<tr style=\"font-size: 12px;\"><td>" + testValue(data.mandataireSociete[ii].personneMandataire.nom) + " " +
                testValue(data.mandataireSociete[ii].personneMandataire.prenom) + "</td>";
            var scannedDocument1 = GetDocumentForEntity(data.mandataireSociete[ii]);
            if (scannedDocument1.length > 0) {
                var documents1 = "";
                for (var v = 0; v < scannedDocument1.length; v++) {
                    documents1 += scannedDocument1[v] + ", ";
                }
                rendermandataireSocietetHtml += "<td>" + documents1 + "</td></tr>";
            } else {
                rendermandataireSocietetHtml = "";
            }
            renderHtml += rendermandataireSocietetHtml;
        }
        renderHtml += "</tbody></table>";
    }
    return renderHtml;
}

function GetPiedPageAccuseReceptionHtml() {
    var renderHtml = "";
    renderHtml += "<div style=\"height:30px;\"></div><p style=\"" + police +
"width:250px;float:left;font-size: 12px;\">Signature du requérant </p>";

    var d = new Date();
    //Generate current date
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = day + "/" +
        (month < 10 ? "0" : "") + month + "/" +
        (day < 10 ? "0" : "") + d.getFullYear();
    renderHtml += "<p style=\"" + police + "width:180px;float:right;font-size: 12px;\">Fait à Kinshasa, le  " + output + "</p>" + remarque() ;

    return renderHtml;

}

function GetPiedPageExtraitRccmHtml() {
    var renderHtml = "";
    var d = new Date();
    //Generate current date
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = day + "/" +
        (month < 10 ? "0" : "") + month + "/" +
        (day < 10 ? "0" : "") + d.getFullYear();


    renderHtml += "<div  style=\"height:20px;\"></div>";
    renderHtml += "<div style=\"" + police + "text-align: left;font-size: 15px;\"><b>RENSEIGNEMENTS RELATIFS AU SURETES<hr/></b></div>";
    renderHtml += "<div style=\"" + police + "text-align: left;font-size: 12px;\">NEANT (Non déclaré sur le système informatique RCCM au " +
        output + ") </div>";


    renderHtml += "<div style=\"height:100px;\"></div><div style=\"" + police +
        "float:left;font-size: 12px;\">Greffier RCCM </div><div style=\"" + police +
        "float:right;font-size: 12px;\">Fait a Kinshasa, le  " + output + "</div>";
    renderHtml += "<div style=\"height:30px;\"></div><div style=\"height:30px;" + police +
        "float:left;font-size: 12px;\">M______________________________</div>" + remarque() ;

    return renderHtml;
}

//------------------ UTILITAIRES------------------------------

function getValueFromDataJson(data, stringData) {
    data = GetListOfPropertyFromJon(data);
    var valuetoReturn = "";
    for (var i = 0; i < data.length; i++) {
        var proprety = data[i];
        var tabResult = proprety.split("||");

        var result = tabResult[0];
        var first = result.split(".");
        var first1 = stringData.split(".");
        if (first[1] == first1[0]) {
            for (var j = 0; j < first1.length; j++) {
                if (first[j+1] === first1[j] && j == first1.length - 1) {
                    valuetoReturn = tabResult[1];
                }
            }
        }
    }
    if (valuetoReturn == "false") {
        return "NON";
    }else if (valuetoReturn == "true") {
        return "OUI";
    } else {
        return valuetoReturn;
    }
}

function getLenghtOfArryFromDataJson(data, stringEntitie) {
    data = GetListOfPropertyFromJon(data);
    var arrayOfEntities = [];

    // Parcours all data
    for (var i = 0; i < data.length; i++) {
        var proprety = data[i];
        var tabResult = proprety.split("||");

        var result = tabResult[0];
        var first = result.split(".");
        var first1 = stringEntitie.split(".");
        if (first[1] == first1[0]) {
            for (var j = 0; j < first1.length; j++) {
                if (first[j + 1] === first1[j] && j == first1.length - 1) {
                    var finded = first[j + 2];
                    if (arrayOfEntities.indexOf(finded) == -1) {
                        arrayOfEntities.push(finded);
                    }
                }
            }
        }
    }
    return arrayOfEntities.length;
}

function getUrlParameter(sParam) {
    var sPageUrl = decodeURIComponent(window.location.search.substring(1)),
        sUrlVariables = sPageUrl.split("&"),
        sParameterName,
        i;

    for (i = 0; i < sUrlVariables.length; i++) {
        sParameterName = sUrlVariables[i].split("=");

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function remarque() {
    return "<div style=\"height:50px;font-size: 10px;\"></div><p style=\"" + police +
        "font-size: 11px;\"><i> Le présent document a été établi sur base des articles 50 et 66 de"+
        " l’AUDCG prévoyant un contrôle à posteriori à intervenir dans les trois mois à compter de la"+
        " date de l’immatriculation au RCCM. Sous réserve des erreurs à déceler qui pourront faire l’objet"+
        " de modification d’office.</i></p>";

}

function testAdresse(data) {
    if (testValue(data.adresseSiegeExploitation) == "") {
        data.adresseSiegeExploitation = testValue(data.adresseSiegeSocial._name);
    }
    return data;
}

function formatDate(dateToFormat) {
    var tab = dateToFormat.split("-");
    var temp = tab[0];
    tab[0] = tab[2];
    tab[2] = temp;
    return tab[0] + "-" + tab[1] + "-" + tab[2];
}

function testValue(value) {
    if (typeof value === "object") {
        if (value != null && value.hasOwnProperty("value")) {
            return value.value;
        } else if (value != null && value.hasOwnProperty("_name")) {
            return value._name;
        } else {
            return "";
        }
    } else if (value != null) {
        if (value == true) {
            return "OUI";
        } else if (value == false) {
            return "NON";
        } else {
            return value;
        }
    } else {
        return "";
    }
}

function testValueWithString(data, dataToString) {
    var valueToDisplay = "";
    var listPlainStringJson = GetListOfPropertyFromJon(data);
    var tabString = dataToString.split(".");

    for (var j = 0; j < listPlainStringJson.length - 1; j++) {
        var tab = listPlainStringJson[j].split(".");
        for (var i = 0; i < tab.length; i++) {
            if (tabString[i] == tab[i+1] && i == tab.length - 1) {
                for (var k = i; k < tab.length; k++) {
                    valueToDisplay += tab[k];
                }
                i = tab.length + 5;
                j = listPlainStringJson.length + 10;
            }
        }
    }


    if (valueToDisplay == "null") {
        return "";
    } else {
        return valueToDisplay;
    }
    
}

function printPage() {
    var mywindow = window.open("", "_blank");
    var currentHeader = $("head").html();
    var eee = $("#textToPrint").html();
   // eee += $("#textToPrintPaysage").html();
    mywindow.document.head.textContent = currentHeader;
    mywindow.document.write(eee);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
}

function GetDocumentForEntity(jsonData) {

    var scannedDocument = [];
    var listOfProperty = GetListOfPropertyFromJon(jsonData);

    for (var i = 0; i < listOfProperty.length; i++) {
        if (listOfProperty[i].indexOf(".pdf") !== -1) { // Vérifier s'il contient .pdf
            var proprety = listOfProperty[i];
            var tabResult = proprety.split("||");

            var xxx = tabResult[0].split(".");
            var ccc = xxx[xxx.length - 1];

            var result = ccc;
            if (result.indexOf("pdf") !== -1) {
                result = result.substring(3, result.length);
                var resultToPrint = result;
                var length = result.length;
                for (var j = 0; j < length; j++) {
                    length = result.length;
                    if (result.charAt(j) !== result.charAt(j).toLowerCase()) {
                        var a = result.charAt(j).toString();
                        var b = " " + result.charAt(j).toLowerCase().toString();
                        result = result.replace(a, b);
                    }
                }
                //r c c m
                if (result.indexOf("r c c m") !== -1) {
                    result = result.replace("r c c m", "RCCM");
                }
                if (result.indexOf("piece identite") !== -1) {
                    result = result.replace("piece identite", "pièce identité");
                }
                if (result.indexOf("titre resident") !== -1) {
                    result = result.replace("titre resident", "titre résident");
                }
                if (result.indexOf("attestation etat civil") !== -1) {
                    result = result.replace("attestation etat civil", "attestation état civil");
                }
                if (tabResult[1] == "***") {
                    result += "(non fournis)";
                } else {
                    result = "<b><u>" + result + "</u></b>";
                }
                scannedDocument.push(result);
            }
        }
    }
    return scannedDocument;
}

function GetLibeleValueFromJsonFirstLevel(jsonData) {
    var keyValueList = [];
    var listOfProperty = GetListOfPropertyFromJon(jsonData);

    for (var i = 0; i < listOfProperty.length; i++) {
        var element = listOfProperty[i].split("||");
        var ee = element[0].split(".");
        if (element[0].indexOf("pdf") == -1 && element[0].indexOf("doc") == -1 && ee.length == 2) { // elimine des string contenant .pdf
            var proprety = listOfProperty[i];
            var tabResult = proprety.split("||");

            var ccc = tabResult[0].split(".");
            var xx = "";

            if (ccc[ccc.length - 1] == "value") {
                xx = ccc[ccc.length - 2];
            } else {
                xx = ccc[ccc.length - 1];
            }

            tabResult[0] = xx;

            // Insert les espaces
            var length = tabResult[0].length;
            for (var j = 0; j < length; j++) {
                if (tabResult[0].charAt(j) !== tabResult[0].charAt(j).toLowerCase()) {
                    var a = tabResult[0].charAt(j).toString();
                    if (tabResult[0].charAt(j + 1) == tabResult[0].charAt(j + 1).toLowerCase()) {
                        var b = " " + tabResult[0].charAt(j).toLowerCase().toString();
                        tabResult[0] = tabResult[0].replace(a, b);
                    }
                }
            }
            if (tabResult[0].indexOf("r c c m") !== -1) {
                tabResult[0] = tabResult[0].replace("r c c m", "RCCM");
            }
            if (tabResult[0].indexOf(" r dC") !== -1) {
                tabResult[0] = tabResult[0].replace(" r dC", "RDC");
            }
            if (tabResult[0].indexOf("NR c") !== -1) {
                tabResult[0] = tabResult[0].replace("NR c", "NRC");
            }
            if (tabResult[0].indexOf("GUC e") !== -1) {
                tabResult[0] = tabResult[0].replace("GUC e", "GUCE");
            }
            if (tabResult[0].indexOf("id nat") !== -1) {
                tabResult[0] = tabResult[0].replace("id nat", " idNAT");
            }
            if (tabResult[0].indexOf("DG i") !== -1) {
                tabResult[0] = tabResult[0].replace("DG i", " DGI");
            }
            if (tabResult[0].indexOf("complete") !== -1) {
                tabResult[0] = tabResult[0].replace("complete", "complète");
            }
            if (tabResult[0].indexOf("umero") !== -1) {
                tabResult[0] = tabResult[0].replace("umero", "uméro");
            } 
            if (tabResult[0].indexOf("teNRC") !== -1) {
                tabResult[0] = tabResult[0].replace("teNRC", "te NRC");
            }
             if (tabResult[0].indexOf("ationRCCM") !== -1) {
                 tabResult[0] = tabResult[0].replace("ationRCCM", "ation RCCM");
            }
            if (tabResult[0].indexOf("libere") !== -1) {
                tabResult[0] = tabResult[0].replace("libere", "liberé");
            }
            if (tabResult[0].indexOf("mere") !== -1) {
                tabResult[0] = tabResult[0].replace("mere", "mère");
            } 
            if (tabResult[0].indexOf("orsOHADA") !== -1) {
                tabResult[0] = tabResult[0].replace("orsOHADA", "ors OHADA");
            }
            if (tabResult[0].indexOf("alaries etranges") !== -1) {
                tabResult[0] = tabResult[0].replace("alaries etranges", "alariés étrangers");
            }
            if (tabResult[0].indexOf("ere horsOHADA") !== -1) {
                tabResult[0] = tabResult[0].replace("ere horsOHADA", "ère hors OHADA");
            } 
            if (tabResult[0].indexOf("elephone") !== -1) {
                tabResult[0] = tabResult[0].replace("elephone", "éléphone");
            }
            if (tabResult[0].indexOf("reGUCE") !== -1) {
                tabResult[0] = tabResult[0].replace("reGUCE", "re GUCE");
            }
            if (tabResult[0].indexOf("debut") !== -1) {
                tabResult[0] = tabResult[0].replace("debut", "début");
            }
            if (tabResult[0].indexOf("ociete") !== -1) {
                tabResult[0] = tabResult[0].replace("ociete", "ociété");
            }
            if (tabResult[0].indexOf("IN pP") !== -1) {
                tabResult[0] = tabResult[0].replace("IN pP", " INPP");
            }
            if (tabResult[0].indexOf("IN sS") !== -1) {
                tabResult[0] = tabResult[0].replace("IN sS", " INSS");
            }
            if (tabResult[0].indexOf("NR c") !== -1) {
                tabResult[0] = tabResult[0].replace("NR c", " NRC");
            }
            if (tabResult[0].indexOf("IN sS") !== -1) {
                tabResult[0] = tabResult[0].replace("IN sS", " INSS");
            }

            var keyValue = [];
            keyValue.push(tabResult[0]);
            keyValue.push(tabResult[1]);


            if (keyValue[0].indexOf("_") == -1 && keyValue[0].indexOf("code") == -1) {
                keyValueList.push(keyValue);
            }
        }
    }
    return keyValueList;
}

function GetLibeleValueFromJson(jsonData) {

    var keyValueList = [];
    var listOfProperty = GetListOfPropertyFromJon(jsonData);

    for (var i = 0; i < listOfProperty.length; i++) {
        if (listOfProperty[i].indexOf(".pdf") == -1) { // elimine des string contenant .pdf
            var proprety = listOfProperty[i];
            var tabResult = proprety.split("||");

            var ccc = tabResult[0].split(".");
            var xx = "";
            if (ccc[ccc.length-1] == "value") {
                xx = ccc[ccc.length - 2];
            } else {
                // construction des adresses
                if (ccc[ccc.length - 1] == "_name" && ccc[ccc.length - 2] == "adresse") {
                    listOfProperty.push("Adresse|| " + tabResult[1]);
                } else {
                    xx = ccc[ccc.length - 1];
                }
            }
            
            tabResult[0] = xx;

            // Insert les espaces
            var length = tabResult[0].length;
            for (var j = 0; j < length; j++) {
                if (tabResult[0].charAt(j) !== tabResult[0].charAt(j).toLowerCase()) {
                    var a = tabResult[0].charAt(j).toString();
                    if (tabResult[0].charAt(j + 1) == tabResult[0].charAt(j + 1).toLowerCase()) {
                        var b = " " + tabResult[0].charAt(j).toLowerCase().toString();
                        tabResult[0] = tabResult[0].replace(a, b);
                    }
                }
            }
            if (tabResult[0].indexOf("r c c m") !== -1) {
                tabResult[0] = tabResult[0].replace("r c c m", "RCCM");
            }
            if (tabResult[0].indexOf(" r dC") !== -1) {
                tabResult[0] = tabResult[0].replace(" r dC", "RDC");
            }

            var keyValue = [];
            keyValue.push(tabResult[0]);
            keyValue.push(tabResult[1]);

            if (keyValue[0] == "ville" ||
                 keyValue[0] == "commune" ||
                 keyValue[0] == "nom voie" ||
                 keyValue[0] == "province" ||
            keyValue[0] == "quartier" ||
            keyValue[0] == "numero voie" ||
            keyValue[0] == "extrait casier judiciaire" ||
            keyValue[0] == "dirigeant represente" ||
                keyValue[0] == "pays" ||
                 keyValue[0] == "type voie") {
                keyValue[0] = "";
            } 
            
            if (keyValue[0].indexOf("_") == -1 && keyValue[0].indexOf("code") == -1 && keyValue[0] !== "" && !$.isNumeric(keyValue[0])) {
                keyValueList.push(keyValue);
            }
        }
    }
    return keyValueList;
}

function GetListOfPropertyFromJon(data) {
    var listOfProperty = [];
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            if (typeof data[property] == "object") {
                var jsonIteraded = iterateJson(data[property], "." + property);
                if (jsonIteraded != null) {
                    for (var k = 0; k < jsonIteraded.length; k++) {
                        listOfProperty.push(jsonIteraded[k]);
                    }
                }
            } else {
                listOfProperty.push("." + property + "||" + data[property]);
            }
        }
    }
    return listOfProperty;
}

function iterateJson(obj, stack) {
    var listOfProperty = [];
    /*
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] == "object") {
                    var jsonIteraded = iterateJson(obj[property], stack + '.' + property);

                    if (jsonIteraded != null) {
                        for (var k = 0; k < jsonIteraded.length; k++) {
                            listOfProperty.push(jsonIteraded[k]);
                        }
                    }
                } else {
                    listOfProperty.push(stack + '.' + property + '||' + obj[property]);
                }
            }
        }
    
    */
     if (obj == null) {
        listOfProperty.push(stack + "||" + " ");
    } else {
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] == "object") {
                    var jsonIteraded = iterateJson(obj[property], stack + "." + property);

                    if (jsonIteraded != null) {
                        for (var k = 0; k < jsonIteraded.length; k++) {
                            listOfProperty.push(jsonIteraded[k]);
                        }
                    }
                } else {
                    listOfProperty.push(stack + "." + property + "||" + obj[property]);
                }
            }
        }
    }
    
    
    return listOfProperty;
}
