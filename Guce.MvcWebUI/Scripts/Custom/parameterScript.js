﻿//

function editParameter(id) {
    var box = startLoader();
    var dialog = new BootstrapDialog({
        title: "Edition " + id,
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: true,
        onhidden: function () {
            stopLoader(box);
        },
        buttons: [
            {
                label: "Valider",
                cssClass: "btn-primary",
                icon: "fa fa-check",
                action: function () {
                    var hasError = false;
                    var panelId = "#editParameter";
                    $(panelId + " .form-control").each(function () {
                        var divFormGroup = $(this).closest(".form-group");
                        var value = this.value;
                        value = checkNotAllowCharacter(value);
                        if (value === "") {
                            $(divFormGroup).addClass("has-error");
                        } else {
                            $(divFormGroup).removeClass("has-error");
                        }
                        var hasErrortag = $(divFormGroup).hasClass("has-error");
                        if (hasErrortag) {
                            hasError = true;
                        }
                    });
                    var json = "{\"Id\":\"" + $("#id").val() + "\", \"Username\":\"" + $("#username").val() + "\", " +
                        "\"Value\":\"" + $("#value").val() + "\", \"Description\":\"" + $("#desc").val() + "\"}";
                    if (!hasError) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);
                        $.ajax({
                            type: "POST",
                            url: editParameterPostUrl,
                            data: {
                                json: json, id: id
                            },
                            success: function (response) {
                                stopLoader(box);
                                dialog.close();
                                window.location.href = response;
                            },
                            error: function () {
                                stopLoader(box);
                                dialog.close();
                                messageAlert("Erreur fatale ...");
                            }
                        });
                    } else {
                        //scrollToFirstErrorElement();
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-default",
                icon: "fa fa-times",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: $("<div></div>").load(editParameterUrl + "?id=" + id)
    });
    dialog.open();
}
