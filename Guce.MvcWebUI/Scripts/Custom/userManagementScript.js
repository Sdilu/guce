﻿
function checkUsername() {
    var element = $(event.target);
    var obj = $(element).closest(".form-group");
    var username = checkNotAllowCharacter($(element).val()).replace(" ", "").replace("'", "").toLowerCase();
    if (username !== "") {
        $.ajax({
            type: "POST",
            url: checkUsernameUrl,
            data: {
                username: username
            },
            success: function (response) {
                $(element).val(username);
                $(obj).find("i").remove();
                if (response === "True") {
                    $(obj).removeClass("has-error");
                    $(obj).addClass("has-feedback");
                    $(element).after("<i class='fa fa-check form-control-feedback text-green'></i>");
                } else {
                    $(obj).addClass("has-error");
                    $(obj).addClass("has-feedback");
                    $(element).after("<i class='fa fa-times form-control-feedback text-red'></i>");
                    messageAlert("L'utilisateur « " + username + " » existe déjà dans la base de données");
                }
            },
            error: function () {
                messageAlert("Erreur fatale");
            }
        });
    } else {
        $(obj).find("i").remove();
        $(obj).removeClass("has-feedback");
        $(obj).removeClass("has-error");
    }
}

function addUser() {
    var box = startLoader();
    var panelId = "#addUser";
    var dialog = new BootstrapDialog({
        title: "Nouveau utilisateur",
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        buttons: [
            {
                label: "Valider",
                cssClass: "btn-sm btn-primary",
                icon: "fa fa-check",
                action: function () {
                    if (!checkRequiredFields(panelId)) {
                        var fullname = checkNotAllowCharacter($("#fullname").val());
                        var username = checkNotAllowCharacter($("#username").val()).replace(" ", "").replace("'", "").toLowerCase();
                        var password = checkNotAllowCharacter($("#password").val());
                        var tab = $("#groups").val();
                        var groups = "";
                        for (var i = 0; i < tab.length; i++) {
                            groups += tab[i] + ",";
                        }
                        if (groups.length > 1) {
                            groups = groups.substring(0, groups.length - 1);
                        }
                        var attributes = $("#attributes").val();
                        var obj = $("#username").closest(".form-group");
                        $.ajax({
                            type: "POST",
                            url: checkUsernameUrl,
                            data: {
                                username: username
                            },
                            success: function (response) {
                                $("#username").val(username);
                                $(obj).find("i").remove();
                                if (response === "True") {
                                    $(obj).removeClass("has-error");
                                    $(obj).addClass("has-feedback");
                                    $("#username").after("<i class='fa fa-check form-control-feedback text-green'></i>");
                                    $.ajax({
                                        type: "POST",
                                        url: createUserUrl,
                                        data: {
                                            fullname: fullname,
                                            username: username,
                                            password: password,
                                            groups: groups,
                                            attributes: attributes
                                        },
                                        success: function (result) {
                                            //stopLoader(box);
                                            dialog.close();
                                            window.location.href = result;
                                        },
                                        error: function () {
                                            stopLoader(box);
                                            messageAlert("Erreur fatale");
                                        }
                                    });
                                } else {
                                    $(obj).addClass("has-error");
                                    $(obj).addClass("has-feedback");
                                    $("#username").after("<i class='fa fa-times form-control-feedback text-red'></i>");
                                    messageAlert("L'utilisateur « " + username + " » existe déjà dans la base de données");
                                }
                            },
                            error: function () {
                                messageAlert("Erreur fatale");
                            }
                        });
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times text-red",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: $(dialogLoader).load(addUserUrl)
    });
    dialog.open();
}

function editUser(username) {
    var box = startLoader();
    var panelId = "#addUser";
    var dialog = new BootstrapDialog({
        title: "Edition de « " + username + " »",
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        buttons: [
            {
                label: "Valider",
                cssClass: "btn-sm btn-primary",
                icon: "fa fa-check",
                action: function () {
                    if (!checkRequiredFields(panelId)) {
                        var fullname = checkNotAllowCharacter($("#fullname").val());
                        var tab = $("#groups").val();
                        var groups = "";
                        for (var i = 0; i < tab.length; i++) {
                            groups += tab[i] + ",";
                        }
                        if (groups.length > 1) {
                            groups = groups.substring(0, groups.length - 1);
                        }
                        var attributes = $("#attributes").val();
                        $.ajax({
                            type: "POST",
                            url: updateUserUrl,
                            data: {
                                fullname: fullname,
                                username: username,
                                groups: groups,
                                attributes: attributes
                            },
                            success: function (result) {
                                //stopLoader(box);
                                dialog.close();
                                window.location.href = result;
                            },
                            error: function () {
                                stopLoader(box);
                                messageAlert("Erreur fatale");
                            }
                        });
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times text-red",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: $(dialogLoader).load(editUserUrl + "?username=" + username)
    });
    dialog.open();
}

function deleteUser(username) {
    var box = startLoader();
    var dialog = new BootstrapDialog({
        title: "Suppression",
        type: BootstrapDialog.TYPE_DANGER,
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        message: "Etes-vous sûr de vouloir supprimer « " + username + " » ?",
        buttons: [
            {
                label: "Oui",
                cssClass: "btn-sm btn-danger",
                icon: "fa fa-check",
                action: function () {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);
                    $.ajax({
                        type: "POST",
                        url: deleteUserUrl,
                        data: {
                            username: username
                        },
                        success: function (response) {
                            //stopLoader(box);
                            dialog.close();
                            window.location.href = response;
                        },
                        error: function (response) {
                            dialog.close();
                            showError(box, response);
                        }
                    });
                }
            }, {
                label: "Non",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ]
    });
    dialog.open();
}

