﻿

function createReport() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function(data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

        },
        error: function(e, status) {
                if (e.status == 500) {
                    alert("Erreur interne au serveur. Probablement le délais d'attente est depassé. Vous allez être redirrigé vers la page de connexion");
                    window.location.replace(logoutUrl);
                } else {
                    messageAlert("Erreur lors de la génération du rapport...");
                }
            }
        });
}