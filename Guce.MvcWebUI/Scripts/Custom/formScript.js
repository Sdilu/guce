﻿//
var dialogResult;
var dialogSearch;
var existedEntities = "[";

function checkVisibility(panelId, json, method) {
    var box = startLoader();
    $("#" + panelId).children().find(".form-control").each(function () {
        var key = $(this).attr("data-technical-name");
        var parentId = $(this).attr("data-parent-id");
        if (key !== undefined && parentId !== undefined && !$(this).is("input:hidden")) {
            if ($(this).is("select")) {
                json[key]["code"] = checkNotAllowCharacter($(this).find(":selected").attr("id"));
            } else if ($(this).is("input:radio")) {
                json[key] = checkNotAllowCharacter($("input[name='" + $(this).attr("name") + "']:checked").val());
            } else {
               json[key] = checkNotAllowCharacter($(this).val());
            }
        }
    });
    method();
    boxVisibility();
    stopLoader(box);
}

function boxVisibility() {
    $(".box").each(function(i, obj) {
        var boxHead = $(obj).children()[0];
        var boxBody = $(obj).children()[1];
        var totalElement = $(boxBody).children().length;
        var hiddenElementCount = 0;
        $(boxBody).children().each(function(j, item) {
            if (!$(item).is(":visible")) {
                hiddenElementCount++;
            }
        });
        if (hiddenElementCount === totalElement) {
            $(obj).css("margin-bottom", "-1px");
            $(obj).css("border-top", "0");
            $(boxHead).hide();
            $(boxBody).removeClass("box-body");
            $(obj).removeClass("box-primary");
        } else {
            $(obj).css("border-top", "3px solid #3c8dbc");
            $(boxHead).show();
            $(boxBody).addClass("box-body");
            $(obj).addClass("box-primary");
            $(obj).css("margin-bottom", "20px");
        }
    });
}

function searchOrAddEntity(id) {
    var searchBtn = $("#search_" + id);
    if (searchBtn.is(":visible")) {
        if ($(searchBtn).is(":enabled")) {
            $(searchBtn).click();
        }
    } else {
        var editBtn = $("#edit_" + id);
        if (editBtn.is(":visible")) {
            if ($("#edit_" + id).is(":enabled")) {
                $("#edit_" + id).click();
            }
        } else {
            if ($("#info_" + id).is(":visible") && $("#info_" + id).is(":enabled")) {
                $("#info_" + id).click();
            }
        }
    }
}

function searchEntity(id, dicId, referenceName, jsonParent, visibilityMethod) {
    var box = startLoader();
    var panelId = "#dialogPanel_" + dicId;
    var entityId = $("#EntityId").val();
    var flowId = $("#FlowId").val();
    var actionId = $("#ActionId").val();
    var jsonExistEntities = existedEntities.length < 5 ? "[]" : existedEntities.substring(0, existedEntities.length - 1) + "]";
    dialogSearch = new BootstrapDialog({
        title: "Recherche " + referenceName,
        type: BootstrapDialog.TYPE_DEFAULT,
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: true,
        onhidden: function() {
            stopLoader(box);
        },
        onshown: function() {
            boxVisibility();
        },
        buttons: [
            {
                id: "btnSearchEntity" + id,
                label: "Rechercher",
                cssClass: "btn-sm btn-default hide-component",
                icon: "fa fa-search text-blue",
                action: function () {
                    if (!checkRequiredFields(panelId)) {
                        var jsonValue = "[";
                        var jsonExistData = "[";
                        $(panelId + " .form-control").each(function () {
                            var codeId = checkNotAllowCharacter($(this).is("select") ? $(this).find(":selected").attr("id") : this.value);
                            var value = checkNotAllowCharacter(this.value);
                            var parentId = $(this).attr("data-parent-id");
                            if (value !== "") {
                                jsonValue += "{\"DflId\":\"" + parentId + "\", \"Value\":\"" + codeId + "\"},";
                                jsonExistData += "{\"DflId\":\"" + parentId + "\", \"Value\":\"" + value + "\"},";
                            }
                        });
                        if (jsonValue.length > 1) {
                            jsonValue = jsonValue.substring(0, jsonValue.length - 1);
                        }
                        jsonValue += "]";

                        if (jsonExistData.length > 1) {
                            jsonExistData = jsonExistData.substring(0, jsonExistData.length - 1);
                        }
                        jsonExistData += "]";

                        dialogSearch.enableButtons(false);
                        dialogSearch.setClosable(false);
                        
                        $.ajax({
                            type: "POST",
                            url: searchEntityUrl,
                            data: {
                                json: jsonValue, dicId: dicId, jsonData: jsonExistData
                            },
                            success: function (response) {
                                stopLoader(box);
                                dialogSearch.close();
                                if (response.length > 2) {
                                    var json = $.parseJSON(response);
                                    var numberElement = Object.keys(json).length;
                                    dialogResult = new BootstrapDialog({
                                        title: numberElement + " " + referenceName + " trouvé(e)s",
                                        draggable: true,
                                        closable: true,
                                        type: BootstrapDialog.TYPE_SUCCESS,
                                        closeByBackdrop: false,
                                        closeByKeyboard: false,
                                        autospin: true,
                                        onhidden: function () {
                                            stopLoader(box);
                                        },
                                        buttons: [
                                            {
                                                label: "Recherche",
                                                cssClass: "btn-sm btn-default",
                                                icon: "fa fa-search",
                                                action: function () {
                                                    stopLoader(box);
                                                    dialogResult.close();
                                                    searchEntity(id, dicId, referenceName, jsonParent, visibilityMethod);
                                                }
                                            }, {
                                                label: "Créer nouveau",
                                                cssClass: "btn-sm btn-success",
                                                icon: "fa fa-plus",
                                                action: function () {
                                                    stopLoader(box);
                                                    dialogResult.close();
                                                    addEntity(id, dicId, referenceName, jsonParent, visibilityMethod);
                                                }
                                            },
                                            {
                                                label: "Fermer",
                                                cssClass: "btn-sm btn-default",
                                                icon: "fa fa-times",
                                                action: function () {
                                                    stopLoader(box);
                                                    dialogResult.close();
                                                }
                                            }
                                        ],
                                        message: createDataTableResult(referenceName, json, id, dicId)
                                    });
                                    dialogResult.open();
                                } else {
                                    if ($("#comp_" + id).attr("data-list-create") === "True") {
                                        addEntity(id, dicId, referenceName, jsonParent, visibilityMethod);
                                    }
                                }
                            },
                            error: function (response) {
                                dialogSearch.close();
                                showError(box, response);
                            }
                        });
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times text-red",
                action: function() {
                    stopLoader(box);
                    dialogSearch.close();
                }
            }
        ],
        message: $(dialogLoader).load(referenceSearchUrl + "?dicId=" + dicId + "&entityId=" + entityId
            + "&flowId=" + flowId + "&actionId=" + actionId + "&id=" + id + "&entities=" + jsonExistEntities, function () {
                $("#btnSearchEntity" + id).show();
            })
    });
    dialogSearch.open();
}

function createDataTableResult(title, json, id, dicId) {
    var content = "<table class='table table-hover table-striped'>" +
        "<thead><tr><th>" + title + "</th>" +
        "<th><i class='fa fa-gears'></i> Action</th>" +
        "</tr></thead>";
    $.each(json, function(index, obj) {
        content += "<tr>";
        content += "<td>" + obj.name + "</td>";
        content += "<td>" +
            "<button type='button' onclick='chooseEntity(\"" + obj.id + "\", \"" + dicId + "\", \"" + id + "\", \"" + obj.name
            + "\", " + true + ")' class='btn btn-sm btn-info'>" + "<i class='fa fa-check'></i> Choisir</button>" +
            "</td>";
        content += "</tr>";
    });
    content += "</table>";
    return content;
}

function saveData() {
    var box = startLoader();
    var wflId = $("#WflId").val();
    var wfvId = $("#WfvId").val();
    var actionId = $("#ActionId").val();
    var flowId = $("#FlowId").val();
    var entityId = $("#EntityId").val();
    var statusId = $("#StatusId").val();
    var json = "[";
    $("#entrepriseForm .form-control").each(function() {
        var divFormGroup = $(this).closest(".form-group");
        var parentId = $(this).attr("data-parent-id");
        if ($(divFormGroup).is(":visible") && parentId !== undefined) {
            var value;
            if ($(this).is("input:radio")) {
                value = checkNotAllowCharacter($("input[name='" + $(this).attr("name") + "']:checked").val());
            } else {
                value = checkNotAllowCharacter(this.value);
            }
            var isDicField = $(this).attr("name").indexOf("Values") !== -1;
            var valueId = $(this).attr("data-value-id");
            if ($(this).attr("data-field-type") === "amount") {
                value = formatAmountValue(value, this.id.substring(5));
            } else if ($(this).attr("data-field-type") === "pdf" || $(this).attr("data-field-type") === "doc") {
                var md5 = $(this).attr("data-md5");
                if (md5 !== undefined && md5 !== "" && value !== "") {
                    value = value + ";" + md5;
                }
            }
            json += "{\"Id\":\"" + valueId + "\", \"Key\":\"" + parentId + "\", \"Value\":\""
                    + value + "\", \"Dfl\":\"" + isDicField + "\"},";
        }
    });
    if (json.length > 1) {
        json = json.substring(0, json.length - 1);
    }
    json += "]";
    $.ajax({
        type: "POST",
        url: formSaveUrl,
        data: {
            json: json,
            wflId: wflId,
            wfvId: wfvId,
            actionId: actionId,
            flowId: flowId,
            entityId: entityId,
            statusId: statusId
        },
        success: function(response) {
            //stopLoader(box);
            window.location.href = response;
        },
        error: function(response) {
            showError(box, response);
        }
    });
}

function saveEntreprise() {
    var panelId = "#entrepriseForm";
    if (!checkRequiredFields(panelId)) {
        var wflId = $("#WflId").val();
        var wfvId = $("#WfvId").val();
        var actionId = $("#ActionId").val();
        var flowId = $("#FlowId").val();
        var entityId = $("#EntityId").val();
        var jsonValue = "[";
        var jsonValidateIndexes = "[";
        var warning = "";
        $(panelId + " .form-control").each(function () {
            var divFormGroup = $(this).closest(".form-group");
            var parentId = $(this).attr("data-parent-id");
            if ($(divFormGroup).is(":visible") && parentId !== undefined) {
                var value;
                if ($(this).is("input:radio")) {
                    value = checkNotAllowCharacter($("input[name='" + $(this).attr("name") + "']:checked").val());
                } else {
                    value = checkNotAllowCharacter(this.value);
                }
                
                var isDicField = $(this).attr("name").indexOf("Values") !== -1;
                var valueId = $(this).attr("data-value-id");
                if ($(this).attr("data-field-type") === "amount") {
                    value = formatAmountValue(value, this.id.substring(5));
                } else if ($(this).attr("data-field-type") === "pdf" || $(this).attr("data-field-type") === "doc") {
                    var md5 = $(this).attr("data-md5");
                    if (md5 !== undefined && md5 !== "" && value !== "") {
                        value = value + ";" + md5;
                    }
                }

                jsonValue += "{\"Id\":\"" + valueId + "\", \"Key\":\"" + parentId + "\", \"Value\":\""
                    + value + "\", \"Dfl\":\"" + isDicField + "\"},";
                var label = $(divFormGroup).find("label").first().text();
                if ($(this).attr("data-empty-type") === "warning" && value === "" && $(this).attr("data-readonly") !== "True") {
                    warning += "<div style='font-weight:bold;'>" + label.substring(0, label.length - 1) + "</div>";
                }
                var idxId = $(this).attr("data-index-id");
                if (idxId !== undefined && idxId !== "" && $(this).attr("data-readonly") !== "True"
                    && parseInt($(this).attr("data-check-index")) !== 0) {
                    jsonValidateIndexes += "{\"IsDfl\":\"" + isDicField + "\", \"ParentId\":\"" + parentId + "\", \"IdxId\":\""
                        + idxId + "\", \"Value\":\"" + value + "\", \"Id\":\"" + this.id + "\"},";
                }
            }
        });
        if (jsonValue.length > 1) {
            jsonValue = jsonValue.substring(0, jsonValue.length - 1);
        }
        jsonValue += "]";
        if (warning !== "") {
            var box1 = startLoader();
            BootstrapDialog.confirm({
                title: "Etes-vous sûre de vouloir valider sans ces informations :",
                message: warning,
                type: BootstrapDialog.TYPE_WARNING,
                draggable: true,
                btnCancelLabel: "Non",
                btnOKLabel: "Oui",
                btnOKClass: "btn-success",
                btnCancelClass: "btn-danger",
                callback: function (result) {
                    if (result) {
                        if (jsonValidateIndexes.length > 1) {
                            jsonValidateIndexes = jsonValidateIndexes.substring(0, jsonValidateIndexes.length - 1);
                        }
                        jsonValidateIndexes += "]";
                        if (jsonValidateIndexes !== "[]") {
                            $.ajax({
                                type: "POST",
                                url: validateIndexUrl,
                                data: {
                                    entityId: entityId,
                                    json: jsonValidateIndexes
                                },
                                success: function(resultIndex) {
                                    var json = $.parseJSON(resultIndex);
                                    var hasErrors = false;
                                    $.each(json, function(i, item) {
                                        var divFg = $("#" + item.Id).closest(".form-group");
                                        $(divFg).addClass("has-error");
                                        hasErrors = true;
                                    });
                                    if (hasErrors) {
                                        messageAlert("Dénomination sociale déjà prise ...");
                                        var firstDivError = $(".has-error").first();
                                        var elementError = $(firstDivError).children().find(":input:not([type=hidden])");
                                        $(elementError).focus();
                                        stopLoader(box1);
                                    } else {
                                        $.ajax({
                                            type: "POST",
                                            url: formIndexUrl,
                                            data: {
                                                json: jsonValue,
                                                wflId: wflId,
                                                wfvId: wfvId,
                                                actionId: actionId,
                                                flowId: flowId,
                                                entityId: entityId
                                            },
                                            success: function(response) {
                                                //stopLoader(box1);
                                                window.location.href = response;
                                            },
                                            error: function(response) {
                                                stopLoader(box1);
                                                messageAlert(response.responseText);
                                                console.log(response.responseText);
                                            }
                                        });
                                    }
                                },
                                error: function (response) {
                                    showError(box1, response);
                                }
                            });
                        } else {
                            $.ajax({
                                type: "POST",
                                url: formIndexUrl,
                                data: {
                                    json: jsonValue,
                                    wflId: wflId,
                                    wfvId: wfvId,
                                    actionId: actionId,
                                    flowId: flowId,
                                    entityId: entityId
                                },
                                success: function(response) {
                                    //stopLoader(box1);
                                    window.location.href = response;
                                },
                                error: function(response) {
                                    showError(box1, response);
                                }
                            });
                        }
                    } else {
                        stopLoader(box1);
                    }
                }
            });
        } else {
            var box = startLoader();
            if (jsonValidateIndexes.length > 1) {
                jsonValidateIndexes = jsonValidateIndexes.substring(0, jsonValidateIndexes.length - 1);
            }
            jsonValidateIndexes += "]";
            if (jsonValidateIndexes !== "[]") {
                $.ajax({
                    type: "POST",
                    url: validateIndexUrl,
                    data: {
                        entityId: entityId, json: jsonValidateIndexes
                    },
                    success: function (resultIndex) {
                        var json = $.parseJSON(resultIndex);
                        var hasErrors = false;
                        $.each(json, function (i, item) {
                            var divFg = $("#" + item.Id).closest(".form-group");
                            $(divFg).addClass("has-error");
                            hasErrors = true;
                        });
                        if (hasErrors) {
                            messageAlert("Dénomination sociale déjà prise ...");
                            var firstDivError = $(".has-error").first();
                            var elementError = $(firstDivError).children().find(":input:not([type=hidden])");
                            $(elementError).focus();
                            stopLoader(box);
                        } else {
                            $.ajax({
                                type: "POST",
                                url: formIndexUrl,
                                data: {
                                    json: jsonValue,
                                    wflId: wflId,
                                    wfvId: wfvId,
                                    actionId: actionId,
                                    flowId: flowId,
                                    entityId: entityId
                                },
                                success: function (response) {
                                    //stopLoader(box);
                                    window.location.href = response;
                                },
                                error: function (response) {
                                    showError(box, response);
                                }
                            });
                        }
                    },
                    error: function (response) {
                        showError(box, response);
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: formIndexUrl,
                    data: {
                        json: jsonValue,
                        wflId: wflId,
                        wfvId: wfvId,
                        actionId: actionId,
                        flowId: flowId,
                        entityId: entityId
                    },
                    success: function(response) {
                        //stopLoader(box);
                        window.location.href = response;
                    },
                    error: function(response) {
                        showError(box, response);
                    }
                });
            }
        }
    }
}

function printEntreprise() {
    var box = startLoader();
    var wflId = $("#WflId").val();
    var wfvId = $("#WfvId").val();
    var actionId = $("#ActionId").val();

    var flowId = $("#FlowId").val();
    var entityId = $("#EntityId").val();
    var reportId = $("#ReportId").val();
    $.ajax({
        type: "POST",
        url: formPrintUrl,
        data: {
            wflId: wflId, wfvId: wfvId, actionId: actionId, flowId: flowId,
            entityId: entityId, reportId: reportId
        },
        success: function (response) {
            //stopLoader(box);
            window.location.href = response;
        },
        error: function () {
            stopLoader(box);
            messageAlert("Erreur pendant l'impression ...");
        }
    });
}

function validateEntreprise() {
    var box = startLoader();
    var wflId = $("#WflId").val();
    var wfvId = $("#WfvId").val();
    var actionId = $("#ActionId").val();
    var flowId = $("#FlowId").val();
    var entityId = $("#EntityId").val();
    $.ajax({
        type: "POST",
        url: formValidateUrl,
        data: {
            wflId: wflId, actionId: actionId, flowId: flowId, entityId: entityId
        },
        success: function (response) {
            //stopLoader(box);
            window.location.href = response;
        },
        error: function () {
            stopLoader(box);
            messageAlert("Erreur pendant la validation ...");
        }
    });
}

function getParentProperty(json, id) {
    var val = "";
    $.each(json, function(index, info) {
        if (info.Id === id.substring(5)) {
            val = info.ParentProperty;
            return;
        }
    });
    return val;
}

function getEmptyProperty(json, id) {
    var val = "";
    $.each(json, function(index, info) {
        if (info.Id === id.substring(5)) {
            val = info.Empty;
            return;
        }
    });
    return val;
}

function getValueProperty(json, id) {
    var val = "";
    $.each(json, function(i, obj) {
        if (obj.DflId === id.substring(5)) {
            val = obj.Value;
            return;
        }
    });
    return val;
}

function addEntity(id, dicId, referenceName, json, visibilityMethod) {
    var box = startLoader();
    var panelId = "#dialogPanel_" + dicId;
    var parentId = $("#comp_" + id).attr("data-parent-id");
    var actionId = $("#ActionId").val();
    var wfvId = $("#WfvId").val();
    var dfl = $("#comp_" + id).attr("data-dfl");
    var dialog = new BootstrapDialog({
        title: "Création " + referenceName,
        draggable: true,
        size: BootstrapDialog.SIZE_WIDE,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: true,
        onhidden: function() {
            stopLoader(box);
        },
        onshown: function() {
            boxVisibility();
        },
        buttons: [
            {
                id: "btnAddEntity" + id,
                label: "Valider",
                cssClass: "btn-sm btn-primary hide-component",
                icon: "fa fa-check",
                action: function () {
                    if (!checkRequiredFields(panelId)) {
                        var jsonValue = "[";
                        var technicalName = $("#entityName_" + id).attr("data-technical-name");
                        var flowId = $("#FlowId").val();
                        var statusId = $("#StatusId").val();
                        var jsonVisibility = {};
                        var warning = "";
                        $(panelId + " .form-control").each(function () {
                            var divFormGroup = $(this).closest(".form-group");
                            var parentIdd = $(this).attr("data-parent-id");
                            if ($(divFormGroup).is(":visible") && parentIdd !== undefined) {
                                var value;
                                if ($(this).is("input:radio")) {
                                    value = checkNotAllowCharacter($("input[name='" + $(this).attr("name") + "']:checked").val());
                                } else {
                                    value = checkNotAllowCharacter(this.value);
                                }
                                if (value !== "") {
                                    if ($(this).attr("data-field-type") === "amount") {
                                        value = formatAmountValue(value, this.id.substring(5));
                                    } else if ($(this).attr("data-field-type") === "pdf" || $(this).attr("data-field-type") === "doc") {
                                        var md5 = $(this).attr("data-md5");
                                        if (md5 !== undefined && md5 !== "" && value !== "") {
                                            value = value + ";" + md5;
                                        }
                                    }
                                    jsonValue += "{\"DflId\":\"" + parentIdd + "\", \"Value\":\"" + value + "\"},";
                                }
                                var label = $(divFormGroup).find("label").first().text();
                                if ($(this).attr("data-empty-type") === "warning" && value === "" && $(this).attr("data-readonly") !== "True") {
                                    warning += "<div style='font-weight:bold;'>" + label.substring(0, label.length - 1) + "</div>";
                                }
                                jsonVisibilityGenerator($(this), jsonVisibility);
                            }
                        });
                        if (jsonValue.length > 1) {
                            jsonValue = jsonValue.substring(0, jsonValue.length - 1);
                        }
                        jsonValue += "]";
                        
                        if (warning !== "") {
                            BootstrapDialog.confirm({
                                title: "Etes-vous sûre de vouloir valider sans ces informations :",
                                message: warning,
                                type: BootstrapDialog.TYPE_WARNING,
                                draggable: true,
                                btnCancelLabel: "Non",
                                btnOKLabel: "Oui",
                                btnOKClass: "btn-success",
                                btnCancelClass: "btn-danger",
                                callback: function (result) {
                                    if (result) {
                                        dialog.enableButtons(false);
                                        dialog.setClosable(false);
                                        $.ajax({
                                            type: "POST",
                                            url: formSaveReferenceUrl,
                                            data: {
                                                json: jsonValue,
                                                dicId: dicId,
                                                actionId: actionId,
                                                flowId: flowId,
                                                wfvId: wfvId,
                                                statusId: statusId
                                            },
                                            success: function (response) {
                                                if (json !== undefined && visibilityMethod !== undefined) {
                                                    if (json != 0 && visibilityMethod != 0) {
                                                        $.each(json, function (key) {
                                                            if (key === technicalName) {
                                                                json[key] = jsonVisibility;
                                                            }
                                                        });
                                                        visibilityMethod();
                                                        boxVisibility();
                                                    }
                                                }
                                                var jsonResult = $.parseJSON(response);
                                                chooseEntity(jsonResult.Entity, dicId, id, jsonResult.Name, false);
                                                $("#FlowId").val(jsonResult.Flow);
                                                $("#StatusId").val(jsonResult.Status);
                                                messageSuccess(jsonResult.Name + " a été créé avec succès");
                                                dialog.close();
                                                stopLoader(box);
                                            },
                                            error: function (response) {
                                                dialog.close();
                                                showError(box, response);
                                            }
                                        });
                                    }
                                }
                            });
                        } else {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);
                            $.ajax({
                                type: "POST",
                                url: formSaveReferenceUrl,
                                data: {
                                    json: jsonValue,
                                    dicId: dicId,
                                    actionId: actionId,
                                    flowId: flowId,
                                    wfvId: wfvId,
                                    statusId: statusId
                                },
                                success: function (response) {
                                    if (json !== undefined && visibilityMethod !== undefined) {
                                        if (json != 0 && visibilityMethod != 0) {
                                            $.each(json, function (key) {
                                                if (key === technicalName) {
                                                    json[key] = jsonVisibility;
                                                }
                                            });
                                            visibilityMethod();
                                            boxVisibility();
                                        }
                                    }
                                    var jsonResult = $.parseJSON(response);
                                    chooseEntity(jsonResult.Entity, dicId, id, jsonResult.Name, false);
                                    $("#FlowId").val(jsonResult.Flow);
                                    $("#StatusId").val(jsonResult.Status);
                                    messageSuccess(jsonResult.Name + " a été créé avec succès");
                                    dialog.close();
                                    stopLoader(box);
                                },
                                error: function (response) {
                                    dialog.close();
                                    showError(box, response);
                                }
                            });
                        }
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times",
                action: function() {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: $(dialogLoader).load(addEntityUrl + "?dicId=" + dicId + "&parentId=" + parentId
            + "&actionId=" + actionId + "&dfl=" + dfl, function () {
                $("#btnAddEntity" + id).show();
            })
    });
    dialog.open();
}

function editEntity(id, dicId, entityId, entityName, checkFields, json, visibilityMethod) {
    var box = startLoader();
    entityId = $("#comp_" + id).val();
    var panelId = "#dialogPanel_" + dicId;
    var parentId = $("#comp_" + id).attr("data-parent-id");
    var actionId = $("#ActionId").val();
    var dfl = $("#comp_" + id).attr("data-dfl");
    var userEntityValidated = false;
    var btnValidateClass = checkFields ? "success" : "primary";
    var dialog = new BootstrapDialog({
        title: checkFields ? "Validation de " + entityName : "Edition de " + entityName,
        type: checkFields ? BootstrapDialog.TYPE_SUCCESS : BootstrapDialog.TYPE_PRIMARY,
        draggable: true,
        size: BootstrapDialog.SIZE_WIDE,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        onhidden: function () {
            if (checkFields) {
                unChooseEntity(userEntityValidated, id);
            }
            stopLoader(box);
        },
        onshown: function() {
            boxVisibility();
        },
        buttons: [
            {
                id: "btnEditEntity" + id,
                label: "Valider",
                cssClass: "btn-sm btn-" + btnValidateClass + " hide-component",
                icon: "fa fa-check",
                action: function () {
                    if (!checkRequiredFields(panelId)) {
                        var jsonValue = "[";
                        var technicalName = $("#entityName_" + id).attr("data-technical-name");
                        var flowId = $("#FlowId").val();
                        var statusId = $("#StatusId").val();
                        var jsonVisibility = {};
                        var warning = "";
                        $(panelId + " .form-control").each(function () {
                            var divFormGroup = $(this).closest(".form-group");
                            var parentIdd = $(this).attr("data-parent-id");
                            if ($(divFormGroup).is(":visible") && parentIdd !== undefined) {
                                var value;
                                if ($(this).is("input:radio")) {
                                    value = checkNotAllowCharacter($("input[name='" + $(this).attr("name") + "']:checked").val());
                                } else {
                                    value = checkNotAllowCharacter(this.value);
                                }
                                if (value !== "") {
                                    var valueId = $(this).attr("data-value-id");
                                    if ($(this).attr("data-field-type") === "amount") {
                                        value = formatAmountValue(value, this.id.substring(5));
                                    } else if ($(this).attr("data-field-type") === "pdf" || $(this).attr("data-field-type") === "doc") {
                                        var md5 = $(this).attr("data-md5");
                                        if (md5 !== undefined && md5 !== "" && value !== "") {
                                            value = value + ";" + md5;
                                        }
                                    }
                                    jsonValue += "{\"ValId\":\"" + valueId + "\", \"DflId\":\"" + parentIdd + "\", \"Value\":\"" + value + "\"},";
                                }
                                var label = $(divFormGroup).find("label").first().text();
                                if ($(this).attr("data-empty-type") === "warning" && value === "" && $(this).attr("data-readonly") !== "True") {
                                    warning += "<div style='font-weight:bold;'>" + label.substring(0, label.length - 1) + "</div>";
                                }
                                jsonVisibilityGenerator($(this), jsonVisibility);
                            }
                        });
                        jsonValue = jsonValue.substring(0, jsonValue.length - 1);
                        jsonValue += "]";

                        if (warning !== "") {
                            BootstrapDialog.confirm({
                                title: "Etes-vous sûre de vouloir valider sans ces informations :",
                                message: warning,
                                type: BootstrapDialog.TYPE_WARNING,
                                draggable: true,
                                btnCancelLabel: "Non",
                                btnOKLabel: "Oui",
                                btnOKClass: "btn-success",
                                btnCancelClass: "btn-danger",
                                callback: function (result) {
                                    if (result) {
                                        dialog.enableButtons(false);
                                        dialog.setClosable(false);
                                        $.ajax({
                                            type: "POST",
                                            url: formEditReferenceUrl,
                                            data: {
                                                json: jsonValue,
                                                actionId: actionId,
                                                flowId: flowId,
                                                entityId: entityId,
                                                statusId: statusId
                                            },
                                            success: function (response) {
                                                if (json !== undefined && visibilityMethod !== undefined) {
                                                    if (json != 0 && visibilityMethod != 0) {
                                                        $.each(json, function (key) {
                                                            if (key === technicalName) {
                                                                json[key] = jsonVisibility;
                                                            }
                                                        });
                                                        visibilityMethod();
                                                        boxVisibility();
                                                    }
                                                }
                                                var jsonResult = $.parseJSON(response);
                                                $("#StatusId").val(jsonResult.Status);
                                                chooseEntity(entityId, dicId, id, jsonResult.Name, false);
                                                messageSuccess(jsonResult.Name + " a été modifié avec succès");
                                                dialog.close();
                                                stopLoader(box);
                                                userEntityValidated = true;
                                            },
                                            error: function (response) {
                                                dialog.close();
                                                showError(box, response);
                                            }
                                        });
                                    }
                                }
                            });
                        } else {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);
                            $.ajax({
                                type: "POST",
                                url: formEditReferenceUrl,
                                data: {
                                    json: jsonValue,
                                    actionId: actionId,
                                    flowId: flowId,
                                    entityId: entityId,
                                    statusId: statusId
                                },
                                success: function (response) {
                                    if (json !== undefined && visibilityMethod !== undefined) {
                                        if (json != 0 && visibilityMethod != 0) {
                                            $.each(json, function (key) {
                                                if (key === technicalName) {
                                                    json[key] = jsonVisibility;
                                                }
                                            });
                                            visibilityMethod();
                                            boxVisibility();
                                        }
                                    }
                                    var jsonResult = $.parseJSON(response);
                                    $("#StatusId").val(jsonResult.Status);
                                    chooseEntity(entityId, dicId, id, jsonResult.Name, false);
                                    messageSuccess(jsonResult.Name + " a été modifié avec succès");
                                    dialog.close();
                                    stopLoader(box);
                                    userEntityValidated = true;
                                },
                                error: function (response) {
                                    dialog.close();
                                    showError(box, response);
                                }
                            });
                        }
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: $(dialogLoader).load(editEntityUrl + "?dicId=" + dicId + "&entityId=" + entityId + "&parentId=" + parentId
            + "&actionId=" + actionId + "&dfl=" + dfl, function () {
                $("#btnEditEntity" + id).show();
            })
    });
    dialog.open();
}

function deleteEntity(id, entityName) {
    var box = startLoader();
    var dialog = new BootstrapDialog({
        title: "Suppression",
        type: BootstrapDialog.TYPE_DANGER,
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        message: "Etes-vous sûr de vouloir le supprimer?",
        onhidden: function() {
            stopLoader(box);
        },
        buttons: [
            {
                label: "Oui",
                cssClass: "btn-sm btn-danger",
                icon: "fa fa-check",
                action: function () {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);
                    var valueId = $("#comp_" + id).attr("data-value-id");
                    var dfl = $("#comp_" + id).attr("data-dfl");
                    var statusId = $("#StatusId").val();
                    $.ajax({
                        type: "POST",
                        url: formDeleteReferenceUrl,
                        data: {
                            valueId: valueId, dfl: dfl, statusId: statusId
                        },
                        success: function () {
                            $("#entityName_" + id).val("");
                            $("#search_" + id).show();
                            $("#edit_" + id).hide();
                            $("#delete_" + id).hide();
                            dialog.close();
                            stopLoader(box);
                        },
                        error: function (response) {
                            dialog.close();
                            showError(box, response);
                        }
                    });
                }
            }, {
                label: "Non",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ]
    });
    dialog.open();
}

function infoEntity(id, dicId, entityName) {
    var box = startLoader();
    var entityId = $("#comp_" + id).val();
    var parentId = $("#comp_" + id).attr("data-parent-id");
    var dfl = $("#comp_" + id).attr("data-dfl");
    var actionId = $("#ActionId").val();
    var dialog = new BootstrapDialog({
        title: "Détails de " + entityName,
        draggable: true,
        size: BootstrapDialog.SIZE_WIDE,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: true,
        onhidden: function () {
            stopLoader(box);
        },
        onshown: function () {
            boxVisibility();
        },
        buttons: [
            {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ],
        message: $(dialogLoader).load(infoEntityUrl + "?dicId=" + dicId + "&entityId=" + entityId + "&parentId=" + parentId
            + "&actionId=" + actionId + "&dfl=" + dfl)
    });
    dialog.open();
}

function removeField() {
    var box = startLoader();
    var trigger = $(event.target).closest(".form-group");
    var elementCount = $(trigger).find(".form-control").length;
    var element = $(trigger).find(".form-control");
    if (elementCount > 1) {
        for (var i = 0; i < elementCount; i++) {
            if ($(element[i]).attr("data-value-id") !== undefined) {
                element = element[i];
                break;
            }
        }
    }
    var valueId = $(element).attr("data-value-id");
    var dfl = $(element).attr("data-dfl");
    var statusId = $("#StatusId").val();
    $.ajax({
        type: "POST",
        url: deleteComponentUrl,
        data: {
            valueId: valueId, dfl: dfl, statusId: statusId
        },
        success: function() {
            $(trigger).remove();
            stopLoader(box);
        },
        error: function() {
            stopLoader(box);
            messageAlert("Erreur lors de la supression du composant ...");
        }
    });
}

function addField(parentId, parentProp) {
    var box = startLoader();
    var trigger = $(event.target).closest(".form-group");
    var label = $(trigger).find("label").first().text();
    if (label.charAt(label.length - 1) === "*" || label.charAt(label.length - 1) === "~") {
        label = label.substring(0, label.length - 1);
    }
    var element = $(trigger.children()[1]).children()[0];
    var dialog = $(element).hasClass("input-group-sm");
    var next = $(".form-group").length;
    $.ajax({
        type: "POST",
        url: createComponentUrl,
        data: {
            parentId: parentId,
            parentProperty: parentProp,
            dialog: dialog,
            index: next, label: label
        },
        success: function(data) {
            $(trigger).after(data);
            stopLoader(box);
        },
        error: function() {
            stopLoader(box);
            messageAlert("Erreur lors de la création du composant ...");
        }
    });
}

function findEntities() {
    $("#createEntity").hide();
    var keyword = checkNotAllowCharacter($("#findEntities").val());
    var wflId = $("#WflId").val();
    var wfvId = $("#WfvId").val();
    var actionId = $("#ActionId").val();
    if (keyword === "") {
        $("#findResult").html("");
        messageAlert("Veuillez saisir un terme de recherche");
        return;
    }
    var criteria = $("#searchCriteria").val();
    var box = startLoader();
    $.ajax({
        type: "POST",
        url: findEntitiesUrl,
        data: { keyword: keyword, criteria: criteria },
        success: function (data) {
            if (data.length > 7) {
                var result = "<table id='findEntitiesResults' class='table table-hover table-striped'>";
                result += "<thead><tr><th>Dénomination sociale</th><th>Enseigne</th><th>Sigle</th><th>RCCM</th><th>" +
                    "<i class='fa fa-gears'></i> Action</th></tr></thead>";
                result += "<tbody>";
                $.each($.parseJSON(data), function (index, item) {
                    var disabled = item.lock_fst_id !== null ? "disabled" : "";
                    var jsonValue = $.parseJSON(item.json);
                    result += "<tr>";
                    result += "<td>" + checkNotAllowCharacter(jsonValue.nomCommercial) + "</td>";
                    result += "<td>" + checkNotAllowCharacter(jsonValue.enseigne) + "</td>";
                    result += "<td>" + checkNotAllowCharacter(jsonValue.sigle) + "</td>";
                    result += "<td style='width: 200px;'>" + checkNotAllowCharacter(jsonValue.rccm) + "</td>";
                    result += "<td style='width: 100px;'>" +
                        "<button id='ent_" + jsonValue._id + "' onclick='executeFindEntity(" + wflId + "," + wfvId + "," + actionId + ","
                        + jsonValue._id + ")' type='button' class='btn btn-sm btn-success' style='font-style: italic;' " + disabled + ">"
                        + "<i class='fa fa-play'></i> Démarrer</button></td>";
                    result += "</tr>";
                });
                result += "</tbody>";
                result += "</table>";
                $("#findResult").html(result);
                $("#findEntitiesResults").DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": false,
                    "pageLength": 6,
                    "language": {
                        "url": dataTableFrenchJson
                    }
                });
            } else {
                $("#findResult").html("");
                messageWarning("Aucune information n'a été trouvée ...");
            }
            $("#createEntity").show();
            stopLoader(box);
        },
        error: function () {
            $("#createEntity").hide();
            stopLoader(box);
            messageAlert("Erreur pendant la recherche ...");
        }
    });
}

function changeSearchCriteria() {
    var criteria = $(event.target).val();
    var element = $("#findEntities");
    $(element).val("");
    if (parseInt(criteria) === 1) {
        $(element).attr("placeholder", "Saisir dénomination sociale");
    } else if (parseInt(criteria) === 2) {
        $(element).attr("placeholder", "Saisir numéro RCCM");
        //$(element).attr("data-inputmask", "'mask': 'CD/aaa/RCCM/99-a-99999'");
        //$(element).attr("data-mask", "");
    } else if (parseInt(criteria) === 3) {
        $(element).attr("placeholder", "Saisir numéro NRC");
    } else if (parseInt(criteria) === 4) {
        $(element).attr("placeholder", "Saisir enseigne");
    } else if (parseInt(criteria) === 5) {
        $(element).attr("placeholder", "Saisir sigle");
    }
}

function executeFindEntity(wflId, wfvId, actionId, entityId) {
    var box = startLoader();
    var trigger = $(event.target);
    var btnId = trigger.attr("id");
    $("#" + btnId).attr("disabled", true);
    $.ajax({
        type: "POST",
        url: executeFindEntityUrl,
        data: { wflId: wflId, wfvId: wfvId, actionId: actionId, entityId: entityId },
        success: function(response) {
            $("#" + btnId).attr("disabled", false);
            window.location.href = response;
            //stopLoader(box);
        },
        error: function() {
            stopLoader(box);
            messageAlert("Erreur fatale ...");
        }
    });
}

function executeCreateEntity(wflId, wfvId, actionId, entityId) {
    var box = startLoader();
    var trigger = $(event.target);
    var btnId = trigger.attr("id");
    $("#" + btnId).attr("disabled", true);
    $.ajax({
        type: "POST",
        url: executeCreateEntityUrl,
        data: { wflId: wflId, wfvId: wfvId, actionId: actionId, entityId: entityId, ajax: true },
        success: function (response) {
            $("#" + btnId).attr("disabled", false);
            window.location.href = response;
            //stopLoader(box);
        },
        error: function () {
            $("#" + btnId).attr("disabled", false);
            stopLoader(box);
            messageAlert("Erreur fatale ...");
        }
    });
}

function jsonVisibilityGenerator(element, json) {
    var value;
    var key = $(element).attr("data-technical-name");
    if ($(element).is("select")) {
        var code = $(element).find(":selected").attr("id");
        value = {"code":code};
    } else if ($(element).is("input:radio")) {
        value = $("input[name='" + $(element).attr("name") + "']:checked").val();
    }  else {
        value = $(element).val();
    }
    json[key] = value;
}

function checkRequiredFieldEntity(entityId, id, dicId, name) {
    $.ajax({
        type: "POST",
        url: checkRequiredWarningUrl,
        data: { entityId: entityId, dicId: dicId }
    }).done(function (response) {
        if (response === "True") {
            editEntity(id, dicId, entityId, name, true, 0, 0);
        }
    });
}

function chooseEntity(entityId, dicId, id, name, check) {
    if (existedEntities === "[") {
        existedEntities += "{\"EntityId\":\"" + entityId + "\",\"DicId\":\"" + dicId + "\"},";
    } else {
        var json = $.parseJSON(existedEntities.substring(0, existedEntities.length - 1) + "]");
        var found = false;
        $.each(json, function (index, obj) {
            if (obj.EntityId === entityId && obj.DicId === dicId) {
                found = true;
                return;
            }
        });
        if (!found && $("#comp_" + id).attr("data-entity-shareable") === "True") {
            existedEntities += "{\"EntityId\":\"" + entityId + "\",\"DicId\":\"" + dicId + "\"},";
        }
    }
    
    var duplicate = false;
    $("#comp_" + id).val(entityId);
    var multivalued = $("#comp_" + id).attr("data-multivalued");
    if (multivalued !== undefined && multivalued === "True") {
        var multivalues = $("input[data-technical-name='" + $("#comp_" + id).attr("data-technical-name") + "']");
        $(multivalues).each(function () {
            if ($(this).is("input:hidden") && $(this).val() === $("#comp_" + id).val()
                && this.id !== $("#comp_" + id).attr("id")) {
                executeUnChooseEntity(id.replace("comp_", ""));
                var divFormGroup = $(this).closest(".form-group");
                var label = $(divFormGroup).find("label").first().text();
                if (label.charAt(label.length - 1) === "*" || label.charAt(label.length - 1) === "~") {
                    label = label.substring(0, label.length - 1);
                }
                messageAlert("Deux « " + label + " » ne peuvent pas être identiques");
                duplicate = true;
            }
        });
    }
    if (check === true && !duplicate) {
        checkRequiredFieldEntity(entityId, id, dicId, name);
    }
    if (!duplicate) {
        $("#entityName_" + id).val(name);
        $("#search_" + id).hide();
        $("#edit_" + id).show();
        $("#delete_" + id).show();
    }
    if (dialogResult) {
        dialogResult.close();
    }
}

function unChooseEntity(validated, id) {
    if (!validated) {
        executeUnChooseEntity(id);
    }
}

function executeUnChooseEntity(id) {
    $("#comp_" + id).val(null);
    $("#entityName_" + id).val(null);
    $("#search_" + id).show();
    $("#edit_" + id).hide();
    $("#delete_" + id).hide();
}

function changeLabel(id, value, empty, validation) {
    var element = $("#label_" + id);
    changeLabelExecute(element, value, empty, validation);
    var technicalName = $("#comp_" + id).attr("data-technical-name");
    var elements = $("input[data-technical-name='" + technicalName + "']");
    $(elements).each(function() {
        if ($(this).attr("data-parent-id") !== undefined) {
            var eltId = $(this).attr("id").substring(5);
            var labelElt = $("#label_" + eltId);
            changeLabelExecute(labelElt, value, empty, validation);
        }
    });
}

function changeLabelExecute(element, value, empty, validation) {
    var idAttr = $(element).attr("id");
    var forAttr = $(element).attr("for");
    var classAttr = $(element).attr("class");
    if (empty === "error") {
        if (validation !== "True") {
            $(element).after("<label id='" + idAttr + "' for='" + forAttr + "' class='" + classAttr + "'>"
                + value + "<span style='color: red;'>*</span></label>");
        } else {
            $(element).after("<label id='" + idAttr + "' for='" + forAttr + "' class='" + classAttr + "'>" + value + "</label>");
        }
        $(element).remove();
    } else if (empty === "warning") {
        if (validation !== "True") {
            $(element).after("<label id='" + idAttr + "' for='" + forAttr + "' class='" + classAttr + "'>"
                + value + "<span style='color: #00ACD6;'>~</span></label>");
        } else {
            $(element).after("<label id='" + idAttr + "' for='" + forAttr + "' class='" + classAttr + "'>" + value + "</label>");
        }
        $(element).remove();
    } else {
        $(element).text(value);
    }
}

function changeSanWay() {
    var value = $(event.target).is(":checked");
    $(".scanWay").prop("checked", value);
    $("#scanningWayId").prop("checked", value);
    $("#scanningWayId").change();
}

function hideComponent(id) {
    var trigger = $("#comp_" + id);
    var triggerTechnicalName = $(trigger).attr("data-technical-name");
    var obj = $(trigger).closest(".form-group");
    $(obj).hide();
    $(obj).children().find(".form-control").each(function(i, elt) {
        if ($(elt).is("input:radio")) {
            $(elt).prop("checked", false);
        } else if ($(elt).is("select")) {
            $(elt).prop("selectedIndex", -1);
        } else {
            $(elt).val("");
            $("#download_" + id).hide();
            $("#preview_" + id).hide();
            $(elt).attr("data-md5", null);
            $("#deleteFile_" + id).hide();

            $("#entityName_" + id).val("");
            $("#search_" + id).show();
            $("#edit_" + id).hide();
            $("#delete_" + id).hide();
        }
    });
    var nextDivs = $(obj).nextAll(".form-group");
    $(nextDivs).each(function () {
        var parentElement = $(this).children()[1];
        var elementFather = $(parentElement).children()[0];
        var element = $(elementFather).find(".form-control");
        var elementTechnicalName = $(element).attr("data-technical-name");
        if (elementTechnicalName !== undefined && triggerTechnicalName !== undefined &&
            elementTechnicalName === triggerTechnicalName) {
            $(this).hide();
        }
    });
}

function showComponent(id) {
    var trigger = $("#comp_" + id);
    var triggerTechnicalName = $(trigger).attr("data-technical-name");
    var obj = $(trigger).closest(".form-group");
    $(obj).show();
    var nextDivs = $(obj).nextAll(".form-group");
    $(nextDivs).each(function () {
        var parentElement = $(this).children()[1];
        var elementFather = $(parentElement).children()[0];
        var element = $(elementFather).find(".form-control");
        var elementTechnicalName = $(element).attr("data-technical-name");
        if (elementTechnicalName !== undefined && triggerTechnicalName !== undefined &&
            elementTechnicalName === triggerTechnicalName) {
            $(this).show();
        }
    });
}

function validateRegExp(element) {
    var divFormGroup = $(element).closest(".form-group");
    var rexp = $(element).attr("data-regexp");
    if (rexp !== undefined) {
        var regexp = new RegExp(rexp);
        if (checkNotAllowCharacter(element.value) !== "" && regexp.test(element.value) === false) {
            var label = $(divFormGroup).find("label").first().text();
            if (label.charAt(label.length - 1) === "*" || label.charAt(label.length - 1) === "~") {
                label = label.substring(0, label.length - 1);
            }
            messageWarning("Le champ <b>« " + label + " »</b> n'est pas valide");
            $(divFormGroup).addClass("has-error");
            return false;
        }
    }
    $(divFormGroup).removeClass("has-error");
    return true;
}

function checkIndex(element, index) {
    var value = $(element).val();
    var entityId = $("#EntityId").val();
    var obj = $(element).closest(".form-group");
    if (value !== "") {
        var box = startLoader();
        $.ajax({
            type: "POST",
            url: checkIndexUrl,
            data: {
                index: index, entityId: entityId, value: value
            },
            success: function(response) {
                var json = $.parseJSON(response);
                $(element).val(json.Canonicalize);
                $(element).attr("data-check-index", json.Check);
                $(obj).find("i").remove();
                if (parseInt(json.Check) >= 0) {
                    $(obj).removeClass("has-error");
                    $(obj).addClass("has-feedback");
                    $(element).after("<i class='fa fa-check form-control-feedback text-green'></i>");
                } else {
                    $(obj).addClass("has-error");
                    $(obj).addClass("has-feedback");
                    $(element).after("<i class='fa fa-times form-control-feedback text-red'></i>");
                }
                stopLoader(box);
            },
            error: function() {
                stopLoader(box);
                messageAlert("Erreur fatale ...");
            }
        });
    } else {
        $(obj).find("i").remove();
        $(obj).removeClass("has-feedback");
        $(obj).removeClass("has-error");
    }
}

/* Chargement dynamique des listes en fonction de leur dependance */

function loadDependencies(trigger, inline) {
    var box = startLoader();
    if (inline !== "True") {
        deleteItemsOfChildren($(trigger).attr("id"), $(trigger).attr("data-code-id"));
    }
    var selectedItem = $(trigger).val();
    if (selectedItem !== "") {
        $.ajax({
            type: "POST",
            url: getDependenciesUrl,
            data: { dependentDciId: selectedItem }
        }).done(function (response) {
            var json = $.parseJSON(response);
            if (inline === "True") {
                if (json.length > 0) {
                    var totalColSize = 9;
                    var suffixClass = "col-sm-";
                    var options = "";
                    $.each(json, function (i, item) {
                        options += "<option id='" + item.Code + "' value='" + item.Id + "'>" + item.Value + "</option>";
                    });

                    var formGroup = $(trigger).closest(".form-group");
                    var multivaluedSpan;
                    var multivaluedInputGroupClass;
                    $(formGroup).children("div").each(function (i, obj) {
                        var inputGroup = $(obj).find(".input-group");
                        if ($(inputGroup).hasClass("input-group")) {
                            multivaluedSpan = $(obj).find(".input-group-btn");
                            if (multivaluedSpan !== undefined) {
                                multivaluedInputGroupClass = inputGroup.attr("class");
                                var selectElement = $(obj).find("select");
                                inputGroup.after(selectElement);
                                inputGroup.remove();
                            }
                        }
                    });

                    var divParent = $(trigger).parent();
                    var idAttribut = "";
                    var nameAttribut = "";
                    var alignAttribut = "";
                    var accessAttribut = "";
                    var dataDflAttribut = "";
                    var dataEmptyAttribut = "";
                    //Remove all next div elements found
                    $(divParent).nextAll().each(function (index, obj) {
                        var selectElement = $(obj).find("select");
                        if ($(selectElement).attr("id") && $(selectElement).attr("name")) {
                            idAttribut = $(selectElement).attr("id");
                            nameAttribut = $(selectElement).attr("name");
                            alignAttribut = $(selectElement).attr("data-parent-id");
                            accessAttribut = $(selectElement).attr("data-value-id");
                            dataDflAttribut = $(selectElement).attr("data-dfl");
                        }
                        $(obj).remove();
                    });
                    var sameElementsCount = $(divParent).closest(".form-group").find("div").length;
                    var elementCount = sameElementsCount + 1;
                    var newColSize = Math.floor(totalColSize / elementCount);
                    var modulus = totalColSize % elementCount;

                    if ($(trigger).attr("id") && $(trigger).attr("name") && $(trigger).attr("data-parent-id")) {
                        idAttribut = $(trigger).attr("id");
                        nameAttribut = $(trigger).attr("name");
                        alignAttribut = $(trigger).attr("data-parent-id");
                        accessAttribut = $(trigger).attr("data-value-id");
                        dataDflAttribut = $(trigger).attr("data-dfl");
                        dataEmptyAttribut = $(trigger).attr("data-empty-type");
                        $(trigger).removeAttr("id");
                        $(trigger).removeAttr("name");
                        $(trigger).removeAttr("data-parent-id");
                        $(trigger).removeAttr("data-value-id");
                        $(trigger).removeAttr("data-dfl");
                        $(trigger).removeAttr("data-empty-type");
                    }
                    $(divParent).closest(".form-group").find("div").each(function (index, obj) {
                        $(obj).removeAttr("class");
                        $(obj).addClass(suffixClass + newColSize);
                    });
                    var onchange = $(trigger).attr("onchange");
                    var newElementDivClassSize = newColSize + modulus;
                    var newSelect = "<select id='" + idAttribut + "' class='form-control' onchange=\"" + onchange + "\" name='"
                        + nameAttribut + "' data-parent-id='" + alignAttribut
                        + "' data-value-id='" + accessAttribut + "' data-dfl='" + dataDflAttribut + "' " +
                        "data-empty-type='" + dataEmptyAttribut + "'></select>";

                    if (multivaluedSpan !== undefined) {
                        $(divParent).after("<div class='" + suffixClass + newElementDivClassSize
                            + "'><div class='" + multivaluedInputGroupClass + "'>" + newSelect + "</div></div>");
                        $("#" + idAttribut).after(multivaluedSpan);
                    } else {
                        $(divParent).after("<div class='" + suffixClass + newElementDivClassSize
                            + "'>" + newSelect + "</div>");
                    }
                    var nextElement = $(divParent).next().find("select");
                    nextElement.append(options);
                    $(nextElement).prop("selectedIndex", -1);
                }
            } else {
                if (json.length > 0) {
                    $(".form-group").each(function (index, obj) {
                        var element = $(obj).children().find("select");
                        if (element.is("select")) {
                            var codeDcdId = $(element).attr("data-code-inline") === "True"
                            ? parseInt($(element).attr("data-dependent-parent"))
                            : parseInt($(element).attr("data-code-id"));
                            if (codeDcdId === json[0].DcdId) {
                                $(element).find("option").remove();
                                deleteItemsOfChildren($(element).attr("id"), $(element).attr("data-code-id"));
                                if ($(element).attr("data-code-inline") === "True") {
                                    deleteSelectChildrenCodeInline(element);
                                }
                                $.each(json, function (i, item) {
                                    $(element).append("<option id='" + item.Code + "' value='" + item.Id
                                        + "'>" + item.Value + "</option>");
                                });
                                if (Object.keys(json).length === 1 && $(element).attr("data-empty-type") === "error") {
                                    $(element).change();
                                } else if (Object.keys(json).length > 1) {
                                    $(element).prop("selectedIndex", -1);
                                }
                            }
                        }
                    });
                } else {
                    deleteItemsOfChildren($(trigger).attr("id"), $(trigger).attr("data-code-id"));
                }
            }
            stopLoader(box);
        });
    }
}

function deleteSelectChildrenCodeInline(element) {
    var divParent = $(element).parent();
    var idAttribut = "";
    var nameAttribut = "";
    var alignAttribut = "";
    var accessAttribut = "";
    var dataDflAttribut = "";
    //Remove all next div elements found
    $(divParent).nextAll().each(function (index, obj) {
        var selectElement = $(obj).find("select");
        if ($(selectElement).attr("id") && $(selectElement).attr("name")) {
            idAttribut = $(selectElement).attr("id");
            nameAttribut = $(selectElement).attr("name");
            alignAttribut = $(selectElement).attr("data-parent-id");
            accessAttribut = $(selectElement).attr("data-value-id");
            dataDflAttribut = $(selectElement).attr("data-dfl");
            $(element).attr("id", idAttribut);
            $(element).attr("name", nameAttribut);
            $(element).attr("data-parent-id", alignAttribut);
            $(element).attr("data-value-id", accessAttribut);
            $(element).attr("data-dfl", dataDflAttribut);
        }
        $(obj).remove();
    });
    $(divParent).attr("class", "col-sm-9");
}

function deleteItemsOfChildren(id, dcdId) {
    var caller = $("#" + id).closest(".form-group");
    $(caller).nextAll().each(function(index, obj) {
        var element = $(obj).children().find("select");
        if ($(element).attr("data-dependent-parent") === dcdId) {
            $(element).find("option").remove();
            $(element).prop("selectedIndex", -1);
            deleteItemsOfChildren($(element).attr("id"), $(element).attr("data-code-id"));
        }
    });
}

function fillCodeInlinesComponent(id, items, data) {
    var box = startLoader();
    var json = $.parseJSON(data);
    var tab = items.split("|");
    for (var i = tab.length - 1; i > 0; i--) {
        var item = tab[i];
        $("#comp_" + id).val(item);
        loadCodeInline(id, item, json);
    }
    $("#comp_" + id).val(tab[0]);
    stopLoader(box);
}

function loadCodeInline(id, selectedItem, json) {
    var trigger = $("#comp_" + id);
    var totalColSize = 9;
    var suffixClass = "col-sm-";
    var options = "";
    $.each(json, function(i, obj) {
        if (obj.DependDciId == selectedItem) {
            options += "<option id='" + obj.Code + "' value='" + obj.Id + "'>" + obj.Value + "</option>";
        }
    });

    var formGroup = $(trigger).closest(".form-group");
    var multivaluedSpan;
    var multivaluedInputGroupClass;
    $(formGroup).children("div").each(function (i, obj) {
        var inputGroup = $(obj).find(".input-group");
        if ($(inputGroup).hasClass("input-group")) {
            multivaluedSpan = $(obj).find(".input-group-btn");
            if (multivaluedSpan !== undefined) {
                multivaluedInputGroupClass = inputGroup.attr("class");
                var selectElement = $(obj).find("select");
                inputGroup.after(selectElement);
                inputGroup.remove();
            }
        }
    });

    var divParent = trigger.parent();
    //Remove all next div elements found
    var idAttribut = "";
    var nameAttribut = "";
    var alignAttribut = "";
    var accessAttribut = "";
    var dataDflAttribut = "";
    var dataEmptyAttribut = "";
    var disabled = "";
    $(divParent).nextAll().each(function(index, obj) {
        var selectElement = $(obj).find("select");
        if ($(selectElement).attr("id") && $(selectElement).attr("name")) {
            idAttribut = $(selectElement).attr("id");
            nameAttribut = $(selectElement).attr("name");
            alignAttribut = $(selectElement).attr("data-parent-id");
            accessAttribut = $(selectElement).attr("data-value-id");
            dataDflAttribut = $(selectElement).attr("data-dfl");
            dataEmptyAttribut = $(selectElement).attr("data-empty-type");
            disabled = $(selectElement).attr("disabled");
        }
        $(obj).remove();
    });
    var sameElementsCount = $(divParent).closest(".form-group").find("div").length;
    var elementCount = sameElementsCount + 1;
    var newColSize = Math.floor(totalColSize / elementCount);
    var modulus = totalColSize % elementCount;
    var disabledAttrib = "";

    if (trigger.attr("id") && trigger.attr("name") && trigger.attr("data-parent-id")) {
        idAttribut = trigger.attr("id");
        nameAttribut = trigger.attr("name");
        alignAttribut = trigger.attr("data-parent-id");
        accessAttribut = trigger.attr("data-value-id");
        dataDflAttribut = trigger.attr("data-dfl");
        dataEmptyAttribut = trigger.attr("data-empty-type");
        disabled = trigger.attr("disabled");
        trigger.removeAttr("id");
        trigger.removeAttr("name");
        trigger.removeAttr("data-parent-id");
        trigger.removeAttr("data-value-id");
        trigger.removeAttr("data-dfl");
        trigger.removeAttr("data-empty-type");
        disabledAttrib = disabled !== undefined ? "disabled='disabled'" : "";
    }
    $(divParent).closest(".form-group").find("div").each(function(index, obj) {
        $(obj).removeAttr("class");
        $(obj).addClass(suffixClass + newColSize);
    });
    var newElementDivClassSize = newColSize + modulus;
    var onchange = $(trigger).attr("onchange");
    var newSelect = "<select class='form-control' onchange=\"" + onchange + "\" name='"
        + nameAttribut + "' data-parent-id='" + alignAttribut + "' id='" + idAttribut + "' data-value-id='"
        + accessAttribut + "' " + disabledAttrib + " data-dfl='" + dataDflAttribut + "' data-empty-type='" + dataEmptyAttribut + "'></select>";

    if (multivaluedSpan !== undefined) {
        $(divParent).after("<div class='" + suffixClass + newElementDivClassSize
            + "'><div class='" + multivaluedInputGroupClass + "'>" + newSelect + "</div></div>");
        $("#" + idAttribut).after(multivaluedSpan);
    } else {
        $(divParent).after("<div class='" + suffixClass + newElementDivClassSize
            + "'>" + newSelect + "</div>");
    }

    var nextElement = $(divParent).next().find("select");
    nextElement.append(options);
}

function loadDependentItems(id) {
    var element = $("#comp_" + id);
    var elementVal = $(element).val();
    if (elementVal === null) {
        var formGroup = $(element).closest(".form-group");
        var prevFormGroup = $(formGroup).prev();
        var prevElement = $(prevFormGroup).children().find("select");
        var formElt = $(prevElement).attr("data-code-id");
        if (formElt !== undefined) {
            var prevElementVal = $(prevElement).val();
            if (prevElementVal !== undefined && prevElementVal !== null) {
                $.ajax({
                    type: "POST",
                    url: getDependenciesUrl,
                    data: { dependentDciId: prevElementVal }
                }).done(function (response) {
                    var json = $.parseJSON(response);
                    if (json.length > 0) {
                        $(element).find("option").remove();
                        $.each(json, function (i, item) {
                            $(element).append("<option id='" + item.Code + "' value='" + item.Id + "'>" + item.Value + "</option>");
                        });
                        $(element).prop("selectedIndex", -1);
                    }
                });
            }
        }
    }
}

function loadDependentCodeInlineItems(id) {
    var element = $("#comp_" + id);
    var parentVal = $("select[data-technical-name='secteurActivite']").val();//FIXME
    if (parentVal !== "" && parentVal !== null && parentVal !== undefined
        && $(element).attr("data-readonly") === "False" && $(element).attr("data-code-inline") === "True") {
        $.ajax({
            type: "POST",
            url: getDependenciesUrl,
            data: { dependentDciId: parentVal }
        }).done(function (response) {
            var json = $.parseJSON(response);
            if (json.length > 0) {
                $(element).find("option").remove();
                $.each(json, function (i, item) {
                    $(element).append("<option id='" + item.Code + "' value='" + item.Id + "'>" + item.Value + "</option>");
                });
                $(element).prop("selectedIndex", -1);
            }
        });
    }
}

function deleteFile(id) {
    var box = startLoader();
    var filename = $("#comp_" + id).val().split(";")[0];
    var valueId = $("#comp_" + id).attr("data-value-id");
    var dialog = new BootstrapDialog({
        title: "Suppression",
        type: BootstrapDialog.TYPE_DANGER,
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        message: "Etes-vous sûr de vouloir supprimer le fichier « " + filename + " » ?",
        onhidden: function () {
            stopLoader(box);
        },
        buttons: [
            {
                label: "Oui",
                cssClass: "btn-danger",
                icon: "fa fa-check",
                action: function () {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);
                    $.ajax({
                        type: "POST",
                        url: deleteFileUrl,
                        data: {
                            filename: filename, valueId: valueId
                        },
                        success: function () {
                            $("#comp_" + id).val("");
                            $("#comp_" + id).attr("data-md5", null);
                            $("#deleteFile_" + id).hide();
                            $("#preview_" + id).hide();
                            $("#download_" + id).hide();
                            dialog.close();
                            stopLoader(box);
                        },
                        error: function () {
                            stopLoader(box);
                            dialog.close();
                            messageAlert("Erreur pendant la suppression ...");
                        }
                    });
                }
            }, {
                label: "Non",
                cssClass: "btn-default",
                icon: "fa fa-times",
                action: function () {
                    stopLoader(box);
                    dialog.close();
                }
            }
        ]
    });
    dialog.open();
}

function previewFile(id) {
    var box = startLoader();
    var element = $("#comp_" + id);
    var tab;
    if ($(element).is("div")) {
        tab = $(element).text().split(" ");
    } else {
        tab = $("#comp_" + id).val().split(";");
    }
    var fileName = tab[0];
    $.ajax({
        type: "POST",
        url: previewPdfUrl,
        success: function (response) {
            if (response !== "") {
                var dirYear = fileName.substring(0, 4);
                var dirMonth = fileName.substring(4, 6);
                var file = response + dirYear + "/" + dirMonth + "/" + fileName;
                if ($(element).is("div")) {
                    stopLoader(box);
                    window.open(file, "_blank");
                } else {
                    var dialog = new BootstrapDialog({
                        title: "Aperçu de " + fileName,
                        draggable: true,
                        closable: true,
                        closeByBackdrop: false,
                        closeByKeyboard: false,
                        onhidden: function() {
                            stopLoader(box);
                        },
                        buttons: [
                            {
                                label: "Agrandir",
                                cssClass: "btn-info",
                                icon: "fa fa-external-link",
                                action: function() {
                                    stopLoader(box);
                                    dialog.close();
                                    window.open(file, "_blank");
                                }
                            }, {
                                label: "Fermer",
                                cssClass: "btn-default",
                                icon: "fa fa-times",
                                action: function() {
                                    stopLoader(box);
                                    dialog.close();
                                }
                            }
                        ],
                        message: "<object data='" + file + "#view=FitV&view=FitH' type='application/pdf' width='100%' height='400'>" +
                            "</object>"
                    });
                    dialog.open();
                }
            } else {
                stopLoader(box);
                messageAlert("Impossible d'afficher l'aperçu !!! ");
            }
        },
        error: function() {
            stopLoader(box);
            messageAlert("Erreur fatale !!! ");
        }
    });
}

function downloadFile(id) {
    var box = startLoader();
    var element = $("#comp_" + id);
    var tab;
    if ($(element).is("div")) {
        tab = $(element).text().split(" ");
    } else {
        tab = $("#comp_" + id).val().split(";");
    }
    var fileName = tab[0];
    $.ajax({
        type: "POST",
        url: previewPdfUrl,
        success: function (response) {
            var dirYear = fileName.substring(0, 4);
            var dirMonth = fileName.substring(4, 6);
            var file = response + dirYear + "/" + dirMonth + "/" + fileName;
            stopLoader(box);
            window.open(file, "_blank");
        },
        error: function () {
            stopLoader(box);
            messageAlert("Erreur fatale !!! ");
        }
    });
}

function chooseFile(id) {
    $("#" + id).trigger("click");
}

function pdfFileChoosen(id, e) {
    var box = startLoader();
    var flowId = $("#FlowId").val();
    var file = $("#choosePdf_" + id).val();
    var name = $("#choosePdf_" + id).attr("data-technical-name");
    var ext = file.split(".");
    ext = ext[ext.length - 1].toLowerCase();
    var arrayExtensions = ["pdf"];
    if (arrayExtensions.lastIndexOf(ext) === -1) {
        $("#choosePdf_" + id).val("");
        $("#comp_" + id).val("");
        $("#preview_" + id).hide();
        messageAlert("Le fichier sélectionné n'est pas valide");
        stopLoader(box);
    } else {
        var formData = new FormData();
        var files = e.target.files;
        formData.append("file", files[0]);
        formData.append("flowId", flowId);
        formData.append("name", name);
        formData.append("valueId", $("#comp_" + id).attr("data-value-id"));
        formData.append("dfl", $("#comp_" + id).attr("data-dfl"));
        formData.append("currentValue", $("#comp_" + id).val());
        $.ajax({
            type: "POST",
            url: uploadDocFileUrl,
            data: formData,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (response) {
                $("#comp_" + id).val(response.split(";")[0]);
                $("#comp_" + id).attr("data-md5", response.split(";")[1]);
                $("#preview_" + id).show();
                $("#deleteFile_" + id).show();
                messageSuccess("Le fichier « " + file.replace(/.*(\/|\\)/, "") + " » a été uploadé avec succès");
                stopLoader(box);
                if ($("#showPreviewId").is(":checked")) {
                    previewFile(id);
                }
            },
            error: function () {
                $("#choosePdf_" + id).val("");
                $("#comp_" + id).val("");
                $("#preview_" + id).hide();
                stopLoader(box);
                messageAlert("Erreur lors du chargement du fichier ...");
            }
        });
    }
}

function docFileChoosen(id, e) {
    var box = startLoader();
    var flowId = $("#FlowId").val();
    var file = $("#chooseDoc_" + id).val();
    var name = $("#comp_" + id).attr("data-technical-name");
    var ext = file.split(".");
    ext = ext[ext.length - 1].toLowerCase();
    var arrayExtensions = ["doc", "docx"];
    if (arrayExtensions.lastIndexOf(ext) === -1) {
        $("#chooseDoc_" + id).val("");
        $("#comp_" + id).val("");
        $("#download_" + id).hide();
        messageAlert("Le fichier << " + file.replace(/.*(\/|\\)/, "") + " >> n'est pas valide");
        stopLoader(box);
    } else {
        var formData = new FormData();
        var files = e.target.files;
        formData.append("file", files[0]);
        formData.append("flowId", flowId);
        formData.append("name", name);
        formData.append("valueId", $("#comp_" + id).attr("data-value-id"));
        formData.append("dfl", $("#comp_" + id).attr("data-dfl"));
        formData.append("currentValue", $("#comp_" + id).val());
        $.ajax({
            type: "POST",
            url: uploadDocFileUrl,
            data: formData,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (response) {
                $("#comp_" + id).val(response.split(";")[0]);
                $("#comp_" + id).attr("data-md5", response.split(";")[1]);
                $("#download_" + id).show();
                $("#deleteFile_" + id).show();
                messageSuccess("Le fichier « " + file.replace(/.*(\/|\\)/, "") + " » a été uploadé avec succès");
                stopLoader(box);
            },
            error: function () {
                $("#chooseDoc_" + id).val("");
                $("#comp_" + id).val("");
                $("#download_" + id).hide();
                stopLoader(box);
                messageAlert("Erreur lors du chargement du fichier ...");
            }
        });
    }
}

function mergeFiles(images, id, scannerId, box) {
    var flowId = $("#FlowId").val();
    var element = $("#comp_" + id);
    var preview = $("#preview_" + id);
    var name = $(element).attr("data-technical-name");
    var valueId = $("#comp_" + id).attr("data-value-id");
    var dfl = $("#comp_" + id).attr("data-dfl");
    var json = "[";
    for (var i = 0; i < images.length; i++) {
        json += "{\"DflId\":\"" + i + "\", \"Value\":\"" + images[i] + "\"},";
    }
    if (json.length > 1) {
        json = json.substring(0, json.length - 1);
    }
    json += "]";
    var currentValue = $("#comp_" + id).val();
    $.ajax({
        type: "POST",
        url: mergeFilesUrl,
        data: {
            json: json, flowId: flowId, name: name, valueId: valueId, dfl: dfl, currentValue: currentValue
        },
        success: function(response) {
            $(element).val(response.split(";")[0]);
            $(element).attr("data-md5", response.split(";")[1]);
            $(preview).show();
            $("#deleteFile_" + id).show();
            //messageSuccess("Le fichier « " + pdf + " » a été créé avec succès");
            stopLoader(box);
            if ($("#showPreviewId").is(":checked")) {
                previewFile(id);
            }
        },
        error: function () {
            if (images.length > 0) {
                BootstrapDialog.confirm({
                    title: "Numérisation",
                    message: "Voulez-vous numériser une autre page?",
                    type: BootstrapDialog.TYPE_DANGER,
                    draggable: true,
                    btnCancelLabel: "Terminer",
                    btnOKLabel: "Autre page",
                    btnOKClass: "btn-success",
                    callback: function (result) {
                        if (result) {
                            addScanFile(images, id, scannerId, box);
                        } else {
                            mergeFiles(images, id, scannerId, box);
                        }
                    }
                });
            } else {
                stopLoader(box);
                messageAlert("Erreur lors de création du fichier pdf !!! ");
            }
        }
    });
}

function chooseScanner(id, favori) {
    var box = startLoader();
    $.ajax({
        type: "POST",
        url: scannersUrl,
        success: function (response) {
            if (response.indexOf("[") !== -1 && response.indexOf("]") !== -1) {
                var json = $.parseJSON(response);
                var content = "<table class='table table-hover table-striped'>" +
                    "<thead><tr><th>ID</th><th>Scanner</th>" +
                    "<th><i class='fa fa-gears'></i> Action</th><th></th>" +
                    "</tr></thead>";
                $.each(json, function(index, obj) {
                    var scannerFavori = favori === obj.Id ? "<i class='fa fa-star text-red'></i>" : "";
                    content += "<tr>";
                    content += "<td><span class='label label-success'>" + obj.Id + "</span></td>";
                    content += "<td>" + obj.Name + "</td>";
                    content += "<td>" +
                        "<button type='button' onclick='dialogResult.close();scan(\"" + id + "\", \"" + obj.Id + "\");'" +
                        " class='btn btn-sm btn-success'><i class='fa fa-copy'></i> Numériser</button></td>";
                    content += "<td>" + scannerFavori + "</td>";
                    content += "</tr>";
                });
                content += "</table>";
                var numberElement = Object.keys(json).length;
                dialogResult = new BootstrapDialog({
                    title: numberElement + " scanner(s) trouvé(s)",
                    draggable: true,
                    closable: false,
                    type: BootstrapDialog.TYPE_SUCCESS,
                    closeByBackdrop: false,
                    closeByKeyboard: false,
                    buttons: [
                        {
                            label: "Fermer",
                            cssClass: "btn-sm btn-default",
                            icon: "fa fa-times",
                            action: function() {
                                stopLoader(box);
                                dialogResult.close();
                            }
                        }
                    ],
                    message: content
                });
                dialogResult.open();
            } else {
                if (response === "NO_SCANNER_FOUND") {
                    stopLoader(box);
                    messageAlert("Aucun scanner n'a été trouvé !!!");
                } else {
                    stopLoader(box);
                    messageAlert(response);
                }
            }
        },
        error: function () {
            stopLoader(box);
            messageAlert("Erreur de communication avec le scanner ...");
        }
    });
}

function scan(id, scannerId) {
    var box = $("#comp_" + id).closest(".box");
    //if (!$("#changeScanner_" + id).is(":visible")) {
    //    $("button.changeScannerBtn").attr("onclick", "chooseScanner('" + id + "', '" + scannerId + "')");
    //    $("button.changeScannerBtn").show();
    //    $("button.scanBtn").attr("onclick", "scan('" + id + "', '" + scannerId + "')");
    //}
    var loader = $(box).find(".overlay");
    if (!loader.is("div")) {
        box.append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
    }

    var mode = $("#scanningWayId").is(":checked") ? 0 : 1;
    var images = new Array();
    $.ajax({
        type: "POST",
        url: scanUrl,
        data: { scannerId: scannerId, mode: mode },
        success: function (response) {
            if (response.indexOf("[") !== -1 && response.indexOf("]") !== -1) {
                if (response !== "[]") {
                    var json = $.parseJSON(response);
                    $.each(json, function (i, item) {
                        if (item.indexOf(".jpeg") !== -1) {
                            images.push(item);// adds a new images  to list
                        }
                    });
                    //messageInfo("Document numérisé avec succès ...");
                    BootstrapDialog.confirm({
                        title: "Numérisation",
                        message: "Voulez-vous numériser une autre page?",
                        type: BootstrapDialog.TYPE_WARNING,
                        draggable: true,
                        btnCancelLabel: "Terminer",
                        btnOKLabel: "Autre page",
                        btnOKClass: "btn-success",
                        callback: function (result) {
                            if (result) {
                                addScanFile(images, id, scannerId, box);
                            } else {
                                mergeFiles(images, id, scannerId, box);
                            }
                        }
                    });
                } else {
                    stopLoader(box);
                    messageAlert("Il n'y a pas de papiers dans le chargeur de documents !!!");
                }
            } else {
                if (images.length > 0) {
                    BootstrapDialog.confirm({
                        title: "Numérisation",
                        message: "Voulez-vous numériser une autre page?",
                        type: BootstrapDialog.TYPE_DANGER,
                        draggable: true,
                        btnCancelLabel: "Terminer",
                        btnOKLabel: "Autre page",
                        btnOKClass: "btn-success",
                        callback: function(result) {
                            if (result) {
                                addScanFile(images, id, scannerId, box);
                            } else {
                                mergeFiles(images, id, scannerId, box);
                            }
                        }
                    });
                } else {
                    messageAlert(response);
                    stopLoader(box);
                }
            }
        },
        error: function () {
            if (images.length > 0) {
                BootstrapDialog.confirm({
                    title: "Numérisation",
                    message: "Voulez-vous numériser une autre page?",
                    type: BootstrapDialog.TYPE_DANGER,
                    draggable: true,
                    btnCancelLabel: "Terminer",
                    btnOKLabel: "Autre page",
                    btnOKClass: "btn-success",
                    callback: function (result) {
                        if (result) {
                            addScanFile(images, id, scannerId, box);
                        } else {
                            mergeFiles(images, id, scannerId, box);
                        }
                    }
                });
            } else {
                stopLoader(box);
                messageAlert("Erreur de communication avec le scanner ...");
            }
        }
    });
}

function addScanFile(images, id, scannerId, box) {
    var mode = $("#scanningWayId").is(":checked") ? 0 : 1;
    $.ajax({
        type: "POST",
        url: scanUrl,
        data: { scannerId: scannerId, mode: mode },
        success: function (response) {
            if (response.indexOf("[") !== -1 && response.indexOf("]") !== -1) {
                if (response !== "[]") {
                    var json = $.parseJSON(response);
                    $.each(json, function (i, item) {
                        if (item.indexOf(".jpeg") !== -1) {
                            images.push(item);// adds a new images  to list
                        }
                    });
                    //messageInfo("Document numérisé avec succès ...");
                    BootstrapDialog.confirm({
                        title: "Numérisation",
                        message: "Voulez-vous numériser une autre page?",
                        type: BootstrapDialog.TYPE_WARNING,
                        draggable: true,
                        btnCancelLabel: "Terminer",
                        btnOKLabel: "Autre page",
                        btnOKClass: "btn-success",
                        callback: function (result) {
                            if (result) {
                                addScanFile(images, id, scannerId, box);
                            } else {
                                mergeFiles(images, id, scannerId, box);
                            }
                        }
                    });
                } else {
                    stopLoader(box);
                    messageAlert("Il n'y a pas de papiers dans le chargeur de documents !!!");
                }
            } else {
                if (images.length > 0) {
                    BootstrapDialog.confirm({
                        title: "Numérisation",
                        message: "Voulez-vous numériser une autre page?",
                        type: BootstrapDialog.TYPE_DANGER,
                        draggable: true,
                        btnCancelLabel: "Terminer",
                        btnOKLabel: "Autre page",
                        btnOKClass: "btn-success",
                        callback: function (result) {
                            if (result) {
                                addScanFile(images, id, scannerId, box);
                            } else {
                                mergeFiles(images, id, scannerId, box);
                            }
                        }
                    });
                } else {
                    messageAlert(response);
                    stopLoader(box);
                }
            }
        },
        error: function () {
            if (images.length > 0) {
                BootstrapDialog.confirm({
                    title: "Numérisation",
                    message: "Voulez-vous numériser une autre page?",
                    type: BootstrapDialog.TYPE_DANGER,
                    draggable: true,
                    btnCancelLabel: "Terminer",
                    btnOKLabel: "Autre page",
                    btnOKClass: "btn-success",
                    callback: function (result) {
                        if (result) {
                            addScanFile(images, id, scannerId, box);
                        } else {
                            mergeFiles(images, id, scannerId, box);
                        }
                    }
                });
            } else {
                stopLoader(box);
                messageAlert("Erreur de communication avec le scanner ...");
            }
        }
    });
}

function checkDate(id) {
    var element = $("#comp_" + id);
    if ($(element).attr("data-technical-name") !== "dateDebutExploitation" 
    && $(element).attr("data-technical-name") !== "dateValiditeTitreSejour") {
        var userDate = $(element).val();
        if (userDate !== undefined && userDate !== null && userDate !== "") {
            var tab = userDate.split("/");
            var composeDate = new Date(tab[2], tab[1] - 1, tab[0]);
            if (composeDate < new Date()) {
                if ($(element).attr("data-technical-name") === "dateNaissance" && (new Date().getFullYear() - parseInt(tab[2])) < 18) {
                    messageWarning("La date de naissance saisie correspond à un mineur (moins de 18 ans)");
                }
            } else {
                messageAlert("La date saisie << " + userDate + " >> n'est pas valide !!!");
                $(element).val(null);
            }
        }
    }
}

function checkDateCurrency(id) {
    var element = $("#dCur_" + id);
    var userDate = $(element).val();
    if (userDate !== undefined && userDate !== null && userDate !== "") {
        var tab = userDate.split("/");
        var composeDate = new Date(tab[2], tab[1] - 1, tab[0]);
        if (composeDate >= new Date()) {
            messageAlert("La date saisie << " + userDate + " >> n'est pas valide !!!");
            $(element).val(null);
        }
    }
}

function changeCurrencyWithValues(id, currency, defaultCurrency) {
    changeCurrency(id, currency, defaultCurrency);
    var element = $("#comp_" + id);
    var value = $(element).val();
    var tab = value.split(":");
    $("#comp_" + id).val(tab[2]);
    $("#oCur_" + id).val(tab[0]);
    $("#dCur_" + id).val(componentFormat(tab[3]));
}

function changeCurrency(id, currency, defaultCurrency) {
    $("#currencyBtn_" + id).text(currency);
    var divCol = $("#comp_" + id).parent().parent();
    var inputGroupClass = $("#comp_" + id).parent().attr("class");
    var readOnlyClass = $("#comp_" + id).attr("data-readonly") === "True" ? "readonly='readonly'" : "";
    var classDivCol = $(divCol).attr("class");
    if (currency !== defaultCurrency) {
        if (parseInt(classDivCol.substring(classDivCol.length - 1)) === 9) {
            $(divCol).attr("class", "col-sm-3");
            $(divCol).after("<div id='divOtherCurrency_" + id + "' class='col-sm-4'><div class='" + inputGroupClass + "'>" +
                "<span class='input-group-addon'>" + defaultCurrency + "</span>" +
                "<input id='oCur_" + id + "' type='text' " + readOnlyClass + " class='form-control'/></div></div>");
            $("#divOtherCurrency_" + id).after("<div id='divDateCurrency_" + id + "' class='col-sm-2'><div class='"
                + inputGroupClass + "'>" + "<input " + readOnlyClass + " id='dCur_" + id
                + "' type='text' class='form-control' onblur='checkDateCurrency(\"" + id + "\")'/>" +
                "<span class='input-group-addon'><i class='fa fa-calendar'></i></span></div></div>");
            $("#dCur_" + id).inputmask("dd/mm/yyyy", { "placeholder": "jj/mm/aaaa" });
        }
        $("#oCur_" + id).keypress(function (e) {
            if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#oCur_" + id).bind("cut copy paste", function (e) {
            e.preventDefault();
        });
    } else {
        $(divCol).attr("class", "col-sm-9");
        $("#divOtherCurrency_" + id).remove();
        $("#divDateCurrency_" + id).remove();
    }
}

function digitsOnly() {
    $(".digitOnly").keypress(function(e) {
        if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $(".digitOnly").bind("cut copy paste", function (e) {
        e.preventDefault();
    });
}

function formatAmountValue(value, idField) {
    var element = $("#comp_" + idField);
    var otherElt = $("#oCur_" + idField);
    var dateElt = $("#dCur_" + idField);
    var divCol = $(element).parent().parent();
    var classDivCol = $(divCol).attr("class");
    if ($(element).attr("data-amount") !== undefined && parseInt(classDivCol.substring(classDivCol.length - 1)) !== 9) {
        if ($(otherElt).val() !== undefined && $(otherElt).val() !== ""
            && $(element).val() !== undefined && $(element).val() !== ""
            && $(dateElt).val() !== undefined && $(dateElt).val() !== "") {
            value = $(otherElt).val() + ":" + $("#currencyBtn_" + idField).text()
                + ":" + $(element).val() + ":" + databaseFormat($(dateElt).val());
        } else {
            return "";
        }
    }
    return value;
}

function databaseFormat(value) {
    var tab = value.split("/");
    return tab[2] + "-" + tab[1] + "-" + tab[0];
}

function componentFormat(value) {
    var tab = value.split("-");
    return tab[2] + "/" + tab[1] + "/" + tab[0];
}

function checkChanges(id) {
    var element = $("#comp_" + id);
    var newValue = $(element).val();
    var oldValue = $(element).attr("data-old-value");
    if (oldValue !== undefined && oldValue !== newValue) {
        $(element).closest(".form-group").addClass("has-warning");
        if (!$(element).is("input:radio") && !$(element).is("select")) {
            if (oldValue !== "") {
                var message = "Revenir à la valeur initiale : « " + oldValue + " »";
                $(element).after("<a id='btnReset_" + id + "' class='reset-field-btn' title='" + message + "' " +
                "onclick='resetChanges(\"" + id + "\")'><i class='fa fa-undo'></i></a>");
            }
        }
    } else {
        resetChanges(id);
    }
}

function resetChanges(id) {
    var element = $("#comp_" + id);
    var oldValue = $(element).attr("data-old-value");
    var resetBtn = $("#btnReset_" + id);
    if ($(element).is("input:radio")) {
        if (oldValue === "") {
            $(element).prop("checked", false);
            $("#comp_" + id + "_non").prop("checked", false);
        } else {
            $(element).val(oldValue);
        }
    } else if ($(element).is("select")) {
        $(element).val(oldValue);
        //console.log($(element).attr("onchange"));
        $(element).change();
    } else {
        $(element).val(oldValue);
    }
    $(element).closest(".form-group").removeClass("has-warning");
    $(resetBtn).removeClass("reset-field-btn");
    $(resetBtn).remove();
}

function loadDefaultData() {
    var acteNotarie = $("input[data-technical-name='numeroActeNotarie']");
    //if ($(acteNotarie).val() === "") {
        $(acteNotarie).val($("#ReferenceId").val());
    //}
}

//Inputmask("\\http://a{+}").mask(selector);