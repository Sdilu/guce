﻿
function hoverField(id) {
    var element = $("#comp_" + id);
    $("#editField_" + id).hide();
    $("#contentField_" + id).hide();
    $("#value_" + id).on({
        mouseenter: function () {
            $("#editField_" + id).show();
        },
        mouseleave: function () {
            $("#editField_" + id).hide();
        }
    });
}

function showEditField(id) {
    $("#showField_" + id).hide();
    $("#contentField_" + id).show();
}

function cancelEditField(id) {
    $("#showField_" + id).show();
    $("#contentField_" + id).hide();
}