﻿
var dialogLoader = "<div><div class='text-center'><i style='font-size: 20px;' class='fa fa-refresh fa-spin'></i></div></div>";

function startLoader() {
    $("#validateBtn").attr("disabled", "disabled");
    var box = $(event.target).closest(".box");
    box.append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
    return box;
}

function stopLoader(box) {
    $(box).children(".overlay").remove();
    $("#validateBtn").removeAttr("disabled");
}

function messageAlert(message) {
    $.notify({
        message: message
    }, {
        type: "danger",
        z_index: 1000000
    });
}

function messageInfo(message) {
    $.notify({
        message: message
    }, {
        type: "info",
        z_index: 1000000
    });
}

function messageSuccess(message) {
    $.notify({
        message: message
    }, {
        type: "success",
        z_index: 1000000
    });
}

function messageWarning(message) {
    $.notify({
        message: message
    }, {
        type: "warning",
        z_index: 1000000
    });
}

function showError(box, response) {
    stopLoader(box);
    messageAlert(response.responseText);
    console.log(response.responseText);
}

function changePassword() {
    var dialog = new BootstrapDialog({
        title: "Modification mot de passe",
        draggable: true,
        closable: true,
        type: BootstrapDialog.TYPE_INFO,
        closeByBackdrop: false,
        closeByKeyboard: false,
        buttons: [
            {
                label: "Valider",
                cssClass: "btn-sm btn-info",
                icon: "fa fa-check",
                action: function () {
                    if (!checkRequiredFields("#changePassword")) {
                        var newPassword = $("#newPassword").val();
                        var repeatPassword = $("#repeatPassword").val();
                        var username = $("#usernameId").val();
                        if (newPassword === repeatPassword) {
                            $.ajax({
                                type: "POST",
                                url: changePasswordUrl,
                                data: {
                                    username: username, password: newPassword
                                },
                                success: function (response) {
                                    dialog.close();
                                    if (parseInt(response) === -1) {
                                        messageSuccess("Mot de passe mise à jour avec succès");
                                    } else {
                                        messageAlert("Erreur fatale pendant la mise à jour de votre mot de passe");
                                    }
                                },
                                error: function () {
                                    messageAlert("Erreur fatale ...");
                                }
                            });
                        } else {
                            messageAlert("Le nouveau mot de passe et celui répéter ne sont pas identiques");
                        }
                    }
                }
            }, {
                label: "Fermer",
                cssClass: "btn-sm btn-default",
                icon: "fa fa-times text-red",
                action: function () {
                    dialog.close();
                }
            }
        ],
        message: $(dialogLoader).load(passworUrl)
    });
    dialog.open();
}

function changeUserParameter(parameterId) {
    var value = $(event.target).is(":checked");

    if (parameterId === "SCANNING_WAY") {
        $(".scanWay").prop("checked", value);
    }

    if (parameterId === "SHOW_PREVIEW" || parameterId === "SCANNING_WAY") {
        value = $(event.target).is(":checked") ? 0 : 1;
    }
    
    $.ajax({
        type: "POST",
        url: setupUserParameterUrl,
        data: { parameterId: parameterId, value: value },
        success: function () {

        },
        error: function(response) {
            showError(box, response);
        }
    });
}

function checkRequiredFields(panelId) {
    var hasError = false;
    var errorFields = "";
    $(panelId + " .form-control").each(function () {
        var divFormGroup = $(this).closest(".form-group");
        if ($(divFormGroup).is(":visible")) {
            var label = $(divFormGroup).find("label").first().text();
            var value;
            if ($(this).is("input:radio")) {
                value = checkNotAllowCharacter($("input[name='" + $(this).attr("name") + "']:checked").val());
            } else {
                value = checkNotAllowCharacter(this.value);
            }

            //check required
            if ($(this).attr("data-empty-type") === "error" && value === "" && $(this).attr("data-readonly") === "False") {
                $(divFormGroup).addClass("has-error");
            } else {
                $(divFormGroup).removeClass("has-error");
            }

            //check regexp
            if (!$(divFormGroup).hasClass("has-error")) {
                if (value !== "" && !checkRegExp(this, label, value)) {
                    $(divFormGroup).addClass("has-error");
                } else {
                    $(divFormGroup).removeClass("has-error");
                }
            }

            if ($(divFormGroup).hasClass("has-error")) {
                hasError = true;
                if (errorFields.indexOf(label) === -1) {
                    errorFields += "<p>" + label + "</p>";
                }
            }
        }
    });
    if (hasError === true) {
        messageAlert("Champs obligatoires non renseignés:<br/>" + errorFields);
        var firstDivError = $(".has-error").first();
        if (panelId.indexOf("dialogPanel") !== -1) {
            var elementError = $(firstDivError).children().find(":input:not([type=hidden])");
            $(elementError).focus();
        } else {
            $("html, body").animate({
                scrollTop: $(firstDivError).offset().top
            }, 1000);
        }
    }
    return hasError;
}

function checkRegExp(element, label, value) {
    var rexp = $(element).attr("data-regexp");
    if (rexp !== undefined) {
        var regexp = new RegExp(rexp);
        if (regexp.test(value) === false) {
            if (label.charAt(label.length - 1) === "*" || label.charAt(label.length - 1) === "~") {
                label = label.substring(0, label.length - 1);
            }
            messageWarning("Le champ « <b>" + label + "</b> » n'est pas valide");
            return false;
        }
    }
    return true;
}

function checkNotAllowCharacter(value) {
    value = value === undefined || value === null ? "" : value;
    if (value !== "") {
        value = value.replace(/\\/g, "").replace(/"/g, "").replace(/(\r\n|\n|\r)/gm, " ");
    }
    return value;
}
