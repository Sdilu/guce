﻿
function startWfAction(wflId, wfvId, actionId, flowId, entityId) {
    var btn = $(event.target);
    $(btn).attr("disabled", "disabled");
    var box = startLoader();
    $.ajax({
        type: "POST",
        url: startWfActionUrl,
        data: {
            wflId: wflId, wfvId: wfvId, actionId: actionId, flowId: flowId, entityId: entityId
        },
        success: function (response) {
            window.location.href = response;
            //$(btn).attr("disabled", null);
            //stopLoader(box);
        },
        error: function (response) {
            messageAlert(response.responseText);
            console.log(response.responseText);
            stopLoader(box);
            messageAlert("Erreur pendant le démarrage de l'action  ...");
        }
    });
}
