﻿// CSS Style

var size = "font-size: 11px;";
var sizeTitre = "font-size: 14px;";
var font = "font-family: Arial, Helvetica, sans-serif;";
var borderCadreRed = "border:2px solid black;padding: 8px;";
var borderCadreBlack = "border:2px solid black;padding: 5px;";
var titreCadre = "text-align: center;vertical-align: middle;line-height: 25px;" + sizeTitre;
var subTitreCadre = "text-align: left;vertical-align: middle;line-height: 22px;";
var valueCheck = "<div style=\"border:1px solid black;margin:1px;width:10px;\"><b>X</b></div>";
var valueUnCheck = "<div style=\"border:1px solid black;margin:1px;width:10px;\">&nbsp;</div>";

var hrStyle = "display: block;height: 1px;border: 0;border-top: 1px solid black;padding: 0;width:100%";
var checkboxStyle = "style='margin-right:3px;height:8px;width:13px;'";
var intercalaireObjetSocialP1 = false;

function PrintGreffier(type, data) {
    var entityId = $("#EntityId").val();
    var flowId = getUrlParameter("flowId");
    $.ajax({
        type: "POST",
        url: FindFluxValues,
        data: { entityId: entityId, flowId: flowId },
        success: function (infoFlux) {
            var infoFluxString = $.parseJSON(infoFlux);
            var formatText = infoFluxString.replace("}{", ",");
            var infoFluxJson = $.parseJSON(formatText);
            if (type) {
                if (inscriptionComplementaire) {
                    PrintMo(data, infoFluxJson);//to be fixed
                    PrintDepotActeModif(data, infoFluxJson);
                } else {
                    PrintMo(data, infoFluxJson);
                    PrintDepotActe(data, infoFluxJson);
                }
            } else {
                if (inscriptionComplementaire) {
                    PrintP1(data, infoFluxJson);//to be fixed
                } else {
                    PrintP1(data, infoFluxJson);
                }
            }
        },
        error: function () {
            messageAlert("Erreur lors de la génération du rapport...");
        }
    });
}

function PrintMo(data, infoFluxJson) {
    var renderHtml = "<div style='font-family: Arial, Helvetica, sans-serif;'>";
    renderHtml += getEnTeteMo_G(data);
    renderHtml += getInfoBase_G(data);

    var renderHtml1 = getAssocieHtml_G(data);
    var renderHtml2 = getDirigeantsHtml_G(data);
    var renderHtml3 = getCommissaireAuCompte(data);
    var rendreHtml4 = getFooterMo(data, infoFluxJson);
    renderHtml3 += "</div>";
    $("#textToPrint").html(renderHtml + renderHtml1 + renderHtml2 + renderHtml3 + rendreHtml4);

    if ($("#interCalaireEntity").is(":visible")) {
        intercalerMoBis(data, infoFluxJson);
    }
}

function PrintDepotActe(data, infoFluxJson) {
    var output = showData(data.dateImmatriculationRCCM, "date");
    $.ajax({
        type: "POST",
        url: GenerateDateToWord,
        data: { dateToConvert: output },
        success: function (dateConverted) {
            var comparant = getRequerantNames(data, infoFluxJson);
            // Début de la DIV principale .
            var renderHtml = "<div style='font-family: Arial, Helvetica, sans-serif;margin:8px;text-align:justify;font-size:14px;padding:12px;'>";
            renderHtml += renderHeaderActeDepot(output);
            renderHtml += "<div style='text-align: center;'>" +
                "<b><u><p style='margin-top:75px;font-size:16px;'>DEPOT  AU  GREFFE DES STATUTS DE LA SOCIETE</p></u></b></div>";
            renderHtml += "<div style='font-size:13px;margin-top:25px;text-transform:uppercase;'> " + dateConverted + ".</div>";
            //var comparant2 = data.dirigeant[0].personneDirigeant.civilite.value + " " + data.dirigeant[0].personneDirigeant.prenom +
            //    " " + data.dirigeant[0].personneDirigeant.nom + (data.dirigeant[0].personneDirigeant.postNom != null
            //    ? " " + data.dirigeant[0].personneDirigeant.postNom : "") + ", " + (data.dirigeant[0].fonction != null ? data.dirigeant[0].fonction.value : "");
            renderHtml += "</br><div style='font-size:13px'><b><u>A COMPARU</u></b> : " + comparant + "</div>";
            renderHtml += "<div style='margin-top:10px;'>";
            renderHtml += "Au Greffe du Guichet Unique de création d’Entreprise de Kinshasa – Gombe, devant nous, " +
                " <b>ONEZIME KAUNDA MBIYA</b>, Greffier Titulaire.</div>";

            renderHtml += "<div style='margin-top:10px;'>Aux fins du dépôt de deux  copies certifiées conformes des statuts (article 47 alinéa " +
                "1 de l’Acte Uniforme sur le Droit commercial général du traité de l’OHADA dûment enregistrés aux domaines " +
                "<b>Folio …………… du ……… / ……… / " + new Date().getFullYear() + "</b> de la société <b>« " + data.nomCommercial + " »</b> " +
                (data.sigle !== null ? "en sigle « <b>" + data.sigle + "</b> »" : "") + " " + getSigleFormeJuridiqueN3(data.formeJuridiqueN3.code) + "";
            renderHtml += "</div>";
            renderHtml += "<div style='margin-top:10px;'><b style='text-transform:uppercase;'><u>Cette société a pour objet</u> : </b></div>";
            renderHtml += "<div style='margin-top:10px;text-align:justify;'>" + (data.objetSocial !== null ? data.objetSocial : "") + ".</div>";

            renderHtml += "<div style='text-align: justify;margin-top:10px;'>Sa durée est fixée à " + data.duree + " années consécutivement  à son immatriculation au Registre de Commerce et de " +
                "Crédit Mobilier, sauf le cas de  dissolution anticipée ou Prorogation.</div>";
            
            renderHtml += "<div style='text-align: justify;margin-top:10px;'>Le siège social est fixé à KINSHASA, " + data.adresseSiegeSocial._name.replace(", CD", ", RD Congo") + ".</div>";
            renderHtml += "<div style='text-align: justify;margin-top:10px;'>Le capital social est fixé à la somme de : <b>" + (data.capitalSocial !== null ? data.capitalSocial + " CDF" : "______________") + "</b></div>";
            renderHtml += "<div style='text-align: justify;margin-top:10px;'>La société a été immatriculée au Registre de  Commerce et du Crédit  Mobilier sous " +
                "le numéro <b>" + data.rccm + "</b> du <b>" + showData(data.dateImmatriculationRCCM, "date") + "</b>.</div>";

            renderHtml += "<div style='text-align: justify;margin-top:10px;'>Acte de dépôt susdit a de suite été octroyé au comparant lequel après lecture des " +
                "présentes a signé avec nous, les jours, mois, heures et an que dessus.";

            renderHtml += "<div style='height:15px;font-weight: bold;margin-top:10px;'>" +
                "<p style='width:180px;float:right;'>LE GREFFIER TITULAIRE</p></div>";

            renderHtml += "</div>";
            $("#acteDepot").html(renderHtml);
        },
        error: function() {
            messageAlert("Erreur lors de la génération du rapport ...");
        }
    });
}

function PrintDepotActeModif(data, infoFluxJson) {
    var output = todayDate();
    $.ajax({
        type: "POST",
        url: GenerateDateToWord,
        data: { dateToConvert: output },
        success: function (dateConverted) {
            var comparant = getRequerantNames(data, infoFluxJson);
            // Début de la DIV principale .
            var renderHtml = "<div style='font-family: Arial, Helvetica, sans-serif;margin:8px;text-align:justify;font-size:14px;padding:12px;'>";
            renderHtml += renderHeaderActeDepot(output);
            renderHtml += "<div style='text-align: center;'><b><u><p style='margin-top:75px;font-size:15px;'>" +
                "DEPOT  AU  GREFFE DU PROCES VERBAL DE L'ASSEMBLEE GENERALE</p></u></b></div>";

            renderHtml += "<div style='text-align: center;'><u><p style='margin-top:10px;font-size:14px;'>" +
                "<b>" + data.nomCommercial + "</b> " + (data.sigle !== null ? "en sigle « <b>" + data.sigle + "</b> »" : "") + " <b>"
                + getSigleFormeJuridiqueN3(data.formeJuridiqueN3.code) + "</b></p></u></div>";
            renderHtml += "<div style='font-size:13px;margin-top:25px;text-transform:uppercase;'> " + dateConverted + ".</div>";
            renderHtml += "<div style='margin-top:10px;'>Au Greffe du Guichet Unique et par devant nous, " +
                " <b>ONEZIME KAUNDA MBIYA</b>, Greffier Titulaire soussigné.</div>";
            //var comparant2 = data.dirigeant[0].personneDirigeant.civilite.value + " " + data.dirigeant[0].personneDirigeant.prenom +
            //    " " + data.dirigeant[0].personneDirigeant.nom + (data.dirigeant[0].personneDirigeant.postNom != null
            //    ? " " + data.dirigeant[0].personneDirigeant.postNom : "") + ", " + (data.dirigeant[0].fonction != null ? data.dirigeant[0].fonction.value : "");
            renderHtml += "</br><div style='font-size:13px'><b><u>A COMPARU</u></b> : " + comparant + "</div>";

            renderHtml += "<div style='margin-top:10px;'>Lequel en exécution des dispositions de l'Acte Uniforme portant sur le Droit Commercial Général a requis " +
                "acte de dépôt qu'il fait présentement au rang des minutes du Greffe du Guichet Unique de Création d'Entreprise.";
            renderHtml += "</div>";
            
            renderHtml += "<div style='margin-top:10px;text-align:justify;font-style:italic;'>" + (data.objetSocial !== null ? data.objetSocial : "") + ".</div>";
            
            renderHtml += "<div style='text-align: justify;margin-top:10px;'>Dûment enregistrés aux domaines du Greffier Titulaire du Guichet Unique de Création " +
                "d'Entreprise de Kinshasa - Gombe, Folio N° ....................... du ............ / .......... / " + new Date().getFullYear() + ".";

            renderHtml += "<div style='text-align: justify;margin-top:10px;'>Acte de dépôt susdit a de suite été octroyé au comparant lequel après lecture des " +
                "présentes a signé avec nous, les jours, mois, heures et an que dessus.";

            renderHtml += "<div style='height:15px;font-weight: bold;margin-top:10px;'>" +
                "<p style='width:180px;float:right;'>LE GREFFIER TITULAIRE</p></div>";

            renderHtml += "</div>";
            $("#acteDepot").html(renderHtml);
        },
        error: function () {
            messageAlert("Erreur lors de la génération du rapport ...");
        }
    });
}

function ModificationPersoneMorale(data) {
    // Début de la DIV principale .
    var renderHtml = "<div style=\"font-family: Arial, Helvetica, sans-serif;\">";
    renderHtml += getEnTeteModificationMersonneMorale(data);
    renderHtml += getInfoBase_G(data);

    var renderHtml1 = getAssocieHtml_G(data);
    var renderHtml2 = getDirigeantsHtml_G(data);
    var renderHtml3 = getCommissaireAuCompte(data);
    var rendreHtml4 = getFooterMo(data);

    renderHtml3 += "</div>";
    $("#textToPrint3").html(renderHtml + renderHtml1 + renderHtml2 + renderHtml3 + rendreHtml4);
}

function PrintP1(data, infoFluxJson) {
    var renderHtml = "<div style='" + font + "'>";
    renderHtml += getEnteteP1(data);
    var renderHtml1 = infoPersonnePhysique(data);
    intercalaireObjetSocialP1 = false;
    var renderHtml2 = infoEtablissement(data);
    var renderHtml3 = infoActiviteAnterieur(data);

    var renderHtml4 = infoAutresPersonnes(data);
    var renderHtml5 = getP1Footer(data, infoFluxJson);
    var renderHtml6 = endP1(data);

    $("#textToPrint1").html(renderHtml4 + renderHtml5 + renderHtml6);
    $("#textToPrint").html(renderHtml + renderHtml1 + renderHtml2 + renderHtml3);
}

function checkProp(data, prop, test, code) {
    if (code) {
        return "<img " + checkboxStyle + " src='" + (data[prop]["code"] === test ? checkUrl : uncheckUrl) + "'>";
    } else {
        return "<img " + checkboxStyle + " src='" + (data[prop] === test ? checkUrl : uncheckUrl) + "'>";
    }
}

function getProp(prop, test) {
    var tab = test.split(",");
    var found = false;
    for (var i = 0; i < tab.length; i++) {
        if (tab[i] === prop) {
            found = true;
            break;
        }
    }
    return "<img " + checkboxStyle + " src='" + (found ? checkUrl : uncheckUrl) + "'>";
}
//-----------------------------------------------
function getEnteteP1(data) {
    var enTete = "<div style='font-size:14px;font-weight:bold;text-align:center;margin-top:20px;'>FORMULAIRE DE DEMANDE<div>";
    enTete += "<table style='margin-top:20px;width:100%;text-align:justify;'>" +
        "<tr><td><p style='font-size:16px;font-weight:bold;text-align:center;width:90px;'>RCCM <br/>2010 - P<sub>1</sub></p></td>" +
            "<td style='font-size:12px;'>" +
            "<div style='margin-left:50px;'>" + checkProp(data, "formeJuridiqueN3", "A10", true) + "D'IMMATRICULATION PRINCIPALE D'UNE PERSONNE PHYSIQUE</div><span style='margin-left:10px;font-size:10px;'>ou</span>" +
            "<div style='margin-left:50px;'>" + checkProp(data, "formeJuridiqueN3", "A20", true) + "D'IMMATRICULATION SECONDAIRE OU D'UNE SUCCURSALE</div><span style='margin-left:10px;font-size:10px;'>ou</span>" +
            "<div style='margin-left:50px;'>" + checkProp(data, "formeJuridiqueN3", "", true) + "REPRISE D'ACTIVITE</div>" +
            "</td></tr></table>" +
        "</div>";
    return enTete;
}

function dashedText(value, width, upper) {
    return "<label style='width:" + width + "%;padding:2px;" + (upper ? "text-transform:uppercase;" : "") + "text-align:center;'>"
        + (value !== null ? value : "") + "</label>";
}

function addIndex(indice, marginLeft) {
    return "<span style='position: static;margin-left:" + marginLeft + "px;margin-right:9px;font-size:12px;'>" + indice + "</span>";
}

function infoPersonnePhysique(data) {
    var renderHtml = "<div style='margin-top:20px;text-align:center;'>RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE ASSUJETTIE</div>";
    renderHtml += "<div style='margin:5px;font-weight: bold;border:2px solid black;padding: 5px;text-align:justify;'>" +
        "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;padding:3px;border-spacing:0 5px;'>" +
           "<tr><td>" + addIndex("1", "-17") + "<b>NOM</b> : "
           + getProp(data.dirigeant[0].personneDirigeant.civilite.code, "M.") + "Mr " +
        "" + getProp(data.dirigeant[0].personneDirigeant.civilite.code, "Mme") + "Mme " +
        " " + getProp(data.dirigeant[0].personneDirigeant.civilite.code, "Mlle") + "Mlle " +
        dashedText(data.dirigeant[0].personneDirigeant.nom + (data.dirigeant[0].personneDirigeant.postNom !== null ? " " + data.dirigeant[0].personneDirigeant.postNom : ""), 30, true) + " "
        + " <b>PRENOM</b> : " + dashedText(data.dirigeant[0].personneDirigeant.prenom, 25, true) + "</td></tr>" +
        "<tr><td style='text-align:justify;'>" + addIndex("2", "-17") + "<b>DATE et LIEU NAISSANCE</b> : "
        + showData(data.dirigeant[0].personneDirigeant.dateNaissance, "date") + " A <span style='text-transform:uppercase;margin-right:10px;'>"
        + data.dirigeant[0].personneDirigeant.lieuNaissance
        + "</span> <b>NATIONALITE</b> : " + data.dirigeant[0].personneDirigeant.paysNaissance.value + "</td></tr>" +

        "<tr><td style='text-align:justify;'>" + addIndex("3", "-17") + "<b>DOMICILE PERSONNEL</b> : "
        + data.dirigeant[0].personneDirigeant.adresse._name.replace(", CD", ", RD Congo") + "</td></tr>" +
        "<tr><td>" + addIndex("4", "-17") + "<b>VILLE</b> : <span style='text-transform:uppercase;margin-right:10px;'>"
        + data.dirigeant[0].personneDirigeant.adresse.adresseRDC.ville.value + "</span></td></tr>" +
        "<tr><td><b>AUTRES PRECISIONS</b> : </td></tr>" +
        "<tr><td><b>COORDONNEES ELECTRONIQUES</b> (<i>s'il y a lieu</i>) : " + (data.dirigeant[0].personneDirigeant.email !== null ? data.dirigeant[0].personneDirigeant.email : "") + "</td></tr>" +
        "<tr><td>" + addIndex("5", "-17") + "<b>SITUATION MATRIMONIALE</b> : " +
        getProp(data.dirigeant[0].personneDirigeant.situationMatrimoniale.code, "C") + " Célibataire " +
        getProp(data.dirigeant[0].personneDirigeant.situationMatrimoniale.code, "M") + " Marié(e) " +
        getProp(data.dirigeant[0].personneDirigeant.situationMatrimoniale.code, "V") + " Veuf(ve) " +
        getProp(data.dirigeant[0].personneDirigeant.situationMatrimoniale.code, "D") + " Divorcé(e) "
        + "</td></tr>" +
        "</table>" +
        "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 11px;width:100%;margin-top:10px;' border='1'>" +
        "<thead><th style='text-align:center;'>Conjoint(s)</th><th style='text-align:center;'>Nom - Prénoms</th><th style='text-align:center;'>Date et lieu <br>" +
        "du mariage</th><th style='text-align:center;'>Option<br>matrimoniale</th>" +
        "<th style='text-align:center;'>Régime<br>matrimoniale</th><th style='text-align:center;'>Clauses restrictives</th>" +
        "<th style='text-align:center;'>Demandes-en<br>séparation de biens</th></thread>" +
        "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>&nbsp;</td></tr>" +
        "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>&nbsp;</td></tr>" +
        "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>&nbsp;</td></tr>" +
        "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>&nbsp;</td></tr>" +
        "</table>" +
        "</div>";

    var result = renderHtml;
    return result;
}

function infoEtablissement(data) {
    var objetSocial = data.objetSocial !== null ? data.objetSocial : "";
    var tab = cutString(objetSocial, 70).split("-||-");
    objetSocial = tab[0];
    if (tab.length > 1) {
        var renderObjetSocial = "<div style='font-family: Arial, Helvetica, sans-serif;margin-top:10px;text-align:center;font-size:13px;'>" +
            "<b><u>INTERCALAIRE COMPLETANT UNE RUBRIQUE</u></b></div>";
        renderObjetSocial += "<div style='font-family: Arial, Helvetica, sans-serif;margin:10px 0;text-align:center;font-size:12px;'>" +
            "Rubrique N° <b><u>7</u></b> (Préciser le numéro de la rubrique) Numéro de la page de l'intercalaire : <b>2</b></div>";
        renderObjetSocial += "<div style='font-family: Arial, Helvetica, sans-serif;font-size:12px;text-align:justify;'>" + tab[1] + ".";
        renderObjetSocial += "<div style='font-family: Arial, Helvetica, sans-serif;margin:5px 0;text-align:center;font-size:12px;'><b><u>... / / / ...</u></b></div>";
        $("#interCalaireObjetSocial").html(renderObjetSocial + "</div>");
        intercalaireObjetSocialP1 = true;
    }

    var renderHtml = "<div style='margin-top:20px;text-align:center;'>RENSEIGNEMENTS RELATIFS A L'ETABLISSEMENT ET A L'ACTIVITE</div>";
    renderHtml += "<div style='margin:5px;font-weight: bold;border:2px solid black;padding: 5px;text-align:justify;'>" +
        "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;padding:3px;'>" +
        "<tr><td style='text-align:justify;'>" + addIndex("6", "-17") + "<b>NOM COMMERCIAL</b> (<i>s'il y a lieu</i>) : <b>« " + data.nomCommercial + " »</b></td></tr>" +
        "<tr><td style='text-align:justify;'><b>SIGLE OU ENSEIGNE</b> (<i>s'il y a lieu</i>) : " + (data.enseigne !== null ? data.enseigne : "") +
        " " + (data.sigle !== null ? "(" + data.sigle + ")" : "") + "</td></tr>" +
        "<tr><td style='text-align:justify;'>" + addIndex("7", "-17") + "<b>ACTIVITES EXERCEES</b> (préciser) : " + objetSocial + "</td></tr>" +
        "<tr><td style='text-align:justify;'>" + addIndex("8", "-17") + "<b>DATE DE DEBUT</b> : " + showData(data.dateDebutExploitation, "date") +
        " <b>N°RCCM</b> (<i>s'il y a lieu</i>) : <b style='font-size:13px;'>" + data.rccm + "</b> " + (data.numeroNRC !== null ? "(" + data.numeroNRC + ")" : "") + "</td></tr>" +
        "<tr><td style='text-align:justify;'>" + addIndex("9", "-17") + "<b>ADRESSE DE L'ETABLISSEMENT PRINCIPAL</b> (géographique et postale) : " + data.adresseSiegeSocial._name.replace(", CD", ", RD Congo") + "</td></tr>" +
        "<tr><td style='text-align:justify;'>" + addIndex("10", "-23") + "<b>ORIGINE</b> : " +
        getProp(data.origine.code, "C,H,R") + "Création " +
        getProp(data.origine.code, "") + "Achat " +
        getProp(data.origine.code, "") + "Prise en location gérance</td></tr>" +
        "<tr><td style='text-align:justify;margin-top:5px;margin-bottom:5px;'>" + addIndex("11", "-23") + "<b>ETABLISSEMENT SECONDAIRE OU SUCCURSALE</b></td></tr>" +
        "<tr><td style='text-align:justify;'><b>NOM COMMERCIAL</b> (<i>s'il y a lieu</i>) : </td></tr>" +
        "<tr><td style='text-align:justify;'><b>SIGLE OU ENSEIGNE</b> (<i>s'il y a lieu</i>) : </td></tr>" +
        "<tr><td style='text-align:justify;'>" + addIndex("12", "-23") + "<b>DATE D'OUVERTURE</b> : </td></tr>" +
        "<tr><td style='text-align:justify;'><b>ADRESSE</b> (géographique et postale) : " + (data.adressesEtablissementsSecondaires[0] !== null
            && data.adressesEtablissementsSecondaires[0] !== undefined ? data.adressesEtablissementsSecondaires[0]._name.replace(", CD", ", RD Congo") : "") + "</td></tr>" +
        "<tr><td style='text-align:justify;'><b>ACTIVITE (S) AJOUTE D'ACTIVITE</b> (préciser) : </td></tr>" +
        "</table>" +
        "</div>";
    return renderHtml;
}

function infoActiviteAnterieur(data) {
    var renderHtml = "<div style='margin-top:20px;text-align:center;'>RENSEIGNEMENTS RELATIFS AUX ACTIVITES ANTERIEURES</div>";
    renderHtml += "<div style='margin:5px;font-weight: bold;border:2px solid black;padding: 5px;text-align:justify;'>" +
        "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;'>" +
        "<tr><td style='text-align:justify;'>" + addIndex("13", "-23") + "<b>Exercice d'une précédente activité</b> : </td><td style='text-align:justify;padding:3px;'>" +
        getProp("non", "") + " NON </td></tr>" +
    "<tr><td></td><td style='text-align:justify;padding:3px;'>" +
    getProp("oui", "") + " OUI " +
    getProp("commerciale", "") + " commerciale " +
    getProp("autre", "") + " autre: (préciser) </td></tr>"
        + "</table>" +
        "<ul style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;'>" +
        "<li>Période : de (mois et an) ................... à ...................., précédent N° RCCM (<i>s'il y a lieu</i>) .............</li>" +
        "<li>Nature de l'activité ...............................................................................................................</li>" +
        "<li>Principal établissement ......................................................................................................</li>" +
        "<li>Etablissement(s) secondaire(s) .......................................................... N° RCCM (<i>s'il y a lieu</i>) .............</li>" +
        "<li>ADRESSE (géographique et postale) : ................................................................................ </li>" +
        "</ul>" +
        "</div>";
    return renderHtml;
}

function infoAutresPersonnes(data) {//data.dirigeant[0]
    var renderHtml = "<p style='font-family: Arial, Helvetica, sans-serif;font-size:16px;font-weight:bold;text-align:center;width:90px;'>RCCM <br/>2010 - P<sub>1</sub></p>";
    renderHtml += "<div style='margin-top:20px;text-align:center;font-family: Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;'>AUTRES PERSONNES POUVANT ENGAGER LA PERSONNE PHYSIQUE ASSUJETTIE</div>";
    renderHtml += "<div style='margin:5px;font-weight: bold;border:2px solid black;padding: 5px;text-align:justify;margin:10px;'>" +
        addIndex("14", "-23") + "<ul style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;text-align:justify;'>";

    if (data.dirigeant[1] !== null && data.dirigeant[1] !== undefined) {
        renderHtml += "<li style='margin-top:10px;'><b>Nom</b> : " + dashedText(data.dirigeant[1].personneDirigeant.nom, 30, true) + " " +
        "<b>Prénom</b> : " + dashedText(data.dirigeant[1].personneDirigeant.prenom, 25, true) + " <br/>" +
        "<b>Date, lieu de naissance</b> : "
        + showData(data.dirigeant[1].personneDirigeant.dateNaissance, "date") + " A <span style='text-transform:uppercase;margin-right:10px;'>"
        + data.dirigeant[1].personneDirigeant.lieuNaissance
        + "</span>" +
        "<b>nationalité</b> : " + data.dirigeant[1].personneDirigeant.paysNaissance.value + " " +
        "<br/><b>Domicile</> : " + data.dirigeant[1].personneDirigeant.adresse._name.replace(", CD", ", RD Congo") + " </li>";
    } else {
        renderHtml += "<li style='margin-top:10px;'>Nom .............................................................................. " +
        "Prénom .............................................................................. <br/>" +
        "Date, lieu de naissance .......................................... " +
        "nationalité .......................................................................... " +
        "<br/>Domicile .......................................................................... " +
        " ............................................................................................ </li>";
    }
        
    if (data.dirigeant[2] !== null && data.dirigeant[2] !== undefined) {
        renderHtml += "<li style='margin-top:10px;'><b>Nom</b> : " + dashedText(data.dirigeant[2].personneDirigeant.nom, 30, true) + " " +
        "<b>Prénom</b> : " + dashedText(data.dirigeant[2].personneDirigeant.prenom, 25, true) + " <br/>" +
        "<b>Date, lieu de naissance</b> : "
        + showData(data.dirigeant[2].personneDirigeant.dateNaissance, "date") + " A <span style='text-transform:uppercase;margin-right:10px;'>"
        + data.dirigeant[2].personneDirigeant.lieuNaissance
        + "</span>" +
        "<b>nationalité</b> : " + data.dirigeant[2].personneDirigeant.paysNaissance.value + " " +
        "<br/><b>Domicile</> : " + data.dirigeant[2].personneDirigeant.adresse._name.replace(", CD", ", RD Congo") + " </li>";
    } else {
        renderHtml += "<li style='margin-top:10px;'>Nom .............................................................................. " +
        "Prénom .............................................................................. <br/>" +
        "Date, lieu de naissance .......................................... " +
        "nationalité .......................................................................... " +
        "<br/>Domicile .......................................................................... " +
        " ............................................................................................ </li>";
    }
    renderHtml += "</ul></div>";
    return renderHtml;
}

function getP1Footer(data, infoFluxJson) {
    var renderHtml1 = "";
    renderHtml1 += "<div style='" + borderCadreBlack + font + size + ";margin-top:20px;text-align:justify;margin:10px;'>";
    renderHtml1 += addIndex("15", "-23") + "LE SOUSSIGNE (préciser si mandataire) : " + getRequerantNames(data, infoFluxJson);
    renderHtml1 += "<table style='" + font + size + "'>" +
        "<tr><td style='width:600px;padding:3px;'>";
    renderHtml1 += "Demande à ce que la présente constitue " +
        "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;text-align:justify;margin-top:12px;'><tr>" +
            "<td>" +
            "<div style='margin-left:50px;padding:5px;'>" + checkProp(data, "formeJuridiqueN3", "A10", true) + " UNE DEMANDE D'IMMATRICULATION PRINIPALE AU R.C.C.M</div>" +
            "<div style='margin-left:50px;padding:5px;'>" + checkProp(data, "formeJuridiqueN3", "A20", true) + " UNE DEMANDE D'IMMATRICULATION SECONDAIRE</div>" +
            "<div style='margin-left:50px;padding:5px;'>" + checkProp(data, "formeJuridiqueN2", "A2", true) + " UNE DEMANDE D'OUVERTURE D'UNE SUCCURSALE</div>" +
            "<div style='margin-left:50px;padding:5px;'>" + checkProp(data, "formeJuridiqueN3", "", true) + " UNE DEMANDE DE REPRISE D'ACTIVITE</div>" +
            "</td><tr></table></td>";
    renderHtml1 += "<td><div style='" + borderCadreBlack + ";width:140px;float:left;height:100px;margin-right:5px;'>Fait à Kinshasa" +
        " <br/>Le " + showData(data.dateImmatriculationRCCM, "date") + " <br/><br/>Signature</div></td></tr>";
    renderHtml1 += "</table>";
    renderHtml1 += "</div>";

    var renderHtml2 = "";

    renderHtml2 += "<div style='" + borderCadreBlack + font + size + "margin-top:20px;text-align:justify;margin:10px;'>";
    renderHtml2 += "<div style='font-size: 12px;'>" + addIndex("16", "-23") + "" +
        "Le greffier ou le responsable de l'organe compétent soussigné a reçu le formulaire sous " +
        "le numéro d'ordre : ......................  du registre d'arrivée. <br/>" +
        "" + addIndex("17", "-23") + "La régularité de la demande a été vérifiée en application de " +
        "l'article 44 de l'Acte Uniforme portant sur le droit commercial général par le Greffier ou le responsable de l'organe  compétent qui a :</div>" +
        "<div style='margin-left: 10px;font-style:italic;margin-top:10px;'>"
        + checkProp(data, "formeJuridiqueN3", "A10", true) + " Immatriculé au RCCM la personne physique sous le numéro <b style='font-size:13px;'>" + data.rccm + "</b> "
        + (data.numeroNRC !== null ? "(" + data.numeroNRC + ")" : "") + " et délivré un accusé d'enregistrement,<br/>"
        + checkProp(data, "formeJuridiqueN3", "...", true) + " Rejeté la demande au(x) motif(s) que : ...................................................... ......................................................";
    renderHtml2 += "</div>";
    renderHtml2 += "<table style='" + font + size + "'><tr><td>";
    if (intercalaireObjetSocialP1) {
        renderHtml2 += "<div style='margin-left:10px;margin-top:20px;font-style:normal;margin-bottom:10px;'>Intercalaire(s) complétant la ou les rubriques(s) n°(s) ..... <b>7</b> .... " +
        "" + getProp("***", "***") + "OUI " +
        "" + getProp("***", "") + "NON (si oui nombre de pages intercalaires : ... 2 ... )</div>";
    } else {
        renderHtml2 += "<div style='margin-left:10px;margin-top:20px;font-style:normal;margin-bottom:10px;'>Intercalaire(s) complétant la ou les rubriques(s) n°(s) ......... " +
        "" + getProp("***", "") + "OUI " +
        "" + getProp("***", "***") + "NON (si oui nombre de pages intercalaires : ..... )</div>";
    }
    
    renderHtml2 += "Fait à Kinshasa, le " + todayDate() + " (JJ/MM/AAAA)";

    renderHtml2 += "<br/><br/>Signature du Greffier (Nom, prénoms, titre et juridiction) ou du responsable de l'organe compétent :<br/>" +
        "<b style='font-size:13px;'>ONEZIME KAUNDA MBIYA</b>, <span style='font-size:13px;'>Greffier Titulaire du Guichet Unique</span> ..................................................." +
        "<div style='" + borderCadreBlack + ";width:140px;float:right;height:50px;margin-top:10px;'><br/>Signature</div></td></div></tr>" +
        "</table></div>";
    return renderHtml1 + renderHtml2;
}

function endP1(data) {
    return "<div style='" + borderCadreBlack + size + font + size + "margin-top:20px;text-align:justify;padding-bottom:15px;margin:10px;'>" +
        "" + addIndex("18", "-23") + "(En cas de rejet de " +
        "la demande par le greffier ou le responsable de l’organe compétent) " +
        "<b>Le demandeur atteste que le présent formulaire y compris le(s) intercalaire(s) y relatifs</b> (s’il y a lieu) " +
        "<b>comportant les motifs du rejet de sa demande lui a été remis le ........ / ........ / ......... (JJ/MM/AAAA) et reconnaît que " +
        "cette remise vaut notification de rejet. (Signature du Demandeur) <br/><br/>" +
        "<div style='float:right;width:200px;border-bottom:1px black solid;'></div></div>";
}

//----------------------------------------------

function labelData(value, percent) {
    return "<label style=';width:" + percent + "%;text-align:center;border-bottom:dotted 1px lightgray;'>" + value + "</label>";
}

function getSigleFormeJuridiqueN3(code) {
    if (code === "B10") {
        return "S.C.S.";
    } else if (code === "B11") {
        return "S.N.S.";
    } else if (code === "B12") {
        return "S.A.R.L.";
    } else if (code === "B13") {
        return "S.A.R.L.U.";
    } else if (code === "B14") {
        return "S.A.";
    } else if (code === "B14b") {
        return "S.A.";
    } else if (code === "B15") {
        return "S.A.U.";
    } else if (code === "B15b") {
        return "S.A.U.";
    } else if (code === "B16") {
        return "S.A.S.";
    } else if (code === "B17") {
        return "S.A.S.U.";
    } else if (code === "B40") {
        return "S.C.I.";
    } else if (code === "B41") {
        return "S.C.P.";
    } else if (code === "C10") {
        return "G.I.E.";
    } else if (code === "C10b") {
        return "G.I.E.";
    }
    return "";
}

function getEnTeteMo_G(data) {
    var enTete = "<div style='border:2px solid black;padding: 8px 8px 0;margin-left:10px;'>" +
        "<table style='width:100%;text-align:justify;'>" +
        "<tr><td><div style='width:80px;float:left;'><img style='float:left;width:50px;height:50px;' src='" + logoMo + "'/></div></td>" +
            "<td style='font-size:12px;'>" +
            "<div style='margin-left:50px;'><b>DECLARATION</b> " + checkProp(data, "formeJuridiqueN2", "B1", true) + " <b>de CONSTITUTION</b> DE PERSONNE MORALE</div>" +
            "<div style='margin-left:50px;'>ou " + checkProp(data, "formeJuridiqueN3", "B20", true) + "d'OUVERTURE <b>d’un ETABLISSEMENT SECONDAIRE</b></div>" +
            "<div style='margin-left:50px;'>ou " + checkProp(data, "formeJuridiqueN3", "B21", true) + "d'OUVERTURE d’une <b>SUCCURSALE</b> d’une personne morale ETRANGERE</div>" +
            "</td></tr></table>" +
        "<div style='font-family: Arial, Helvetica, sans-serif;font-size:9px;text-align: left;'>A.P. Porto Novo 23/24 juin 1999</div>" +
        "</div>";
    return enTete;
}

function getEnTeteModificationMersonneMorale(data) {
    var enTete = "";
    var origine = getValueFromDataJson(data, "origine.value");
    enTete += "<div style=\"" + borderCadreBlack + "\">" +
        "<div style=\"height:50px;\">" +
            "<div style=\"text-align: center;vertical-align: middle;line-height: 50px;\">DECLARATION DE MODIFICATION</div>" +
            "<div style=\"text-align: center;vertical-align: middle;line-height: 50px;\">DE LA PERSONNE MORALE </div>" +
            "<div style=\"text-align: center;vertical-align: middle;line-height: 50px;\">D'UN ETABLISSEMENT</div>" +
            "<div style=\"text-align: center;vertical-align: middle;line-height: 50px;\">D'UN ETABLISSEMENT</div>" +
        "<div style=\"text-align: left;\">A.P. Porto Novo 23/24 juin 1999</div>" +
        "</div></div>";

    return enTete;
}

function getInfoBase_G(data) {
    var renderHtml = "<div style='margin-top:5px;text-align:center;font-size:13px;'><b>RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE</b></div>";
    renderHtml += "<div style='font-family: Arial, Helvetica, sans-serif;border:2px solid black;margin-left:10px;'>" +
        "<table style='width:100%;font-family: Arial, Helvetica, sans-serif;font-size: 11px;padding:3px;border-spacing:0 5px;'>" +
        "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("1", "-17") + "DENOMINATION : <b style='font-size:13px;'>" + data.nomCommercial + "</b></td></tr>" +
            "<tr><td style='text-align:justify;padding-left:3px;'>NOM COMMERCIAL : ______________________ ENSEIGNE : <b>"
            + (data.enseigne !== null ? data.enseigne : "_________________") + "</b> SIGLE : <b>" + (data.sigle !== null ? data.sigle : "_______") + "</b></td></tr>" +
           "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("2", "-17") + "ADRESSE DU SIEGE : <b>" + data.adresseSiegeSocial._name.replace(", CD", ", RD Congo") + "</b></td></tr>" +
           "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("3", "-17") + "ADRESSE DE L’ETABLISSEMENT CREE : <b>" +
           (data.adresseSiegeExploitation !== null ? data.adresseSiegeExploitation._name.replace(", CD", ", RD Congo") : "______________________________________________________")
           + "</b></td></tr>" +
           "<tr><td style='text-align:justify;padding-left:3px;'>FORME JURIDIQUE : <b>" + getSigleFormeJuridiqueN3(data.formeJuridiqueN3.code) + "</b> N° R.C.C.M. du siège : <b>" +
           data.rccm + "</b> </td></tr>" +
           "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("4", "-17") + "CAPITAL SOCIAL : <b>" + (data.capitalSocial !== null ? data.capitalSocial + " CDF" : "______________") + "</b> DONT NUMERAIRES : ______________ DONT EN NATURE : _____________</td></tr>" +
           "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("5", "-17") + "DUREE : <b>" + data.duree + "</b> Années entières consécutives</td></tr>" +
        "</table>" +
        "</div>";

    var objetSocial = data.objetSocial !== null ? data.objetSocial : "";
    var tab = cutString(objetSocial, 70).split("-||-");
    objetSocial = tab[0];
    if (tab.length > 1) {
        var renderObjetSocial = "<div style='font-family: Arial, Helvetica, sans-serif;margin-top:10px;text-align:center;font-size:13px;'>" +
            "<b><u>INTERCALAIRE COMPLETANT UNE RUBRIQUE</u></b></div>";
        renderObjetSocial += "<div style='font-family: Arial, Helvetica, sans-serif;margin:10px 0;text-align:center;font-size:12px;'>" +
            "Rubrique N° <b><u>6 & 7</u></b> (Préciser le numéro de la rubrique) Numéro de la page de l'intercalaire : <b>2</b></div>";
        renderObjetSocial += "<div style='font-family: Arial, Helvetica, sans-serif;font-size:12px;text-align:justify;'>" + tab[1] + ".";
        renderObjetSocial += "<div style='font-family: Arial, Helvetica, sans-serif;margin:5px 0;text-align:center;font-size:12px;'><b><u>... / / / ...</u></b></div>";
        $("#interCalaireObjetSocial").html(renderObjetSocial + "</div>");
    }

    renderHtml += "<div style='margin-top:5px;text-align:center;font-size:13px;'><b>RENSEIGNEMENTS RELATIFS A L'ACTIVITE ET AUX ETABLISSEMENTS</b></div>" +
        "<div style='font-family: Arial, Helvetica, sans-serif;border:2px solid black;margin-left:10px;'>" +
        "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 11px;padding:3px;border-spacing:0 5px;'>" +
        "<tr><td style='text-align:justify;padding: 1px 3px;'>" + addIndex("6", "-17") + "<b>ACTIVITE</b> : ACTIVITE PRINCIPALE : (préciser) " + objetSocial + "</td></tr>" +
        //"<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("7", "-17") + "</td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("8", "-17") + "<span style='margin-left:20px;'>Date de début : <b>"
        + showData(data.dateDebutExploitation, "date") + "</b><span style='margin-right:100px;'></span> Nombre de salariés prévus : <b>" + (data.nbSalariesEtranges + data.nbSalaries) + "</b></span></td></tr>" +
        "<tr style='border-top:1px solid black;'><td style='text-align:justify;padding-left:3px;'><b>PRINCIPAL ETABLISSEMENT OU SUCCURSALE</b> : </td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("9", "-17") + "<span style='margin-left:20px;'>Adresse : "
        + (data.adressesEtablissementsSecondaires[0] !== null && data.adressesEtablissementsSecondaires[0] !== undefined ? data.adressesEtablissementsSecondaires[0]._name
            : "____________________________________________________________________________________") + "</span></td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("10", "-23") + "<span style='margin-left:20px;'>Origine : " +
        getProp(data.origine.code, "C,H,R") + "Création " +
        getProp(data.origine.code, "") + "Achat " + getProp(data.origine.code, "") + "Apport " +
        getProp(data.origine.code, "") + "Prise en location gérance " + getProp(data.origine.code, "") + " Autres </span></td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("11", "-23") + "<span style='margin-left:20px;'>(préciser) : ___________________________ </span></td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'>" + addIndex("12", "-23") + "<span style='margin-left:20px;'>Précédent exploitant : Nom : ___________________________ " +
        "<span style='margin-right:22px;'></span> Prénoms : _____________________________ </span></td></tr>" +
        "<tr style='margin-bottom:5px;'><td style='text-align:justify;padding-left:3px;'>" + addIndex("13", "-23") + "<span style='margin-left:20px;'>Adresse : __________________________________________ " +
        "<span style='margin-right:22px;'></span> N° RCCM : _____________________________ </span></td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'><span style='margin-left:20px;'>Loueur de fonds (nom/dénomination, adresse) : </span></td></tr>" +
        "<tr style='border-top:1px solid black;'><td style='text-align:justify;padding-left:3px;'>" + addIndex("14", "-23") + "<b>ETABLISSEMENTS SECONDAIRES</b> : (autres que celui créé) " +
        "" + getProp(data.origine.code, "") + "Non " + getProp(data.origine.code, "") + "Oui (préciser) : </td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'><span style='margin-left:20px;'>Adresse : ______________________________________________________ </span></td></tr>" +
        "<tr><td style='text-align:justify;padding-left:3px;'><span style='margin-left:20px;'>Acivité : ________________________________________________________ </span></td></tr>" +
        "</table>" +
        "</div>";
    return renderHtml;
}

function getinfoP1(data)
{
    var renderHtml = "<div style=\"" + titreCadre + "\"><b>RENSEIGNEMENTS RELATIFS A L'ETABLISSEMENT ET A L'ACTIVITE </b></div>";
    renderHtml += "<div style=\"" + borderCadreBlack + "\">" +
        "<table style=\"font-size: 12px;\"><tbody>" +
           "<tr><td style=\"width:150px;\">NOM COMMERCIAL(<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "nomCommercial") + "</td></tr>" +
           "<tr><td>ENSEIGNE(<i>s'il y a lieu</i>)</td><td>:</td><td><table style=\"font-size: 10px;\"><tbody><tr><td style=\"width:160px;\">" +
           getValueFromDataJson(data, "enseigne") + "</td><td style=\"width:50px;\">SIGLE(<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" +
           getValueFromDataJson(data, "sigle") + "</td></tr></tbody></table></td>" +
           "<tr><td>ACTIVITE(E) EXERCEE(S)</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "secteurActivite") + "</td></tr>" +
           "<tr><td>ADRESSE DE L'ETABLISSEMENT PRINCIPALE (<i>gographique et postale</i>)</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "adresseSiegeSocial._name") + "</td></tr>" +
           "<tr><td>ORIGINE</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "adresseSiegeSocial._name") + "</td></tr>" +
           "</tbody></table>" +

            "<div style=\"" + subTitreCadre + ";margin-top:10px;\"><b>ETABLISSEMENT SECONDAIRE OU SUCCURSALE</b></div>" +
            "<table style=\"font-size: 10px;\"><tbody>" +
                "<tr><td style=\"width:150px;\"> NOM COMMERCIALE (<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" +
                getValueFromDataJson(data, "adresseSiegeEion") + "</td></tr>" +
                 "<tr><td style=\"width:150px;\"> SIGLE OU ENSEIGNE (<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" +
                getValueFromDataJson(data, "adresseSiegeEion") + "</td></tr>" +
                 "<tr><td style=\"width:150px;\">DATE D'OUVERTURE</td><td style=\"width:10px;\">:</td><td>" +
                getValueFromDataJson(data, "adresseSiegeEion") + "</td></tr>" +
                "<tr><td>ADRESSE (<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "igine") + "</td></tr>" +
                "<tr><td>ACTIVITE(5) (<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "igine") + "</td></tr>" +
            "</tbody></table>" +
        "</div>";

    var renderHtml1 = "<div style=\"" + titreCadre + "\"><b>RENSEIGNEMENTS RELATIFS AUX ACTIVITES ANTERIEURES</b></div>";
    renderHtml1 += "<div style=\"" + borderCadreBlack + "\">" +
         "<tr><td>Exercice d'une précédente activites</td><td style=\"width:10px;\">:</td><td>" + getValueFromDataJson(data, "adresseSi") + "</td></tr>" +

        "<table style=\"font-size: 12px;margin-left:10px;\"><tbody>" +
                "<tr><td>Période</td><td>:</td><td><table style=\"font-size: 10px;\"><tbody><tr><td style=\"width:120px;\">" +
           getValueFromDataJson(data, "enseigne") + "</td><td style=\"width:150px;\">Précédent No RCCM (<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" +
           getValueFromDataJson(data, "sigle") + "</td></tr></tbody></table></td>" +
                 "<tr><td style=\"width:150px;\">Nature de l'activite</td><td style=\"width:10px;\">:</td><td>" +
                getValueFromDataJson(data, "adre") + "</td></tr>" +
                 "<tr><td style=\"width:150px;\">Principale établissement</td><td style=\"width:10px;\">:</td><td>" +
                getValueFromDataJson(data, "adresseSiegeEion") + "</td></tr>" +
                "<tr><td>Etablissement(s) secondaire(s)</td><td>:</td><td><table style=\"font-size: 10px;\"><tbody><tr><td style=\"width:160px;\">" +
           getValueFromDataJson(data, "enseigne") + "</td><td style=\"width:110px;\">No RCCM (<i>s'il y a lieu</i>)</td><td style=\"width:10px;\">:</td><td>" +
           getValueFromDataJson(data, "sigle") + "</td></tr></tbody></table></td>" +
            "<tr><td style=\"width:150px;\">Adresse (<i>géographique et postale</i>)</td><td style=\"width:10px;\">:</td><td>" +
                getValueFromDataJson(data, "adresseSiegeEion") + "</td></tr>" +
            "</tbody></table>" +
       "</div>";
    var render = renderHtml + renderHtml1;
    return render;
}

function getAssocieHtml_G(data) {
    var renderHtml = "";
    renderHtml += "<div style='margin-top:5px;text-align:center;font-size:13px;'><b>ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT (*)</b></div>";

    renderHtml += "<div style='" +borderCadreRed + font + size + ";margin-left:10px'>";
    renderHtml += "<div style='font-size:9px;'>" + addIndex("15", "-27") + "(*) La totalité des renseignements relatifs à ces associés doit IMPERATIVEMENT figurer " +
        "sur le formulaire complémentaires M.o Bis annexé.</div>";
    renderHtml += "<div style='font-size:10px;'>RESUME DES INFORMATIONS :</div>";
    renderHtml += "<table border='1' style='font-size: 10px;border-collapse: collapse;width:100%;'><thead><tr>" +
        "<td style='width:20%;padding:2px;'>NOM</td>" +
        "<td style='width:15%;padding:2px;'>PRENOM</td>" +
        "<td style='width:25%;padding:2px;'>DATE LIEU DE NAISSANCE</td>" +
        "<td style='width:40%;padding:2px;'>ADRESSE</td></tr></thead><tbody>";

        var code = data.formeJuridiqueN3.code;
        if (code === "B11" || code === "B10" || code === "C10" || code === "C10b") {
            for (var i = 0; i < data.associe.length; i++) {
                if (i < 3) {
                    var civilite = data.associe[i].personneAssocie.civilite.code;
                    var nom = data.associe[i].personneAssocie.nom + " " + (data.associe[i].personneAssocie.postNom !== null
                        ? data.associe[i].personneAssocie.postNom : "");
                    var prenom = data.associe[i].personneAssocie.prenom;
                    var dateNaissance = data.associe[i].personneAssocie.dateNaissance;
                    var lieuNaissance = data.associe[i].personneAssocie.lieuNaissance;
                    var adresse = data.associe[i].personneAssocie.adresse._name;
                    renderHtml += "<tr><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + civilite + " " + nom +
                        "</td><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + prenom +
                        "</td><td style='text-align:justify;padding:2px;'>" + showData(dateNaissance, "date") + " A <span style='text-transform:uppercase;'>"
                        + lieuNaissance + "</span></td><td style='text-align:justify;padding:2px;'>" + adresse.replace(", CD", ", RD Congo") +
                        "</td></tr>";
                }
            }
        } else {
            renderHtml += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
           "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
           "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
        }
   
    renderHtml += "</tbody></table>";
    renderHtml += "</div>";
    return renderHtml;
}

function getDirigeantsHtml_G(data) {
    var renderHtml = "";
    renderHtml += "<div style='margin-top:5px;text-align:center;font-size:13px;'><b>RENSEIGNEMENTS RELATIFS AUX DIRIGEANTS (*)(**)</b></div>";

    renderHtml += "<div style='" + borderCadreRed + font + size + ";margin-left:10px'>";
    renderHtml += "<div style='font-size:9px;'>" + addIndex("16", "-27") + "&nbsp;(*) Concerne les Gérants, Administrateurs ou associés <u>ayant le pouvoir d’engager la personne morale</u></div>";
    renderHtml += "<div style='font-size:9px;'>(**) Les renseignements ne pouvant figurer ci-dessous doivent IMPERATIVEMENT être reportés sur le formulaire M.o Bis annexé.</div>";
    renderHtml += "<table border='1' style='font-size: 10px;border-collapse: collapse;width:100%;'><thead><tr>" +
        "<td style='width:20%;padding:2px;'>NOM</td>" +
        "<td style='width:10%;padding:2px;'>PRENOM</td>" +
        "<td style='width:23%;padding:2px;'>DATE LIEU DE NAISSANCE</td>" +
        "<td style='width:32%;padding:2px;'>ADRESSE</td>" +
        "<td style='width:15%;padding:2px;'>FONCTION(***)</td></tr></thead><tbody>";

    for (var i = 0; i < data.dirigeant.length; i++) {
        if (i < 3) {
            var civilite = data.dirigeant[i].personneDirigeant.civilite.code;
            var noms = data.dirigeant[i].personneDirigeant.nom + " " + (data.dirigeant[i].personneDirigeant.postNom !== null
                ? data.dirigeant[i].personneDirigeant.postNom : "");
            var prenom = data.dirigeant[i].personneDirigeant.prenom;
            var dateNaissance = data.dirigeant[i].personneDirigeant.dateNaissance;
            var lieuNaissance = data.dirigeant[i].personneDirigeant.lieuNaissance;
            var adresse = data.dirigeant[i].personneDirigeant.adresse._name;
            var fonction = data.dirigeant[i].fonction !== null ? data.dirigeant[i].fonction.value : "";
            renderHtml += "<tr><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + civilite + " " + noms +
                "</td><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + prenom +
                "</td><td style='text-align:justify;padding:2px;'>" + showData(dateNaissance, "date") + " A <span style='text-transform:uppercase;'>"
                + lieuNaissance + "</span></td><td style='text-align:justify;padding:2px;'>" + adresse.replace(", CD", ", RD Congo") +
                "</td><td style='text-align:justify;padding:2px;'>" + fonction +
                "</td></tr>";
        }
    }

    renderHtml += "</tbody></table>";
    renderHtml += "<div style='font-size:9px;'>(***) Préciser : Gérant, PDG, Administrateur, Associé</div>";
    renderHtml += "</div>";
    return renderHtml;
}

function getCommissaireAuCompte(data) {
    var renderHtml = "";
    renderHtml += "<div style='margin-top:5px;text-align:center;font-size:13px;'><b>COMMISSAIRES AUX COMPTES</b></div>";

    renderHtml += "<div style='" + borderCadreRed + font + size + ";margin-left:10px'>";
    renderHtml += "<div style='font-size:9px;'>" + addIndex("17", "-27") + "</div>";
    renderHtml += "<table border='1' style='font-size: 10px;border-collapse: collapse;width:100%;'><thead><tr>" +
        "<td style='width:20%;padding:2px;'>NOM</td>" +
        "<td style='width:12%;padding:2px;'>PRENOM</td>" +
        "<td style='width:23%;padding:2px;'>DATE LIEU DE NAISSANCE</td>" +
        "<td style='width:35%;padding:2px;'>ADRESSE</td>" +
        "<td style='width:10%;padding:2px;'>FONCTION</td></tr></thead><tbody>";
    if (data.commissaireAuxComptes.length > 0) {
        for (var i = 0; i < data.commissaireAuxComptes.length; i++) {
            if (i < 2) {
                var noms = data.commissaireAuxComptes[i].nomCommissaire + " " + (data.commissaireAuxComptes[i].postNomCommissaire !== null
                    ? data.commissaireAuxComptes[i].postNomCommissaire : "");
                var prenom = data.commissaireAuxComptes[i].prenomCommissaire;
                var dateNaissance = data.commissaireAuxComptes[i].dateNaissanceCommissaire;
                var lieuNaissance = data.commissaireAuxComptes[i].lieuNaissanceCommissaire;
                var adresse = data.commissaireAuxComptes[i].adresseCommissaire._name;
                var fonction = data.commissaireAuxComptes[i].fonctionCommissaire != null ? data.commissaireAuxComptes[i].fonctionCommissaire.value : "";
                renderHtml += "<tr><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + noms +
                    "</td><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + prenom +
                    "</td><td style='text-align:justify;padding:2px;'>" + showData(dateNaissance, "date") + " A <span style='text-transform:uppercase;'>"
                    + lieuNaissance + "</span></td><td style='text-align:justify;padding:2px;'>" + adresse.replace(", CD", ", RD Congo") +
                    "</td><td style='text-align:justify;padding:2px;'>" + fonction +
                    "</td></tr>";
            }
        }
    } else {
        renderHtml += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>TITULAIRE</td></tr>" +
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>SUPPLEANT</td></tr>";
    }
    renderHtml += "</tbody></table>";
    renderHtml += "</div>";
    return renderHtml;
}

function getFooterMo(data, infoFluxJson) {
    var renderHtml = "<div style='margin-top:5px;margin-left:15px;text-align:justify;'>";
    renderHtml += "<table style='" + font + size + "'><tr><td style='font-size:11;padding-right:3px;text-align:justify;'>LE SOUSSIGNE (préciser si mandataire) : " +
        getRequerantNames(data, infoFluxJson);
    renderHtml += "<br/>demande à ce que la présente constitue " + checkProp(data, "formeJuridiqueN2", "B1", true) + "<b>DEMANDE D’IMMATRICULATION AU R.C.C.M.</b>" +
        "<p style='margin-top:3px;text-align:justify;'>" + addIndex("18", "-23") + "La conformité de la déclaration  avec les pièces justificatives  produites en application de L’Acte Uniforme " +
        "sur le Droit commercial général a été vérifiée par le Greffier titulaire du Guichet Unique soussigné qui a procédé à l’inscription"+
        " le <b style='font-size:13px;'>" + showData(data.dateImmatriculationRCCM, "date") + "</b> sous le NUMERO <b style='font-size:13px;'>" + data.rccm + "</b></p></td>";
    renderHtml += "<td valign='top'><div style='" + borderCadreRed + ";width:120px;float:left;height:60px;'>Fait à Kinshasa<br/>Le " + todayDate() + "<br/>Signature</div></td></tr>";
    renderHtml += "</table>";
    renderHtml += "</div>";
    return renderHtml;
}

function cutString(text, wordsToCut) {
    var wordsArray = text.split(" ");
    if (wordsArray.length > wordsToCut) {
        var strShort = "";
        var rest = "";
        for (var i = 0; i < wordsArray.length; i++) {
            if (i < wordsToCut) {
                strShort += wordsArray[i] + " ";
            } else {
                rest += wordsArray[i] + " ";
            }
        }
        return strShort + "...-||-" + rest;
    } else {
        return text;
    }
}

function renderHeaderActeDepot(date) {
    return "<div style='height:100px;font-size: 14px;'>" +
        "<div style='margin-left:48px;'><b>REPUBLIQUE DEMOCRATIQUE DU CONGO</b></div>" +
        "<div style='font-size:15px;'>MINISTERE DE LA JUSTICE ET GARDE DES SCEAUX</div>" +
        "<div style='font-size:16px;margin-left:110px;'>GREFFE DU R.C.C.M.</div>" +
        "<div style='font-size:16px;margin-left:7px;'><u>GUICHET UNIQUE DE CREATION D'ENTREPRISE</u></div>" +
        "<div style='font-size:14px;margin-left:120px;'><b>SERVICE DU GREFFE</b></div>" +
        "<div style='margin-top:30px;'>" +
        "<table style='width:100%;'>" +
        "<tr>" +
        "<td><u>REPERTOIRE</u> <u>N° ................................ AS N° ............................</u> </td>" +
        "<td style='float:right;padding-right:10px;'><u>Du " + date + "</u></td>" +
        "</tr>" +
        "</table>" +
        "</div>" +
        "</div>";
}

function getRequerantNames(data, infoFluxJson) {
    var fonction = "";
    if (infoFluxJson.mandataire) {
        fonction = ", MANDATAIRE";
    } else {
        var id = infoFluxJson.requerant._id;
        var entities = infoFluxJson.dirigeant;
        for (var i = 0; i < entities.length; i++) {
            if (entities[i].personneDirigeant._id === id) {
                fonction = entities[i].fonction != null ? entities[i].fonction.value : "***";
            }
        }
        fonction = fonction === "" ? ", MANDATAIRE" :  ", " + fonction;
    }
    return "<span style='text-transform:uppercase;'><b>" + infoFluxJson.requerant.civilite.value + " " + infoFluxJson.requerant.prenom + " "
        + infoFluxJson.requerant.nom +(infoFluxJson.requerant.postNom != null ? " " + infoFluxJson.requerant.postNom : "") + "</b>" + fonction + "</span>";
}

//------------------------------------------------------
function intercalerMoBis(data, infoFluxJson) {
    var renderHtml = "<div style='" + font + "'>";
    //header
    renderHtml += "<div style='border:2px solid black;padding: 8px 8px 0;margin-left:10px;'>" +
        "<table style='width:100%;text-align:justify;'>" +
        "<tr><td style='width:80px;'><div style='float:left;'><img style='float:left;width:120px;height:50px;' src='" + logoMoBis + "'/></div></td>" +
        "<td style='font-size:12px;'><div style='text-align:center;'><b>INTERCALAIRE COMPLEMENTAIRE <br/>AU FORMULAIRE M0 (*)</b></div>"
        + "</td></tr></table><div style='font-family: Arial, Helvetica, sans-serif;font-size:9px;text-align: left;'>A.P. Porto Novo 23/24 juin 1999</div>" +
        "</div>";
    renderHtml += "<div style='font-size:10px;padding:8px 15px;'><b>* Cette intercalaire doit IMPERATIVEMENT être annexée au formulaire M0 lorsque les rubriques " +
        "15 et 16 de ce formulaire n’ont pu être entièrement renseignées.</b></div>";
    //associe
    var renderHtml2 = intercalairAssocie(data);
    //dirigeant
    var renderHtml3 = intercalaireDirigeant(data);
    //footer
    var renderHtml4 = "<div style='margin-top:5px;margin-left:15px;text-align:justify;'>";
    renderHtml4 += "<table style='" + font + size + "'><tr><td style='font-size:11;padding-right:3px;text-align:justify;'>" +
        "<p style='margin-top:3px;text-align:justify;'>La conformité de la déclaration  avec les pièces justificatives  produites en application de L’Acte Uniforme " +
        "sur le Droit commercial général a été vérifiée par le Greffier titulaire du Guichet Unique soussigné qui a procédé à l’inscription" +
        " le <b style='font-size:13px;'>" + showData(data.dateImmatriculationRCCM, "date") + "</b> sous le NUMERO <b style='font-size:13px;'>" + data.rccm + "</b>." +
        " (reporter ici le numéro de formalité figurant sur le formulaire M0)</p></td>";
    renderHtml4 += "<td valign='top'><div style='" + borderCadreRed + ";width:120px;float:left;height:60px;'>Fait à Kinshasa<br/>Le " + todayDate() + "<br/>Signature</div></td></tr>";
    renderHtml4 += "</table>";
    renderHtml4 += "</div>";
    
    $("#interCalaireEntity").html(renderHtml + renderHtml2 + renderHtml3 + renderHtml4 + "</div>");
}

function intercalairAssocie(data) {
    var renderHtml = "";
    renderHtml += "<div style='margin-top:5px;text-align:center;'><b>15 ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT</b></div>";

    renderHtml += "<div style='" + borderCadreRed + font + size + ";margin-left:10px'>";
    renderHtml += "<div style='font-size:11px;text-align:justify;padding-bottom:5px;'>INSCRIRE CI-DESSOUS LES NOMS, PRENOMS, DOMICILE PERSONNEL, DATE ET LIEU DE NAISSANCE, NATIONALITE," +
        " DATE ET LIEU DU OU DES MARIAGES, REGIME MATRIMONIAL, CLAUSES RESTRICTIVES OPPOSABLES AUX TIERS, SEPARATIONS DE BIENS," +
        " DE <u>TOUS LES ASSOCIES</u> TENUS INDEFINIMENT ET PERSONNELLEMENT DES DETTES SOCIALES :</div>";
    renderHtml += "<table border='1' style='font-size: 10px;border-collapse: collapse;width:100%;'><thead><tr>" +
        "<td style='width:20%;padding:2px;'>NOM</td>" +
        "<td style='width:15%;padding:2px;'>PRENOM</td>" +
        "<td style='width:25%;padding:2px;'>DATE LIEU DE NAISSANCE</td>" +
        "<td style='width:40%;padding:2px;'>ADRESSE</td></tr></thead><tbody>";

    var code = data.formeJuridiqueN3.code;
    if (code === "B11" || code === "B10" || code === "C10" || code === "C10b") {
        for (var i = 0; i < data.associe.length; i++) {
            var civilite = data.associe[i].personneAssocie.civilite.code;
            var nom = data.associe[i].personneAssocie.nom + " " + (data.associe[i].personneAssocie.postNom !== null
                ? data.associe[i].personneAssocie.postNom : "");
            var prenom = data.associe[i].personneAssocie.prenom;
            var dateNaissance = data.associe[i].personneAssocie.dateNaissance;
            var lieuNaissance = data.associe[i].personneAssocie.lieuNaissance;
            var adresse = data.associe[i].personneAssocie.adresse._name;
            renderHtml += "<tr><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + civilite + " " + nom +
                "</td><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + prenom +
                "</td><td style='text-align:justify;padding:2px;'>" + showData(dateNaissance, "date") + " A <span style='text-transform:uppercase;'>"
                + lieuNaissance + "</span></td><td style='text-align:justify;padding:2px;'>" + adresse.replace(", CD", ", RD Congo") +
                "</td></tr>";
        }
    } else {
        renderHtml += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
       "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
       "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
       "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
       "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
    }

    renderHtml += "</tbody></table>";
    renderHtml += "</div>";
    return renderHtml;
}

function intercalaireDirigeant(data) {
    var renderHtml = "<div style='margin-top:15px;text-align:center;'><b>16  RENSEIGNEMENTS RELATIFS AUX DIRIGEANTS (*)</b></div>";

    renderHtml += "<div style='" + borderCadreRed + font + size + ";margin-left:10px'>";
    renderHtml += "<div style='font-size:10px;text-align:justify;'><b>(*) Concerne les Gérants, administrateurs ou associés pouvant engager la personne morale.</b></div>";
    renderHtml += "<div style='font-size:11px;text-align:justify;padding-bottom:5px;'>INSCRIRE CI-DESSOUS LES NOMS, PRENOMS, DATE ET LIEU DE NAISSANCE, ADRESSE, QUALITE (Préciser : Gérant, PDG, PCA, " +
    "administrateur ou associés) CONCERNES ET QUI N’ONT PU ETRE INSCRITS SUR LE FORMULAIRE M0 EN RUBRIQUE 16 :</div>";
    renderHtml += "<table border='1' style='font-size: 10px;border-collapse: collapse;width:100%;'><thead><tr>" +
        "<td style='width:20%;padding:2px;'>NOM</td>" +
        "<td style='width:10%;padding:2px;'>PRENOM</td>" +
        "<td style='width:23%;padding:2px;'>DATE LIEU DE NAISSANCE</td>" +
        "<td style='width:32%;padding:2px;'>ADRESSE</td>" +
        "<td style='width:15%;padding:2px;'>FONCTION</td></tr></thead><tbody>";
    if (data.dirigeant.length > 0) {
        for (var i = 0; i < data.dirigeant.length; i++) {
            if (i >= 3) {
                var civilite = data.dirigeant[i].personneDirigeant.civilite.code;
                var noms = data.dirigeant[i].personneDirigeant.nom + " " + (data.dirigeant[i].personneDirigeant.postNom !== null
                    ? data.dirigeant[i].personneDirigeant.postNom : "");
                var prenom = data.dirigeant[i].personneDirigeant.prenom;
                var dateNaissance = data.dirigeant[i].personneDirigeant.dateNaissance;
                var lieuNaissance = data.dirigeant[i].personneDirigeant.lieuNaissance;
                var adresse = data.dirigeant[i].personneDirigeant.adresse._name;
                var fonction = data.dirigeant[i].fonction !== null ? data.dirigeant[i].fonction.value : "";
                renderHtml += "<tr><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + civilite + " " + noms +
                    "</td><td style='text-align:justify;padding:2px;text-transform:uppercase;'>" + prenom +
                    "</td><td style='text-align:justify;padding:2px;'>" + showData(dateNaissance, "date") + " A <span style='text-transform:uppercase;'>"
                    + lieuNaissance + "</span></td><td style='text-align:justify;padding:2px;'>" + adresse.replace(", CD", ", RD Congo") +
                    "</td><td style='text-align:justify;padding:2px;'>" + fonction +
                    "</td></tr>";
            }
        }
    } else {
        renderHtml += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
    }

    renderHtml += "</tbody></table>";
    renderHtml += "</div>";
    return renderHtml;
}
