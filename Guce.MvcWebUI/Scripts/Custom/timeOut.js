﻿var diag = createNewDialog();

var showDialogUI = true;
var msecPageLoad;
var msecTimeOut;
var msecWarning;
var timeOutUrl = "Login/timeOut";

SessionTimerInit();

function SessionTimerInit() {
    SetPageTimes();
    //setTimeout("ShowPendingTimeoutDialog()", msecWarning);
}

function createNewDialog() {
    var dialog = new BootstrapDialog({
        title: "Expiration de session",
        draggable: true,
        size: BootstrapDialog.SIZE_WIDE,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: true,
        message: 'hello',
        buttons: [
            {
                label: "Continuer",
                cssClass: "btn-primary",
                icon: "fa fa-check",
                action: function () {
                    window.location.replace(window.location.href);
                }
            }
        ]
    });
    //dialog.open();
    return dialog;
}

function SetPageTimes() {
    msecPageLoad = new Date().getTime();

     $.ajax({
        type: "POST",
        url: timeOutUrl,
        data: {  },
        success: function(data) {
            msecTimeOut = (1 * 60 * 1000);
            msecWarning = (1 * 60 * 1000 * .80);
            setTimeout("ShowPendingTimeoutDialog()", msecWarning);
        },
        error: function (e, status) {
                if (e.status == 500) {
                    alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                    window.location.replace(logoutUrl);
                } else {
                    messageAlert("Erreur lors du chargement du temp limite de la session");
                }
            }
        });

        //msecTimeOut = (10 * 60 * 1000);
    //msecWarning = (<%=Session.Timeout%> * 60 * 1000 * .75);  // .75 = 75% of total timeout variable. i.e. if timeout is set for 20 minutes, the dialog should pop up after 15 minutes
    // msecWarning = 0;
}

function ShowPendingTimeoutDialog() {
    UpdateTimeoutMessage();
    if (showDialogUI) {
        diag.open();
    }
}

/*
function ResetTimeout() {
    diag.close();
    $.ajax({
        url: 'KeepAlive.ashx'
    });
    SessionTimerInit();
}
*/

function UpdateTimeoutMessage() {

    var msecElapsed = (new Date().getTime()) - msecPageLoad;
    var timeLeft = msecTimeOut - msecElapsed; //time left in miliseconds

    if (timeLeft <= 0) {
        RedirectToWelcomePage();
    }
    else {
        var minutesLeft = Math.floor(timeLeft / 60000);
        var secondsLeft = Math.floor((timeLeft % 60000) / 1000);
        var sMinutesLeft = ("00" + (minutesLeft).toString()).slice(-2) + ":";
        var sSecondsLeft = ("00" + (secondsLeft).toString()).slice(-2);

        diag.setMessage("<p>Votre session est sur le point d'expirer. S'il vous plaît cliquer " +
            "sur <b>Continuer</b> pour continuer a travailler.<br>Temps restant: " + sMinutesLeft + sSecondsLeft + "</p>");
        setTimeout("UpdateTimeoutMessage()", 50);
    }
}

function RedirectToWelcomePage() {
    diag.close();
    var dialog = new BootstrapDialog({
        title: "Session expiré",
        draggable: true,
        size: BootstrapDialog.SIZE_WIDE,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        autospin: true,
        message: "<p>Votre session a expiré. Cliquez sur 'OK' pour être redirigé vers la page de connexion.</p>",
        buttons: [
            {
                label: 'OK',
                action: function (dialogRef) {
                    window.location = "Login/Logout";
                    dialogRef.close();
                }
            }]
    });
    if (showDialogUI) {
        dialog.open();
    } else {
        window.location = "Login/Logout";
    }
}
