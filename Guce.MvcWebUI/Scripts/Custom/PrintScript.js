﻿
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

var dataSource = null;
var orderData = null;
var orderSchema = null;
var report = null;

function PrintDocumennt() {

    var reportId = $("#FunctionToPrint").attr("value");
    $("#textToPrint").after("<script>" + reportId + "();</script>");
    /*
    $.ajax({
        type: "POST",
        url: reportToPrintUrl,
        data: { reportId: reportId },
        success: function (data) {
            $("#textToPrint").after("<script>"+data+"();</script>");
        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
    
    if (titre == "101") {
        $("#textToPrint").after("<script></script>");
        Recepisse_demande_immatriculation();
    } else if (titre == "301" || titre == "311" || titre == "106") {
        Kbis();
    }else if (titre == "201") {
        Recepisse_demande_inscription_complementaire();
    }*/
}

function Recepisse_demande_inscription_complementaire() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

          if (orderData.formeJuridiqueN1.code  == "B") {
                Recepisse_demande_inscription_complementaire_personne_moral();
            } else if (orderData.formeJuridiqueN1.code == "A") {
                Recepisse_demande_inscription_complementaire_personne_physique();
            }

        },

        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function Recepisse_demande_inscription_complementaire_personne_physique() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

    // test adresse
            if (orderData.adresseSiegeExploitation == null) {
                orderData.adresseSiegeExploitation = orderData.adresseSiegeSocial.adresseRDC._name;
            }

            dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "nomCommercial": IsEmptyString(orderData.nomCommercial),
                    "sigle": IsEmptyString(orderData.sigle),
                    "enseigne": IsEmptyString(orderData.enseigne),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "adresseEtablissementCree": IsEmptyString(orderData.adresseSiegeExploitation),
                    "formeJuridique": IsEmptyString(orderData.formeJuridiqueN1.value),
                    "capitalSocial": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree) + " ans",
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "",
                    "personnePhysique": new Array(),
                    "associe": new Array(),
                    "commissaireCompte": new Array(),
                    "acteNaissance": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeNaissance),
                    "pieceIdentite": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfPieceIdentite),
                    "extraitCasierJudiciaire": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.extraitCasierJudiciaire),
                    "mandatDirrigeant": testDocumentScanne(orderData.dirigeant["0"].pdfMandatDirigeant),
                    "contratMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfContratMariage),
                    "acteMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeMariage),
                    "specimenSignature": testDocumentScanne(orderData.pdfSpecimenSignatureDirigeant["0"]),
                    "titreResidenceVISA": testDocumentScanne(orderData.dirigeant["0"].pdfTitreResidentVisa),
                    "titreResidence": testDocumentScanne(orderData.dirigeant["0"].pdfTitreResident),
                    "neantAssocie": ""
                }
                ]
            };

            // test adresse
            if (dataSource.data["0"].adresseEtablissementCree == "   ") {
                dataSource.data["0"].adresseEtablissementCree = dataSource.data["0"].adresseSiegeSocial;
            }

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IsEmptyString(orderData.dirigeant[ii].fonction.name)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                    }

                }
            }
            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                        "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                        "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                        "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson;
                    }

                }
            } else {
                dataSource.data["0"].neantAssocie = "   ";
            }

            // ajout des commissaire au comptes
            if (orderData.hasOwnProperty("commissaireAuxComptes") && orderData.commissaireAuxComptes.length > 0) {
                for (var ii = 0; ii < orderData.commissaireAuxComptes.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.commissaireAuxComptes[ii].nom),
                        "prenom": IsEmptyString(orderData.commissaireAuxComptes[ii].prenom),
                        "dateNaissance": IsEmptyString(orderData.commissaireAuxComptes[ii].dateNaissance),
                        "adresse": IsEmptyString(orderData.commissaireAuxComptes[ii].adresse.adresseRDC._name)
                    };
                    if (ii < 4) { dataSource.data["0"].commissaireCompte[dataSource.data["0"].commissaireCompte.length + 1] = unDirigenantJson; }

                }
            }

            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + d.getFullYear();
            $.ajax({
                type: "POST",
                url: findNumeroDosierUrl,
                data: { entityId: entitiID },
                success: function (numeroDossier) {
                    var json = $.parseJSON(numeroDossier);
                    report = jsreports.createReport('Accusé_réception_' + $(".labebackl-warning").text())
                        .data('orders') // The report will look for a data source with ID "orders"
                        .page(8.5, 12.5, 'inches')
                        .header()
                        .text('Greffe du Registre du Commerce', 0, 0.2, 5, 3, { fontsize: 14, bold: true })
                        .text('Et du Crédit Mobilier (RCCM)', 0.4, 0.4, 10, 3, { fontsize: 12, bold: true })
                        .text('Kinshasa - RDC', 0.8, 0.6, 3, 3, { fontsize: 12, bold: true })
                        .image(logoUrl, 6.5, 0.1, 1, 1)
                        .text('ACCUSE DE RECEPTION', 1.5, 1, 5, 0.5, { fontsize: 22, bold: true, align: 'center' })
                        .text('A la date ci-dessous, le Guichet Unique de Création d\'Entreprises (GUCE) accuse réception de votre dossier pour la ' +
                                " MODIFICATION " +
                                ' de votre entreprise dont la dénomination est : ', 0.1, 1.8, 7.5, 0.5, { fontsize: 10 })
                        .text('[denomination]', 0.1, 2.3, 7.5, 0.5, { fontsize: 14, bold: true })
                        .text('Numéro du dossier :', 0.1, 2.7, 7.5, 0.5, { fontsize: 10 })
                        .text('  ' + IsEmptyString($(".label-warning").text()), 1.6, 2.7, 5, 0.5, { fontsize: 12, bold: true })
                        .text('Avec les informations suivantes:', 0.1, 2.9, 7.5, 0.5, { fontsize: 10 })
                        .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE', 0, 3.3, 6, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 3.6, 7.5, 0.02)
                        .text('Dénomination sociale', 0.1, 3.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [denomination]', 3, 3.7, 5, 0.5, { fontsize: 10 })
                        .text('Enseigne', 0.1, 3.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [enseigne]', 3, 3.9, 5, 0.5, { fontsize: 10 })
                        .text('Sigle', 0.1, 4.1, 2, 0.5, { fontsize: 10 })
                        .text(':  [sigle]', 3, 4.1, 5, 0.5, { fontsize: 10 })
                        .text('Adresse de l\'établissement', 0.1, 4.3, 2, 0.5, { fontsize: 10 })
                        .text(':  [adresseSiegeSocial]', 3, 4.3, 5, 0.5, { fontsize: 10 })
                        .text('Adresse du lieu d\éxploitation', 0.1, 4.5, 3, 0.5, { fontsize: 10 })
                        .text(':  [adresseEtablissementCree]', 3, 4.5, 5, 0.5, { fontsize: 10 })
                        .text('Activité principale', 0.1, 4.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [activitePrincipale]', 3, 4.7, 5, 0.5, { fontsize: 10 })
                        .text('Date de début d\'exploitation', 0.1, 4.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [debutExploitation]', 3, 4.9, 5, 0.5, { fontsize: 10 })
                        .text(' [neantAssocie]', 3, 6.5, 5, 0.5, { fontsize: 10 })
                         .text('ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT ', 0, 5.3, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 5.6, 7.5, 0.02)
                        .table(0, 5.8, 7.5, 2, {
                            data: 'associe', hasFooter: true, fontSize: 6.2
                        }).column('20%', '[nom]', 'Nom', '', {
                            align: 'left', fontsize: 10
                        }).column('20%', '[prenom]', 'Postnom', '', {
                            align: 'left', fontsize: 10
                        }).column('20%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left', fontsize: 10
                        }).column('40%', '[adresse]', 'Adresse', '', {
                            align: 'left', fontsize: 10
                        })
                        .text('RENSEIGNEMENTS RELATIFS AUX DIRIGANTS ', 0, 8.1, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 8.4, 7.5, 0.02)
                        .table(0, 8.5, 7.5, 2, {
                            data: 'personnePhysique', hasFooter: true, fontSize: 6.2
                        }).column('15%', '[nom]', 'Nom', '', {
                            align: 'left', fontsize: 8
                        }).column('15%', '[prenom]', 'Postnom', '', {
                            align: 'left', fontsize: 8
                        }).column('20%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left', fontsize: 8
                        }).column('15%', '[fonction]', 'Fonction', '', {
                            align: 'left', fontsize: 8
                        }).column('35%', '[adresse]', 'Adresse', '', {
                            align: 'left', fontsize: 8
                        })
                        .text('LISTE DES DOCUMENTS FOURNIS ', 0, 11.1, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 11.4, 7.5, 0.02)
                        .text('Extrait de l\'acte de naissance ou attestation', 0.1, 11.6, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [acteNaissance] ', 4, 11.6, 5, 0.5, { fontsize: 11 })
                        .text('Pièce d\'identité reconnue', 0.1, 11.8, 2.5, 0.5, { fontsize: 11 })
                        .text(':  [pieceIdentite]', 4, 11.8, 5, 0.5, { fontsize: 10 })
                        .text('Extrait du casier judiciaire', 0.1, 12, 3, 0.5, { fontsize: 10 })
                        .text(':  [extraitCasierJudiciaire]', 4, 12, 5, 0.5, { fontsize: 10 })
                        .text('Attestation de résidence', 0.1, 12.2, 2.5, 0.5, { fontsize: 10 })
                        .text(':  [titreResidence]', 4, 12.2, 5, 0.5, { fontsize: 10 })
                        .text('Extrait d\'acte de mariage', 0.1, 12.4, 2, 0.5, { fontsize: 10 })
                        .text(':  [acteMariage]', 4, 12.4, 5, 0.5, { fontsize: 10 })
                        .text('Contrat de mariage (pour les étrangers)', 0.1, 12.6, 5, 0.5, { fontsize: 10 })
                        .text(':  [contratMariage]', 4, 12.6, 5, 0.5, { fontsize: 10 })
                        .text('Titre de résidence - visa (pour les étranges)', 0.1, 12.8, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [titreResidenceVISA]', 4, 12.8, 5, 0.5, { fontsize: 10 })
                        .text('Pouvoir du mandataire (Procuration)', 0.1, 13, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [mandatDirrigeant]', 4, 13, 5, 0.5, { fontsize: 10 })
                        .text('Spécimen de la signature du commerçant', 0.1, 13.2, 3, 0.5, { fontsize: 10 })
                        .text(':  [specimenSignature]', 4, 13.2, 5, 0.5, { fontsize: 10 })
                        .text('Je reconnais avoir lu toutes les informations ci-dessus et certifie qu\'elles sont conforme a l\'objet de ma demande', 0, 15.4, 7.5, 0.5, { fontsize: 10 })
                        .text('Fait à Kinshasa le ' + output, 16, 1.2, 4, 2, { fontsize: 10 })
                        .text('Signature ', 0, 16.2, 4, 2, { fontsize: 11 })
                        .footer()
                        .pageFooter()
                        .text('Page [PAGE_NUMBER] de [PAGE_COUNT]', 0, 0.5, 1.5, 0.25, { fontsize: 10, italic: true })
                        .done();
                    /*
                                            
                                            // Alias JasperReportDef for brevity
                                var JasperReportDef = jsreports.integrations.jasper.JasperReportDef;
                                // Fetch .jrxml file
                                $.get(report1, function (jrxml) {
                                    // Convert to jsreports format
                                    var reportDef = JasperReportDef.fromJRXML(jrxml);
                                    // Create designer
                                    var designer = new jsreports.Designer({
                                        container: $("#textToPrint"),
                                        data_sources: [dataSource],   // Will use the embedded data source information
                                        report: reportDef
                                    });
                                });
                                */
                    // Render the report
                    jsreports.render({
                        report_def: report,
                        target: $('#textToPrint'),
                        showToolbar: false,
                        datasets: [dataSource]
                    });

                    $('#pdf_button').on('click', function () {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });

                    $('#validateBtn').on('click', function () {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });
                },
                error: function (e, status) {
                    if (e.status == 500) {
                        alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                        window.location.replace(logoutUrl);
                    } else {
                        messageAlert("Erreur lors de la génération du rapport...");
                    }
                }
            });

        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function Recepisse_demande_inscription_complementaire_personne_moral() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

            if (orderData.adresseSiegeExploitation == null) {
                orderData.adresseSiegeExploitation = orderData.adresseSiegeSocial;
            }

            dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "nomCommercial": IsEmptyString(orderData.nomCommercial),
                    "sigle": IsEmptyString(orderData.sigle),
                    "enseigne": IsEmptyString(orderData.enseigne),
                    "adresseSiegeExploitation": IsEmptyString(orderData.adresseSiegeExploitation.adresseRDC._name),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "formeJuridique": IsEmptyString(orderData.formeJuridiqueN1.value),
                    "capitalSocial": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree) + " ans",
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "",
                    "personnePhysique": new Array(),
                    "associe": new Array(),
                    "commissaireCompte": new Array(),
                    "acteNaissance": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeNaissance),
                    "pieceIdentite": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfPieceIdentite),
                    "extraitCasierJudiciaire": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.extraitCasierJudiciaire),
                    "mandatDirrigeant": testDocumentScanne(orderData.dirigeant["0"].pdfMandatDirigeant),
                    "contratMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfContratMariage),
                    "acteMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeMariage),
                    "specimenSignature": testDocumentScanne(orderData.pdfSpecimenSignatureDirigeant["0"]),
                    "titreResidenceVISA": testDocumentScanne(orderData.dirigeant["0"].pdfTitreResidentVisa),
                    "statutNotaire": testDocumentScanne(orderData.pdfStatutsSignes),
                    "declarationPartSocial": testDocumentScanne(orderData.pdfDeclarationSouscriptionPartSociale),
                    "preuveLiberationCapital": testDocumentScanne(orderData.pdfPreuveLIberationCapitalSocial),
                    "PV": " ",
                    "neantAssocie": ""
                }
                ]
            };

            // Test adresse
            if (dataSource.data["0"].adresseSiegeExploitation == "   ") {
                dataSource.data["0"].adresseSiegeExploitation = dataSource.data["0"].adresseSiegeSocial;
            }

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IsEmptyString(orderData.dirigeant[ii].fonction.value)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                    }

                }
            }
            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                        "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                        "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                        "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson;
                    }

                }
            } else {
                dataSource.data["0"].neantAssocie = "   ";
            }

            // test PV assemblé generale
            if (IsEmptyString(orderData.pdfPVAGE) != "   " ||
                IsEmptyString(orderData.pdfPVAGO) != "   " ||
                IsEmptyString(orderData.pdfPVCA) != "   ") {
                dataSource.data["0"].PV = "OUI";
            } else {
                dataSource.data["0"].PV = "NON";
            }

            
            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + d.getFullYear();
            $.ajax({
                type: "POST",
                url: findNumeroDosierUrl,
                data: { entityId: entitiID },
                success: function (numeroDossier) {
                    var json = $.parseJSON(numeroDossier);
                    report = jsreports.createReport('Accusé_réception_' + $(".label-warning").text())
                        .data('orders') // The report will look for a data source with ID "orders"
                        .page(8.5, 12.5, 'inches')
                        .header()
                        .text('Greffe du Registre du Commerce', 0, 0.2, 5, 3, { fontsize: 14, bold: true })
                        .text('Et du Crédit Mobilier (RCCM)', 0.4, 0.4, 10, 3, { fontsize: 12, bold: true })
                        .text('Kinshasa - RDC', 0.9, 0.6, 3, 3, { fontsize: 12, bold: true })
                        .image(logoUrl, 6.5, 0.1, 1, 1)
                        .text('ACCUSE DE RECEPTION', 1.5, 1, 5, 0.5, { fontsize: 22, bold: true, align: 'center' })
                        .text('A la date ci-dessous, le Guichet Unique de Création d\'Entreprises (GUCE) accuse réception de votre dossier pour la '
                                + " MODIFICATION " +
                                ' de votre entreprise dont la dénomination est : ', 0.1, 1.8, 7.5, 0.5, { fontsize: 10 })
                        .text('[denomination]', 0.1, 2.3, 8, 0.5, { fontsize: 14, bold: true })
                        .text('Numéro du dossier :', 0.1, 2.7, 9, 0.5, { fontsize: 10 })
                        .text('  ' + IsEmptyString($(".label-warning").text()), 1.6, 2.7, 9, 0.5, { fontsize: 12, bold: true })
                        .text('Avec les informations suivantes:', 0.1, 2.9, 9, 0.5, { fontsize: 10 })
                        .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE', 0, 3.3, 6, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 3.6, 7.5, 0.02)
                        .text('Dénomination social', 0.1, 3.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [denomination]', 3, 3.7, 5, 0.5, { fontsize: 10 })
                        .text('Enseigne', 0.1, 3.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [enseigne]', 3, 3.9, 5, 0.5, { fontsize: 10 })
                        .text('Sigle', 0.1, 4.1, 2, 0.5, { fontsize: 10 })
                        .text(':  [sigle]', 3, 4.1, 5, 0.5, { fontsize: 10 })
                        .text('Adresse du siege social', 0.1, 4.3, 2, 0.5, { fontsize: 10 })
                        .text(':  [adresseSiegeSocial]', 3, 4.3, 5, 0.5, { fontsize: 10 })
                        .text('Adresse du siege d\'exploitation', 0.1, 4.5, 3, 0.5, { fontsize: 10 })
                        .text(':  [adresseSiegeExploitation]', 3, 4.5, 5, 0.5, { fontsize: 10 })
                        .text('Activité principale', 0.1, 4.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [activitePrincipale]', 3, 4.7, 5, 0.5, { fontsize: 10 })
                        .text('Date de début d\'exploitation', 0.1, 4.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [debutExploitation]', 3, 4.9, 5, 0.5, { fontsize: 10 })
                            .text('  [neantAssocie]', 3, 6.7, 5, 0.5, { fontsize: 10 })
                        .text('ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT ', 0, 5.2, 8, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 5.5, 7.5, 0.02)
                        .table(0, 5.7, 8, 2, {
                            data: 'associe', hasFooter: true, fontSize: 7, cellpadding: 10
                        }).column('15%', '[nom]', 'Nom', '', {
                            align: 'left', fontsize: 11, height: 15
                        }).column('15%', '[prenom]', 'Postnom', '', {
                            align: 'left', fontsize: 11, height: 15
                        }).column('20%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left', fontsize: 11, height: 15
                        }).column('50%', '[adresse]', 'Adresse', '', {
                            align: 'left', fontsize: 11, height: 15
                        })
                        .text('RENSEIGNEMENTS RELATIFS AUX DIRIGANTS ', 0, 8.1, 8, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 8.4, 7.5, 0.02)
                        .table(0, 8.5, 7.5, 2, {
                            data: 'personnePhysique', hasFooter: true, fontSize: 7
                        }).column('15%', '[nom]', 'Nom', '', {
                            align: 'left', fontsize: 11
                        }).column('15%', '[prenom]', 'Postnom', '', {
                            align: 'left', fontsize: 11
                        }).column('15%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left', fontsize: 11
                        }).column('15%', '[fonction]', 'Fonction', '', {
                            align: 'left', fontsize: 11
                        }).column('40%', '[adresse]', 'Adresse', '', {
                            align: 'left', fontsize: 11
                        })
                        .text('LISTE DES DOCUMENTS FOURNIS ', 0, 11.1, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 11.4, 7.5, 0.02)
                        .text('Extrait de l\'acte de naissance ou attestation', 0.1, 11.6, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [acteNaissance] ', 4, 11.6, 5, 0.5, { fontsize: 10 })
                        .text('Pièce d\'identité reconnue', 0.1, 11.8, 2.5, 0.5, { fontsize: 10 })
                        .text(':  [pieceIdentite]', 4, 11.8, 5, 0.5, { fontsize: 10 })
                        .text('Extrait du casier judiciaire', 0.1, 12, 3, 0.5, { fontsize: 10 })
                        .text(':  [extraitCasierJudiciaire]', 4, 12, 5, 0.5, { fontsize: 10 })
                        .text('Attestation de résidence', 0.1, 12.2, 2.5, 0.5, { fontsize: 10 })
                        .text(':  a préciser ', 4, 12.2, 5, 0.5, { fontsize: 10 })
                        .text('Extrait d\'acte de mariage', 0.1, 12.4, 2, 0.5, { fontsize: 10 })
                        .text(':  [acteMariage]', 4, 12.4, 5, 0.5, { fontsize: 10 })
                        .text('Contrat de mariage (pour les étrangers)', 0.1, 12.6, 5, 0.5, { fontsize: 10 })
                        .text(':  [contratMariage]', 4, 12.6, 5, 0.5, { fontsize: 10 })
                        .text('Titre de résidence - visa (pour les étranges)', 0.1, 12.8, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [titreResidenceVISA]', 4, 12.8, 5, 0.5, { fontsize: 10 })
                        .text('Pouvoir du mandataire (Procuration)', 0.1, 13, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [mandatDirrigeant]', 4, 13, 5, 0.5, { fontsize: 10 })
                        .text('Spécimen de la signature du commerçant', 0.1, 13.2, 3, 0.5, { fontsize: 10 })
                        .text(':  [specimenSignature]', 4, 13.2, 5, 0.5, { fontsize: 10 })
                        .text('Statuts notarié', 0.1, 13.4, 3, 0.5, { fontsize: 10 })
                        .text(':  [statutNotaire]', 4, 13.4, 5, 0.5, { fontsize: 10 })
                        .text('Répartitions du capital(liste des actionnaires)', 0.1, 13.6, 4, 0.5, { fontsize: 10 })
                        .text(':  [declarationPartSocial]', 4, 13.6, 5, 0.5, { fontsize: 10 })
                        .text('Preuve de la libération de capital (extrait de compte)', 0.1, 13.8, 4, 0.5, { fontsize: 10 })
                        .text(':  [preuveLiberationCapital]', 4, 13.8, 5, 0.5, { fontsize: 10 })
                        .text('PV Assemblé généralee constitutive (pour les SA)', 0.1, 14, 4, 0.5, { fontsize: 10 })
                        .text(':  [PV]', 4, 14, 5, 0.5, { fontsize: 10 })
                        .text('Je reconnais avoir lu toutes les informations ci-dessus et certifie qu\'elles sont conforme a l\'objet de ma demande', 0, 15.2, 8, 0.5, { fontsize: 10 })
                        .text('Fait à Kinshasa le ' + output, 5, 16, 4, 2, { fontsize: 10 })
                        .text('Signature ', 0, 16, 4, 2, { fontsize: 10 })
                        .footer()
                        .pageFooter()
                        .text('Page [PAGE_NUMBER] de [PAGE_COUNT]', 0, 0.5, 1.5, 0.25, { fontsize: 10, italic: true })
                        .done();
                    /*
                                            
                                            // Alias JasperReportDef for brevity
                                var JasperReportDef = jsreports.integrations.jasper.JasperReportDef;
                                // Fetch .jrxml file
                                $.get(report1, function (jrxml) {
                                    // Convert to jsreports format
                                    var reportDef = JasperReportDef.fromJRXML(jrxml);
                                    // Create designer
                                    var designer = new jsreports.Designer({
                                        container: $("#textToPrint"),
                                        data_sources: [dataSource],   // Will use the embedded data source information
                                        report: reportDef
                                    });
                                });
                                */
                    // Render the report
                    jsreports.render({
                        report_def: report,
                        target: $('#textToPrint'),
                        showToolbar: false,
                        datasets: [dataSource]
                    });

                    $('#pdf_button').on('click', function () {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });

                    $('#validateBtn').on('click', function () {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });
                },
                error: function (e, status) {
                    if (e.status == 500) {
                        alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                        window.location.replace(logoutUrl);
                    } else {
                        messageAlert("Erreur lors de la génération du rapport...");
                    }
                }
            });

        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function Recepisse_demande_immatriculation() {
    var entitiID = getUrlParameter("entityId");

    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function(data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);
            /*
            // Alias JasperReportDef for brevity
            var JasperReportDef = jsreports.integrations.jasper.JasperReportDef;
            // Fetch .jrxml file
            $.get("/report4.jrxml", function(jrxml) {
                // Convert to jsreports format
                var reportDef = JasperReportDef.fromJRXML(jrxml);
                // Create designer
                var designer = new jsreports.Designer({
                    container: $("#textToPrint"),
                    data_sources: [orderData],   // Will use the embedded data source information
                    report: reportDef
                });
            });
            */
            if (orderData.formeJuridiqueN1.code  == "B") {
                Recepisse_demande_immatriculation_personne_moral();
            } else if (orderData.formeJuridiqueN1.code == "A") {
                Recepisse_demande_immatriculation_personne_physique();
            }

            /*
            dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "nomCommercial": IsEmptyString(orderData.nomCommercial),
                    "sigle": IsEmptyString(orderData.sigle),
                    "enseigne": IsEmptyString(orderData.enseigne),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "formeJuridique": IsEmptyString(orderData.formeJuridiqueN1.value),
                    "capitalSocial": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree) + " ans",
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "debutExploitation": "",
                    "modeExploitation": "",
                    "personnePhysique": new Array(),
                    "associe": new Array(),
                    "commissaireCompte": new Array()
                    }
                ]
            };

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IfExistProperty(orderData.dirigeant[ii],"fonction")
                    };
                    if (ii < 4) {
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                    }

                }
            }
            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {
                var unDirigenantJson = {
                    "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                    "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                    "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                    "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                };
                if (ii < 4) { dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson; }

                }
            }
            
            // ajout des commissaire au comptes
            if (orderData.hasOwnProperty("commissaireAuxComptes") && orderData.commissaireAuxComptes.length > 0) {
                for (var ii = 0; ii < orderData.commissaireAuxComptes.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.commissaireAuxComptes[ii].nom),
                        "prenom": IsEmptyString(orderData.commissaireAuxComptes[ii].prenom),
                        "dateNaissance": IsEmptyString(orderData.commissaireAuxComptes[ii].dateNaissance),
                        "adresse": IsEmptyString(orderData.commissaireAuxComptes[ii].adresse.adresseRDC._name)
                    };
                    if (ii < 4) { dataSource.data["0"].commissaireCompte[dataSource.data["0"].commissaireCompte.length + 1] = unDirigenantJson; }
                    
                }
            }
         
            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + d.getFullYear();
            $.ajax({
                    type: "POST",
                    url: findNumeroDosierUrl,
                    data: { entityId: entitiID },
                    success: function(numeroDossier) {
                        var json = $.parseJSON(numeroDossier);
                        report = jsreports.createReport('Accusé_réception_' + $(".label-warning").text())
                            .data('orders') // The report will look for a data source with ID "orders"
                            .page(9, 13, 'inches')
                            .header()
                            .text('Greffe du Registre du Commerce', 0, 0.2, 4, 3, { fontsize: 14, bold: true })
                            .text('Et du Crédit Mobilier (RCCM)', 0.4, 0.4, 10, 3, { fontsize: 12, bold: true })
                            .text('Kinshasa - RDC', 0.8, 0.6, 3, 3, { fontsize: 12, bold: true })
                            .image(logoUrl, 6.5, 0.1, 1, 1)
                            .text('ACCUSE DE RECEPTION', 1.5, 1, 5, 0.5, { fontsize: 22, bold: true, align: 'center' })
                            .text('A la date ci-dessous, le Guichet Unique de Création d\'Entreprises (GUCE) accuse réception de votre dossier pour la CREATION de votre entreprise dont la dénomination est : ', 0.1, 1.8, 8, 0.5, { fontsize: 10 })
                            .text('[denomination]', 0.1, 2.3, 8, 0.5, { fontsize: 14, bold: true })
                            .text('Numéro du dossier :', 0.1, 2.7, 9, 0.5, { fontsize: 10 })
                            .text('  ' + IsEmptyString($(".label-warning").text()), 1.6, 2.7, 9, 0.5, { fontsize: 12, bold: true })
                            .text('Avec les informations suivantes:', 0.1, 2.9, 9, 0.5, { fontsize: 10 })
                            .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE', 0, 3.3, 6, 0.1, { fontsize: 13, bold: true })
                            .box('black', '#00', 0, 3.6, 8.2, 0.02)
                            .text('Dénomination', 0.1, 3.7, 2, 0.5, { fontsize: 10 })
                            .text(':  [denomination]', 3, 3.7, 5, 0.5, { fontsize: 10 })
                            .text('Nom commercial', 0.1, 3.9, 2, 0.5, { fontsize: 10 })
                            .text(':  [nomCommercial]', 3, 3.9, 5, 0.5, { fontsize: 10 })
                            .text('Enseigne', 0.1, 4.1, 2, 0.5, { fontsize: 10 })
                            .text(':  [enseigne]', 3, 4.1, 5, 0.5, { fontsize: 10 })
                            .text('Sigle', 0.1, 4.3, 2, 0.5, { fontsize: 10 })
                            .text(':  [sigle]', 3, 4.3, 5, 0.5, { fontsize: 10 })
                            .text('Adresse du Siege sociale', 0.1, 4.5, 2, 0.5, { fontsize: 10 })
                            .text(':  [adresseSiegeSocial]', 3, 4.5, 5, 0.5, { fontsize: 10 })
                            .text('Adresse de l\'établissement créé', 0.1, 4.7, 3, 0.5, { fontsize: 10 })
                            .text('Activité principale', 0.1, 4.9, 2, 0.5, { fontsize: 10 })
                            .text(':  [activitePrincipale]', 3, 4.9, 5, 0.5, { fontsize: 11 })
                            .text('ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT ', 0, 5.2, 8, 0.1, { fontsize: 13, bold: true })
                            .box('black', '#00', 0, 5.5, 8.2, 0.02)
                            .table(0, 5.7, 8, 2, {
                                data: 'associe', hasFooter: true, fontSize: 9
                            }).column('20%', '[nom]', 'Nom', '', {
                                align: 'left',fontsize: 11
                            }).column('20%', '[prenom]', 'Postnom', '', {
                                align: 'left',fontsize: 11
                            }).column('20%', '[dateNaissance]', 'Date de naissance', '', {
                                align: 'left', fontsize: 11
                            }).column('40%', '[adresse]', 'Adresse', '', {
                                align: 'left',fontsize: 11
                            })
                            .text('RENSEIGNEMENTS RELATIFS AUX DIRIGANTS ', 0, 8.1, 8, 0.1, { fontsize: 13, bold: true })
                            .box('black', '#00', 0, 8.4, 8.2, 0.02)
                            .table(0, 8.5, 8, 2, {
                                data: 'personnePhysique', hasFooter: true, fontSize: 9
                            }).column('15%', '[nom]', 'Nom', '', {
                                align: 'left',fontsize: 11
                            }).column('15%', '[prenom]', 'Postnom', '', {
                                align: 'left',fontsize: 11
                            }).column('15%', '[dateNaissance]', 'Date de naissance', '', {
                                align: 'left',fontsize: 11
                            }).column('15%', '[fonction]', 'Fonction', '', {
                                align: 'left', fontsize: 11
                            }).column('40%', '[adresse]', 'Adresse', '', {
                                align: 'left',fontsize: 11
                            })
                            .text('COMMISSAIRES AUX COMPTES ', 0, 11.1, 8, 0.1, { fontsize: 13, bold: true })
                            .box('black', '#00', 0, 11.4, 8.2, 0.02)
                            .table(0, 11.5, 8, 2, {
                                data: 'commissaireCompte', fontSize: 9
                            }).column('20%', '[nom]', 'Nom', '', {
                                align: 'left', fontsize: 11
                            }).column('20%', '[prenom]', 'Postnom', '', {
                                align: 'left', fontsize: 11
                            }).column('20%', '[dateNaissance]', 'Date de naissance', '', {
                                align: 'left',fontsize: 11
                            }).column('40%', '[adresse]', 'Adresse', '', {
                                align: 'left', fontsize: 11
                            })
                            .text('Je SOUSSIGNE (préciser si mandataire) ______________________________________ ' +
                                    'demande à ce que la présent constitue une demande d\'immatriculation au RCCM', 0, 15.2, 8, 0.5, { fontsize: 11 })
                            .text('Fait à Kinshasa le ' + output, 5, 16, 4, 2, { fontsize: 11 })
                            .text('Signature ', 0, 16, 4, 2, { fontsize: 11 })
                            .footer()
                            .pageFooter()
                            .text('Page [PAGE_NUMBER] de [PAGE_COUNT]', 0, 0.5, 1.5, 0.25, { fontsize: 10, italic: true })
                            .done();

                        
                        // Alias JasperReportDef for brevity
            var JasperReportDef = jsreports.integrations.jasper.JasperReportDef;
            // Fetch .jrxml file
            $.get(report1, function (jrxml) {
                // Convert to jsreports format
                var reportDef = JasperReportDef.fromJRXML(jrxml);
                // Create designer
                var designer = new jsreports.Designer({
                    container: $("#textToPrint"),
                    data_sources: [dataSource],   // Will use the embedded data source information
                    report: reportDef
                });
            });
            
                        // Render the report
                        jsreports.render({
                            report_def: report,
                            target: $('#textToPrint'),
                            showToolbar: false,
                            datasets: [dataSource]
                        });

            $('#pdf_button').on('click', function() {
                            jsreports.export({
                                report_def: report,
                                format: 'pdf',
                                datasets: [dataSource]
                            });
                        });

            $('#validateBtn').on('click', function() {
                            jsreports.export({
                                report_def: report,
                                format: 'pdf',
                                datasets: [dataSource]
                            });
            });
                         },
        error: function(e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
            });

           */        
        },
        error: function(e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function Recepisse_demande_immatriculation_personne_moral() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function(data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

            // test adresse
            if (orderData.adresseSiegeExploitation == null) {
                orderData.adresseSiegeExploitation = orderData.adresseSiegeSocial;
            }

            dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "nomCommercial": IsEmptyString(orderData.nomCommercial),
                    "sigle": IsEmptyString(orderData.sigle),
                    "enseigne": IsEmptyString(orderData.enseigne),
                    "adresseSiegeExploitation": IsEmptyString(orderData.adresseSiegeExploitation.adresseRDC._name),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "formeJuridique": IsEmptyString(orderData.formeJuridiqueN1.value),
                    "capitalSocial": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree) + " ans",
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "juridiction": IsEmptyString(orderData._ste_name),
                    "modeExploitation": "",
                    "personnePhysique": new Array(),
                    "associe": new Array(),
                    "commissaireCompte": new Array(),
                    "acteNaissance": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeNaissance),
                    "pieceIdentite": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfPieceIdentite),
                    "extraitCasierJudiciaire": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.extraitCasierJudiciaire),
                    "mandatDirrigeant": testDocumentScanne(orderData.dirigeant["0"].pdfMandatDirigeant),
                    "contratMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfContratMariage),
                    "acteMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeMariage),
                    "specimenSignature": testDocumentScanne(orderData.pdfSpecimenSignatureDirigeant["0"]),
                    "titreResidenceVISA": testDocumentScanne(orderData.dirigeant["0"].pdfTitreResidentVisa),
                    "statutNotaire": testDocumentScanne(orderData.pdfStatutsSignes),
                    "declarationPartSocial": testDocumentScanne(orderData.pdfDeclarationSouscriptionPartSociale),
                    "preuveLiberationCapital": testDocumentScanne(orderData.pdfPreuveLIberationCapitalSocial),
                    "PV": " ",
                    "neantAssocie" : ""
        }
                ]
            };

            // test date
            if (IsEmptyString(orderData.debutExploitation) != "   ") {
                var myDate = IsEmptyString(orderData.debutExploitation);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].debutExploitation = tab[0] + "-" + tab[1] + "-" + tab[2];
            }

            // Test adresse
            if (dataSource.data["0"].adresseSiegeExploitation == "   ") {
                dataSource.data["0"].adresseSiegeExploitation = dataSource.data["0"].adresseSiegeSocial;
            }

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var trueDate;
                    // test date
                    if (IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance) != "   ") {
                        var myDate = IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance);
                        var tab = myDate.split("-");
                        var temp = tab[0];
                        tab[0] = tab[2];
                        tab[2] = temp;
                        trueDate = tab[0] + "-" + tab[1] + "-" + tab[2];
                    }

                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IsEmptyString(orderData.dirigeant[ii].fonction.value)
                    };
                    if (ii < 4) {
                        
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length - 1].dateNaissance = trueDate;
                    }

                }
            }
            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {

                    var trueDate;
                    // test date
                    if (IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance) != "   ") {
                        var myDate = IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance);
                        var tab = myDate.split("-");
                        var temp = tab[0];
                        tab[0] = tab[2];
                        tab[2] = temp;
                        trueDate = tab[0] + "-" + tab[1] + "-" + tab[2];
                    }

                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                        "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                        "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                        "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson;
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length - 1].dateNaissance = trueDate;
                    }

                }
            } else {
                dataSource.data["0"].neantAssocie = "   ";
            }
            
            // test PV assemblé generale
            if (IsEmptyString(orderData.pdfPVAGE) != "   " ||
                IsEmptyString(orderData.pdfPVAGO) != "   " ||
                IsEmptyString(orderData.pdfPVCA) != "   ") {
                dataSource.data["0"].PV = "OUI";
            } else {
                dataSource.data["0"].PV = "NON";
            }


          
            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + d.getFullYear();
            $.ajax({
                type: "POST",
                url: findNumeroDosierUrl,
                data: { entityId: entitiID },
                success: function(numeroDossier) {
                    var json = $.parseJSON(numeroDossier);
                    report = jsreports.createReport('Accusé_réception_' + $(".label-warning").text())
                        .data('orders') // The report will look for a data source with ID "orders"
                        .page(8.5, 12.5, 'inches')
                        .header()
                        .text('Antenne de GUCE Kinshasa/Matete', 0.6, 0.2, 5, 3, { fontsize: 10, bold: true })
                .text('Registre du Commerce et du Crédit Mobilier (RCCM)', 0, 0.4, 10, 3, { fontsize: 10, bold: true })
                        .image(logoUrl, 6.5, 0.1, 1, 1)
                        .text('ACCUSE DE RECEPTION', 1.5, 1, 5, 0.5, { fontsize: 22, bold: true, align: 'center' })
                        .text('A la date ci-dessous, le Guichet Unique de Création d\'Entreprises (GUCE) accuse ' +
                        'réception de votre dossier pour la '
                                + IsEmptyString(orderData.origine.value).toUpperCase() +
                                ' de votre entreprise dont la dénomination sociale est : ', 0.1, 1.8, 7.5, 0.5, { fontsize: 10 })
                        .text('[denomination]', 0.1, 2.3, 8, 0.5, { fontsize: 14, bold: true })
                        .text('Numéro du dossier :', 0.1, 2.7, 9, 0.5, { fontsize: 10 })
                        .text('  ' + IsEmptyString($(".label-warning").text()), 1.6, 2.7, 9, 0.5, { fontsize: 12, bold: true })
                        .text('Avec les informations suivantes:', 0.1, 3, 9, 0.5, { fontsize: 10 })
                        .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE', 0, 3.3, 6, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 3.6, 7.5, 0.02)
                        .text('Dénomination social', 0.1, 3.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [denomination]', 3, 3.7, 5, 0.5, { fontsize: 10 })
                        .text('Enseigne', 0.1, 3.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [enseigne]', 3, 3.9, 5, 0.5, { fontsize: 10 })
                        .text('Sigle', 0.1, 4.1, 2, 0.5, { fontsize: 10 })
                        .text(':  [sigle]', 3, 4.1, 5, 0.5, { fontsize: 10 })
                        .text('Adresse du siège social', 0.1, 4.3, 2, 0.5, { fontsize: 10 })
                        .text(':  [adresseSiegeSocial]', 3, 4.3, 5, 0.5, { fontsize: 10 })
                        .text('Adresse du siège d\'exploitation', 0.1, 4.5, 3, 0.5, { fontsize: 10 })
                        .text(':  [adresseSiegeExploitation]', 3, 4.5, 5, 0.5, { fontsize: 10 })
                        .text('Activité principale', 0.1, 4.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [activitePrincipale]', 3, 4.7, 5, 0.5, { fontsize: 10 })
                        .text('Date de début d\'exploitation', 0.1, 4.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [debutExploitation]', 3, 4.9, 5, 0.5, { fontsize: 10 })
                            .text('  ', 3, 6.7, 5, 0.5, { fontsize: 10 })
                        .text('ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT ', 0, 5.2, 8, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 5.5,7.5, 0.02)
                        .table(0, 5.6, 8, 2, {
                            data: 'associe', hasFooter: true, fontSize: 7, cellpadding : 10
                        }).column('15%', '[nom]', 'Nom', '', {
                            align: 'left',fontsize: 11,height : 15
                        }).column('15%', '[prenom]', 'Postnom', '', {
                            align: 'left', fontsize: 11, height: 15
                        }).column('20%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left', fontsize: 11, height: 15
                        }).column('50%', '[adresse]', 'Adresse', '', {
                            align: 'left', fontsize: 11, height: 15
                        })
                        .text('RENSEIGNEMENTS RELATIFS AUX DIRIGANTS ', 0, 6.8, 8, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 7.1, 7.5, 0.02)
                        .table(0, 7.2, 7.5, 2, {
                            data: 'personnePhysique', hasFooter: true, fontSize: 7
                        }).column('15%', '[nom]', 'Nom', '', {
                            align: 'left',fontsize: 11
                        }).column('15%', '[prenom]', 'Postnom', '', {
                            align: 'left',fontsize: 11
                        }).column('15%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left',fontsize: 11
                        }).column('15%', '[fonction]', 'Fonction', '', {
                            align: 'left', fontsize: 11
                        }).column('40%', '[adresse]', 'Adresse', '', {
                            align: 'left',fontsize: 11
                        })
                        .text('LISTE DES DOCUMENTS FOURNIS ', 0, 8.1, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 8.4, 7.5, 0.02)
                        .text('Extrait de l\'acte de naissance ou attestation', 0.1, 8.6, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [acteNaissance] ', 4, 8.6, 5, 0.5, { fontsize: 10 })
                        .text('Pièce d\'identité reconnue', 0.1, 8.8, 2.5, 0.5, { fontsize: 10 })
                        .text(':  [pieceIdentite]', 4, 8.8, 5, 0.5, { fontsize: 10 })
                        .text('Extrait du casier judiciaire', 0.1, 9, 3, 0.5, { fontsize: 10 })
                        .text(':  [extraitCasierJudiciaire]', 4, 9, 5, 0.5, { fontsize: 10 })
                        .text('PV Assemblée générale constitutive (pour les SA)', 0.1, 9.2, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [PV]', 4, 9.2, 5, 0.5, { fontsize: 10 })
                        .text('Extrait d\'acte de mariage', 0.1, 9.4, 2, 0.5, { fontsize: 10 })
                        .text(':  [acteMariage]', 4, 9.4, 5, 0.5, { fontsize: 10 })
                        .text('Contrat de mariage (pour les étrangers)', 0.1, 9.6, 5, 0.5, { fontsize: 10 })
                        .text(':  [contratMariage]', 4, 9.6, 5, 0.5, { fontsize: 10 })
                        .text('Titre de résidence - visa (pour les étranges)', 0.1, 9.8, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [titreResidenceVISA]', 4, 9.8, 5, 0.5, { fontsize: 10 })
                        .text('Pouvoir du mandataire (Procuration)', 0.1, 10, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [mandatDirrigeant]', 4, 10, 5, 0.5, { fontsize: 10 })
                        .text('Spécimen de la signature du commerçant', 0.1, 10.2, 3, 0.5, { fontsize: 10 })
                        .text(':  [specimenSignature]', 4, 10.2, 5, 0.5, { fontsize: 10 })
                        .text('Statuts notarié', 0.1, 10.4, 3, 0.5, { fontsize: 10 })
                        .text(':  [statutNotaire]', 4, 10.4, 5, 0.5, { fontsize: 10 })
                        .text('Répartitions du capital(liste des actionnaires)', 0.1, 10.6, 4, 0.5, { fontsize: 10 })
                        .text(':  [declarationPartSocial]', 4, 10.6, 5, 0.5, { fontsize: 10 })
                        .text('Preuve de la libération de capital (extrait de compte)', 0.1, 10.8, 4, 0.5, { fontsize: 10 })
                        .text(':  [preuveLiberationCapital]', 4, 10.8, 5, 0.5, { fontsize: 10 })
                        .text('Je reconnais avoir lu toutes les informations ci-dessus et ' +
                        'certifie qu\'elles sont conforme à l\'objet de ma demande.', 0, 12.3, 8, 0.5, { fontsize: 10 })
                        .text('Fait à Kinshasa le ' + output, 5, 14, 4, 2, { fontsize: 10 })
                        .text('Signature du requérant', 0, 14, 4, 2, { fontsize: 10 })
                        .footer()
                        .pageFooter()
                        .text('Page [PAGE_NUMBER] de [PAGE_COUNT]', 0, 0.5, 1.5, 0.25, { fontsize: 10, italic: true })
                        .done();
                    /*
                                            
                                            // Alias JasperReportDef for brevity
                                var JasperReportDef = jsreports.integrations.jasper.JasperReportDef;
                                // Fetch .jrxml file
                                $.get(report1, function (jrxml) {
                                    // Convert to jsreports format
                                    var reportDef = JasperReportDef.fromJRXML(jrxml);
                                    // Create designer
                                    var designer = new jsreports.Designer({
                                        container: $("#textToPrint"),
                                        data_sources: [dataSource],   // Will use the embedded data source information
                                        report: reportDef
                                    });
                                });
                                */
                    // Render the report
                    jsreports.render({
                        report_def: report,
                        target: $('#textToPrint'),
                        showToolbar: false,
                        datasets: [dataSource]
                    });

                    $('#pdf_button').on('click', function() {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });

                    $('#validateBtn').on('click', function() {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });
                },
                error: function(e, status) {
                    if (e.status == 500) {
                        alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                        window.location.replace(logoutUrl);
                    } else {
                        messageAlert("Erreur lors de la génération du rapport...");
                    }
                }
            });
                   
        },
        error: function(e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function Recepisse_demande_immatriculation_personne_physique() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function(data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);

            // test adresse
            if (orderData.adresseSiegeExploitation == null) {
                orderData.adresseSiegeExploitation = orderData.adresseSiegeSocial.adresseRDC._name;
            }

            dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "nomCommercial": IsEmptyString(orderData.nomCommercial),
                    "sigle": IsEmptyString(orderData.sigle),
                    "enseigne": IsEmptyString(orderData.enseigne),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "adresseEtablissementCree": IsEmptyString(orderData.adresseSiegeExploitation),
                    "formeJuridique": IsEmptyString(orderData.formeJuridiqueN1.value),
                    "capitalSocial": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree) + " ans",
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "",
                    "personnePhysique": new Array(),
                    "acteNaissance": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeNaissance),
                    "pieceIdentite": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfPieceIdentite),
                    "extraitCasierJudiciaire": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfextraitCasierJudiciaire),
                    "mandatDirrigeant": testDocumentScanne(orderData.dirigeant["0"].pdfMandatDirigeant),
                    "contratMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfContratMariage),
                    "acteMariage": testDocumentScanne(orderData.dirigeant["0"].personneDirigeant.pdfActeMariage),
                    "specimenSignature": testDocumentScanne(orderData.pdfSpecimenSignatureDirigeant["0"]),
                    "titreResidenceVISA": testDocumentScanne(orderData.dirigeant["0"].pdfTitreResidentVisa),
                    "titreResidence" : testDocumentScanne(orderData.dirigeant["0"].pdfTitreResident)
        }
                ]
            };

           
            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var trueDate;
                    // test date
                    if (IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance) != "   ") {
                        var myDate = IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance);
                        var tab = myDate.split("-");
                        var temp = tab[0];
                        tab[0] = tab[2];
                        tab[2] = temp;
                        trueDate = tab[0] + "-" + tab[1] + "-" + tab[2];
                    }

                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IsEmptyString(orderData.dirigeant[ii].fonction.value)
                    };
                    if (ii < 4) {

                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length - 1].dateNaissance = trueDate;
                    }

                }
            }
           
            // test date duree exploitation
            if (IsEmptyString(orderData.debutExploitation) != "   ") {
                var myDate = IsEmptyString(orderData.debutExploitation);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].debutExploitation = tab[0] + "-" + tab[1] + "-" + tab[2];
            }
           
            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + d.getFullYear();
            $.ajax({
                type: "POST",
                url: findNumeroDosierUrl,
                data: { entityId: entitiID },
                success: function(numeroDossier) {
                    var json = $.parseJSON(numeroDossier);
                    report = jsreports.createReport('Accusé_réception_' + $(".labebackl-warning").text())
                        .data('orders') // The report will look for a data source with ID "orders"
                        .page(8.5, 12.5, 'inches')
                        .header()
                        .text('Antenne de GUCE Kinshasa/Matete', 0.6, 0.2, 5, 3, { fontsize: 10, bold: true })
                .text('Registre du Commerce et du Crédit Mobilier (RCCM)', 0, 0.4, 10, 3, { fontsize: 10, bold: true })
                        .image(logoUrl, 6.5, 0.1, 1, 1)
                        .text('ACCUSE DE RECEPTION', 1.5, 1, 5, 0.5, { fontsize: 22, bold: true, align: 'center' })
                        .text('A la date ci-dessous, le Guichet Unique de Création d\'Entreprises (GUCE) accuse réception de votre dossier pour la '+ 
                                IsEmptyString(orderData.origine.value).toUpperCase() +
                                ' de votre entreprise dont la dénomination sociale est : ', 0.1, 1.8, 7.5, 0.5, { fontsize: 10 })
                        .text('[denomination]', 0.1, 2.3, 7.5, 0.5, { fontsize: 14, bold: true })
                        .text('Numéro du dossier :', 0.1, 2.7, 7.5, 0.5, { fontsize: 10 })
                        .text('  ' + IsEmptyString($(".label-warning").text()), 1.6, 2.7, 5, 0.5, { fontsize: 12, bold: true })
                        .text('Avec les informations suivantes:', 0.1, 3, 7.5, 0.5, { fontsize: 10 })
                        .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE', 0, 3.3, 6, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 3.6, 7.5, 0.02)
                        .text('Dénomination sociale', 0.1, 3.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [denomination]', 2.5, 3.7, 5, 0.5, { fontsize: 10 })
                        .text('Enseigne', 0.1, 3.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [enseigne]', 2.5, 3.9, 5, 0.5, { fontsize: 10 })
                        .text('Sigle', 0.1, 4.1, 2, 0.5, { fontsize: 10 })
                        .text(':  [sigle]', 2.5, 4.1, 5, 0.5, { fontsize: 10 })
                        .text('Adresse de l\'établissement', 0.1, 4.3, 2, 0.5, { fontsize: 10 })
                        .text(':  [adresseSiegeSocial]', 2.5, 4.3, 5, 0.5, { fontsize: 10 })
                        .text('Adresse du lieu d\'éxploitation', 0.1, 4.5, 5, 0.5, { fontsize: 10 })
                        .text(':  [adresseEtablissementCree]', 2.5, 4.5, 5, 0.5, { fontsize: 10 })
                        .text('Activité principale', 0.1, 4.7, 2, 0.5, { fontsize: 10 })
                        .text(':  [activitePrincipale]', 2.5, 4.7, 5, 0.5, { fontsize: 10 })
                        .text('Date de début d\'exploitation', 0.1, 4.9, 2, 0.5, { fontsize: 10 })
                        .text(':  [debutExploitation]', 2.5, 4.9, 5, 0.5, { fontsize: 10 })
                         .text('RENSEIGNEMENTS RELATIFS AUX DIRIGANTS ', 0, 5.3, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 5.6, 7.5, 0.02)
                        .table(0, 5.7, 7.5, 2, {
                            data: 'personnePhysique', hasFooter: true, fontSize: 6.2
                        }).column('15%', '[nom]', 'Nom', '', {
                            align: 'left', fontsize: 8
                        }).column('15%', '[prenom]', 'Postnom', '', {
                            align: 'left', fontsize: 8
                        }).column('20%', '[dateNaissance]', 'Date naissance', '', {
                            align: 'left', fontsize: 8
                        }).column('15%', '[fonction]', 'Fonction', '', {
                            align: 'left', fontsize: 8
                        }).column('35%', '[adresse]', 'Adresse', '', {
                            align: 'left', fontsize: 8
                        })
                        .text('LISTE DES DOCUMENTS NUMERISES ', 0, 6.5, 7.5, 0.1, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 6.8, 7.5, 0.02)
                        .text('Extrait de l\'acte de naissance ou attestation' , 0.1, 7, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [acteNaissance] ', 4, 7, 5, 0.5, { fontsize: 11 })
                        .text('Pièce d\'identité reconnue', 0.1, 7.2, 2.5, 0.5, { fontsize: 10 })
                        .text(':  [pieceIdentite]', 4, 7.2, 5, 0.5, { fontsize: 10 })
                        .text('Extrait du casier judiciaire', 0.1, 7.4, 3, 0.5, { fontsize: 10 })
                        .text(':  [extraitCasierJudiciaire]', 4, 7.4, 5, 0.5, { fontsize: 10 })
                        .text('Attestation de résidence', 0.1, 7.6, 2.5, 0.5, { fontsize: 10 })
                        .text(':  [titreResidence]', 4, 7.6, 5, 0.5, { fontsize: 10 })
                        .text('Extrait d\'acte de mariage', 0.1, 7.8, 2, 0.5, { fontsize: 10 })
                        .text(':  [acteMariage]', 4, 7.8, 5, 0.5, { fontsize: 10 })
                        .text('Contrat de mariage (pour les étrangers)', 0.1, 8, 5, 0.5, { fontsize: 10 })
                        .text(':  [contratMariage]', 4, 8, 5, 0.5, { fontsize: 10 })
                        .text('Titre de résidence - visa (pour les étranges)', 0.1, 8.2, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [titreResidenceVISA]', 4, 8.2, 5, 0.5, { fontsize: 10 })
                        .text('Pouvoir du mandataire (Procuration)', 0.1, 8.4, 3.5, 0.5, { fontsize: 10 })
                        .text(':  [mandatDirrigeant]', 4, 8.4, 5, 0.5, { fontsize: 10 })
                        .text('Spécimen de la signature du commerçant', 0.1, 8.6, 3, 0.5, { fontsize: 10 })
                        .text(':  [specimenSignature]', 4, 8.6, 5, 0.5, { fontsize: 10 })
                        .text('Je reconnais avoir lu toutes les informations ci-dessus et ' +
                        'certifie qu\'elles sont conforme à l\'objet de ma demande.', 0, 9.2, 7.5, 0.5, { fontsize: 10 })
                        .text('Fait à Kinshasa le ' + output, 0, 9.7, 4, 2, { fontsize: 10 })
                        .text('Signature ', 6, 9.7, 4, 2, { fontsize: 10 })

                        .done();
                    /*
                                            
                                            // Alias JasperReportDef for brevity
                                var JasperReportDef = jsreports.integrations.jasper.JasperReportDef;
                                // Fetch .jrxml file
                                $.get(report1, function (jrxml) {
                                    // Convert to jsreports format
                                    var reportDef = JasperReportDef.fromJRXML(jrxml);
                                    // Create designer
                                    var designer = new jsreports.Designer({
                                        container: $("#textToPrint"),
                                        data_sources: [dataSource],   // Will use the embedded data source information
                                        report: reportDef
                                    });
                                });
                                */
                    // Render the report

                  
                    jsreports.render({
                        report_def: report,
                        target: $('#textToPrint'),
                        showToolbar: false,
                        datasets: [dataSource]
                    });

                    $('#pdf_button').on('click', function() {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });

                    $('#validateBtn').on('click', function() {
                        jsreports.export({
                            report_def: report,
                            format: 'pdf',
                            datasets: [dataSource]
                        });
                    });
                },
                error: function(e, status) {
                    if (e.status == 500) {
                        alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                        window.location.replace(logoutUrl);
                    } else {
                        messageAlert("Erreur lors de la génération du rapport...");
                    }
                }
            });
                   
        },
        error: function(e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}


// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"' + obj + '"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof (v);
            if (t == "string") v = '"' + v + '"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

function test_adresse_sigeSocial(json) {
    //if(json.hasOwnProperty(''))
}

function Kbis_personne_physique() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);
            
            var fff = JSON.stringify(orderData);
            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "juridiction": IsEmptyString(orderData._ste_name),
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "numeroRCCM": IsEmptyString(orderData.rccm),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "ancienNRC": ": "+IsEmptyString(orderData.numeroNRC),
                    "dateNRC": ": " + IsEmptyString(orderData.dateNRC),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "   ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IsEmptyString(orderData.dirigeant["0"].fonction),
                    "situationMatrimonial": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.situationMatrimoniale.value),
                    "telDirigeant":IsEmptyString(orderData.dirigeant["0"].personneDirigeant.telephoneMobile),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "nationalite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nationalite.value),
                    "personnePhysique": new Array(),
                    "duree": IsEmptyString(orderData.duree),
                    "etablissementsSecondaires" : new Array(),
                    "LibeleNRC": "Numéro NRC",
                    "DateNRC": "Date d\'\immatriculation NRC"
                }]
            };
           
             // test numero NRC
            if (IsEmptyString(orderData.numeroNRC) == "   ") {
                dataSource.data["0"].LibeleNRC = "";
                dataSource.data["0"].DateNRC = ""; 
                dataSource.data["0"].ancienNRC = "";
                dataSource.data["0"].dateNRC = "";
            }

            if (orderData.adressesEtablissementsSecondaires.length > 0) {
                
            }


            // test date duree exploitation
            if (IsEmptyString(orderData.debutExploitation) != "   ") {
                var myDate = IsEmptyString(orderData.debutExploitation);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].debutExploitation = tab[0] + "-" + tab[1] + "-" + tab[2];
            }

            // test date immatriculation RCCM
            if (IsEmptyString(orderData.dateImmatriculationRCCM) != "   ") {
                var myDate = IsEmptyString(orderData.dateImmatriculationRCCM);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].dateImmatriculationRCCM = tab[0] + "-" + tab[1] + "-" + tab[2];
            }

            // test date immatriculation RCCM
            if (IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance) != "   ") {
                var myDate = IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].dateNaissance = tab[0] + "-" + tab[1] + "-" + tab[2];
            }

            // Ajout des adresse pour un etablissement secondaire
            if (orderData.adressesEtablissementsSecondaires.length > 0) {
                for (var ii = 0; ii < orderData.adressesEtablissementsSecondaires.length; ii++) {

                    var unEtablissementSecondaireJson = {
                        "adresse": IsEmptyString(orderData.adressesEtablissementsSecondaires[ii])
                    };
                    if (ii < 4) {
                        dataSource.data["0"].etablissementsSecondaires[dataSource.data["0"].etablissementsSecondaires.length + 1] = unEtablissementSecondaireJson;
                    }

                }
            }

            if (orderData.adressesEtablissementsSecondaires.length == 0) {
                var a = 8.2;
            } else {
                
            }

            

            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(8.5, 12.5, 'inches')
                .margins(0.5)
                .header()
                .text('Antenne de GUCE Kinshasa/Gombe', 0.4, 0.2, 5, 3, { fontsize: 8, bold: true })
                .text('Registre du Commerce et du Crédit Mobilier (RCCM)', 0, 0.35, 10, 3, { fontsize: 8, bold: true })
                .image(logoUrl, 6.5, 0.1, 1, 1)
                .text('Extrait du Registre du Commerce et du Crédit Mobilier', 0.4, 1.2, 7.8, 0.5, { fontsize: 21, bold: true })
                .text('IDENTIFICATION DE L\'ETABLISSEMENT', 0, 2.2, 4, 0.1, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 2.5, 7.5, 0.02)
                .text('Dénomination sociale', 0.1, 2.6, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 2.5, 2.6, 5, 0.5, { fontsize: 10 })
                .text('Numéro RCCM', 0.1, 2.8, 2, 0.5, { fontsize: 10 })
                .text(':  [numeroRCCM]', 2.5, 2.8, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 3, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 2.5, 3, 4, 0.5, { fontsize: 10 })
                .text('Adresse de l\'établissement', 0.1, 3.2, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 2.5, 3.2, 5, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 3.4, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 2.5, 3.4, 6, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 3.6, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 2.5, 3.6, 6, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 3.8, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 2.5, 3.8, 4, 0.5, { fontsize: 10 })
                .text('Durée', 0.1, 4, 7, 0.5, { fontsize: 10 })
                .text(':  [duree]', 2.5, 4, 4, 0.5, { fontsize: 10 })
                .text('[LibeleNRC]', 0.1, 4.2, 2, 0.5, { fontsize: 10 })
                .text('[ancienNRC]', 2.5, 4.2, 3, 0.5, { fontsize: 10 })
                .text('[DateNRC]', 0.1, 4.4, 7, 0.5, { fontsize: 10 })
                .text('[dateNRC]', 2.5, 4.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE', 0, 4.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 4.9, 7.5, 0.02)
                .text('Nom', 0.1, 5.1, 2, 0.5, { fontsize: 10 })
                .text(':  [civilite] [nom]', 2.5, 5.1, 3, 0.5, { fontsize: 10 })
                .text('Prenom', 0.1, 5.3, 7, 0.5, { fontsize: 10 })
                .text(':  [prenom]', 2.5, 5.3, 4, 0.5, { fontsize: 10 })
                .text('Date de naissance', 0.1, 5.5, 4, 0.5, { fontsize: 10 })
                .text(':  [dateNaissance]', 2.5, 5.5, 5, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 5.7, 7, 0.5, { fontsize: 10 })
                .text(':  [adresse]', 2.5, 5.7, 6, 0.5, { fontsize: 10 })
                .text('Nationalite', 0.1, 5.9, 7, 0.5, { fontsize: 10 })
                .text(':  [nationalite]', 2.5, 5.9, 4, 0.5, { fontsize: 10 })
                .text('Situation matrimoniale', 0.1, 6.1, 7, 0.5, { fontsize: 10 })
                .text(':  [situationMatrimonial]', 2.5, 6.1, 4, 0.5, { fontsize: 10 })
                .text('Numéro de telephone', 0.1, 6.3, 7, 0.5, { fontsize: 10 })
                .text(':  [telDirigeant]', 2.5, 6.3, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A L\'ETABLISSEMENT SECONDAIRE OU SUCCURSALE', 0, 6.8, 8, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 7.1, 7.5, 0.02)
                .table(0, 7.2, 7.5, 2, {
                    data: 'etablissementsSecondaires', hasFooter: true, fontSize: 6.2
                }).column('100%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 8
                })
                .text('RENSEIGNEMENTS RELATIFS AU SURETES', 0, a, 8, 0.2, { fontsize: 13, bold: true })
                .box('black', '#00', 0, 8.5, 7.5, 0.02)
                .text('NEANT (Non déclaré sur le système informatique RCCM au ' + output + ')'
                , 0.1, 8.6, 7, 0.5, { fontsize: 10, align: 'center' })
                .text('Toute Modification ou falsification du présent extrait expose à des poursuites pénales, '+
                'y compris toute reproduction du présent extrait, même certifié conforme est sans valeur légale. '
                , 0.2, 9.2, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 9.7, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 9.9, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 10.1, 4, 0.5, { fontsize: 10 })
            .detail(3)
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de la Gombe ', 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });
            
        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Kbis_personne_morale() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);

            var fff = JSON.stringify(orderData);

            // test adresse
            if (orderData.adresseSiegeExploitation == null) {
                orderData.adresseSiegeExploitation = orderData.adresseSiegeSocial;
            }

            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "numeroRCCM": IsEmptyString(orderData.rccm),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "ancienNRC": ": " + IsEmptyString(orderData.numeroNRC),
                    "dateNRC": ": " + IsEmptyString(orderData.dateNRC),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "adresseSiegeExploitation": IsEmptyString(orderData.adresseSiegeExploitation.adresseRDC._name),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "   ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IsEmptyString(orderData.dirigeant["0"].fonction),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "formJuridique": IsEmptyString(orderData.formeJuridiqueN3.value),
                    "capital": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree),
                    "nationalite": "   ",
                    "personnePhysique": new Array(),
                    "associe": new Array(),
                    "reantAssocie": " ",
                    "LibeleNRC": "Numéro NRC",
                    "DateNRC": "Date d\'\immatriculation NRC"
                }]
            };

            //test durée
            if (orderData.duree == 1) {
                dataSource.data["0"].duree = "un ";
            }

            // test numero NRC
            if (IsEmptyString(orderData.numeroNRC) == "   ") {
                dataSource.data["0"].LibeleNRC = "";
                dataSource.data["0"].DateNRC = ""; 
                dataSource.data["0"].ancienNRC = "";
                dataSource.data["0"].dateNRC = "";
            }

            // test date immatriculation RCCM
            if (IsEmptyString(orderData.dateImmatriculationRCCM) != "   ") {
                var myDate = IsEmptyString(orderData.dateImmatriculationRCCM);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].dateImmatriculationRCCM = tab[0] + "-" + tab[1] + "-" + tab[2];
            }

             // test date duree exploitation
            if (IsEmptyString(orderData.debutExploitation) != "   ") {
                var myDate = IsEmptyString(orderData.debutExploitation);
                var tab = myDate.split("-");
                var temp = tab[0];
                tab[0] = tab[2];
                tab[2] = temp;
                dataSource.data["0"].debutExploitation = tab[0] + "-" + tab[1] + "-" + tab[2];
            }

            // test adresse
            if (dataSource.data["0"].adresseSiegeExploitation == "   ") {
                dataSource.data["0"].adresseSiegeExploitation = dataSource.data["0"].adresseSiegeSocial;
            }

            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {
                    var trueDate;
                    // test date
                    if (IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance) != "   ") {
                        var myDate = IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance);
                        var tab = myDate.split("-");
                        var temp = tab[0];
                        tab[0] = tab[2];
                        tab[2] = temp;
                        trueDate = tab[0] + "-" + tab[1] + "-" + tab[2];
                    }

                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                        "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                        "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                        "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson;
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length - 1].dateNaissance = trueDate;
                    }


                }
            } else {
                dataSource.data["0"].reantAssocie = "   ";
            }

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var trueDate;
                    // test date
                    if (IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance) != "   ") {
                        var myDate = IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance);
                        var tab = myDate.split("-");
                        var temp = tab[0];
                        tab[0] = tab[2];
                        tab[2] = temp;
                        trueDate = tab[0] + "-" + tab[1] + "-" + tab[2];
                    }

                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IsEmptyString(orderData.dirigeant[ii].fonction.value)
                    };
                    if (ii < 4) {

                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length - 1].dateNaissance = trueDate;
                    }

                }
            }

            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(8.5, 12.5, 'inches')
                .header()
                .text('Antenne de GUCE Kinshasa/Matete', 0.6, 0.2, 5, 3, { fontsize: 10, bold: true })
                .text('Registre du Commerce et du Crédit Mobilier (RCCM)', 0, 0.4, 10, 3, { fontsize: 10, bold: true })
                .image(logoUrl, 6.5, 0.1, 1, 1)
                .text('Extrait du Registre du Commerce et du Crédit Mobilier', 0.4, 1.2, 7.8, 0.5, { fontsize: 21, bold: true })
                .text('IDENTIFICATION DE LA SOCIETE', 0, 2.2, 4, 0.1, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 2.5, 7.5, 0.02)
                .text('Dénomination social', 0.1, 2.6, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 3, 2.6, 5, 0.5, { fontsize: 10 })
                .text('Numéro RCCM', 0.1, 2.8, 2, 0.5, { fontsize: 10 })
                .text(':  [numeroRCCM]', 3, 2.8, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 3, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 3, 3, 4, 0.5, { fontsize: 10 })
                .text('Adresse du Siège Sociale', 0.1, 3.2, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 3.2, 5, 0.5, { fontsize: 10 })
                .text('Adresse du Siège d\'exploitation', 0.1, 3.4, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeExploitation]', 3, 3.4, 5, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 3.6, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 3.6, 5, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 3.8, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 3, 3.8, 4, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 4, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 3, 4, 4, 0.5, { fontsize: 10 })
                .text('[LibeleNRC]', 0.1, 4.2, 2, 0.5, { fontsize: 10 })
                .text('[ancienNRC]', 3, 4.2, 3, 0.5, { fontsize: 10 })
                .text('[DateNRC]', 0.1, 4.4, 7, 0.5, { fontsize: 10 })
                .text('[dateNRC]', 3, 4.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE', 0, 4.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 4.9, 7.5, 0.02)
                .text('Forme juridique', 0.1, 5.1, 2, 0.5, { fontsize: 10 })
                .text(':  [formJuridique]', 3, 5.1, 5, 0.5, { fontsize: 10 })
                .text('Au Capital de ', 0.1, 5.3, 7, 0.5, { fontsize: 10 })
                .text(':  [capital] USD', 3, 5.3, 4, 0.5, { fontsize: 10 })
                .text('Durée de la société', 0.1, 5.5, 4, 0.5, { fontsize: 10 })
                .text(':  [duree] ans', 3, 5.5, 5, 0.5, { fontsize: 10 })
                .text('Constitution - dépots de l\'acte constitutif', 0.1, 5.7, 7, 0.5, { fontsize: 10 })
                .text(':  ' + IsEmptyString(orderData._ste_name), 3, 5.7, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS AU DIRIGANTS', 0, 5.9, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 6.2, 7.5, 0.02)
                .table(0, 6.3, 7.5, 2, {
                    data: 'personnePhysique', hasFooter: true, fontSize: 7
                }).column('15%', '[nom]', 'Nom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[prenom]', 'Postnom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[dateNaissance]', 'Date naissance', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[fonction]', 'Fonction', '', {
                    align: 'left', fontsize: 10
                }).column('40%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 10
                })
                .text('RENSEIGNEMENTS RELATIFS AU ASSOCIES', 0, 7, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 7.3, 7.5, 0.02)
                .table(0, 7.4, 7.5, 2, {
                    data: 'associe', hasFooter: true, fontSize: 7
                }).column('15%', '[nom]', 'Nom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[prenom]', 'Postnom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[dateNaissance]', 'Date naissance', '', {
                    align: 'left', fontsize: 10
                }).column('65%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 10
                })
                .text('RENSEIGNEMENTS RELATIFS AU SURETES', 0, 8, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 8.3, 7.5, 0.02)
                .text('NEANT (Non déclaré sur le système informatique RCCM au ' + output + ')', 0.1, 8.5, 7, 0.5, { fontsize: 10, align: 'center' })
                .text('Toute Modification ou falsification du présent extrait expose à des poursuites pénales,' +
                ' y compris toute reproduction du présent extrait, même certifié conforme est sans valeur légale. ', 0.2, 9, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 9.3, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 9.5, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 9.7, 4, 0.5, { fontsize: 10 })
            .detail()
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de la Gombe ', 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Kbis_modification_personne_physique() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);

            var fff = JSON.stringify(orderData);
            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "juridiction": IsEmptyString(orderData._ste_name),
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "numeroRCCM": IsEmptyString(orderData.rccm),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "ancienNRC": ": " + IsEmptyString(orderData.numeroNRC),
                    "dateNRC": ": " + IsEmptyString(orderData.dateNRC),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "   ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IsEmptyString(orderData.dirigeant["0"].fonction),
                    "situationMatrimonial": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.situationMatrimoniale),
                    "telDirigeant": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.telephoneMobile),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "nationalite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nationalite.value),
                    "personnePhysique": new Array(),
                    "adressesEtablissementsSecondaires" : new Array(),
                    "duree": IsEmptyString(orderData.duree),
                    "LibeleNRC": "Numéro NRC",
                    "DateNRC": "Date d\'\immatriculation NRC"
                }]
            };

            // test numero NRC
            if (IsEmptyString(orderData.numeroNRC) == "   ") {
                dataSource.data["0"].LibeleNRC = "";
                dataSource.data["0"].DateNRC = "";
                dataSource.data["0"].ancienNRC = "";
                dataSource.data["0"].dateNRC = "";
            }



            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(9, 10, 'inches')
                .margins(0.5)
                .header()
                .text('Antenne de [juridiction] ', 0.65, 0, 4, 0.2, { fontsize: 10, bold: true })
                .text('Registre du Commerce Et du Crédit Mobilier (RCCM)', 0, 0.2, 4, 0.2, { fontsize: 10, bold: true })
                .image(logoUrl, 6.5, 0.1, 1, 1)
                .text('Extrait du Registre du Commerce et du Crédit Mobilier', 0.4, 1.2, 7.8, 0.5, { fontsize: 21, bold: true })
                .text('IDENTIFICATION DE L\'ETABLISSEMENT', 0, 2.2, 4, 0.1, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 2.5, 8.2, 0.02)
                .text('Dénomination sociale', 0.1, 2.6, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 3, 2.6, 5, 0.5, { fontsize: 10 })
                .text('Numéro RCCM', 0.1, 2.8, 2, 0.5, { fontsize: 10 })
                .text(':  [numeroRCCM]', 3, 2.8, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 3, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 3, 3, 4, 0.5, { fontsize: 10 })
                .text('Adresse de l\'établissement', 0.1, 3.2, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 3.2, 5, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 3.4, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 3.4, 6, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 3.6, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 3, 3.6, 6, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 3.8, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 3, 3.8, 4, 0.5, { fontsize: 10 })
                .text('Durée', 0.1, 4, 7, 0.5, { fontsize: 10 })
                .text(':  [duree]', 3, 4, 4, 0.5, { fontsize: 10 })
                .text('[LibeleNRC]', 0.1, 4.2, 2, 0.5, { fontsize: 10 })
                .text('[ancienNRC]', 3, 4.2, 3, 0.5, { fontsize: 10 })
                .text('[DateNRC]', 0.1, 4.4, 7, 0.5, { fontsize: 10 })
                .text('[dateNRC]', 3, 4.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE', 0, 4.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 4.9, 8.1, 0.02)
                .text('Nom', 0.1, 5.1, 2, 0.5, { fontsize: 10 })
                .text(':  [civilite] [nom]', 3, 5.1, 3, 0.5, { fontsize: 10 })
                .text('Prenom', 0.1, 5.3, 7, 0.5, { fontsize: 10 })
                .text(':  [prenom]', 3, 5.3, 4, 0.5, { fontsize: 10 })
                .text('Date de naissance', 0.1, 5.5, 4, 0.5, { fontsize: 10 })
                .text(':  [dateNaissance]', 3, 5.5, 5, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 5.7, 7, 0.5, { fontsize: 10 })
                .text(':  [adresse]', 3, 5.7, 4, 0.5, { fontsize: 10 })
                .text('Nationalite', 0.1, 5.9, 7, 0.5, { fontsize: 10 })
                .text(':  [nationalite]', 3, 5.9, 4, 0.5, { fontsize: 10 })
                .text('Situation matrimoniale', 0.1, 6.1, 7, 0.5, { fontsize: 10 })
                .text(':  [situationMatrimonial]', 3, 6.1, 4, 0.5, { fontsize: 10 })
                .text('Numéro de telephone', 0.1, 6.3, 7, 0.5, { fontsize: 10 })
                .text(':  [telDirigeant]', 3, 6.3, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A L\'ETABLISSEMENT SECONDAIRE OU SUCCURSALE', 0, 6.8, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 7.1, 8.1, 0.02)
                .text('Nom commerciale', 0.1, 7.2, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.2, 4, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 7.4, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.4, 4, 0.5, { fontsize: 10 })
                .text('Activité', 0.1, 7.6, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.6, 4, 0.5, { fontsize: 10 })
                .text('Date d\'ouverture', 0.1, 7.8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.8, 4, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 8, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA CREATION, AUX MODIFICATIONS, AUX DEPOTS D\'ACTE', 0, 8.4, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 8.7, 8.1, 0.02)

                .text('RENSEIGNEMENTS RELATIFS AU SURETES', 0, 9.2, 8.6, 0.2, { fontsize: 13, bold: true })
                .box('black', '#00', 0, 9.5, 8.1, 0.02)
                .text('NEANT (Non déclaré sur le système informatique RCCM au ' + output + ')', 0.1, 9.6, 7, 0.5, { fontsize: 10, align: 'center' })
                .text('Toute Modification ou falsification du présent extrait expose à des poursuites pénales, y compris toute reproduction du présent extrait, même certifié conforme est sans valeur légale. ', 0.2, 10.2, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 11.2, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 11.6, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 12, 4, 0.5, { fontsize: 10 })
            .detail(3)
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de la Gombe ', 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Kbis_modification_personne_morale() {
     var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);
            
            if (orderData.adresseSiegeExploitation == null) {
                orderData.adresseSiegeExploitation = orderData.adresseSiegeSocial;
            }

            var fff = JSON.stringify(orderData);
            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "numeroRCCM": IsEmptyString(orderData.rccm),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "ancienNRC": ": " + IsEmptyString(orderData.numeroNRC),
                    "dateNRC": ": " + IsEmptyString(orderData.dateNRC),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "adresseSiegeExploitation": IsEmptyString(orderData.adresseSiegeExploitation.adresseRDC._name),
                    "debutExploitation": IsEmptyString(orderData.debutExploitation),
                    "modeExploitation": "   ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IsEmptyString(orderData.dirigeant["0"].fonction),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "formJuridique": IsEmptyString(orderData.formeJuridiqueN3.value),
                    "capital": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree),
                    "nationalite": "   ",
                    "personnePhysique": new Array(),
                    "associe": new Array(),
                    "reantAssocie": " ",
                    "LibeleNRC": "Numéro NRC",
                    "DateNRC": "Date d\'\immatriculation NRC"
                }]
            };

            //test durée
            if (orderData.duree == 1) {
                dataSource.data["0"].duree = "un ";
            }

            // test numero NRC
            if (IsEmptyString(orderData.numeroNRC) == "   ") {
                dataSource.data["0"].LibeleNRC = "";
                dataSource.data["0"].DateNRC = ""; 
                dataSource.data["0"].ancienNRC = "";
                dataSource.data["0"].dateNRC = "";
            }

            // test adresse
            if (dataSource.data["0"].adresseSiegeExploitation == "   ") {
                dataSource.data["0"].adresseSiegeExploitation = dataSource.data["0"].adresseSiegeSocial;
            }

            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                        "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                        "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                        "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson;
                    }

                }
            } else {
                dataSource.data["0"].reantAssocie = "   ";
            }

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IsEmptyString(orderData.dirigeant[ii].fonction.value)
                    };
                    if (ii < 4) {
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                    }

                }
            }

            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(9, 10, 'inches')
                .header()
                .text('Antenne de ' + IsEmptyString(orderData._ste_name), 0.65, 0, 4, 0.2, { fontsize: 10, bold: true })
                .text('Registre du Commerce et du Crédit Mobilier (RCCM)', 0, 0.2, 4, 0.2, { fontsize: 10, bold: true })
                .image(logoUrl, 6.5, 0.1, 1, 1)
                .text('Extrait du Registre du Commerce et du Crédit Mobilier', 0.4, 1.2, 7.8, 0.5, { fontsize: 21, bold: true })
                .text('IDENTIFICATION DE LA SOCIETE', 0, 2.2, 4, 0.1, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 2.5, 8.2, 0.02)
                .text('Dénomination social', 0.1, 2.6, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 3, 2.6, 5, 0.5, { fontsize: 10 })
                .text('Numéro RCCM', 0.1, 2.8, 2, 0.5, { fontsize: 10 })
                .text(':  [numeroRCCM]', 3, 2.8, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 3, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 3, 3, 4, 0.5, { fontsize: 10 })
                .text('Adresse du Siege Sociale', 0.1, 3.2, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 3.2, 5, 0.5, { fontsize: 10 })
                .text('Adresse du Siege d\'exploitation', 0.1, 3.4, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeExploitation]', 3, 3.4, 5, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 3.6, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 3.6, 5, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 3.8, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 3, 3.8, 4, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 4, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 3, 4, 4, 0.5, { fontsize: 10 })
                .text('[LibeleNRC]', 0.1, 4.2, 2, 0.5, { fontsize: 10 })
                .text('[ancienNRC]', 3, 4.2, 3, 0.5, { fontsize: 10 })
                .text('[DateNRC]', 0.1, 4.4, 7, 0.5, { fontsize: 10 })
                .text('[dateNRC]', 3, 4.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE', 0, 4.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 4.9, 8.1, 0.02)
                .text('Forme juridique', 0.1, 5.1, 2, 0.5, { fontsize: 10 })
                .text(':  [formJuridique]', 3, 5.1, 5, 0.5, { fontsize: 10 })
                .text('Au Capital de ', 0.1, 5.3, 7, 0.5, { fontsize: 10 })
                .text(':  [capital]', 3, 5.3, 4, 0.5, { fontsize: 10 })
                .text('Durée de la société', 0.1, 5.5, 4, 0.5, { fontsize: 10 })
                .text(':  [duree] ans', 3, 5.5, 5, 0.5, { fontsize: 10 })
                .text('Constitution - dépots de l\'acte constitutif', 0.1, 5.7, 7, 0.5, { fontsize: 10 })
                .text(':  ' + IsEmptyString(orderData._ste_name), 3, 5.7, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS AU DIRIGANTS', 0, 5.9, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 6.2, 8.1, 0.02)
                .table(0, 6.3, 7.5, 2, {
                    data: 'personnePhysique', hasFooter: true, fontSize: 7
                }).column('15%', '[nom]', 'Nom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[prenom]', 'Postnom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[dateNaissance]', 'Date naissance', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[fonction]', 'Fonction', '', {
                    align: 'left', fontsize: 10
                }).column('40%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 10
                })
                .text('RENSEIGNEMENTS RELATIFS AU ASSOCIES', 0, 7.2, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 7.5, 8.1, 0.02)
                .table(0, 7.6, 7.5, 2, {
                    data: 'associe', hasFooter: true, fontSize: 7
                }).column('15%', '[nom]', 'Nom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[prenom]', 'Postnom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[dateNaissance]', 'Date naissance', '', {
                    align: 'left', fontsize: 10
                }).column('65%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 10
                })
                .text(' [reantAssocie] ', 3, 8, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA CREATION, AUX MODIFICATIONS, AUX DEPOTS D\'ACTE', 0, 8.5, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 8.8, 8.1, 0.02)
                .text('Fiche NRC', 10, 10, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 10, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS AU SURETES', 0, 9.5, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 9.8, 8.1, 0.02)
                .text('NEANT (Non déclaré sur le système informatique RCCM au ' + output + ')', 0.1, 10, 7, 0.5, { fontsize: 10, align: 'center' })
                .text('Toute Modification ou falsification du présent extrait expose à des poursuites pénales,' +
                ' y compris toute reproduction du présent extrait, même certifié conforme est sans valeur légale. ', 0.2, 12.2, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 13.2, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 13.6, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 13, 4, 0.5, { fontsize: 10 })
            .detail()
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de la Gombe ', 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Kbis_modif() {
    var entitiID = getUrlParameter("entityId");

    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            orderData = $.parseJSON(stringData);
          if (orderData.formeJuridiqueN1.code == "B") {
              Kbis_modification_personne_morale();
            } else if (orderData.formeJuridiqueN1.code == "A") {
                Kbis_modification_personne_physique();
            }

        },
        error: function (e, status) {
            if (e.status == 500) {
                alert("Erreur interne au serveur. Probablement le délais d'attente est dépassé. Vous allez être redirrigé vers la page de connexion");
                window.location.replace(logoutUrl);
            } else {
                messageAlert("Erreur lors de la génération du rapport...");
            }
        }
    });
}

function Kbis() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);


            if (orderData.formeJuridiqueN1.code == "B") {
                Kbis_personne_morale();
            } else if (orderData.formeJuridiqueN1.code == "A") {
                Kbis_personne_physique();
                //Fiche_RCCM_personne_physique();
            }
            /*
            var fff = JSON.stringify(orderData);
            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "numeroRCCM": IsEmptyString(orderData.rccm),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "ancienNRC": IsEmptyString(orderData.numeroNRC),
                    "dateNRC": IsEmptyString(orderData.dateNRC),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "debutExploitation": " NEANT ",
                    "modeExploitation": " NEANT ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IfExistProperty(orderData.dirigeant["0"], "fonction"),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "nationalite": " NEANT ",
                    "personnePhysique": new Array()
                }]
            };
           
            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(9, 10, 'inches')
                .margins(0.5)
                .header()
                .text('Antenne de ' + IsEmptyString(orderData._ste_name).substring(6), 0.6, 0, 4, 0.2, { fontsize: 10, bold: true })
                .text('Registre du Commerce Et du Crédit Mobilier (RCCM)', 0, 0.2, 4, 0.2, { fontsize: 10, bold: true })
                .image(logoUrl, 6.5, 0.1, 1, 1)
                .text('Extrait du Registre du Commerce et du Crédit Mobilier', 0.4, 1.2, 7.8, 0.5, { fontsize: 21, bold: true })
                .text('IDENTIFICATION DE L\'ETABLISSEMENT', 0, 2.2, 4, 0.1, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 2.5, 8.2, 0.02)
                .text('Dénomination', 0.1, 2.6, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 3, 2.6, 5, 0.5, { fontsize: 10 })
                .text('Numéro RCCM', 0.1, 2.8, 2, 0.5, { fontsize: 10 })
                .text(':  [numeroRCCM]', 3, 2.8, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 3, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 3, 3, 4, 0.5, { fontsize: 10 })
                .text('Numéro NRC', 0.1, 3.2, 2, 0.5, { fontsize: 10 })
                .text(':  [ancienNRC]', 3, 3.2, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation NRC', 0.1, 3.4, 7, 0.5, { fontsize: 10 })
                .text(':  [dateNRC]', 3, 3.4, 4, 0.5, { fontsize: 10 })
                .text('Adresse du Siege Social', 0.1, 3.6, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 3.6, 5, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 3.8, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 3.8, 4, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 4, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 3, 4, 4, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 4.2, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 3, 4.2, 4, 0.5, { fontsize: 10 })
                .text('Mode d\'exploitation', 0.1, 4.4, 7, 0.5, { fontsize: 10 })
                .text(':  [modeExploitation]', 3, 4.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE', 0, 4.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 4.9, 8.1, 0.02)
                .text('Nom', 0.1, 5.1, 2, 0.5, { fontsize: 10 })
                .text(':  [civilite] [nom]', 3, 5.1, 3, 0.5, { fontsize: 10 })
                .text('Prenom', 0.1, 5.3, 7, 0.5, { fontsize: 10 })
                .text(':  [prenom]', 3, 5.3, 4, 0.5, { fontsize: 10 })
                .text('Date de naissance', 0.1, 5.5, 4, 0.5, { fontsize: 10 })
                .text(':  [dateNaissance]', 3, 5.5, 5, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 5.7, 7, 0.5, { fontsize: 10 })
                .text(':  [adresse]', 3, 5.7, 4, 0.5, { fontsize: 10 })
                .text('Nationalite', 0.1, 5.9, 7, 0.5, { fontsize: 10 })
                .text(':  [nationalite]', 3, 5.9, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A L\'ETABLISSEMENT SECONDAIRE ou SUCCURSALE', 0, 6.8, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 7.1, 8.1, 0.02)
                .text('Nom commerciale', 0.1, 7.2, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.2, 4, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 7.4, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.4, 4, 0.5, { fontsize: 10 })
                .text('Activité', 0.1, 7.6, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.6, 4, 0.5, { fontsize: 10 })
                .text('Date d\'ouverture', 0.1, 7.8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.8, 4, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 8, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA CREATION, AUX MODIFICATIONS, AUX DEPOTS D\'ACTE', 0, 8.4, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 8.7, 8.1, 0.02)
                .text('Fiche NRC', 0.1, 8.8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 8.8, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS AU SURETES', 0, 9.2, 8.6, 0.2, { fontsize: 13, bold: true })
                .box('black', '#00', 0, 9.5, 8.1, 0.02)
                .text('NEANT (Non déclaré sur le système informatique RCCM au ' + output + ')', 0.1, 9.6, 7, 0.5, { fontsize: 10, align: 'center' })
                .text('Toute Modification ou falsification du présent extrait expose à des poursuites pénales, y compris toute reproduction du présent extrait, même certifié conforme est sans valeur légale. ', 0.2, 10.2, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 11.2, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 11.6, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 12, 4, 0.5, { fontsize: 10 })
            .detail(3)
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de la Gombe ', 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });
            */
        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Fiche_RCCM_personne_physique() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);
            var fff = JSON.stringify(orderData);
            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "numeroRCCM": IsEmptyString(orderData.rccm),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "ancienNRC": IsEmptyString(orderData.numeroNRC),
                    "dateNRC": IsEmptyString(orderData.dateNRC),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "debutExploitation": "   ",
                    "modeExploitation": "   ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IfExistProperty(orderData.dirigeant["0"], "fonction"),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "nationalite": "   ",
                    "personnePhysique": new Array()
                }]
            };

            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(9, 10, 'inches')
                .margins(0.5)
                .header()
                .box("#ffffff", "#8a2be2", 0, 0, 8, 1.11,{border : 2})
                .text('Po ' , 0, 0, 4, 0.2, { fontsize: 60, fontcolor : "blue" })
                .text('DECLARATION D\'OUVERTURE D\'UN ETABLISSEMENT PRINCIPAL', 1.5, 0.2, 7, 0.2, { fontsize: 13, bold: true })
                .text('A.P Porto Novo 23/24 juin 1999', 0.1, 0.9, 4, 0.2, { fontsize: 9 })

                .text('RENSEIGNEMENTS RELATIFS A L\'EXPLOITANT', 2.4, 1.35, 7.8, 0.5, { fontsize: 10, bold: true })
                .box("#ffffff", "#8a2be2", 0, 1.5, 8, 1.11, { border: 5,fontsize: 9, bold: true })
                .text('Nom :', 0, 1.5, 4, 0.1, { fontsize: 10 })
                .text('[denomination]', 0.4, 1.5, 5, 0.5, { fontsize: 10 })
                .text('Prenom :', 4, 1.5, 4, 0.1, { fontsize: 10 })
                .text('[denomination]', 4.6, 1.5, 5, 0.5, { fontsize: 10 })
                .text('Date et lieu de naissance', 0, 1.7, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 3, 1.7, 5, 0.5, { fontsize: 10 })
                .text('Nationalité :', 4, 1.9, 2, 0.5, { fontsize: 10 })
                .text('[numeroRCCM]', 4.3, 1.9, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 3, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 3, 3, 4, 0.5, { fontsize: 10 })
                .text('Numéro NRC', 0.1, 3.2, 2, 0.5, { fontsize: 10 })
                .text(':  [ancienNRC]', 3, 3.2, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation NRC', 0.1, 3.4, 7, 0.5, { fontsize: 10 })
                .text(':  [dateNRC]', 3, 3.4, 4, 0.5, { fontsize: 10 })
                .text('Adresse du Siege Social', 0.1, 3.6, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 3.6, 5, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 3.8, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 3.8, 4, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 4, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 3, 4, 4, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 4.2, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 3, 4.2, 4, 0.5, { fontsize: 10 })
                .text('Mode d\'exploitation', 0.1, 4.4, 7, 0.5, { fontsize: 10 })
                .text(':  [modeExploitation]', 3, 4.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE PHYSIQUE', 0, 4.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 4.9, 8.1, 0.02)
                .text('Nom', 0.1, 5.1, 2, 0.5, { fontsize: 10 })
                .text(':  [civilite] [nom]', 3, 5.1, 3, 0.5, { fontsize: 10 })
                .text('Prenom', 0.1, 5.3, 7, 0.5, { fontsize: 10 })
                .text(':  [prenom]', 3, 5.3, 4, 0.5, { fontsize: 10 })
                .text('Date de naissance', 0.1, 5.5, 4, 0.5, { fontsize: 10 })
                .text(':  [dateNaissance]', 3, 5.5, 5, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 5.7, 7, 0.5, { fontsize: 10 })
                .text(':  [adresse]', 3, 5.7, 4, 0.5, { fontsize: 10 })
                .text('Nationalite', 0.1, 5.9, 7, 0.5, { fontsize: 10 })
                .text(':  [nationalite]', 3, 5.9, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A L\'ETABLISSEMENT SECONDAIRE ou SUCCURSALE', 0, 6.8, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 7.1, 8.1, 0.02)
                .text('Nom commerciale', 0.1, 7.2, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.2, 4, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 7.4, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.4, 4, 0.5, { fontsize: 10 })
                .text('Activité', 0.1, 7.6, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.6, 4, 0.5, { fontsize: 10 })
                .text('Date d\'ouverture', 0.1, 7.8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 7.8, 4, 0.5, { fontsize: 10 })
                .text('Adresse', 0.1, 8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 8, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A LA CREATION, AUX MODIFICATIONS, AUX DEPOTS D\'ACTE', 0, 8.4, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 8.7, 8.1, 0.02)
                .text('Fiche NRC', 0.1, 8.8, 7, 0.5, { fontsize: 10 })
                .text(':  ', 3, 8.8, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS AU SURETES', 0, 9.2, 8.6, 0.2, { fontsize: 13, bold: true })
                .box('black', '#00', 0, 9.5, 8.1, 0.02)
                .text('NEANT (Non déclaré sur le système informatique RCCM au ' + output + ')', 0.1, 9.6, 7, 0.5, { fontsize: 10, align: 'center' })
                .text('Toute Modification ou falsification du présent extrait expose à des poursuites pénales, y compris toute reproduction du présent extrait, même certifié conforme est sans valeur légale. ', 0.2, 10.2, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 11.2, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 11.6, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 12, 4, 0.5, { fontsize: 10 })
            .detail(3)
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de la Gombe ', 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Extrait_RCCM_personne_physique() {
    
}

function Extrait_RCCM_personne_morale() {
    
}

function Fiche_RCCM_personne_morale() {
    var entitiID = getUrlParameter("entityId");
    $.ajax({
        type: "POST",
        url: findEntitieUrl,
        data: { entitiId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);
            var orderData = $.parseJSON(stringData);
            var fff = JSON.stringify(orderData);
            var dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(orderData.nomCommercial),
                    "nomComerciale": IsEmptyString(orderData.nomCommercial),
                    "Enseigne": IsEmptyString(orderData.enseigne),
                    "sigle": IsEmptyString(orderData.sigle),
                    "formJuridique": IsEmptyString(orderData.formeJuridiqueN3.value),
                    "dateImmatriculationRCCM": IsEmptyString(orderData.dateImmatriculationRCCM),
                    "capitalSocial": IsEmptyString(orderData.capitalSocial),
                    "duree": IsEmptyString(orderData.duree),
                    "rccm": IsEmptyString(orderData.rccm),
                    "ancienNRC": IsEmptyString(orderData.numeroNRC),
                    "dateNRC": IsEmptyString(orderData.dateNRC),
                    "activitePrincipale": IsEmptyString(orderData.activitePrincipale.value),
                    "adresseSiegeSocial": IsEmptyString(orderData.adresseSiegeSocial.adresseRDC._name),
                    "debutExploitation": "   ",
                    "modeExploitation": "   ",
                    "nom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.nom),
                    "prenom": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.prenom),
                    "dateNaissance": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.dateNaissance),
                    "adresse": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.adresse.adresseRDC._name),
                    "fonction": IfExistProperty(orderData.dirigeant["0"], "fonction"),
                    "civilite": IsEmptyString(orderData.dirigeant["0"].personneDirigeant.civilite.value),
                    "origine": IsEmptyString(orderData.origine.value),
                    "precedentExploitant": IsEmptyString(orderData.precedentExploitant),
                    "loueurFond": IsEmptyString(orderData.loueurFond),
                    "nationalite": "   ",
                    "associe": new Array(),
                    "etablissementSecondaire": new Array(),
                    "personnePhysique": new Array(),
                    "commissaireCompte" : new Array()
        }]
            };

            // Ajout des des etablissements secondaire
            for (var ii = 0; ii < 1; ii++) {
                var unEtablissementJson = {
                    "nom": "   ",
                    "activite": "   "
                };
                if (ii < 4) {
                    dataSource.data["0"].etablissementSecondaire[11] = unEtablissementJson;
                }
            }
            
            // ajout des accocié
            if (orderData.associe.length > 0) {
                for (var ii = 0; ii < orderData.associe.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.associe[ii].personneAssocie.nom),
                        "prenom": IsEmptyString(orderData.associe[ii].personneAssocie.prenom),
                        "dateNaissance": IsEmptyString(orderData.associe[ii].personneAssocie.dateNaissance),
                        "adresse": IsEmptyString(orderData.associe[ii].personneAssocie.adresse.adresseRDC._name)
                    };
                    if (ii < 4) { dataSource.data["0"].associe[dataSource.data["0"].associe.length + 1] = unDirigenantJson; }

                }
            }

            // ajout des commissaire au comptes
            if (orderData.hasOwnProperty("commissaireAuxComptes") && orderData.commissaireAuxComptes.length > 0) {
                for (var ii = 0; ii < orderData.commissaireAuxComptes.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.commissaireAuxComptes[ii].nom),
                        "prenom": IsEmptyString(orderData.commissaireAuxComptes[ii].prenom),
                        "dateNaissance": IsEmptyString(orderData.commissaireAuxComptes[ii].dateNaissance),
                        "adresse": IsEmptyString(orderData.commissaireAuxComptes[ii].adresse.adresseRDC._name)
                    };
                    if (ii < 4) { dataSource.data["0"].commissaireCompte[dataSource.data["0"].commissaireCompte.length + 1] = unDirigenantJson; }

                }
            }

            // Ajout des dirigeants
            if (orderData.dirigeant.length > 0) {
                for (var ii = 0; ii < orderData.dirigeant.length; ii++) {
                    var unDirigenantJson = {
                        "nom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.nom),
                        "prenom": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.prenom),
                        "dateNaissance": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.dateNaissance),
                        "adresse": IsEmptyString(orderData.dirigeant[ii].personneDirigeant.adresse.adresseRDC._name),
                        "fonction": IfExistProperty(orderData.dirigeant[ii], "fonction")
                    };
                    if (ii < 4) {
                        dataSource.data["0"].personnePhysique[dataSource.data["0"].personnePhysique.length + 1] = unDirigenantJson;
                    }

                }
            }


            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();

            var report = jsreports.createReport('Extrait_RCCM_' + $(".label-warning").text())
                .data('orders') // The report will look for a data source with ID "orders"
                .page(9, 10, 'inches')
                .margins(0.5)
                .header()
                .text('Antenne de ' + IsEmptyString(orderData._ste_name).substring(6), 0.6, 0, 4, 0.2, { fontsize: 10, bold: true })
                .text('Registre du Commerce Et du Crédit Mobilier (RCCM)', 0, 0.2, 4, 0.2, { fontsize: 10, bold: true })
                .image(logoUrl, 6.5, 0.1, 1, 1)
                .text('Fiche du Registre du Commerce et du Crédit Mobilier', 0.4, 1.2, 7.8, 0.5, { fontsize: 21, bold: true })
                .text('RENSEIGNEMENTS RELATIFS A LA PERSONNE MORALE', 0, 2.2, 5, 0.1, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 2.5, 8.2, 0.02)
                .text('Dénomination', 0.1, 2.6, 2, 0.5, { fontsize: 10 })
                .text(':  [denomination]', 3, 2.6, 5, 0.5, { fontsize: 10 })
                .text('Nom commerciale', 0.1, 2.8, 2, 0.5, { fontsize: 10 })
                .text(':  [nomComerciale]', 3, 2.8, 3, 0.5, { fontsize: 10 })
                .text('Enseigne', 0.1, 3, 4, 0.5, { fontsize: 10 })
                .text(': [Enseigne]', 3, 3, 4, 0.5, { fontsize: 10 })
                .text('Sigle', 0.1, 3.2, 4.2, 0.5, { fontsize: 10 })
                .text(': [sigle]', 3, 3.2, 4.2, 0.5, { fontsize: 10 })
                .text('Adresse du Siege Social', 0.1, 3.4, 4, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 3.4, 5, 0.5, { fontsize: 10 })
                .text('Forme juridique', 0.1, 3.6, 4, 0.5, { fontsize: 10 })
                .text(':  [formJuridique]', 3, 3.6, 5, 0.5, { fontsize: 10 })
                .text('Capital social', 0.1, 3.8, 4, 0.5, { fontsize: 10 })
                .text(':  [capitalSocial]', 3, 3.8, 5, 0.5, { fontsize: 10 })
                .text('Numéro RCCM', 0.1, 4, 2, 0.5, { fontsize: 10 })
                .text(':  [ancienNRC]', 3, 4, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation RCCM', 0.1, 4.2, 7, 0.5, { fontsize: 10 })
                .text(':  [dateImmatriculationRCCM]', 3, 4.2, 4, 0.5, { fontsize: 10 })
                .text('Numéro NRC', 0.1, 4.4, 2, 0.5, { fontsize: 10 })
                .text(':  [rccm]', 3, 4.4, 3, 0.5, { fontsize: 10 })
                .text('Date d\'\immatriculation NRC', 0.1, 4.6, 7, 0.5, { fontsize: 10 })
                .text(':  [dateNRC]', 3, 4.6, 4, 0.5, { fontsize: 10 })
                .text('Activite Principale', 0.1, 4.8, 7, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 4.8, 4, 0.5, { fontsize: 10 })
                .text('Début d\'exploitation', 0.1, 5, 7, 0.5, { fontsize: 10 })
                .text(':  [debutExploitation]', 3, 5, 4, 0.5, { fontsize: 10 })
                .text('Mode d\'exploitation', 0.1, 5.2, 7, 0.5, { fontsize: 10 })
                .text(':  [modeExploitation]', 3, 5.2, 4, 0.5, { fontsize: 10 })
                .text('Durée', 0.1, 5.4, 7, 0.5, { fontsize: 10 })
                .text(':  [duree]', 3, 5.4, 4, 0.5, { fontsize: 10 })
                .text('RENSEIGNEMENTS RELATIFS A L\'ACTIVITE ET AUX ETABLISSEMENTS', 0, 5.6, 7, 0.2, { fontsize: 13, bold: true })
                        .box('black', '#00', 0, 5.9, 8.1, 0.02)
                .text('Activité principale', 0.1, 6.1, 2, 0.5, { fontsize: 10 })
                .text(':  [activitePrincipale]', 3, 6.2, 3, 0.5, { fontsize: 10 })
                .text('PRINCIPALE ETABLISSEMENT OU SUCCURSALE', 0, 6.4, 7, 0.2, { fontsize: 10, bold: true })
                .text('Adresse', 0.1, 6.6, 7, 0.5, { fontsize: 10 })
                .text(':  [adresseSiegeSocial]', 3, 6.6, 4, 0.5, { fontsize: 10 })
                .text('Origine', 0.1, 6.8, 4, 0.5, { fontsize: 10 })
                .text(':  [origine]', 3, 6.8, 5, 0.5, { fontsize: 10 })
                .text('Précédent exploitant', 0.1, 7, 7, 0.5, { fontsize: 10 })
                .text(':  [precedentExploitant]', 3, 7, 4, 0.5, { fontsize: 10 })
                .text('Loueur de fond', 0.1, 7.2, 7, 0.5, { fontsize: 10 })
                .text(':  [loueurFond]', 3, 7.2, 4, 0.5, { fontsize: 10 })
                .text('ETABLISSEMENTS SECONDAIRES', 0, 7.4, 7, 0.2, { fontsize: 10, bold: true })
                 .table(0, 7.6, 8, 2, {
                     data: 'etablissementSecondaire', hasFooter: true, fontSize: 7
                 }).column('60%', '[nom]', '', '', {
                     align: 'left', fontsize: 10
                 }).column('40%', '[activite]', '', '', {
                     align: 'left', fontsize: 10
                 })
                .text('ASSOCIES TENUS INDEFINIMENT ET PERSONNELLEMENT', 0, 8, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 8.2, 8.1, 0.02)
                .table(0, 8.3, 8, 2, {
                    data: 'associe', hasFooter: true, fontSize: 7
                }).column('20%', '[nom]', 'Nom', '', {
                    align: 'left', fontsize: 10
                }).column('20%', '[prenom]', 'Postnom', '', {
                    align: 'left', fontsize: 10
                }).column('20%', '[dateNaissance]', 'Date de naissance', '', {
                    align: 'left', fontsize: 10
                }).column('40%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 10
                })
                .text('RENSEIGNEMENTS RELATIFS AUX DIRIGANTS ', 0, 9, 8, 0.1, { fontsize: 13, bold: true })
                .box('black', '#00', 0, 9.2, 8.2, 0.02)
                .table(0, 9.4, 8, 2, {
                    data: 'personnePhysique', hasFooter: true, fontSize: 9
                }).column('15%', '[nom]', 'Nom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[prenom]', 'Postnom', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[dateNaissance]', 'Date de naissance', '', {
                    align: 'left', fontsize: 10
                }).column('15%', '[fonction]', 'Fonction', '', {
                    align: 'left', fontsize: 10
                }).column('40%', '[adresse]', 'Adresse', '', {
                    align: 'left', fontsize: 10
                })
                .text('COMMISSAIRES AUX COMPTES', 0, 11, 8.6, 0.2, { fontsize: 13, bold: true })
                    .box('black', '#00', 0, 11.2, 8.1, 0.02)
                .table(0, 11.3, 8, 2, {
                        data: 'commissaireCompte', hasFooter: true, fontSize: 9
                    }).column('15%', '[nom]', 'Nom', '', {
                        align: 'left', fontsize: 10
                    }).column('15%', '[prenom]', 'Postnom', '', {
                        align: 'left', fontsize: 10
                    }).column('15%', '[dateNaissance]', 'Date de naissance', '', {
                        align: 'left', fontsize: 10
                    }).column('15%', '[fonction]', 'Fonction', '', {
                        align: 'left', fontsize: 10
                    }).column('40%', '[adresse]', 'Adresse', '', {
                        align: 'left', fontsize: 10
                    })
               .text('LE SOUSSIGNE (préciser si mandataire) ___________ demande à ce que la précédente constitue'
               , 0.2, 13.2, 7, 0.5, { fontsize: 10, italic: true, bold: true, align: 'center' })
                .text('Greffier RCCM', 0.1, 14.2, 4, 0.5, { fontsize: 10 })
                .text('M______________________', 0.1, 14.6, 4, 0.5, { fontsize: 10 })
                .text('Le ' + output, 0.1, 15, 4, 0.5, { fontsize: 10 })
            .detail(3)
            .pageFooter()
                .text('Page [PAGE_NUMBER] de [PAGE_COUNT] | Greffe du Registre du Commerce et du Crédit Mobilier de '
                + IsEmptyString(orderData._ste_name).substring(6), 0, 0.5, 6, 0.25, { fontsize: 10, italic: true })
            .done();


            // Render the report
            jsreports.render({
                report_def: report,
                target: $('#textToPrint'),
                showToolbar: false,
                datasets: [dataSource]
            });

            $('#pdf_button').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

            $('#validateBtn').on('click', function () {
                jsreports.export({
                    report_def: report,
                    format: 'pdf',
                    datasets: [dataSource]
                });
            });

        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });
}

function Acte_notaire() {
    var entitiID = getUrlParameter("entityId");
    var flowId = getUrlParameter("flowId");
    $.ajax({
        type: "POST",
        url: findInfoNotaire,
        data: { entityId: entitiID },
        success: function (data) {
            var stringData = $.parseJSON(data);

            // A schema is used to assist the report designer and report engine to know
            // about the data types used in the data source.
            dataSource = {
                id: "orders", // Internal reference ID
                name: "Orders", // Data source name shown to report designer
                data: [
                {
                    "denomination": IsEmptyString(stringData.nomCommercial),
                    "nomCommercial": IsEmptyString(stringData.nomCommercial),
                    "sigle": IsEmptyString(stringData.sigle),
                    "enseigne": IsEmptyString(stringData.enseigne),
                    "adresseSiegeSocial": IsEmptyString(stringData.adresseSiegeSocial.adresseRDC._name),
                    "formeJuridique": IsEmptyString(stringData.formeJuridiqueN1.value),
                    "capitalSocial": IsEmptyString(stringData.capitalSocial),
                    "notaireName": "   ",
                    "activitePrincipale": IsEmptyString(stringData.activitePrincipale.value),
                    "comparantActeNotarie1": "   ",
                    "adresseComparant1": "   ",
                    "comparantActeNotarie2": "   ",
                    "adresseComparant2": "   ",
                    "temoinActeNotarie1": "   ",
                    "adresseTemoinActeNotarie1": "   ",
                    "temoinActeNotarie2": "   ",
                    "adresseTemoinActeNotarie2": "   ",
                    "juridiction": IsEmptyString(stringData.juridiction),
                    "numeroPaiementActeNotarie" : IsEmptyString(stringData.numeroPaiementActeNotarie),
                    "typeDocumentClef": "",
                    "fonctionNotaire": "   ",
                    "decretNotaire" : " aux prescrits des articles 9, 10, et 15 du Décret" +
                                ' N⁰ 14/014 portant création, Organisation et Foctionnement du Guichet Unique de Création d\'Entreprise,' +
                                'à l\'Arrêté Ministeriel N⁰037/CAB/MIN/J et DH/2013 du 01 mars 2013 portant Nomination des Notaires au' +
                                ' Guichet Unique de Création d\'Entreprise, ainsi qu\'à l\'Ordonnance n⁰66/344 du 09 juin 1966 relative' +
                                ' aux acts notariés '
              }
                ]
            };

            if (stringData.comparantActeNotarie.length > 0) {
                dataSource.data["0"].comparantActeNotarie1 = IsEmptyString(stringData.comparantActeNotarie["0"].prenom) + " " +
                                                                IsEmptyString(stringData.comparantActeNotarie["0"].nom);
                dataSource.data["0"].adresseComparant1 = IsEmptyString(stringData.comparantActeNotarie["0"].adresse.adresseRDC._name);
            }

            if (stringData.comparantActeNotarie.length > 1) {
                dataSource.data["0"].comparantActeNotarie2 = IsEmptyString(stringData.comparantActeNotarie["1"].prenom) + " " +
                                                                IsEmptyString(stringData.comparantActeNotarie["1"].nom);
                dataSource.data["0"].adresseComparant2 = IsEmptyString(stringData.comparantActeNotarie["1"].adresse.adresseRDC._name);
            }

            if (stringData.temoinActeNotarie.length > 0) {
                dataSource.data["0"].temoinActeNotarie1 = IsEmptyString(stringData.temoinActeNotarie["0"].personneTemoin);
                dataSource.data["0"].adresseTemoinActeNotarie1 = IsEmptyString(stringData.temoinActeNotarie["0"].villeTemoin);
            }

            if (stringData.temoinActeNotarie.length > 1) {
                dataSource.data["0"].temoinActeNotarie2 = IsEmptyString(stringData.temoinActeNotarie["1"].personneTemoin);
                dataSource.data["0"].adresseTemoinActeNotarie2 = IsEmptyString(stringData.temoinActeNotarie["1"].villeTemoin);
            }
            
            var d = new Date();
            //Generate current date
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + d.getFullYear();
                
            $.ajax({
                type: "POST",
                url: GenerateDateToWord,
                data: { dateToConvert: output },
                success: function (dateConverted) {


                    $.ajax({
                        type: "POST",
                        url: findMoreInfoNotaire,
                        data: { entityId: entitiID },
                        success: function(infoNotaire) {
                            var moreInfo = $.parseJSON(infoNotaire);
                            var moreInfoJson = $.parseJSON(moreInfo);
                            
                            //Amélioration du json
                            dataSource.data["0"].notaireName = IsEmptyString(moreInfoJson.notaire);
                            dataSource.data["0"].fonctionNotaire = IsEmptyString(moreInfoJson.fonctionNotaire);
                            dataSource.data["0"].decretNotaire = IsEmptyString(moreInfoJson.decretNominationNotaire);
                            dataSource.data["0"].juridiction = IsEmptyString(moreInfoJson.villeEditionActeNotarie);

                            $.ajax({
                                type: "POST",
                                url: FindFluxValues,
                                data: { entityId: entitiID, flowId: flowId },
                                success: function(infoFlux) {
                                    var infoFluxString = $.parseJSON(infoFlux);
                                    var formatText = infoFluxString.replace('}{', ',');
                                    var infoFluxJson = $.parseJSON(formatText);
                                    

                                    // Document a notarier
                                    if (infoFluxJson.documentANotarier) {
                                        for (var ii = 0; ii < infoFluxJson.documentANotarier.length; ii++) {
                                            dataSource.data["0"].typeDocumentClef += infoFluxJson.documentANotarier[ii].value + ", ";
                                        
                                        }
                                    }
                                    
                                    
                                    // autres document a notarier .autreDocumentANotarier
                                    if (infoFluxJson.autreDocumentANotarier) {
                                        for (var ii = 0; ii < infoFluxJson.autreDocumentANotarier.length; ii++) {
                                            dataSource.data["0"].typeDocumentClef += infoFluxJson.autreDocumentANotarier[ii] + ", ";
                                        
                                    }
                                    }
                                   

                                    var report = jsreports.createReport('Acte_notarié_' + $(".label-warning").text())
                           .data('orders') // The report will look for a data source with ID "orders"
                           .page(8.5, 12.3, 'inches')
                           .header()
                           .text('République Démocratique du Congo', 0.35, 0.2, 4, 0.2, { fontsize: 9, bold: true })
                           .text('Ministère de la Justice et Droits Humains', 0.2, 0.35, 4, 0.2, { fontsize: 9, bold: true })
                           .image(drapeauUrl, 0.8, 0.58, 0.8, 0.8)
                           .text('Guichet Unique de Création d\'Entreprise', 0.17, 1.4, 3, 0.1, { fontsize: 9, bold: true })
                           .text('OFFICE NOTARIAL', 0.65, 1.22, 2, 0.1, { fontsize: 9, bold: true, underline: true })
                           .image(logoUrl, 6.5, 0.1, 1, 1)
                           .text('ACTE NOTARIE No ' + $(".label-warning").text(), 0.38, 2, 7.8, 0.5, { fontsize: 15, bold: true, align: 'center', underline: true })
                           .text(dateConverted, 0.1, 2.5, 7, 0.2, { fontsize: 10 })
                           .text('Nous soussignons, [notaireName], [fonctionNotaire] à l\'Office Notarial du Guichet Unique de Création ' +
                                   'd\'Entreprise à [juridiction], assigne conformément [decretNotaire]; certifions que le document ci-après:' +
                                   ' [typeDocumentClef] de la société [nomCommercial] ayant son' +
                                   ' siège  situé sur [adresseSiegeSocial]' +
                                   ' , dont les clauses ci-dessous insérés nous ont été présenté ce jour, à [juridiction]' +
                                   ' par [comparantActeNotarie1]' +
                                   ' , dûment mandaté, ayant son adresse professionnelle situé sur [adresseComparant1] ; comparaissant en personne' +
                                   ' en présence de [temoinActeNotarie1]' +
                                   ', Agent de l\'Administration, résidant à [adresseTemoinActeNotarie1], et de [temoinActeNotarie2], Agent de l\'Administration,résident à [adresseTemoinActeNotarie2], témoins instrumentaire à ce requis' +
                                   ' réunissant les conditions exigées par la loi en la matière; lecture du contenu de l\'acte susmentionné a' +
                                   ' été fait par nous, tant au comptant qu\'aux témoins [temoinActeNotarie1]' +
                                   ', ci-dessus identifié et [temoinActeNotarie2], ci-dessous identifié.****',
                                   0.1, 3, 7.1, 4, { align: 'justify', fontsize: 12 })
                           .text('Le comparant pré-qualifié persiste et signe devant témoins et nous que, l\'économie du document à authentifier renferme ' +
                                   'bien l\'expression de la volonté des signataires, qu\'ils sont seul responsables de toutes contestations pouvant' +
                                   ' naître de l\'exécution dudit document sans évoquer la complicité de l\'Office Noarial ainsi que du Notaire.' +
                                   ' En foi de quoi, le présent acte vient d\'être signé par les comparants, témoins et nous, et revêtu' +
                                   ' du sceau de l\'Office Notarial du Guichet Unique de Création d\'Entreprise à [juridiction]', 0.1, 5.6, 7.1, 0.5, { align: 'justify', fontsize: 12, justify: true })
                           .text('SIGNATURE DU COMPARANT ', 0.2, 6.9, 3, 0.5, { fontsize: 13, italic: true, bold: true, underline: true })
                           .text('SIGNATURE DU NOTAIRE ', 5, 6.9, 3, 0.5, { fontsize: 13, italic: true, bold: true, underline: true })
                           .text('[comparantActeNotarie1]', 0.4, 7.3, 3, 0.5, { fontsize: 13, italic: true, bold: true })
                           .text('[notaireName]', 5.3, 7.3, 3, 0.5, { fontsize: 13, italic: true, bold: true })
                           .text('NOM ET SIGNATURE DES TEMOINS ', 0.2, 7.7, 7, 0.5, { fontsize: 13, italic: true, bold: true, align: 'center', underline: true })
                           .text('[temoinActeNotarie1]', 0.4, 8.2, 3, 0.5, { fontsize: 13, italic: true, bold: true })
                           .text('[temoinActeNotarie2]', 5.3, 8.2, 2, 0.5, { fontsize: 13, italic: true, bold: true })
                           .text('Droits perçu : Frais d\'acte de ' + stringData.fraisActeNotarie + ' dont ' + stringData.fraisAuthentification +
                           ' pour l\'authentification.****. Suivant la note de perception n ' + IsEmptyString(stringData.numeroPerceptionActeNotarie) + ' et ainsi' +
                                ' que l\'attestation de paiement n [numeroPaiementActeNotarie]' +
                                ' de ce jour. Enregistré par nous soussignés, ce ' + dateConverted + ' sous le ' +
                                $(".label-warning").text(), 0.1, 8.7, 7.1, 0.5, { fontsize: 10, align: 'justify' })
                           .text('SIGNATURE DU NOTAIRE ', 5, 9.6, 3, 0.5, { fontsize: 10, italic: true, bold: true, underline: true })
                           .text('[notaireName]', 5, 9.9, 3, 0.5, { fontsize: 13, italic: true, bold: true })
                                
                        .footer()
                        .pageFooter()
                       .done();


                                    // Render the report
                                    //jsreports.registerFont("arial", "normal",'normal', fontUrl);

                                    jsreports.render({
                                        report_def: report,
                                        target: $('#textToPrint'),
                                        showToolbar: false,
                                        datasets: [dataSource]
                                    });



                                    $('#pdf_button').on('click', function () {
                                        jsreports.export({
                                            report_def: report,
                                            format: 'pdf',
                                            datasets: [dataSource]
                                        });
                                    });

                                    $('#validateBtn').on('click', function () {
                                        jsreports.export({
                                            report_def: report,
                                            format: 'pdf',
                                            datasets: [dataSource]
                                        });
                                    });
                                },
                                error: function () {
                                    messageAlert("Erreur d'affichage ...");
                                }
                            });

                           
                           
                        },
                        error: function () {
                            messageAlert("Erreur d'affichage ...");
                        }
                    });


                },
                error: function () {
                    messageAlert("Erreur de conversion de date ...");
                }
            });


        },
        error: function () {
            messageAlert("Erreur d'affichage ...");
        }
    });

}

function Acte_notaire_modif() {
    Acte_notaire();
}

function testDocumentScanne(value) {
    if (value != null) {
        return " OUI ";
    } else {
        return " NON ";
    }
}

function IsEmptyString(value) {
    if (value != null) {
        if (value == true) {
            return "OUI";
        } else {
            return value;
        }
    } else {
        return "   ";
    }
}

function IfExistProperty(obj, hasProperty) {
    if (typeof obj == "object") {
        if (obj.hasOwnProperty(hasProperty)) {
            if (typeof obj == "object") {
                return IsEmptyString(obj.fonction.value);
            } else {
                return IsEmptyString(obj.fonction);
            }
        }
    } else if ($.isArray(obj)) {
        for (var k = 0; k < obj.length; k++) {
            if (obj[k].hasOwnProperty(hasProperty)) {
                return IsEmptyString(obj[k].name + hasProperty);
            }
        }
    } else {
        return " ";
    }
    
    if (obj.hasOwnProperty(hasProperty)) {
        return IsEmptyString(obj.fonction.value);
    } else {
        return " ";
    }
}

function PrintElem() {
    Popup("textToPrint");
}

function Popup(data) {
    /*
        var dataExport = jsreports.export({
            report_def: report,
            format: 'pdf',
            datasets: [dataSource]
        });
    */
    var restorepage = $('body').html();
    var printcontent = $('#' + data).clone();
    $('body').empty().html(printcontent);
    window.print();
    $('body').html(restorepage);
    setTimeout(function () {
        printEntreprise();
    }, 8000);

    /*
        var mywindow = window.open('', 'textToPrint div', 'height=400,width=600');
        var res = jQuery('<script/>', {
            src: "~/Scripts/jsreports-all.min.js"
        });
        var doc = $(mywindow.document);
        
        mywindow.document.write("<script src='~/Scripts/jsreports-all.min.js'/>");
        mywindow.document.write(data);
    
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
    
        mywindow.print();
        //mywindow.close();
    
        return true;
        */
}
