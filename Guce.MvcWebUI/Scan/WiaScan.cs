﻿/*=================================================================*
 * Classe: <WiaScan>
 * Version/date: <2.0.0> <2016.10.12>
 *
 * Description: <” Cette classe permet de piloter un scanner via WIA ”>
 * Specificities: <“ ... ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Linq;
using Guce.Entities.Custom;
using WIA;
using static Guce.Entities.Custom.Configuration;
using static Guce.Entities.Custom.ParameterId;
using static Guce.Entities.Custom.ScanMode;
using static Guce.MvcWebUI.Scan.WiaScan.WiaDpsDocumentHandlingSelect;
using static Guce.MvcWebUI.Scan.WiaScan.WiaDpsDocumentHandlingStatus;
using static WIA.WiaDeviceType;

namespace Guce.MvcWebUI.Scan
{
    public class WiaScan
    {
        private const string WiaFormatBmp = "{B96B3CAB-0728-11D3-9D7B-0000F81EF32E}";

        public class WiaDpsDocumentHandlingSelect
        {
            public const uint Feeder = 0x00000001;
            public const uint Flatbed = 0x00000002;
        }

        public class WiaDpsDocumentHandlingStatus
        {
            public const uint FeedReady = 0x00000001;
        }

        public class WiaProperties
        {
            private const uint WiaReservedForNewProps = 1024;
            private const uint WiaDipFirst = 2;
            private const uint WiaDpaFirst = WiaDipFirst + WiaReservedForNewProps;
            private const uint WiaDpcFirst = WiaDpaFirst + WiaReservedForNewProps;
            //
            // Scanner only device properties (DPS)
            //
            private const uint WiaDpsFirst = WiaDpcFirst + WiaReservedForNewProps;
            public const uint WiaDpsDocumentHandlingStatus = WiaDpsFirst + 13;
            public const uint WiaDpsDocumentHandlingSelect = WiaDpsFirst + 14;
        }

        public static List<Image> Scan()
        {
            ICommonDialog dialog = new CommonDialog();
            var device = dialog.ShowSelectDevice(ScannerDeviceType, true);
            if (device != null)
            {
                return Scan(device.DeviceID);
            }
            throw new Exception("Vous devez sélectionner un périphérique pour la numérisation.");
        }
        
        public static List<Image> Scan(string scannerId)
        {
            var images = new List<Image>();
            var hasMorePages = true;
            while (hasMorePages)
            {
                // select the correct scanner using the provided scannerId parameter
                var manager = new DeviceManager();
                var device = (from DeviceInfo info in manager.DeviceInfos where info.DeviceID == scannerId select info.Connect()).FirstOrDefault();
                // device was not found
                if (device == null)
                {
                    // enumerate available devices
                    var availableDevices = manager.DeviceInfos.Cast<DeviceInfo>().Aggregate("", (current, info) => current + info.DeviceID + "</br>");
                    // show error with available devices
                    throw new Exception("Le périphérique avec l'ID fourni n'a pas été trouvé. Périphériques disponibles :</br></br>"
                        + availableDevices);
                }
                var item = device.Items[1];
                //int scanMode;
                //if (Parameters[ParameterId.ScanMode].ToString().Equals(GrayScale))
                //{
                //    scanMode = 2;
                //}
                //else if (Parameters[ParameterId.ScanMode].ToString().Equals(BlackAndWhite))
                //{
                //    scanMode = 4;
                //}
                //else
                //{
                //    scanMode = 1;
                //}
                item.Properties.get_Item("6147").set_Value(Parameters[ScanDpi]);
                    // horizontal DPI
                item.Properties.get_Item("6148").set_Value(Parameters[ScanDpi]);
                    // vertical DPI
                item.Properties.get_Item("6146").set_Value(1); // mode de couleur de numerisation
                try
                {
                    // scan image
                    ICommonDialog wiaCommonDialog = new CommonDialog();
                    var image = (ImageFile)wiaCommonDialog.ShowTransfer(item, WiaFormatBmp); // scan image

                    // save to temp file
                    var fileName = Path.GetTempFileName();
                    File.Delete(fileName);
                    image.SaveFile(fileName);

                    // add file to output list
                    images.Add(Image.FromFile(fileName));

                    //determine if there are any more pages waiting
                    Property documentHandlingSelect = null;
                    Property documentHandlingStatus = null;

                    foreach (Property prop in device.Properties)
                    {
                        if (prop.PropertyID == WiaProperties.WiaDpsDocumentHandlingSelect)
                            documentHandlingSelect = prop;

                        if (prop.PropertyID == WiaProperties.WiaDpsDocumentHandlingStatus)
                            documentHandlingStatus = prop;
                    }
                    // assume there are no more pages
                    hasMorePages = false;
                    // may not exist on flatbed scanner but required for feeder
                    if (documentHandlingSelect != null)
                    {
                        // check for document feeder
                        if ((Convert.ToUInt32(documentHandlingSelect.get_Value()) & Feeder) != 0)
                        {
                            if (documentHandlingStatus != null)
                            {
                                hasMorePages = (Convert.ToUInt32(documentHandlingStatus.get_Value()) & FeedReady) != 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("0x80210003"))
                    {//Exception from HRESULT: 0x80210003
                        break;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            return images;
        }
        
        public static List<string> GetDevices()
        { // Gets the list of available WIA devices.
            var manager = new DeviceManager();
            var devices = new List<string>();
            //var devices = (from DeviceInfo info in manager.DeviceInfos
            //              select info.Connect()).ToList();
            //foreach (var item in devices.Select(device => device.Items[1]))
            //{
            //    var name = item.Properties.get_Item("2");
            //    var namufacturer = item.Properties.get_Item("3");
            //    var feederCapabilities = item.Properties.get_Item("3086");
            //    var colorMode = item.Properties.get_Item("6146");
            //}
            for (var i = 1; i <= manager.DeviceInfos.Count; i++)
            {
                var name = manager.DeviceInfos[i].Properties["Name"].get_Value().ToString();
                //var deviceId = manager.DeviceInfos[i].Properties["2"].get_Value().ToString();
                //var manufacturer = manager.DeviceInfos[i].Properties["3"].get_Value().ToString();
                //var connectStatus = manager.DeviceInfos[i].Properties["1027"].get_Value().ToString();
                devices.Add(name);
            }
            return null;
            //return (from DeviceInfo info in manager.DeviceInfos select info.DeviceID).ToList();
        }

    }
}
