﻿/*=================================================================*
 * Classe: <NinjectControllerFactory>
 * Version/date: <2.0.0> <2016.07.08>
 *
 * Description: <” Cette classe permet de resoudre le probleme d'injection entre couche. :: Ninject helps 
 * you use the technique of dependency injection to break your applications into loosely-coupled, 
 * highly-cohesive components, and then glue them back together in a flexible manner :: ”>
 * Specificities: <“ Cette classe est un résolveurs de dépendance (dependency resolvers) ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Web.Mvc;
using System.Web.Routing;
using Guce.Business.Abstract;
using Guce.Business.Concrete.Manager;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete;
using Ninject;

namespace Guce.MvcWebUI.DependencyResolvers
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel _kernel;

        public NinjectControllerFactory()
        {
            _kernel = new StandardKernel();

            _kernel.Bind<IBusinessRuleService>().To<BusinessRuleManager>().InSingletonScope();
            _kernel.Bind<IBusinessRuleDal>().To<EfBusinessRuleDal>().InSingletonScope();

            _kernel.Bind<IDicCodeItemService>().To<DicCodeItemManager>().InSingletonScope();
            _kernel.Bind<IDicCodeItemDal>().To<EfDicCodeItemDal>().InSingletonScope();

            _kernel.Bind<IDicCodeService>().To<DicCodeManager>().InSingletonScope();
            _kernel.Bind<IDicCodeDal>().To<EfDicCodeDal>().InSingletonScope();

            _kernel.Bind<IDicFieldService>().To<DicFieldManager>().InSingletonScope();
            _kernel.Bind<IDicFieldDal>().To<EfDicFieldDal>().InSingletonScope();

            _kernel.Bind<IDicVersionCodeService>().To<DicVersionCodeManager>().InSingletonScope();
            _kernel.Bind<IDicVersionCodeDal>().To<EfDicVersionCodeDal>().InSingletonScope();

            _kernel.Bind<IDicVersionFieldService>().To<DicVersionFieldManager>().InSingletonScope();
            _kernel.Bind<IDicVersionFieldDal>().To<EfDicVersionFieldDal>().InSingletonScope();

            _kernel.Bind<IDicVersionService>().To<DicVersionManager>().InSingletonScope();
            _kernel.Bind<IDicVersionDal>().To<EfDicVersionDal>().InSingletonScope();

            _kernel.Bind<IDictionaryService>().To<DictionaryManager>().InSingletonScope();
            _kernel.Bind<IDictionaryDal>().To<EfDictionaryDal>().InSingletonScope();

            _kernel.Bind<IEntityService>().To<EntityManager>().InSingletonScope();
            _kernel.Bind<IEntityDal>().To<EfEntityDal>().InSingletonScope();

            _kernel.Bind<IFStatusService>().To<FStatusManager>().InSingletonScope();
            _kernel.Bind<IFStatusDal>().To<EfFStatusDal>().InSingletonScope();

            _kernel.Bind<IFStepService>().To<FStepManager>().InSingletonScope();
            _kernel.Bind<IFStepDal>().To<EfFStepDal>().InSingletonScope();

            _kernel.Bind<IFVariableValueService>().To<FVariableValueManager>().InSingletonScope();
            _kernel.Bind<IFVariableValueDal>().To<EfFVariableValueDal>().InSingletonScope();

            _kernel.Bind<IFlowService>().To<FlowManager>().InSingletonScope();
            _kernel.Bind<IFlowDal>().To<EfFlowDal>().InSingletonScope();

            _kernel.Bind<IIndexService>().To<IndexManager>().InSingletonScope();
            _kernel.Bind<IIndexDal>().To<EfIndexDal>().InSingletonScope();

            _kernel.Bind<IGroupService>().To<GroupManager>().InSingletonScope();
            _kernel.Bind<IGroupDal>().To<EfGroupDal>().InSingletonScope();

            _kernel.Bind<IParameterService>().To<ParameterManager>().InSingletonScope();
            _kernel.Bind<IParameterDal>().To<EfParameterDal>().InSingletonScope();

            _kernel.Bind<IReportService>().To<ReportManager>().InSingletonScope();
            _kernel.Bind<IReportDal>().To<EfReportDal>().InSingletonScope();

            _kernel.Bind<IUserGroupService>().To<UserGroupManager>().InSingletonScope();
            _kernel.Bind<IUserGroupDal>().To<EfUserGroupDal>().InSingletonScope();

            _kernel.Bind<IUserService>().To<UserManager>().InSingletonScope();
            _kernel.Bind<IUserDal>().To<EfUserDal>().InSingletonScope();

            _kernel.Bind<IValueService>().To<ValueManager>().InSingletonScope();
            _kernel.Bind<IValueDal>().To<EfValueDal>().InSingletonScope();

            _kernel.Bind<IWfActionFieldService>().To<WfActionFieldManager>().InSingletonScope();
            _kernel.Bind<IWfActionFieldDal>().To<EfWfActionFieldDal>().InSingletonScope();

            _kernel.Bind<IWfActionService>().To<WfActionManager>().InSingletonScope();
            _kernel.Bind<IWfActionDal>().To<EfWfActionDal>().InSingletonScope();

            _kernel.Bind<IWfTransitionService>().To<WfTransitionManager>().InSingletonScope();
            _kernel.Bind<IWfTransitionDal>().To<EfWfTransitionDal>().InSingletonScope();

            _kernel.Bind<IWfVariableService>().To<WfVariableManager>().InSingletonScope();
            _kernel.Bind<IWfVariableDal>().To<EfWfVariableDal>().InSingletonScope();

            _kernel.Bind<IWfVersionService>().To<WfVersionManager>().InSingletonScope();
            _kernel.Bind<IWfVersionDal>().To<EfWfVersionDal>().InSingletonScope();

            _kernel.Bind<IWorkflowService>().To<WorkflowManager>().InSingletonScope();
            _kernel.Bind<IWorkflowDal>().To<EfWorkflowDal>().InSingletonScope();

            _kernel.Bind<IVwActiveWorkflowService>().To<VwActiveWorkflowManager>().InSingletonScope();
            _kernel.Bind<IVwActiveWorkflowDal>().To<EfVwActiveWorkflowDal>().InSingletonScope();

            _kernel.Bind<IVwWorkflowService>().To<VwWorkflowManager>().InSingletonScope();
            _kernel.Bind<IVwWorkflowDal>().To<EfVwWorkflowDal>().InSingletonScope();

            _kernel.Bind<IVwUserNextActionService>().To<VwUserNextActionManager>().InSingletonScope();
            _kernel.Bind<IVwUserNextActionDal>().To<EfVwUserNextActionDal>().InSingletonScope();

            _kernel.Bind<IVwActiveNextActionService>().To<VwActiveNextActionManager>().InSingletonScope();
            _kernel.Bind<IVwActiveNextActionDal>().To<EfVwActiveNextActionDal>().InSingletonScope();

            _kernel.Bind<IVwCurrentValueService>().To<VwCurrentValueManager>().InSingletonScope();
            _kernel.Bind<IVwCurrentValueDal>().To<EfVwCurrentValueDal>().InSingletonScope();

            _kernel.Bind<IVwCurrentVariableValueService>().To<VwCurrentVariableValueManager>().InSingletonScope();
            _kernel.Bind<IVwCurrentVariableValueDal>().To<EfVwCurrentVariableValueDal>().InSingletonScope();

            _kernel.Bind<IVwFormFieldService>().To<VwFormFieldManager>().InSingletonScope();
            _kernel.Bind<IVwFormFieldDal>().To<EfVwFormFieldDal>().InSingletonScope();

            _kernel.Bind<IVwAllNextActionService>().To<VwAllNextActionManager>().InSingletonScope();
            _kernel.Bind<IVwAllNextActionDal>().To<EfVwAllNextActionDal>().InSingletonScope();

            _kernel.Bind<IVwAllValueService>().To<VwAllValueManager>().InSingletonScope();
            _kernel.Bind<IVwAllValueDal>().To<EfVwAllValueDal>().InSingletonScope();

            _kernel.Bind<IVwAllVariableValueService>().To<VwAllVariableValueManager>().InSingletonScope();
            _kernel.Bind<IVwAllVariableValueDal>().To<EfVwAllVariableValueDal>().InSingletonScope();

            _kernel.Bind<IVwDependantDicCodeService>().To<VwDependantDicCodeManager>().InSingletonScope();
            _kernel.Bind<IVwDependantDicCodeDal>().To<EfVwDependantDicCodeDal>().InSingletonScope();

            _kernel.Bind<IVwDependantDicCodeItemService>().To<VwDependantDicCodeItemManager>().InSingletonScope();
            _kernel.Bind<IVwDependantDicCodeItemDal>().To<EfVwDependantDicCodeItemDal>().InSingletonScope();

            _kernel.Bind<IVwLastValidatedValuesService>().To<VwLastValidatedValuesManager>().InSingletonScope();
            _kernel.Bind<IVwLastValidatedValuesDal>().To<EfVwLastValidatedValuesDal>().InSingletonScope();

            _kernel.Bind<ISiteService>().To<SiteManager>().InSingletonScope();
            _kernel.Bind<ISiteDal>().To<EfSiteDal>().InSingletonScope();

            _kernel.Bind<IStatPageService>().To<StatPageManager>().InSingletonScope();
            _kernel.Bind<IStatPageDal>().To<EfStatPageDal>().InSingletonScope();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)_kernel.Get(controllerType);
        }
    }
}