﻿/*=================================================================*
 * Interface: <IEntity>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Toutes les entités de la base de données seront implementées par IEntity.
 * Cette classe permet d'identifier les entites qui sont directement liées à une table de la base de données.”>
 * Specificities: <“ - ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

namespace Guce.Entities.Abstract
{
    public interface IEntity
    {
         
    }
}