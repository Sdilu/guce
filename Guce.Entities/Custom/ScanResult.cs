﻿using System.IO;

namespace Guce.Entities.Custom
{
    public class ScanResult
    {
        public Stream Stream { get; set; }
        public string Message { get; set; }
    }
}