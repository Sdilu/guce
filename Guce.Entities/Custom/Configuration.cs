﻿using System.Collections;

namespace Guce.Entities.Custom
{
    public static class Configuration
    {
        public static Hashtable Parameters = new Hashtable();
        public const string Server = "localhost";
        public const string Database = "guce";
        public const int PageSize = 5;
    }
}