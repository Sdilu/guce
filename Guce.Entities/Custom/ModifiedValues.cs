﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guce.Entities.Custom
{
    public class ModifiedValues
    {
        public string oldValue { get; set; }
        public string currentValue { get; set; }
        public DateTime modifiedTime { get; set; }
        public string libele { get; set; }
        public string Name { get; set; }
    }
}
