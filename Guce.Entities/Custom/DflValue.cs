﻿/*=================================================================*
 * Class: <MyClass>
 * Version/date: <x.y.z> <yyyy.mm.dd>
 *
 * Description: <”Cette classe permet de …”>
 * Specificities: <e.g. “Cette classe est un singleton”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
namespace Guce.Entities.Custom
{
    public class DflValue
    {
        public int DflId { get; set; }
        public string Value { get; set; }
    }
}
