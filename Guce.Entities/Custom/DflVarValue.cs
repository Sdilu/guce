﻿namespace Guce.Entities.Custom
{
    public class DflVarValue
    {
        public int Id { get; set; }
        public int Key { get; set; }
        public string Value { get; set; }
        public bool Dfl { get; set; } //Determine si le champ est un dic_fields ou pas 
    }
}