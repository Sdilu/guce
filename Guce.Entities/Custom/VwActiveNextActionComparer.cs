﻿using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Entities.Custom
{
    public class VwActiveNextActionComparer : IEqualityComparer<VwActiveNextAction>
    {
        public bool Equals(VwActiveNextAction x, VwActiveNextAction y)
        {
            return x.EntId == y.EntId && x.WacId == y.WacId && x.FlwId == y.FlwId;
        }

        public int GetHashCode(VwActiveNextAction obj)
        {
            return obj.WacId.GetHashCode();
        }
    }
}