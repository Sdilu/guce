﻿/*=================================================================*
 * Classe: <HomeController>
 * Version/date: <2.0.0> <2016.07.08>
 *
 * Description: <” Cette classe contient les types de données personnalisé utilisées dans la base de données. ”>
 * Specificities: <“ Cette classe est un enumerateur. ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Globalization;

namespace Guce.Entities.Custom
{
    public static class OriginType
    {
        public const string Ohada = "OHADA";
        public const string Guce = "GUCE";
    }
    public static class FieldType
    {
        public const string String = "string";
        public const string Date = "date";
        public const string Amount = "amount";
        public const string Integer = "integer";
        public const string Code = "code";
        public const string Boolean = "boolean";
        public const string Reference = "reference";
        public const string Pdf = "pdf";
        public const string Doc = "doc";
    }
    public static class EmptyType
    {
        public const string Ok = "ok";
        public const string Warning = "warning";
        public const string Error = "error";
    }
    public static class ActionType
    {
        public const string Null = "Null";
        public const string Form = "Form";
        public const string Validate = "Validate";
        public const string Print = "Print";
        public const string CreateEntity = "CreateEntity";
        public const string FindEntity = "FindEntity";
        public const string FindAndLockEntity = "FindAndLockEntity";
        public const string FindOrCreateEntity = "FindOrCreateEntity";
    }
    public static class BranchType
    {
        public const string Xor = "XOR";
        public const string And = "AND";
    }
    public static class ScanMode
    {
        public const string Color = "color";
        public const string GrayScale = "grayscale";
        public const string BlackAndWhite = "blackandwhite";
    }
    public static class ParameterId
    {
        public const string SiteId = "SITE_ID";
        public const string App = "APP";
        public const string TmpDir = "TMP_DIR";
        public const string DocDir = "DOC_DIR";
        public const string SessionTimeOut = "SESSION_TIMEOUT";
        public const string RccmPrefix = "RCCM_PREFIX";
        public const string ScannerId = "SCANNER_ID";
        public const string ScanDpi = "SCAN_DPI";
        public const string ScanMode = "SCAN_MODE";
        public const string ScanAddFile = "SCAN_ADD_FILE";
        public const string StartFileNumber = "START_FLOW_NUMBER";
        public const string StartRccmNumber = "START_RCCM_NUMBER";
        public const string NextPhyRccm = "NEXT_PHYS_RCCM_SEQ";
        public const string NextMorRccm = "NEXT_MOR_RCCM_SEQ";
        public const string TypeAmountCurrency = "TYPE_AMOUNT_CURRENCIES";
        public const string TypeAmountMax = "TYPE_AMOUNT_MAX";
        public const string TypeIntegerMax = "TYPE_INTEGER_MAX";

    }

    public static class UserParameterId
    {
        public const string ScanningWay = "SCANNING_WAY";
        public const string ShowPreview = "SHOW_PREVIEW";
    }

    public static class ConvertDate
{
        public static string DatabaseFormat(string value)
        {   //05/02/2016 to 2016-02-05
            if (string.IsNullOrWhiteSpace(value)) return null;
            var dt2 = DateTime.Parse(value, CultureInfo.GetCultureInfo("fr-FR"));
            return dt2.ToString("yyyy-MM-dd");
        }
        public static string ComponentFormat(string value)
        {   //2016-02-05 to 02/05/2016 
            if (string.IsNullOrWhiteSpace(value)) return null;
            var dt2 = DateTime.Parse(value);
            return dt2.ToString("dd/MM/yyyy");
        }
        public static string UserFormat(string value)
        {   //2016-02-05 to 05 février 2016 
            if (string.IsNullOrWhiteSpace(value)) return null;
            var dt2 = DateTime.Parse(value);
            var dfi = DateTimeFormatInfo.GetInstance(CultureInfo.GetCultureInfo("fr-FR"));
            return dt2.Day.ToString("00") + " " + dfi?.MonthNames[dt2.Month - 1] + " " + dt2.Year;
        }


        public static string DateToText(DateTime dt, bool includeTime, bool isUK)
        {
            string[] ordinals =
            {
                "premier",
                "seconde",
                "troisième",
                "quatrième",
                "cinquième",
                "sixième",
                "septième",
                "huitième",
                "neuvième",
                "dixième",
                "onzième",
                "douzième",
                "treizième",
                "quatorzième",
                "quinzième",
                "seizième",
                "dix-septième",
                "dix-huitième",
                "dix-neuvième",
                "vingtième",
                "vingt et un",
                "vingt secondes",
                "vingt-troisième",
                "vingt-quatrième",
                "vingt-cinquième",
                "vingt six",
                "vingt-sept",
                "vingt-huit",
                "vingt-neuf",
                "trentième",
                "trente et un"
            };
            int day = dt.Day;
            int month = dt.Month;
            int year = dt.Year;
            DateTime dtm = new DateTime(1, month, 1);
            string date;
            if (isUK)
            {
                date = "Le " + ordinals[day - 1] + " jour du mois de  " + dtm.ToString("MMMM") + " de l'année " + NumberToText(year, true);
            }
            else
            {
                date = " L'an " + NumberToText(year, false) + ", le " + ordinals[day - 1] + " jour du mois de "+ dtm.ToString("MMMM");
            }
            if (includeTime)
            {
                int hour = dt.Hour;
                int minute = dt.Minute;
                string ap = " (avant midi)";
                if (hour >= 12)
                {
                    ap = " (apres midi)";
                    hour = hour - 12;
                }
                if (hour == 0) hour = 12;
                string time = NumberToText(hour, false);
                if (minute > 0) time += " heur " + NumberToText(minute, false);
                time += " " + ap;
                date += ", " + time;
            }
            return date;
        }

        public static string MoisEnFrancais(string moisEnAnglais)
        {
            string resultMois = null;
            switch (moisEnAnglais)
            {
                case "january":
                    resultMois = "Janvier";
                    break;
                case "february":
                    resultMois = "Février";
                    break;
                case "march":
                    resultMois = "Mars";
                    break;
                case "april":
                    resultMois = "Avril";
                    break;
                case "may":
                    resultMois = "Mai";
                    break;
                case "june":
                    resultMois = "Juin";
                    break;
                case "july":
                    resultMois = "Juillet";
                    break;
                case "august":
                    resultMois = "Aout";
                    break;
                case "september":
                    resultMois = "Septembre";
                    break;
                case "october":
                    resultMois = "Octobre";
                    break;
                case "november":
                    resultMois = "Novembre";
                    break;
                case "december":
                    resultMois = "Décembre";
                    break;
                default:
                    resultMois = " ";
                    break;
            }
            return resultMois;
        }

        public static string NumberToText(int number, bool isUK)
        {
            if (number == 0) return "Zero";
            string and = isUK ? "et " : ""; // deals with UK or US numbering
            if (number == -2147483648)
                return "moins de deux milliards cent " + and +
                       "quarante sept millions quatre cent " + and + "quatre - vingt trois mille " +
                       "six cent " + and + "Quarante huit";
            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (number < 0)
            {
                sb.Append("moins ");
                number = -number;
            }
            string[] words0 = { "", "un ", "deux ", "trois ", "quatre ", "cinq ", "six ", "sept ", "huit ", "neuf " };
            string[] words1 = { "dix ", "onze ", "douze ", "Treize", "quatorze ", "quinze ", "seize ", "dix-sept ", "dix-huit ", "dix-neuf " };
            string[] words2 = { "vingt ", "trente ", "quarante ", "cinquante ", "soixante ", "soixante-dix ", "quatre-vingt ", "quatre-vingt dix " };
            string[] words3 = { "mille ", "million ", "milliards " };
            num[0] = number % 1000;           // units
            num[1] = number / 1000;
            num[2] = number / 1000000;
            num[1] = num[1] - 1000 * num[2];  // thousands
            num[3] = number / 1000000000;     // billions
            num[2] = num[2] - 1000 * num[3];  // millions
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10;              // ones
                t = num[i] / 10;
                h = num[i] / 100;             // hundreds
                t = t - 10 * h;               // tens
                if (h > 0) sb.Append(words0[h] + "Cent");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i < first) sb.Append(and);
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }
}
}