﻿using System;

namespace Guce.Entities.Custom
{
    public class EntityMainSearch
    {
        public int id { get; set; }
        public int dic_id { get; set; }
        public string name { get; set; }
        public DateTime creation_time { get; set; }
        public string json { get; set; }
        public string created_by { get; set; }
        public string unvalidated_canonicalized_json { get; set; }
    }
}