﻿using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Entities.Custom
{
    public class VwUserNextActionComparer : IEqualityComparer<VwUserNextAction>
    {
        public bool Equals(VwUserNextAction x, VwUserNextAction y)
        {
            return x.EntId == y.EntId && x.WacId == y.WacId && x.FlwId == y.FlwId;
        }

        public int GetHashCode(VwUserNextAction obj)
        {
            return obj.WacId.GetHashCode();
        }
    }
}