﻿namespace Guce.Entities.Custom
{
    public class FieldProperty
    {
        public string Name { get; set; }
        public string Field { get; set; }
        public string Type { get; set; }
        public bool Multivalued { get; set; }
    }
}