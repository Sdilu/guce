﻿namespace Guce.Entities.Custom
{
    public class ValDflValue
    {
        public int ValId { get; set; }
        public int DflId { get; set; }
        public string Value { get; set; }
    }
}