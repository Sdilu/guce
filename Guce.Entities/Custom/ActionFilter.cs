﻿namespace Guce.Entities.Custom
{
    public class ActionFilter
    {
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}