﻿using System;

namespace Guce.Entities.Custom
{
    public class DbEntity
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}