﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guce.Entities.Custom
{
    public class FolderMainSearch
    {
        public int id { get; set; }
        public int dic_id { get; set; }
        public long current_txId { get; set; }
        public string name { get; set; }
        public DateTime creation_time { get; set; }
        public string created_by { get; set; }
        public string json { get; set; }
        public string unvalidated_canonicalized_json { get; set; }
    }
}
