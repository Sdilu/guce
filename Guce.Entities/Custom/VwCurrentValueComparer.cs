﻿using System.Collections.Generic;
using Guce.Entities.ComplexType;

namespace Guce.Entities.Custom
{
    public class VwCurrentValueComparer : IEqualityComparer<VwCurrentValue>
    {
        public bool Equals(VwCurrentValue x, VwCurrentValue y)
        {
            return x.EntId == y.EntId && x.DflId == y.DflId && x.DicId == y.DicId;
        }

        public int GetHashCode(VwCurrentValue obj)
        {
            return obj.ValId.GetHashCode();
        }
    }
}