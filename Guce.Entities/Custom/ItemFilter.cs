﻿namespace Guce.Entities.Custom
{
    public class ItemFilter
    {
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}