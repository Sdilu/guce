﻿/*=================================================================*
 * Classe: <User>
 * Version/date: <2.0.0> <2016.07.09>
 *
 * Description: <” Cette classe represente un utilisateur avec un nom d'utilisateur et un mot de passe. ”>
 * Specificities: <“ - ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
namespace Guce.Entities.Custom
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Authentifie { get; set; }
        public string UserGroup { get; set; }
    }
}