﻿using System;

namespace Guce.Entities.Custom
{
    public class SearchEntity
    {
        public int id { get; set; }
        public int? lock_fst_id { get; set; }
        public string json { get; set; }
    }
}