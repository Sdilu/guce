﻿/*=================================================================*
 * Classe: <VwDependantDicCodeItem>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le type complexe qui représente la vue vw_dependant_dic_code_items 
 * dans la base de données. ">
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwDependantDicCodeItem : IEntity
    {
        public int DciId { get; set; }
        public int DcdId { get; set; }
        public string Value { get; set; }
        public string DcdName { get; set; }
        public int DependantDcdId { get; set; }
        public int DependantDciId { get; set; }
        public string DependantValue { get; set; }
        public string DependantDcdName { get; set; }
        public string DependantCode { get; set; }
        public string DependantDescription { get; set; }
    }
}