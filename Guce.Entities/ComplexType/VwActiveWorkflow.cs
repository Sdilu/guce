﻿/*=================================================================*
 * Classe: <VwActiveWorkflow>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est le type complexe qui représente la vue vw_active_workflows 
 * dans la base de données. ">
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwActiveWorkflow: IEntity
    {
        public int WflId { get; set; }
        public int WfvId { get; set; }
        public string Name { get; set; }
        public int Version { get; set; } 
    }
}