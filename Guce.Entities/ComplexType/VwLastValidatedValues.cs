﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwLastValidatedValues : IEntity
    {
        public int? val_id { get; set; }
        public int? ent_id { get; set; }
        public int? fst_id { get; set; }
        public int? flw_id { get; set; }
        public int? dfl_id { get; set; }
        public int? dic_id { get; set; }
        public int? txid { get; set; }
        public int? ent_current_txid { get; set; }
        public string field_name { get; set; }
        public string field_display_name { get; set; }
        public string type { get; set; }
        public bool? multivalued { get; set; }
        public string reference_dic_id { get; set; }
        public string value { get; set; }
        public DateTime? creation_time { get; set; }
        public DateTime? validation_time { get; set; }
        public string deferenced_value { get; set; }
        public int? code_id { get; set; }
        public double? value_num { get; set; }
        public int? order { get; set; }
        public int? max_fst_id { get; set; }
    }
}
