﻿/*=================================================================*
 * Classe: <VwAllVariableValue>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est le type complexe qui représente la vue vw_all_variable_values 
 * dans la base de données. ">
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwAllVariableValue : IEntity
    {
        public int FvvId { get; set; }
        public int WvaId { get; set; }
        public int FstId { get; set; }
        public int FlwId { get; set; }
        public string WvaName { get; set; }
        public string WvaDisplayName { get; set; }
        public string WvaType { get; set; }
        public string Value { get; set; }
        public DateTime CreationTime { get; set; }
        public int Order { get; set; }
        public string CreatedBy { get; set; }
        public long Pos { get; set; }
        public int? DciId { get; set; }
    }
}