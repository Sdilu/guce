﻿/*=================================================================*
 * Classe: <VwAllValue>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est le type complexe qui représente la vue vw_all_values 
 * dans la base de données. ">
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwAllValue: IEntity
    {
        public int ValId { get; set; }
        public int EntId { get; set; }
        public int FstId { get; set; }
        public int FlwId { get; set; }
        public int DflId { get; set; }
        public int? DicId { get; set; }
        public long TxId { get; set; }
        public long EntCurrentTxId { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayName { get; set; }
        public string Type { get; set; }
        public bool MultiValued { get; set; }
        public string Value { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? ValidationTime { get; set; }
        public int Order { get; set; }
        public int? CodeId { get; set; }
        public double? ValueNum { get; set; }
    }
}