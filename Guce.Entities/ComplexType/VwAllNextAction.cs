﻿/*=================================================================*
 * Classe: <VwActiveNextAction>
 * Version/date: <2.0.0> <2016.07.12>
 *
 * Description: <” Cette classe est le type complexe qui représente la vue vw_active_next_actions
 * dans la base de données. ">
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwAllNextAction : IEntity
    {
        public int WflId { get; set; }
        public string WflName { get; set; }
        public int WfvId { get; set; }
        public int WfvVersion { get; set; }
        public int FlwId { get; set; }
        public int FseId { get; set; }
        public int EntId { get; set; }
        public string EntName { get; set; }
        public int WacId { get; set; }
        public string WacAction { get; set; }
        public string WacName { get; set; }
        public int GrpId { get; set; }
        public string GrpName { get; set; }
        public string BranchJoin { get; set; }
        public long NeededWtr { get; set; }
        public long AvailableWtr { get; set; }
    }
}