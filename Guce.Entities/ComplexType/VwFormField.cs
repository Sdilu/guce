﻿/*=================================================================*
 * Classe: <VwFormField>
 * Version/date: <2.0.0> <2016.08.05>
 *
 * Description: <” Cette classe est le type complexe qui représente la vue vw_form_fields 
 * dans la base de données. ">
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.Entities.Abstract;

namespace Guce.Entities.ComplexType
{
    public class VwFormField: IEntity
    {
        public int WafId { get; set; }
        public int WacId { get; set; }
        public int? DflId { get; set; }
        public int? WvaId { get; set; }
        public int? ParentDflId { get; set; }
        public int? ParentWvaId { get; set; }
        public bool ReadOnly { get; set; }
        public string Origin { get; set; }
        public int? SearchCriteria { get; set; }
        public int? ReferenceDicId { get; set; }
        public bool? MultiValued { get; set; }
        public bool? AdminOnly { get; set; }
        public string TechnicalName { get; set; }
        public string DicName { get; set; }
        public string DisplayName { get; set; }
        public string Type { get; set; }
        public string Empty { get; set; }
        public string Visibility { get; set; }
        public string ConditionalDisplayName { get; set; }
        public int? CodeDcdId { get; set; }
        public string DefaultValue { get; set; }
        public int? Order { get; set; }
        public int? CodeInlineDcdId { get; set; }
        public string Group { get; set; }
        public string Description { get; set; }
        public string Css { get; set; }
        public bool TextArea { get; set; }
        public string Mask { get; set; }
        public string ValidationRegexp { get; set; }
        public string DicColor { get; set; }
        public bool? DicShareable { get; set; }
        public int? IdxId { get; set; }
    }
}