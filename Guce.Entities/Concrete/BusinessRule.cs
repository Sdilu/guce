﻿/*=================================================================*
 * Classe: <BusinessRule>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe est l'entité qui représente la table business_rules dans la base de données. 
 * ”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class BusinessRule: IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Scope { get; set; }
        public string Selector { get; set; }
        public string Rule { get; set; }
        public string Description { get; set; }

    }
}