﻿/*=================================================================*
 * Classe: <FStatus>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table f_status dans la base de données. 
 * fst - Dynamique: Status du flux (instances de wf_actions)”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class FStatus: IEntity
    {
        public FStatus()
        {
            Entities = new List<Entity>();
            Values = new List<Value>();
            FVariableValues = new List<FVariableValue>();
        }

        public int Id { get; set; }
        public int FlwId { get; set; }
        public int WacId { get; set; }
        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        public Flow Flow { get; set; }
        public WfAction WfAction { get; set; }
        public ICollection<Entity> Entities { get; set; }
        public ICollection<Value> Values { get; set; }
        public ICollection<FVariableValue> FVariableValues { get; set; }
    }
}