﻿/*=================================================================*
 * Classe: <Entity>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table entities dans la base de données. 
 * ent - Dynamique: Entités (instances de dictionaries)”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Entity: IEntity
    {
        public Entity()
        {
            Flows = new List<Flow>();
            Values = new List<Value>();
        }

        public int Id { get; set; }
        public int DicId { get; set; }
        public long CurrentTxId { get; set; }
        public string Name { get; set; }
        public DateTime? LockTime { get; set; }
        public int? LockFstId { get; set; }
        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        public string Json { get; set; }
        public FStatus FStatus { get; set; }
        public Dictionary Dictionary { get; set; }
        public ICollection<Flow> Flows { get; set; }
        public ICollection<Value> Values { get; set; }

    }
}