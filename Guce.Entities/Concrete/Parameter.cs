﻿/*=================================================================*
 * Classe: <Parameter>
 * Version/date: <2.0.0> <2016.08.30>
 *
 * Description: <” Cette classe est l'entité qui représente la table parameters dans la base de données. 
 * flw - Dynamique: Flux (parametres de l'application)”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Parameter: IEntity
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }

    }
}