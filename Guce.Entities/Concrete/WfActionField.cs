﻿/*=================================================================*
 * Classe: <WfActionField>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table wf_action_fields dans la base de données. 
 * waf - Statique: Liste des champs affichés par un formulaire pour une action Form”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class WfActionField: IEntity
    {
        public int Id { get; set; }
        public int WacId { get; set; }
        public int? DflId { get; set; }
        public int? WvaId { get; set; }
        public string Empty { get; set; }
        public string DefaultValue { get; set; }
        public bool ReadOnly { get; set; }
        public bool AdminOnly { get; set; }
        public string ValidationRegexp { get; set; }
        public int Order { get; set; }
        public string Group { get; set; }
        public string Css { get; set; }
        public bool TextArea { get; set; }
        public WfAction WfAction { get; set; }
        public DicField DicField { get; set; }
        public WfVariable WfVariable { get; set; }
        
    }
}