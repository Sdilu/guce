﻿/*=================================================================*
 * Classe: <Report>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table reports dans la base de données. 
 * rpt - Statique: Rapports”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Report: IEntity
    {
        public Report()
        {
            WfActions = new List<WfAction>();
        }
        public int Id { get; set; }
        public int DicId { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public Dictionary Dictionary { get; set; }
        public ICollection<WfAction> WfActions { get; set; }
    }
}