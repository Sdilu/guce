﻿/*=================================================================*
 * Classe: <FVariableValue>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table wf_variables dans la base de données. 
 * wfl - Statique: Variables des workflows”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class WfVariable: IEntity
    {
        public WfVariable()
        {
            FVariableValues = new List<FVariableValue>();
            WfActionFields = new List<WfActionField>();
        }

        public int Id { get; set; }
        public int WfvId { get; set; }
        public int? CodeDcdId { get; set; }
        public int? CodeInlineDcdId { get; set; }
        public int? ReferenceDicId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Type { get; set; }
        public string Empty { get; set; }
        public bool MultiValued { get; set; }
        public string ValidationRegexp { get; set; }
        public string Visibility { get; set; }
        public string ConditionalDisplayName { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public string Group { get; set; }
        public string Css { get; set; }
        public string DefaultValue { get; set; }
        public bool TextArea { get; set; }
        public string Mask { get; set; }
        public WfVersion WfVersion { get; set; }
        public DicCode DicCode { get; set; }
        public ICollection<FVariableValue> FVariableValues { get; set; }
        public ICollection<WfActionField> WfActionFields { get; set; }
    }
}