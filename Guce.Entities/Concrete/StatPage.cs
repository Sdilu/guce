﻿/*=================================================================*
 * Classe: <StatPage>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette classe est l'entité qui représente la table stats_page dans la base de données. 
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class StatPage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Request { get; set; }
        public string ColName { get; set; }
        public int GrpId { get; set; }
        public Group Group { get; set; }
    }
}