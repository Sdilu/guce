﻿/*=================================================================*
 * Classe: <Site>
 * Version/date: <2.0.0> <2016.09.21>
 *
 * Description: <” Cette classe est l'entité qui représente la table sites dans la base de données.”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Site: IEntity
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}