﻿/*=================================================================*
 * Classe: <FStep>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table f_steps dans la base de données. 
 * fse - Dynamique: Étapes des flux (instances de wf_transitions)”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class FStep: IEntity
    {

        public int Id { get; set; }
        public int FlwId { get; set; }
        public int WtrId { get; set; }
        public int FromFstId { get; set; }
        public int? ToFstId { get; set; }
        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        public Flow Flow { get; set; }
        public WfTransition WfTransition { get; set; }
    }
}