﻿/*=================================================================*
 * Classe: <WfAction>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table wf_actions dans la base de données. 
 * wac - Statique: Actions d'un processus”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class WfAction: IEntity
    {
        public WfAction()
        {
            FStatuses = new List<FStatus>();
            WfActionFields = new List<WfActionField>();
            WfTransitionsFrom = new List<WfTransition>();
            WfTransitionsTo = new List<WfTransition>();
        }

        public int Id { get; set; }
        public int WfvId { get; set; }
        public int? GrpId { get; set; }
        public int? RptId { get; set; }
        public string Name { get; set; }
        public bool WfBegin { get; set; }
        public bool WfEnd { get; set; }
        public string BranchFork { get; set; }
        public string BranchJoin { get; set; }
        public string Action { get; set; }
        public WfVersion WfVersion { get; set; }
        public Group Group { get; set; }
        public Report Report { get; set; }
        public ICollection<FStatus> FStatuses { get; set; }
        public ICollection<WfActionField> WfActionFields { get; set; }
        public ICollection<WfTransition> WfTransitionsFrom { get; set; }
        public ICollection<WfTransition> WfTransitionsTo { get; set; }
    }
}