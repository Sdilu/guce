﻿/*=================================================================*
 * Classe: <DicVersionCode>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dic_version_codes dans la base de données. 
 * dvc - Statique: Association des éléments des codes à une version d'un dictionnaire”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class DicVersionCode: IEntity
    {
        public int DvsId { get; set; }
        public int DcdId { get; set; }
        public DicVersion DicVersion { get; set; }
        public DicCode DicCode { get; set; }
    }
}