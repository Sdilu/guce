﻿/*=================================================================*
 * Classe: <User>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette classe est l'entité qui représente la table users dans la base de données. 
 * ugr - Statique: Association groupes/utilisateurs”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class User: IEntity
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Description { get; set; }
        public string Attributes { get; set; }
        [NotMapped]
        public string Group { get; set; }
        [NotMapped]
        public string Role { get; set; }
    }
}