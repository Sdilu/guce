﻿/*=================================================================*
 * Classe: <DicField>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dic_fields dans la base de données. 
 * dfl - Statique: Champs des dictionnaires”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class DicField: IEntity
    {
        public DicField()
        {
            Values = new List<Value>();
            WfActionFields = new List<WfActionField>();
            DicVersionFields = new List<DicVersionField>();
        }

        public int Id { get; set; }
        public int DicId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Type { get; set; }
        public string Empty { get; set; }
        public bool MultiValued { get; set; }
        public string Origin { get; set; }
        public int SearchCriteria { get; set; }
        public string ValidationRegexp { get; set; }
        public string Visibility { get; set; }
        public string ConditionalDisplayName { get; set; }
        public int? CodeDcdId { get; set; }
        public int? CodeInlineDcdId { get; set; }
        public int? ReferenceDicId { get; set; }
        public string DefaultValue { get; set; }
        public int Order { get; set; }
        public string Group { get; set; }
        public string Description { get; set; }
        public string OriginId { get; set; }
        public string OriginName { get; set; }
        public string Mask { get; set; }
        public string Css { get; set; }
        public bool TextArea { get; set; }
        public int? IdxId { get; set; }
        public Dictionary Dictionary { get; set; }
        public Dictionary ReferenceDic { get; set; }
        public DicCode DicCode { get; set; }
        public ICollection<Value> Values { get; set; }
        public ICollection<WfActionField> WfActionFields { get; set; }
        public ICollection<DicVersionField> DicVersionFields { get; set; }
    }
}