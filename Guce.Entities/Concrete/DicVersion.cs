﻿/*=================================================================*
 * Classe: <DicVersion>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dic_versions dans la base de données. 
 * dvs - Statique: Versions des dictionnaires”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class DicVersion: IEntity
    {
        public DicVersion()
        {
            DicVersionCodes = new List<DicVersionCode>();
            DicVersionFields = new List<DicVersionField>();
        }

        public int Id { get; set; }
        public int DicId { get; set; }
        public int Version { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ActivationDate { get; set; }
        public Dictionary Dictionary { get; set; }
        public ICollection<DicVersionCode> DicVersionCodes { get; set; }
        public ICollection<DicVersionField> DicVersionFields { get; set; }
        
    }
}