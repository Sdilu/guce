﻿/*=================================================================*
 * Classe: <Value>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table values dans la base de données. 
 * val - Dynamique: Valeurs (instances de dic_fields)>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Value: IEntity
    {
        public int Id { get; set; }
        public int EntId { get; set; }
        public int DflId { get; set; }
        public int FstId { get; set; }
        public long TxId { get; set; }
        public string ValueContent { get; set; }
        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        public Entity Entity { get; set; }
        public DicField DicField { get; set; }
        public FStatus FStatus { get; set; }
    }
}