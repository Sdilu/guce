﻿/*=================================================================*
 * Classe: <DicCode>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dic_codes dans la base de données. 
 * dcd - Statique: Codifications”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class DicCode: IEntity
    {
        public DicCode()
        {
            DicCodeItems = new List<DicCodeItem>();
            DicFields = new List<DicField>();
            DicVersionCodes = new List<DicVersionCode>();
            WfVariables = new List<WfVariable>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<DicCodeItem> DicCodeItems { get; set; }
        public ICollection<DicField> DicFields { get; set; }
        public ICollection<DicVersionCode> DicVersionCodes { get; set; }
        public ICollection<WfVariable> WfVariables { get; set; }
    }
}