﻿/*=================================================================*
 * Classe: <Workflow>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table workflows dans la base de données. 
 * wfl - Statique: Processus”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Workflow: IEntity
    {
        public Workflow()
        {
            WfVersions = new List<WfVersion>();
        }
        public int Id { get; set; }
        public int? DicId { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public bool Active { get; set; }
        public Dictionary Dictionary { get; set; }
        public ICollection<WfVersion> WfVersions { get; set; }
    }
}