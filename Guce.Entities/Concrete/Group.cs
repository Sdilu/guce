﻿/*=================================================================*
 * Classe: <Group>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table group dans la base de données. 
 * grp - Statique: Groupes fonctionnels”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Group: IEntity
    {
        public Group()
        {
            UserGroups = new List<UserGroup>();
            WfActions = new List<WfAction>();
            WfVersions = new List<WfVersion>();
            StatPages = new List<StatPage>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<UserGroup> UserGroups { get; set; }
        public ICollection<WfAction> WfActions { get; set; }
        public ICollection<WfVersion> WfVersions { get; set; }
        public ICollection<StatPage> StatPages { get; set; }
    }
}