﻿/*=================================================================*
 * Classe: <DicCodeItem>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dic_code_items dans la base de données. 
 * dci - Statique: Éléments des codifications”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class DicCodeItem: IEntity
    {
        public int Id { get; set; }
        public int DcdId { get; set; }
        public int? DependDciId { get; set; }
        public string Value { get; set; }
        public string Code { get; set; }
        public string Description { get; set; } 
        public DicCode DicCode { get; set; }
    }
}