﻿/*=================================================================*
 * Classe: <DicVersionField>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dic_version_fields dans la base de données. 
 * dvf - Statique: Association des champs à une version d'un dictionnaire”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class DicVersionField: IEntity
    {
        public int DvsId { get; set; }
        public int DflId { get; set; }
        public DicVersion DicVersion { get; set; }
        public DicField DicField { get; set; }
    }
}