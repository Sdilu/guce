﻿/*=================================================================*
 * Classe: <UserGroup>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table user_groups dans la base de données. 
 * ugr - Statique: Association groupes/utilisateurs”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class UserGroup: IEntity
    {
        public string Username { get; set; }
        public int GrpId { get; set; }
        public Group Group { get; set; }
    }
}