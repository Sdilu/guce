﻿/*=================================================================*
 * Classe: <Index>
 * Version/date: <2.0.0> <2016.12.01>
 *
 * Description: <” Cette classe est l'entité qui représente la table indexes dans la base de données. 
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Index: IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Unique { get; set; }
        public bool Unaccent { get; set; }
        public string CanonicalizationRegexp { get; set; }

    }
}