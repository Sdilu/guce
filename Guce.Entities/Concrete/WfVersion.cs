﻿/*=================================================================*
 * Classe: <WfVersion>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table wf_versions dans la base de données. 
 * wfv - Statique: Versions des processus”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class WfVersion: IEntity
    {
        public WfVersion()
        {
            Flows = new List<Flow>();
            WfActions = new List<WfAction>();
            WfVariables = new List<WfVariable>();
        }

        public int Id { get; set; }
        public int WflId { get; set; }
        public int? GrpId { get; set; }
        public int Version { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ActivationDate { get; set; }
        public Workflow Workflow { get; set; }
        public Group Group { get; set; }
        public ICollection<Flow> Flows { get; set; }
        public ICollection<WfAction> WfActions { get; set; }
        public ICollection<WfVariable> WfVariables { get; set; }
    }
}