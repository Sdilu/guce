﻿/*=================================================================*
 * Classe: <Dictionary>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table dictionaries dans la base de données. 
 * dic - Statique: Dictionnaires”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Dictionary: IEntity
    {
        public Dictionary()
        {
            DicFields = new List<DicField>();
            ReferenceDicFields = new List<DicField>();
            DicVersions = new List<DicVersion>();
            Entities = new List<Entity>();
            Reports = new List<Report>();
            Workflows = new List<Workflow>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Origin { get; set; }
        public string OriginId { get; set; }
        public string OriginName { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public bool Shareable { get; set; }
        public bool ListCreate { get; set; }
        public ICollection<DicField> DicFields { get; set; }
        public ICollection<DicField> ReferenceDicFields{ get; set; }
        public ICollection<DicVersion> DicVersions { get; set; }
        public ICollection<Entity> Entities { get; set; }
        public ICollection<Report> Reports { get; set; }
        public ICollection<Workflow> Workflows { get; set; }
    }
}