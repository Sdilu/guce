﻿/*=================================================================*
 * Classe: <FVariableValue>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table f_variable_values dans la base de données. 
 * fva - Dynamique: Variables attachées au flux (instances de dic_fields)”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class FVariableValue: IEntity
    {
        public int Id { get; set; }
        public int WvaId { get; set; }
        public int FstId { get; set; }
        public string Value { get; set; }
        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        public WfVariable WfVariable { get; set; }
        public FStatus FStatus { get; set; }
    }
}