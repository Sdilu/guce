﻿/*=================================================================*
 * Classe: <WfTransition>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table wf_transitions dans la base de données. 
 * wtr - Statique: Transitions entre les actions”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class WfTransition: IEntity
    {
        public WfTransition()
        {
            FSteps = new List<FStep>();
        }

        public int Id { get; set; }
        public int FromWacId { get; set; }
        public int ToWacId { get; set; }
        public int Weight { get; set; }
        public string Condition { get; set; }
        public string Name { get; set; }
        public WfAction WfActionFrom { get; set; }
        public WfAction WfActionTo { get; set; }
        public ICollection<FStep> FSteps { get; set; }
    }
}