﻿/*=================================================================*
 * Classe: <Flow>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est l'entité qui représente la table flows dans la base de données. 
 * flw - Dynamique: Flux (instances de workflows)”>
 * Specificities: <“ Cette classe est une classe entité ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Guce.Entities.Abstract;

namespace Guce.Entities.Concrete
{
    public class Flow: IEntity
    {
        public Flow()
        {
            FStatuses = new List<FStatus>();
            FSteps = new List<FStep>();
        }

        public int Id { get; set; }
        public int WfvId { get; set; }
        public string Reference { get; set; }
        public int? EntId { get; set; }
        public bool Closed { get; set; }
        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        [NotMapped]
        public string Procedure { get; set; }
        public Entity Entity { get; set; }
        public WfVersion WfVersion { get; set; } 
        public ICollection<FStatus> FStatuses { get; set; }
        public ICollection<FStep> FSteps { get; set; }
    }
}