﻿/*=================================================================*
 * Classe: <EfVwFormFieldDal>
 * Version/date: <2.0.0> <2016.08.05>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour le type complexe VwFormField. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete
{
    public class EfVwFormFieldDal : EfEntityRepositoryBase<VwFormField>, IVwFormFieldDal
    {
         
    }
}