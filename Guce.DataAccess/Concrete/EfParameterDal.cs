﻿/*=================================================================*
 * Classe: <EfParameterDal>
 * Version/date: <2.0.0> <2016.08.30>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité Parameter. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;

namespace Guce.DataAccess.Concrete
{
    public class EfParameterDal : EfEntityRepositoryBase<Parameter>, IParameterDal
    {
        public string GetValue(string id, string username)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var parameter = context.Parameters.ToList().Find(p => p.Id.Equals(id) && p.Username.Equals(username));
                return id.Equals(ParameterId.SiteId) && parameter.Value.Contains("/") ? parameter.Value.Split('/')[1] : parameter.Value;
            }
        }

        public Parameter GetParameter(string id, string username)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.Parameters.ToList().Find(p => p.Id.Equals(id) && p.Username.Equals(username));
            }
        }

        public List<Parameter> GetParameters(string username)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.Parameters.Where(p => p.Username.Equals(username)).ToList();
            }
        }
    }
}