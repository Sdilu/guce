﻿/*=================================================================*
 * Classe: <EfFlowDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité Flow. ”>
 * Specificities: <“ - ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Data.Entity;
using System.Linq;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfFlowDal : EfEntityRepositoryBase<Flow>, IFlowDal
    {
        public int AddReturnKey(Flow flow)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var addedEntity = context.Entry(flow);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
                return flow.Id;
            }
        }

        public string GetLastFlowReference()
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var flows = context.Flows.ToList().Where(f => Convert.ToInt32(f.Reference.Split('/')[0]) == Convert.ToByte(DateTime.Now.ToString("yy"))).OrderByDescending(f => int.Parse(f.Reference.Split('/')[2])).ToList();
                return flows.FirstOrDefault()?.Reference;
            }
        }
    }
}