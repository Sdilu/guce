﻿/*=================================================================*
 * Classe: <EfUserDal>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité User. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfUserDal : EfEntityRepositoryBase<User>, IUserDal
    {

    }
}