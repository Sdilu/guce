﻿/*=================================================================*
 * Classe: <EfFStepDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité FStep. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfFStepDal : EfEntityRepositoryBase<FStep>, IFStepDal
    {
        public bool IsReadyToBeActivated(List<WfTransition> transitions, int flowId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                foreach (var s in context.FSteps.SelectMany(st => transitions.
                Where(tr => st.FlwId == flowId && tr.Id == st.WtrId)))
                {
                    transitions.Remove(s);
                }
                return transitions.Count == 0;
            }
        }

        public int AddReturnKey(FStep step)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var addedEntity = context.Entry(step);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
                return step.Id;
            }
        }
    }
}