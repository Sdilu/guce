﻿/*=================================================================*
 * Classe: <VwFormFieldMap>
 * Version/date: <2.0.0> <2016.08.05>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_form_fields. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwFormFieldMap : EntityTypeConfiguration<VwFormField>
    {
        public VwFormFieldMap()
        {
            // Primary Key
            HasKey(t => t.WafId);

            // Table & Column Mappings
            ToTable("vw_form_fields", "public");
            Property(t => t.WafId).HasColumnName("waf_id");
            Property(t => t.WacId).HasColumnName("wac_id");
            Property(t => t.DflId).HasColumnName("dfl_id");
            Property(t => t.WvaId).HasColumnName("wva_id");
            Property(t => t.ParentDflId).HasColumnName("parent_dfl_id");
            Property(t => t.ParentWvaId).HasColumnName("parent_wva_id");
            Property(t => t.ReadOnly).HasColumnName("read_only");
            Property(t => t.MultiValued).HasColumnName("multivalued");
            Property(t => t.AdminOnly).HasColumnName("admin_only");
            Property(t => t.Origin).HasColumnName("origin");
            Property(t => t.SearchCriteria).HasColumnName("search_criteria");
            Property(t => t.ReferenceDicId).HasColumnName("reference_dic_id");
            Property(t => t.TechnicalName).HasColumnName("field_name");
            Property(t => t.DicName).HasColumnName("dic_name");
            Property(t => t.DisplayName).HasColumnName("display_name");
            Property(t => t.Type).HasColumnName("type");
            Property(t => t.Empty).HasColumnName("empty");
            Property(t => t.Visibility).HasColumnName("visibility");
            Property(t => t.ConditionalDisplayName).HasColumnName("conditional_display_name");
            Property(t => t.CodeDcdId).HasColumnName("code_dcd_id");
            Property(t => t.CodeInlineDcdId).HasColumnName("code_inline_dcd_id");
            Property(t => t.DefaultValue).HasColumnName("default_value");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.Group).HasColumnName("group");
            Property(t => t.Description).HasColumnName("description");
            Property(t => t.Css).HasColumnName("css");
            Property(t => t.Mask).HasColumnName("mask");
            Property(t => t.TextArea).HasColumnName("textarea");
            Property(t => t.ValidationRegexp).HasColumnName("validation_regexp");
            Property(t => t.DicColor).HasColumnName("dic_color");
            Property(t => t.DicShareable).HasColumnName("dic_shareable");
            Property(t => t.IdxId).HasColumnName("idx_id");
        }
    }
}
