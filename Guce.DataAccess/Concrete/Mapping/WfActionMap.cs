﻿/*=================================================================*
 * Classe: <WfActionMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité WfAction. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class WfActionMap : EntityTypeConfiguration<WfAction>
    {
        public WfActionMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Action)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("wf_actions", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.WfvId).HasColumnName("wfv_id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.WfBegin).HasColumnName("wf_begin");
            Property(t => t.WfEnd).HasColumnName("wf_end");
            Property(t => t.BranchFork).HasColumnName("branch_fork");
            Property(t => t.BranchJoin).HasColumnName("branch_join");
            Property(t => t.Action).HasColumnName("action");
            Property(t => t.GrpId).HasColumnName("grp_id");
            Property(t => t.RptId).HasColumnName("rpt_id");

            // Relationships
            HasRequired(t => t.WfVersion)
                .WithMany(t => t.WfActions)
                .HasForeignKey(d => d.WfvId);
            HasOptional(t => t.Group)
                .WithMany(t => t.WfActions)
                .HasForeignKey(d => d.GrpId);
            HasOptional(t => t.Report)
                .WithMany(t => t.WfActions)
                .HasForeignKey(d => d.RptId);

        }
    }
}
