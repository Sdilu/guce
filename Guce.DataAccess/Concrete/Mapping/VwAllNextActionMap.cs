﻿/*=================================================================*
 * Classe: <VwAllNextActionMap>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_all_next_actions. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwAllNextActionMap : EntityTypeConfiguration<VwAllNextAction>
    {
        public VwAllNextActionMap()
        {
            // Primary Key
            HasKey(t => new { t.WflId, t.FlwId, t.EntId, t.WacId });

            // Table & Column Mappings
            ToTable("vw_all_next_actions", "public");
            Property(t => t.WflId).HasColumnName("wfl_id");
            Property(t => t.WflName).HasColumnName("wfl_name");
            Property(t => t.WfvId).HasColumnName("wfv_id");
            Property(t => t.WfvVersion).HasColumnName("wfv_version");
            Property(t => t.FlwId).HasColumnName("flw_id");
            Property(t => t.FseId).HasColumnName("fse_id");
            Property(t => t.EntId).HasColumnName("ent_id");
            Property(t => t.EntName).HasColumnName("ent_name");
            Property(t => t.WacId).HasColumnName("wac_id");
            Property(t => t.WacAction).HasColumnName("wac_action");
            Property(t => t.WacName).HasColumnName("wac_name");
            Property(t => t.GrpId).HasColumnName("grp_id");
            Property(t => t.GrpName).HasColumnName("grp_name");
            Property(t => t.BranchJoin).HasColumnName("branch_join");
            Property(t => t.NeededWtr).HasColumnName("needed_wtr");
            Property(t => t.AvailableWtr).HasColumnName("available_wtr");
        } 
    }
}