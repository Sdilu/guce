﻿/*=================================================================*
 * Classe: <VwDependantDicCodeMap>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_dependant_dic_codes. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwDependantDicCodeMap : EntityTypeConfiguration<VwDependantDicCode>
    {
        public VwDependantDicCodeMap()
        {
            // Primary Key
            HasKey(t => t.DcdId);

            // Table & Column Mappings
            ToTable("vw_dependant_dic_codes", "public");
            Property(t => t.DcdId).HasColumnName("dcd_id");
            Property(t => t.DcdName).HasColumnName("dcd_name");
            Property(t => t.DependantDcdId).HasColumnName("dependant_dcd_id");
            Property(t => t.DependantDcdName).HasColumnName("dependant_dcd_name");
        }
    }
}
