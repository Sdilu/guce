﻿/*=================================================================*
 * Classe: <ReportMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Report. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class ReportMap : EntityTypeConfiguration<Report>
    {
        public ReportMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Template)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("reports", "public");
            this.Property(t => t.Id).HasColumnName("id");
            this.Property(t => t.DicId).HasColumnName("dic_id");
            this.Property(t => t.Name).HasColumnName("title");
            this.Property(t => t.Template).HasColumnName("report_name");

            // Relationships
            this.HasRequired(t => t.Dictionary)
                .WithMany(t => t.Reports)
                .HasForeignKey(d => d.DicId);

        }
    }
}
