﻿/*=================================================================*
 * Classe: <WfTransitionMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité WfTransition. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class WfTransitionMap : EntityTypeConfiguration<WfTransition>
    {
        public WfTransitionMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .HasMaxLength(8000);

            Property(t => t.Condition)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("wf_transitions", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.FromWacId).HasColumnName("from_wac_id");
            Property(t => t.ToWacId).HasColumnName("to_wac_id");
            Property(t => t.Weight).HasColumnName("weight");
            Property(t => t.Condition).HasColumnName("condition");
            Property(t => t.Name).HasColumnName("name");

            // Relationships
            HasRequired(t => t.WfActionFrom)
                .WithMany(t => t.WfTransitionsFrom)
                .HasForeignKey(d => d.FromWacId);
            HasRequired(t => t.WfActionTo)
                .WithMany(t => t.WfTransitionsTo)
                .HasForeignKey(d => d.ToWacId);
        }
    }
}
