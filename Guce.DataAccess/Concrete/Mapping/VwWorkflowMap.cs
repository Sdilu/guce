﻿/*=================================================================*
 * Classe: <VwWorkflowMap>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_workflows. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwWorkflowMap : EntityTypeConfiguration<VwWorkflow>
    {
        public VwWorkflowMap()
        {
            // Primary Key
            HasKey(t => t.WflId);

            // Table & Column Mappings
            ToTable("vw_workflows", "public");
            Property(t => t.WflId).HasColumnName("wfl_id");
            Property(t => t.WfvId).HasColumnName("wfv_id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Version).HasColumnName("version");
            Property(t => t.Active).HasColumnName("active");
            Property(t => t.ActivationDate).HasColumnName("activation_date");
            Property(t => t.CurrentVersion).HasColumnName("current_version");
            Property(t => t.GrpId).HasColumnName("grp_id");
        }
    }
}
