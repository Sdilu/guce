﻿/*=================================================================*
 * Classe: <FStepMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité FStep. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class FStepMap : EntityTypeConfiguration<FStep>
    {
        public FStepMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            // Table & Column Mappings
            this.ToTable("f_steps", "public");
            this.Property(t => t.Id).HasColumnName("id");
            this.Property(t => t.FlwId).HasColumnName("flw_id");
            this.Property(t => t.WtrId).HasColumnName("wtr_id");
            this.Property(t => t.FromFstId).HasColumnName("from_fst_id");
            this.Property(t => t.ToFstId).HasColumnName("to_fst_id");
            this.Property(t => t.CreationTime).HasColumnName("creation_time");
            this.Property(t => t.CreatedBy).HasColumnName("created_by");

            // Relationships
            this.HasRequired(t => t.Flow)
                .WithMany(t => t.FSteps)
                .HasForeignKey(d => d.FlwId);
            this.HasRequired(t => t.WfTransition)
                .WithMany(t => t.FSteps)
                .HasForeignKey(d => d.WtrId);

        }
    }
}
