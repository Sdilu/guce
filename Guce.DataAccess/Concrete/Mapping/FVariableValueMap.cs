﻿/*=================================================================*
 * Classe: <FVariableValueMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité FVariableValue. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class FVariableValueMap : EntityTypeConfiguration<FVariableValue>
    {
        public FVariableValueMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            Property(t => t.Value)
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("f_variable_values", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.WvaId).HasColumnName("wva_id");
            Property(t => t.FstId).HasColumnName("fst_id");
            Property(t => t.Value).HasColumnName("value");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.CreatedBy).HasColumnName("created_by");

            // Relationships
            HasRequired(t => t.WfVariable)
                .WithMany(t => t.FVariableValues)
                .HasForeignKey(d => d.WvaId);
            HasRequired(t => t.FStatus)
                .WithMany(t => t.FVariableValues)
                .HasForeignKey(d => d.FstId);

        }
    }
}
