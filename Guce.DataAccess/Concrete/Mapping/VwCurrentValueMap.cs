﻿/*=================================================================*
 * Classe: <VwCurrentValueMap>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_current_values. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwCurrentValueMap : EntityTypeConfiguration<VwCurrentValue>
    {
        public VwCurrentValueMap()
        {
            // Primary Key
            HasKey(t => t.ValId);

            // Table & Column Mappings
            ToTable("vw_current_values", "public");
            Property(t => t.ValId).HasColumnName("val_id");
            Property(t => t.EntId).HasColumnName("ent_id");
            Property(t => t.FlwId).HasColumnName("flw_id");
            Property(t => t.DflId).HasColumnName("dfl_id");
            Property(t => t.DicId).HasColumnName("dic_id");
            Property(t => t.EntCurrentTxId).HasColumnName("ent_current_txid");
            Property(t => t.TxId).HasColumnName("txid");
            Property(t => t.FieldName).HasColumnName("field_name");
            Property(t => t.FieldDisplayName).HasColumnName("field_display_name");
            Property(t => t.Type).HasColumnName("type");
            Property(t => t.MultiValued).HasColumnName("multivalued");
            Property(t => t.ReferenceDicId).HasColumnName("reference_dic_id");
            Property(t => t.Value).HasColumnName("value");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.ValidationTime).HasColumnName("validation_time");
            Property(t => t.DeferencedValue).HasColumnName("deferenced_value");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.CodeId).HasColumnName("code_id");
            Property(t => t.ValueNum).HasColumnName("value_num");
        }
    }
}
