﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwLastValidatedValuesMap : EntityTypeConfiguration<VwLastValidatedValues>
    {
        public VwLastValidatedValuesMap()
        {
             // Primary Key
            HasKey(t =>new { t.val_id, t.ent_id, t.fst_id, t.flw_id, t.dfl_id, t.dic_id});

            // Table & Column Mappings
            ToTable("vw_last_validated_values", "public");
            Property(t => t.val_id).HasColumnName("val_id");
            Property(t => t.ent_id).HasColumnName("ent_id");
            Property(t => t.fst_id).HasColumnName("fst_id");
            Property(t => t.flw_id).HasColumnName("flw_id");
            Property(t => t.dfl_id).HasColumnName("dfl_id");
            Property(t => t.dic_id).HasColumnName("dic_id");
            Property(t => t.txid).HasColumnName("txid");
            Property(t => t.ent_current_txid).HasColumnName("ent_current_txid");
            Property(t => t.field_name).HasColumnName("field_name");
            Property(t => t.field_display_name).HasColumnName("field_display_name");
            Property(t => t.type).HasColumnName("type");
            Property(t => t.multivalued).HasColumnName("multivalued");
            Property(t => t.reference_dic_id).HasColumnName("reference_dic_id");
            Property(t => t.value).HasColumnName("value");
            Property(t => t.creation_time).HasColumnName("creation_time");
            Property(t => t.validation_time).HasColumnName("validation_time");
            Property(t => t.deferenced_value).HasColumnName("deferenced_value");
            Property(t => t.code_id).HasColumnName("code_id");
            Property(t => t.value_num).HasColumnName("value_num");
            Property(t => t.order).HasColumnName("order");
            Property(t => t.max_fst_id).HasColumnName("max_fst_id");
        }
         
    }
}
