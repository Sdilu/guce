﻿/*=================================================================*
 * Classe: <ParameterMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Parameter. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class ParameterMap : EntityTypeConfiguration<Parameter>
    {
        public ParameterMap()
        {
            // Primary Key
            HasKey(t => new {t.Id, t.Username});

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            
            // Table & Column Mappings
            ToTable("parameters", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.Username).HasColumnName("username");
            Property(t => t.Value).HasColumnName("value");
            Property(t => t.Description).HasColumnName("description");
        }
    }
}
