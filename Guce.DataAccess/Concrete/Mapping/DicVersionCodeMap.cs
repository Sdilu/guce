﻿/*=================================================================*
 * Classe: <DicVersionCodeMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité DicVersionCode. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class DicVersionCodeMap : EntityTypeConfiguration<DicVersionCode>
    {
        public DicVersionCodeMap()
        {
            // Primary Key
            HasKey(t => new { t.DvsId, t.DcdId });

            // Properties
            Property(t => t.DvsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(t => t.DcdId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("dic_version_codes", "public");
            Property(t => t.DvsId).HasColumnName("dvs_id");
            Property(t => t.DcdId).HasColumnName("dcd_id");

            // Relationships
            HasRequired(t => t.DicVersion)
                .WithMany(t => t.DicVersionCodes)
                .HasForeignKey(d => d.DvsId);
            HasRequired(t => t.DicCode)
                .WithMany(t => t.DicVersionCodes)
                .HasForeignKey(d => d.DcdId);
        }
    }
}