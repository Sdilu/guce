﻿/*=================================================================*
 * Classe: <StatPageMap>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité StatPage. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class StatPageMap : EntityTypeConfiguration<StatPage>
    {
        public StatPageMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            
            // Table & Column Mappings
            ToTable("stats_pages", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Type).HasColumnName("type");
            Property(t => t.Request).HasColumnName("request");
            Property(t => t.GrpId).HasColumnName("grp_id");
            Property(t => t.ColName).HasColumnName("col_names");

            // Relationships
            HasRequired(t => t.Group)
                .WithMany(t => t.StatPages)
                .HasForeignKey(d => d.GrpId);
        }
    }
}
