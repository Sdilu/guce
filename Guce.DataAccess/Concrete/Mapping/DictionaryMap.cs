﻿/*=================================================================*
 * Classe: <DictionaryMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Dictionary. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class DictionaryMap : EntityTypeConfiguration<Dictionary>
    {
        public DictionaryMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);

            Property(t => t.Origin)
                .IsRequired()
                .HasMaxLength(8000);

            Property(t => t.OriginId)
                .HasMaxLength(8000);

            Property(t => t.OriginName)
                .HasMaxLength(8000);

            Property(t => t.Description)
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("dictionaries", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Origin).HasColumnName("origin");
            Property(t => t.OriginId).HasColumnName("origin_id");
            Property(t => t.OriginName).HasColumnName("origin_name");
            Property(t => t.Description).HasColumnName("description");
            Property(t => t.Color).HasColumnName("color");
            Property(t => t.Shareable).HasColumnName("shareable");
            Property(t => t.ListCreate).HasColumnName("list_create");
        }
    }
}
