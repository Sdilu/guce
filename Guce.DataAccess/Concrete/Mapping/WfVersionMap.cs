﻿/*=================================================================*
 * Classe: <WfVersionMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité WfVersion. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class WfVersionMap : EntityTypeConfiguration<WfVersion>
    {
        public WfVersionMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("wf_versions", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.WflId).HasColumnName("wfl_id");
            Property(t => t.Version).HasColumnName("version");
            Property(t => t.GrpId).HasColumnName("grp_id");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.ActivationDate).HasColumnName("activation_date");

            // Relationships
            HasOptional(t => t.Group)
                .WithMany(t => t.WfVersions)
                .HasForeignKey(d => d.GrpId);
            HasRequired(t => t.Workflow)
                .WithMany(t => t.WfVersions)
                .HasForeignKey(d => d.WflId);

        }
    }
}
