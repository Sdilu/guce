﻿/*=================================================================*
 * Classe: <VwAllVariableValueMap>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_all_variable_values. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwAllVariableValueMap : EntityTypeConfiguration<VwAllVariableValue>
    {
        public VwAllVariableValueMap()
        {
            // Primary Key
            HasKey(t => t.FvvId);

            // Table & Column Mappings
            ToTable("vw_all_variable_values", "public");
            Property(t => t.WvaId).HasColumnName("wva_id");
            Property(t => t.WvaName).HasColumnName("wva_name");
            Property(t => t.WvaDisplayName).HasColumnName("wva_display_name");
            Property(t => t.WvaType).HasColumnName("wva_type");
            Property(t => t.FlwId).HasColumnName("flw_id");
            Property(t => t.FstId).HasColumnName("fst_id");
            Property(t => t.FvvId).HasColumnName("fvv_id");
            Property(t => t.Value).HasColumnName("value");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.CreatedBy).HasColumnName("created_by");
            Property(t => t.Pos).HasColumnName("max_fst_id");
            Property(t => t.DciId).HasColumnName("dci_id");
        }
    }
}
