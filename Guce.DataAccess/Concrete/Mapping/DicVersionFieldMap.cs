﻿/*=================================================================*
 * Classe: <DicVersionFieldMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité DicVersionField. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class DicVersionFieldMap : EntityTypeConfiguration<DicVersionField>
    {
        public DicVersionFieldMap()
        {
            // Primary Key
            HasKey(t => new { t.DvsId, t.DflId });

            // Properties
            Property(t => t.DvsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(t => t.DflId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("dic_version_fields", "public");
            Property(t => t.DvsId).HasColumnName("dvs_id");
            Property(t => t.DflId).HasColumnName("dfl_id");

            // Relationships
            HasRequired(t => t.DicVersion)
                .WithMany(t => t.DicVersionFields)
                .HasForeignKey(d => d.DvsId);
            HasRequired(t => t.DicField)
                .WithMany(t => t.DicVersionFields)
                .HasForeignKey(d => d.DflId);
        }
    }
}