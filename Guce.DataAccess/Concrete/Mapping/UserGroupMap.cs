﻿/*=================================================================*
 * Classe: <UserGroupMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité UserGroup. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class UserGroupMap : EntityTypeConfiguration<UserGroup>
    {
        public UserGroupMap()
        {
            // Primary Key
            HasKey(t => new { t.Username, t.GrpId });

            // Properties
            Property(t => t.Username)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(t => t.GrpId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("user_groups", "public");
            Property(t => t.Username).HasColumnName("username");
            Property(t => t.GrpId).HasColumnName("grp_id");

            // Relationships
            HasRequired(t => t.Group)
                .WithMany(t => t.UserGroups)
                .HasForeignKey(d => d.GrpId);
        }
    }
}