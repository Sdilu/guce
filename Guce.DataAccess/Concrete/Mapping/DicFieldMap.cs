﻿/*=================================================================*
 * Classe: <DicFieldMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité DicField. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class DicFieldMap : EntityTypeConfiguration<DicField>
    {
        public DicFieldMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.DisplayName)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Type)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Empty)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Origin)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Visibility).HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("dic_fields", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.DicId).HasColumnName("dic_id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.DisplayName).HasColumnName("display_name");
            Property(t => t.Type).HasColumnName("type");
            Property(t => t.Empty).HasColumnName("empty");
            Property(t => t.MultiValued).HasColumnName("multivalued");
            Property(t => t.Origin).HasColumnName("origin");
            Property(t => t.SearchCriteria).HasColumnName("search_criteria");
            Property(t => t.ValidationRegexp).HasColumnName("validation_regexp");
            Property(t => t.Visibility).HasColumnName("visibility");
            Property(t => t.ConditionalDisplayName).HasColumnName("conditional_display_name");
            Property(t => t.CodeDcdId).HasColumnName("code_dcd_id");
            Property(t => t.CodeInlineDcdId).HasColumnName("code_inline_dcd_id");
            Property(t => t.ReferenceDicId).HasColumnName("reference_dic_id");
            Property(t => t.DefaultValue).HasColumnName("default_value");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.Group).HasColumnName("group");
            Property(t => t.Description).HasColumnName("description");
            Property(t => t.OriginId).HasColumnName("origin_id");
            Property(t => t.OriginName).HasColumnName("origin_name");
            Property(t => t.Css).HasColumnName("css");
            Property(t => t.Mask).HasColumnName("mask");
            Property(t => t.TextArea).HasColumnName("textarea");
            Property(t => t.IdxId).HasColumnName("idx_id");

            // Relationships
            HasRequired(t => t.Dictionary)
                .WithMany(t => t.DicFields)
                .HasForeignKey(d => d.DicId);
            HasOptional(t => t.ReferenceDic)
                .WithMany(t => t.ReferenceDicFields)
                .HasForeignKey(d => d.ReferenceDicId);
            HasOptional(t => t.DicCode)
                .WithMany(t => t.DicFields)
                .HasForeignKey(d => d.CodeDcdId);

        }
    }
}