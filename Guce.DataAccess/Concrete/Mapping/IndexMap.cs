﻿/*=================================================================*
 * Classe: <IndexMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Index. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class IndexMap : EntityTypeConfiguration<Index>
    {
        public IndexMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            
            // Table & Column Mappings
            ToTable("indexes", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Unique).HasColumnName("unique");
            Property(t => t.Unaccent).HasColumnName("unaccent");
            Property(t => t.CanonicalizationRegexp).HasColumnName("canonicalization_regexp");
            

        }
    }
}
