﻿/*=================================================================*
 * Classe: <FlowMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Flow. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class FlowMap : EntityTypeConfiguration<Flow>
    {
        public FlowMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            Property(t => t.Reference)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("flows", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.WfvId).HasColumnName("wfv_id");
            Property(t => t.Reference).HasColumnName("reference");
            Property(t => t.EntId).HasColumnName("ent_id");
            Property(t => t.Closed).HasColumnName("closed");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.CreatedBy).HasColumnName("created_by");
            
            // Relationships
            HasOptional(t => t.Entity)
                .WithMany(t => t.Flows)
                .HasForeignKey(d => d.EntId);
            HasRequired(t => t.WfVersion)
                .WithMany(t => t.Flows)
                .HasForeignKey(d => d.WfvId);

        }
    }
}
