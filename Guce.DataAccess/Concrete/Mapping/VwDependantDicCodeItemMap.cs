﻿/*=================================================================*
 * Classe: <VwDependantDicCodeItemMap>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe est une classe de mappage pour la vue vw_dependant_dic_code_items. 
 * Cette vue retourne la liste des workflows active pour un utilisateur connecté. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity.ModelConfiguration;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class VwDependantDicCodeItemMap : EntityTypeConfiguration<VwDependantDicCodeItem>
    {
        public VwDependantDicCodeItemMap()
        {
            // Primary Key
            HasKey(t => new { t.DciId, t.DcdId, t.DependantDciId});

            // Table & Column Mappings
            ToTable("vw_dependant_dic_code_items", "public");
            Property(t => t.DciId).HasColumnName("dcd_id");
            Property(t => t.DciId).HasColumnName("dci_id");
            Property(t => t.Value).HasColumnName("value");
            Property(t => t.DcdName).HasColumnName("dcd_name");
            Property(t => t.DependantDcdId).HasColumnName("dependant_dcd_id");
            Property(t => t.DependantDcdName).HasColumnName("dependant_dcd_name");
            Property(t => t.DependantDciId).HasColumnName("dependant_dci_id");
            Property(t => t.DependantValue).HasColumnName("dependant_value");
            Property(t => t.DependantCode).HasColumnName("dependant_code");
            Property(t => t.DependantCode).HasColumnName("dependant_code");
            Property(t => t.DependantDescription).HasColumnName("dependant_description");
        }
    }
}
