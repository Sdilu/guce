﻿/*=================================================================*
 * Classe: <DicVersionMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité DicVersion. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class DicVersionMap : EntityTypeConfiguration<DicVersion>
    {
        public DicVersionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(t => t.Version)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("dic_versions", "public");
            this.Property(t => t.Id).HasColumnName("id");
            this.Property(t => t.DicId).HasColumnName("dic_id");
            this.Property(t => t.Version).HasColumnName("version");
            this.Property(t => t.CreationTime).HasColumnName("creation_time");
            this.Property(t => t.ActivationDate).HasColumnName("activation_date");

            // Relationships
            this.HasRequired(t => t.Dictionary)
                .WithMany(t => t.DicVersions)
                .HasForeignKey(d => d.DicId);

        }
    }
}
