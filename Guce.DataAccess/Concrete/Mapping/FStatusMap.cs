﻿/*=================================================================*
 * Classe: <FStatusMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité FStatus. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class FStatusMap : EntityTypeConfiguration<FStatus>
    {
        public FStatusMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            // Table & Column Mappings
            ToTable("f_status", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.FlwId).HasColumnName("flw_id");
            Property(t => t.WacId).HasColumnName("wac_id");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.CreatedBy).HasColumnName("created_by");

            // Relationships
            HasRequired(t => t.Flow)
                .WithMany(t => t.FStatuses)
                .HasForeignKey(d => d.FlwId);
            HasRequired(t => t.WfAction)
                .WithMany(t => t.FStatuses)
                .HasForeignKey(d => d.WacId);

        }
    }
}
