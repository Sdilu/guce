﻿/*=================================================================*
 * Classe: <DicCodeItemMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité DicCodeItem. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class DicCodeItemMap : EntityTypeConfiguration<DicCodeItem>
    {
        public DicCodeItemMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("dic_code_items", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.DcdId).HasColumnName("dcd_id");
            Property(t => t.DependDciId).HasColumnName("depends_dci_id");
            Property(t => t.Value).HasColumnName("value");
            Property(t => t.Code).HasColumnName("code");
            Property(t => t.Description).HasColumnName("description");

            // Relationships
            HasRequired(t => t.DicCode)
                .WithMany(t => t.DicCodeItems)
                .HasForeignKey(d => d.DcdId);

        }
    }
}