﻿/*=================================================================*
 * Classe: <EntityMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Entity. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class EntityMap : EntityTypeConfiguration<Entity>
    {
        public EntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            Property(t => t.Name)
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("entities", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.DicId).HasColumnName("dic_id");
            Property(t => t.CurrentTxId).HasColumnName("current_txid");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.LockFstId).HasColumnName("lock_fst_id");
            Property(t => t.LockTime).HasColumnName("lock_time");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.CreatedBy).HasColumnName("created_by"); 
            Property(t => t.Json).HasColumnName("json");

            // Relationships
            HasRequired(t => t.Dictionary)
                .WithMany(t => t.Entities)
                .HasForeignKey(d => d.DicId);
            HasOptional(t => t.FStatus)
                .WithMany(t => t.Entities)
                .HasForeignKey(d => d.LockFstId);
        }
    }
}
