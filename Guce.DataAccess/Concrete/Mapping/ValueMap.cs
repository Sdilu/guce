﻿/*=================================================================*
 * Classe: <ValueMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Value. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class ValueMap : EntityTypeConfiguration<Value>
    {
        public ValueMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            Property(t => t.ValueContent)
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("values", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.EntId).HasColumnName("ent_id");
            Property(t => t.DflId).HasColumnName("dfl_id");
            Property(t => t.FstId).HasColumnName("fst_id");
            Property(t => t.TxId).HasColumnName("txid");
            Property(t => t.ValueContent).HasColumnName("value");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.CreatedBy).HasColumnName("created_by");

            // Relationships
            HasRequired(t => t.DicField)
                .WithMany(t => t.Values)
                .HasForeignKey(d => d.DflId);
            HasRequired(t => t.Entity)
                .WithMany(t => t.Values)
                .HasForeignKey(d => d.EntId);
            HasRequired(t => t.FStatus)
                .WithMany(t => t.Values)
                .HasForeignKey(d => d.FstId);

        }
    }
}
