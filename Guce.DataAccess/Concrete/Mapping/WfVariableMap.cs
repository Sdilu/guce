﻿/*=================================================================*
 * Classe: <WfVariableMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité WfVariable. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class WfVariableMap : EntityTypeConfiguration<WfVariable>
    {
        public WfVariableMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("wf_variables", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.WfvId).HasColumnName("wfv_id");
            Property(t => t.CodeDcdId).HasColumnName("code_dcd_id");
            Property(t => t.CodeInlineDcdId).HasColumnName("code_inline_dcd_id");
            Property(t => t.ReferenceDicId).HasColumnName("reference_dic_id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.DisplayName).HasColumnName("display_name");
            Property(t => t.ValidationRegexp).HasColumnName("validation_regexp");
            Property(t => t.Type).HasColumnName("type");
            Property(t => t.Empty).HasColumnName("empty");
            Property(t => t.MultiValued).HasColumnName("multivalued");
            Property(t => t.DefaultValue).HasColumnName("default_value");
            Property(t => t.Visibility).HasColumnName("visibility");
            Property(t => t.ConditionalDisplayName).HasColumnName("conditional_display_name");
            Property(t => t.Description).HasColumnName("description");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.Group).HasColumnName("group");
            Property(t => t.Css).HasColumnName("css");
            Property(t => t.TextArea).HasColumnName("textarea");
            Property(t => t.Mask).HasColumnName("mask");

            // Relationships
            HasRequired(t => t.WfVersion)
                .WithMany(t => t.WfVariables)
                .HasForeignKey(d => d.WfvId);
            HasOptional(t => t.DicCode)
                .WithMany(t => t.WfVariables)
                .HasForeignKey(d => d.CodeDcdId);
        }
    }
}
