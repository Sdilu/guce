﻿/*=================================================================*
 * Classe: <WorkflowMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité Workflow. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class WorkflowMap : EntityTypeConfiguration<Workflow>
    {
        public WorkflowMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("workflows", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.DicId).HasColumnName("dic_id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.CreationTime).HasColumnName("creation_time");
            Property(t => t.Active).HasColumnName("active");

            // Relationships
            HasOptional(t => t.Dictionary)
                .WithMany(t => t.Workflows)
                .HasForeignKey(d => d.DicId);

        }
    }
}
