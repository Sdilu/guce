﻿/*=================================================================*
 * Classe: <UserMap>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité User. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            HasKey(t => t.Username);
            
            // Table & Column Mappings
            ToTable("users", "public");
            Property(t => t.Username).HasColumnName("username");
            Property(t => t.Fullname).HasColumnName("fullname");
            Property(t => t.Description).HasColumnName("description");
            Property(t => t.Attributes).HasColumnName("attributes");

        }
    }
}