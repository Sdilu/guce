﻿/*=================================================================*
 * Classe: <BusinessRuleMap>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est une classe de mappage pour l'entité BusinessRule. ”>
 * Specificities: <“ Cette classe est une classe de mappage ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class BusinessRuleMap : EntityTypeConfiguration<BusinessRule>
    {
        public BusinessRuleMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            
            // Table & Column Mappings
            ToTable("business_rules", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Type).HasColumnName("type");
            Property(t => t.Scope).HasColumnName("scope");
            Property(t => t.Selector).HasColumnName("selector");
            Property(t => t.Rule).HasColumnName("rule");
            Property(t => t.Description).HasColumnName("description");
        }
    }
}