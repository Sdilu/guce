﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class SiteMap : EntityTypeConfiguration<Site>
    {
        public SiteMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(8000);

            Property(t => t.Code)
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("sites", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.Code).HasColumnName("code");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Location).HasColumnName("location");
        }
    }
}
