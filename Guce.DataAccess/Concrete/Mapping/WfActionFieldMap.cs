﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete.Mapping
{
    public class WfActionFieldMap : EntityTypeConfiguration<WfActionField>
    {
        public WfActionFieldMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Empty)
                .IsRequired()
                .HasMaxLength(8000);
            Property(t => t.Group)
                .HasMaxLength(8000);

            // Table & Column Mappings
            ToTable("wf_action_fields", "public");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.WacId).HasColumnName("wac_id");
            Property(t => t.DflId).HasColumnName("dfl_id");
            Property(t => t.WvaId).HasColumnName("wva_id");
            Property(t => t.Empty).HasColumnName("empty");
            Property(t => t.DefaultValue).HasColumnName("default_value");
            Property(t => t.ReadOnly).HasColumnName("read_only");
            Property(t => t.AdminOnly).HasColumnName("admin_only");
            Property(t => t.ValidationRegexp).HasColumnName("validation_regexp");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.Group).HasColumnName("group");
            Property(t => t.Css).HasColumnName("css");
            Property(t => t.TextArea).HasColumnName("textarea");

            // Relationships
            HasRequired(t => t.WfAction)
                .WithMany(t => t.WfActionFields)
                .HasForeignKey(d => d.WacId);
            HasOptional(t => t.DicField)
                .WithMany(t => t.WfActionFields)
                .HasForeignKey(d => d.DflId);
            HasOptional(t => t.WfVariable)
                .WithMany(t => t.WfActionFields)
                .HasForeignKey(d => d.WvaId);

        }
    }
}
