﻿/*=================================================================*
 * Classe: <EfEntityDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité Entity. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfEntityDal : EfEntityRepositoryBase<Entity>, IEntityDal
    {
        public int AddReturnKey(Entity entity)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
                return entity.Id;
            }
        }
    }
}