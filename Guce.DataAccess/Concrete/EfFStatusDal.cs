﻿/*=================================================================*
 * Classe: <EfFStatusDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité FStatus. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Data.Entity;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfFStatusDal : EfEntityRepositoryBase<FStatus>, IFStatusDal
    {
        public int AddReturnKey(FStatus status)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var addedEntity = context.Entry(status);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
                return status.Id;
            }
        }
    }
}