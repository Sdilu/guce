﻿/*=================================================================*
 * Classe: <EfWfActionFieldDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité WfActionField. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfWfActionFieldDal : EfEntityRepositoryBase<WfActionField>, IWfActionFieldDal
    {
        public List<WfActionField> GetActionFieldsByAction(int actionId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.WfActionFields.Where(af => af.WacId == actionId).OrderBy(af => af.Order).ToList();
            }
        }
    }
}