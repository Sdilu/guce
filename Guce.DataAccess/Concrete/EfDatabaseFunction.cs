﻿/*=================================================================*
 * Classe: <Operateurs>
 * Version/date: <2.0.0> <2016.08.15>
 *
 * Description: <” Cette classe contient les fonctions qui sont implémenté dans le sereur de base de donée ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using System.Web.WebPages;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;
using Guce.Entities.Custom;
using Npgsql;

namespace Guce.DataAccess.Concrete
{
    public static class EfDatabaseFunction
    {
        public enum FunGenerateNotValidatedJsonPropertie
        {
            NotNull,
            NotValidated,
            All
        }
       
        public static List<string> f_new_flow(int wfvId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select f_new_flow(" + wfvId + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }

        public static List<string> fun_compute_entity_name(string compose, string json)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_compute_entity_name(" + compose + "," + json + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }

        
        public static List<string> fun_entity2json(int entId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_entity2json(" + entId + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }

        public static List<string> fun_eval_json_expr(string expr, string json)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_eval_json_expr(" + expr + "," + json + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }

        public static List<string> fun_generate_not_validated_json(int entId, string currentUser,
            FunGenerateNotValidatedJsonPropertie getAllProperties = FunGenerateNotValidatedJsonPropertie.NotNull)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                string parameter="not_null";
                switch (getAllProperties)
                {
                    case FunGenerateNotValidatedJsonPropertie.NotValidated:
                        parameter = "not_validated";
                        break;
                    case FunGenerateNotValidatedJsonPropertie.All:
                        parameter = "all";
                        break;
                    case FunGenerateNotValidatedJsonPropertie.NotNull:
                        parameter = "not_null";
                        break;
                }
                //select fun_generate_not_validated_json(1, 'all')
   
                var query = "select  fun_generate_not_validated_json(" + entId + ",'" + parameter +
                    "') || fun_generate_variables_json(" + entId + ")|| coalesce(attributes, '{}'::jsonb) from users where username = '" + currentUser+"'; ";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }
        
        public static List<string> fun_generate_variables_json(int flwId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_generate_variables_json(" + flwId + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }

        public static List<int> fun_test_transitions(int fstId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select * from fun_test_transitions(" + fstId + ")";
                var result = context.Database.SqlQuery<int>(query).ToList();
                return result;
            }
        }
        
        public static List<string> fun_validate_entity_values(int entId, int newTxid)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_validate_entity_values(" + entId + "," + newTxid + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }

        public static List<int> fun_validate_flow_values(int flwId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_validate_flow_values(" + flwId + ");";
                var result = context.Database.SqlQuery<int>(query).ToList();
                return result;
            }
        }

        public static List<string> fun_value2json(string value, string type, string code)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_value2json(" + value + "," + type + "," + code + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result;
            }
        }
        
        public static string fun_get_not_validated_name(int entityId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_get_not_validated_name(" + entityId + ");";
                var result = context.Database.SqlQuery<string>(query);
                return result.ToList().FirstOrDefault();
            }
        }
        
        public static string FindFluxValues(int entityId,int flowId, string currentUser)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {

                var query = "select fun_generate_not_validated_json("+entityId+", 'all')"+
                    "|| fun_generate_variables_json("+flowId+")|| "+
                    "coalesce(attributes, '{}'::jsonb) from users where username = '" + currentUser+"'; ";
                var result = context.Database.SqlQuery<string>(query);
                return result.ToList().FirstOrDefault();
            }
        }

        /// <summary>
        /// Verifier l'appartenance de l'uitilisateur courant a un role précis
        /// </summary>
        /// <param name="group">Le nom du role auuquel l'utilisateur courant peut appartenir</param>
        /// <returns>True si il appartient, sinon false</returns>
        public static bool pg_has_role(string group)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                bool booleanResult;
                var query = "select pg_has_role('" + group + "', 'MEMBER')";

                try
                {
                    var result = context.Database.SqlQuery<bool>(query);
                    booleanResult = result.ToList().FirstOrDefault();
                }
                catch (System.InvalidOperationException)
                {
                    booleanResult = false;
                }
                catch (NpgsqlException)
                {
                    booleanResult = false;
                }
                return booleanResult;
            }
        }

        public static int ChangePassword(string username, string password)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.Database.ExecuteSqlCommand("ALTER ROLE \"" + username + "\" PASSWORD '" + password + "';");
            }
        }

        public static bool IsAdmin()
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                bool booleanResult;
                var query = "select pg_has_role('grp_admins', 'MEMBER');";
                try
                {
                    var result = context.Database.SqlQuery<bool>(query);
                    booleanResult = result.ToList().FirstOrDefault();
                }
                catch (System.InvalidOperationException)
                {
                    booleanResult = false;
                }
                catch (NpgsqlException)
                {
                    booleanResult = false;
                }
                return booleanResult;
            }
        }

        /// <summary>
        /// Renvoie la liste des roles auquel l'utilisateur appartient dans la base de donnée
        /// </summary>
        /// <param name="username">Le username de l'utilisateur</param>
        /// <returns>Le role auquel il appartient</returns>
        public static List<string> GetUserRoles(string username)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select rolname from pg_user join pg_auth_members on(pg_user.usesysid = pg_auth_members.member) " +
                            "join pg_roles on(pg_roles.oid = pg_auth_members.roleid) where pg_user.usename = '" + username + "';";
                var result = context.Database.SqlQuery<string>(query);
                return result.ToList();
            }
        }

        public static List<string> GetRoles()
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select distinct rolname from pg_user join pg_auth_members on(pg_user.usesysid = pg_auth_members.member) " +
                            "join pg_roles on(pg_roles.oid = pg_auth_members.roleid);";
                var result = context.Database.SqlQuery<string>(query);
                return result.ToList();
            }
        }

        /// <summary>
        /// Recherche une entité grace a la fonction (fun_search_entity)
        /// </summary>
        /// <param name="value">Le text a rechercher</param>
        /// <returns>La liste des résultats de recherceh</returns>
        public static List<EntityMainSearch> fun_search_entity(string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select * from fun_search_entity('" + value + "')";

                var result = context.Database.SqlQuery<EntityMainSearch>(query);
                return result.ToList();
            }
        }

        public static List<SearchEntity> SearchEntities(string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select id, lock_fst_id, json from fun_search_entity('" + value + "')";
                var result = context.Database.SqlQuery<SearchEntity>(query);
                return result.ToList();
            }
        }

        public static List<FolderMainSearch> fun_search_Folder(string value)
        {
            List<FolderMainSearch> resultOfAll = null;
            int resultOfTryingParse;
            if (int.TryParse(value, out resultOfTryingParse))
            {
                int formatValue = int.Parse(value);
                using (var context = new GuceContext(GuceContextConnexion.Initialize()))
                {
                    var queryIdFlows = "select id from flows where reference = '" + formatValue.ToString("D6") + "'";
                    var resultId = context.Database.SqlQuery<int>(queryIdFlows);

                    var queryEntities = "select * from entities where id = '" + resultId.FirstOrDefault() + "'";
                    var resultEntities = context.Database.SqlQuery<FolderMainSearch>(queryEntities);

                    resultOfAll = resultEntities.ToList();
                }
            }

            return resultOfAll;
        }

        public static string CanonicalizedSearchCriteria(int dflId, string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select fun_generate_canonicalized_search_criteria(" + dflId + ", '" + value + "');";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result.FirstOrDefault();
            }
        }

        public static string FindNumeroDossier(string entityId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                try
                {
 var query = "select (fun_generate_variables_json->>'_reference') as aaa from fun_generate_variables_json(" + entityId + "');";
                var result = context.Database.SqlQuery<string>(query);
                }
                catch (NpgsqlException ex)
                {
                    string mess = ex.Message;
                }

                return "aaa"; //result.FirstOrDefault();
            }
        }

        public static List<DbEntity> ExecuteQuery(string query)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var result = context.Database.SqlQuery<DbEntity>(query);
                return result.ToList();
            }
        }

        public static List<int> GetAllDicFromEntity(int entityId, int dicId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select * from fun_get_all_dic_from_entity(" + entityId + ", " + dicId + ");";
                var result = context.Database.SqlQuery<int>(query);
                return result.ToList();
            }
        }

        public static List<Entity> SearchGlobalEntities(string searchCriterias)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select * from search_global_entities(" + searchCriterias + ");";
                var result = context.Database.SqlQuery<Entity>(query);
                return result.ToList();
            }
        }

        public static string IdxCanonicalize(int index, string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select idx_canonicalize(" + index + ",'" + value + "');";
                var result = context.Database.SqlQuery<string>(query);
                return result.FirstOrDefault();
            }
        }

        public static int IdxCheck(int index, int entityId, string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select idx_check(" + index + ", " + entityId + ", '" + value + "');";
                var result = context.Database.SqlQuery<int>(query);
                return result.FirstOrDefault();
            }
        }

        public static int IdxAddValue(int index, int entityId, string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select idx_add_value(" + index + ", " + entityId + ", '" + value + "');";
                var result = context.Database.SqlQuery<int>(query);
                return result.FirstOrDefault();
            }
        }

        public static bool IdxRemoveValue(int index, int entity, string value)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select idx_remove_value(" + index + ", " + entity + ", '" + value + "');";
                var result = context.Database.SqlQuery<bool>(query);
                return result.FirstOrDefault();
            }
        }

        public static string ImportOrUpdateGlobalEntity(string siteId, int remoteId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var query = "select import_or_update_global_entity('" + siteId + "', " + remoteId + ");";
                var result = context.Database.SqlQuery<string>(query).ToList();
                return result.FirstOrDefault();
            }
        }

        public static int CreateUser(string username, string fullname, string password, int group, string attributes)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                attributes = attributes.IsEmpty() ? "null" : "'" + attributes + "'";
                var query = "select create_user('" + username + "', '" + fullname + "', '" + password + "', " + group +
                            ", " + attributes + ");";
                return context.Database.ExecuteSqlCommand(query);
            }
        }

        public static int DeleteRole(string username)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.Database.ExecuteSqlCommand("DROP ROLE " + username + ";");
            }
        }

        public static int DeleteUserGroups(string username)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var result = context.Database.ExecuteSqlCommand("delete from user_groups where username = '" + username + "';");
                return result;
            }
        }

        public static int DeleteUser(string username)
        {
            var result = DeleteUserGroups(username);
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                if (result > 0)
                {
                    result = context.Database.ExecuteSqlCommand("delete from users where username = '" + username + "';");
                }
                return result;
            }
        }

        public static string ExecuteStat(string request)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.Database.SqlQuery<string>(request).FirstOrDefault();
            }
        }
    }
}