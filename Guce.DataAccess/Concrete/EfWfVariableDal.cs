﻿/*=================================================================*
 * Classe: <EfWfVariableDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité WfVariable. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfWfVariableDal : EfEntityRepositoryBase<WfVariable>, IWfVariableDal
    {

    }
}