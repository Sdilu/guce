﻿/*=================================================================*
 * Classe: <GuceContextConnexion>
 * Version/date: <1.0.0> <2016.07.13>
 *
 * Description: <” Cette classe fournit les fonctions pour tester la validité d'un utilisateur. ”>
 * Specificities: <“ C'est une classe utilitaire ”>
 *
 * Authors: <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Data;
using Guce.Entities.Custom;
using Npgsql;

namespace Guce.DataAccess.Concrete.Context
{
    /// <summary>
    /// Cette classe fournit les fonctions pour tester la validité d'un
    /// utilisateur
    /// </summary>
    public class GuceContextConnexion
    {
        /// <summary>
        /// Initialise les éléments de connection: Le provider, le Builder de connexion..
        /// 
        /// </summary>
        /// <returns>L'objet NpgsqlConnection nécessaire pour se connecter</returns>
        public static NpgsqlConnection Initialize()
        {
            // Initialize the connection string builder for the
            // underlying provider.
            var sqlBuilder = new NpgsqlConnectionStringBuilder
            {
                Host = Configuration.Server,
                Database = Configuration.Database
            };

            // Set the properties for the data source.

            var oneUser = GuceContextSession.ReadCookie();
            sqlBuilder.Username = oneUser.Username;
            sqlBuilder.Password = oneUser.Password;


            // Build the SqlConnection connection string.
            var providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            var entityBuilder = new NpgsqlConnectionStringBuilder
            {
                ConnectionString = providerString,
                CommandTimeout = 360
            };

            // Set the provider-specific connection string.
            var conn = new NpgsqlConnection(entityBuilder.ToString());
            
            return conn;
        }

        /// <summary>
        /// Teste la validité de l'utilisateur, elle effectue juste une ouverture et une
        /// fermeture de connexion pour verifier la possibilité de se connecter avec les
        /// informations de conexion fournit en paramètre
        /// </summary>
        /// <param name="user">Le nom d'utilisateur</param>
        /// <param name="password">Le mot de passe</param>
        /// <returns>
        ///     True : La connetion est possible
        ///     False : La connexion n'est pas possible</returns>
        public static bool TestContext(string user, string password)
        {
            var context = new GuceContext(Initialize());
            var test = true;
            try
            {
                context.Database.Connection.Open();
            }
            catch (NpgsqlException ex)
            {
                test = false;
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                test = false;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (context.Database.Connection.State == ConnectionState.Open && test)
                {
                    test = true;
                    context.Database.Connection.Close();
                }
                else
                {
                    test = false;
                }
            }
            context.Dispose();
            return test;
        }
    }
}