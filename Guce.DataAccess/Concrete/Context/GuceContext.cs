﻿/*=================================================================*
 * Classe: <GuceContext>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est la classe de contexte pour la connexion à la base de données. ”>
 * Specificities: <“ Cette classe est une DbContext ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Data.Entity;
using Guce.DataAccess.Concrete.Mapping;
using Guce.Entities.ComplexType;
using Guce.Entities.Concrete;
using Npgsql;

namespace Guce.DataAccess.Concrete.Context
{
    public class GuceContext : DbContext
    {
        static GuceContext()
        {
            Database.SetInitializer<GuceContext>(null);
        }

        public GuceContext(NpgsqlConnection conn)
            : base(conn, contextOwnsConnection: true)
        {
           
        }

        public DbSet<BusinessRule> BusinessRules { get; set; }
        public DbSet<DicCodeItem> DicCodeItems { get; set; }
        public DbSet<DicCode> DicCodes { get; set; }
        public DbSet<DicField> DicFields { get; set; }
        public DbSet<DicVersionCode> DicVersionCodes { get; set; }
        public DbSet<DicVersionField> DicVersionFields { get; set; }
        public DbSet<DicVersion> DicVersions { get; set; }
        public DbSet<Dictionary> Dictionnaries { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<FStatus> FStatus { get; set; }
        public List<FStep> FSteps { get; set; }
        public DbSet<FVariableValue> FVariableValues { get; set; }
        public DbSet<Flow> Flows { get; set; }
        public DbSet<Parameter> Parameters { get; set; } 
        public DbSet<Group> Groups { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<StatPage> StatPages { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Value> Values { get; set; }
        public DbSet<WfActionField> WfActionFields { get; set; }
        public DbSet<WfAction> WfActions { get; set; }
        public DbSet<WfTransition> WfTransitions { get; set; }
        public DbSet<WfVariable> WfVariables { get; set; }
        public DbSet<WfVersion> WfVersions { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<VwActiveWorkflow> VwActiveWorkflows { get; set; }
        public DbSet<VwWorkflow> VwWorkflows { get; set; }
        public DbSet<VwUserNextAction> VwUserNextActions { get; set; }
        public DbSet<VwActiveNextAction> VwActiveNextActions { get; set; }
        public DbSet<VwCurrentValue> VwCurrentValues { get; set; }
        public DbSet<VwCurrentVariableValue> VwCurrentVariableValues { get; set; }
        public DbSet<VwFormField> VwFormFields { get; set; }
        public DbSet<VwAllNextAction> VwAllNextActions { get; set; }
        public DbSet<VwAllValue> VwAllValues { get; set; }
        public DbSet<VwAllVariableValue> VwAllVariableValues { get; set; }
        public DbSet<VwDependantDicCode> VwDependantDicCodes { get; set; }
        public DbSet<VwDependantDicCodeItem> VwDependantDicCodeItems { get; set; }
        public DbSet<VwValidatedValue> VwValidatedValues { get; set; }
        public DbSet<VwLastValidatedValues> VwLastValidatedValues { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<Index> Indexes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BusinessRuleMap());
            modelBuilder.Configurations.Add(new DicCodeItemMap());
            modelBuilder.Configurations.Add(new DicCodeMap());
            modelBuilder.Configurations.Add(new DicFieldMap());
            modelBuilder.Configurations.Add(new DicVersionCodeMap());
            modelBuilder.Configurations.Add(new DicVersionFieldMap());
            modelBuilder.Configurations.Add(new DicVersionMap());
            modelBuilder.Configurations.Add(new DictionaryMap());
            modelBuilder.Configurations.Add(new EntityMap());
            modelBuilder.Configurations.Add(new FStatusMap());
            modelBuilder.Configurations.Add(new FStepMap());
            modelBuilder.Configurations.Add(new FVariableValueMap());
            modelBuilder.Configurations.Add(new FlowMap());
            modelBuilder.Configurations.Add(new IndexMap());
            modelBuilder.Configurations.Add(new GroupMap());
            modelBuilder.Configurations.Add(new SiteMap());
            modelBuilder.Configurations.Add(new ReportMap());
            modelBuilder.Configurations.Add(new StatPageMap());
            modelBuilder.Configurations.Add(new ParameterMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserGroupMap());
            modelBuilder.Configurations.Add(new ValueMap());
            modelBuilder.Configurations.Add(new WfActionFieldMap());
            modelBuilder.Configurations.Add(new WfActionMap());
            modelBuilder.Configurations.Add(new WfTransitionMap());
            modelBuilder.Configurations.Add(new WfVariableMap());
            modelBuilder.Configurations.Add(new WfVersionMap());
            modelBuilder.Configurations.Add(new WorkflowMap());

            modelBuilder.Configurations.Add(new VwActiveNextActionMap()); 
            modelBuilder.Configurations.Add(new VwActiveWorkflowMap());
            modelBuilder.Configurations.Add(new VwAllNextActionMap());
            modelBuilder.Configurations.Add(new VwAllValueMap());
            modelBuilder.Configurations.Add(new VwAllVariableValueMap());
            modelBuilder.Configurations.Add(new VwCurrentValueMap());
            modelBuilder.Configurations.Add(new VwCurrentVariableValueMap());
            modelBuilder.Configurations.Add(new VwDependantDicCodeMap());
            modelBuilder.Configurations.Add(new VwDependantDicCodeItemMap());
            modelBuilder.Configurations.Add(new VwFormFieldMap());
            modelBuilder.Configurations.Add(new VwUserNextActionMap());
            modelBuilder.Configurations.Add(new VwValidatedValueMap());
            modelBuilder.Configurations.Add(new VwWorkflowMap());
            modelBuilder.Configurations.Add(new VwLastValidatedValuesMap());
        }
    }
}
