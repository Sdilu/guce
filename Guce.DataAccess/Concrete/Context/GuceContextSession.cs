﻿/*=================================================================*
 * Classe: <GuceContextSession>
 * Version/date: <1.0.0> <2016.07.13>
 *
 * Description: <” Cette classe permet de gérer les variables de session. ”>
 * Specificities: <“ C'est une classe utilitaire ”>
 *
 * Authors: <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Web;
using System.Web.Security;
using Guce.Entities.Custom;
using static Guce.Entities.Custom.Configuration;

namespace Guce.DataAccess.Concrete.Context
{
    /// <summary>
    /// Permet de gérer les sessions pour l'authentification de l'utilisateur.
    /// Elle ne comporte que des classes public static
    /// </summary>
    public class GuceContextSession
    {
        /// <summary>
        /// Crée un nouveau cookie pour l'authentification de l'utilisateur
        /// </summary>
        /// <param name="values">L'objet User, il comporte les informations de connexion (User et password) sa
        /// saise par l'utilisateur
        /// <remarks>La session est valide selon les parametre de la fonction 'Configuration.SessionDuration', apres quoi l'utilisateur doit se reconnecter</remarks>
        /// </param>
        public static void CreateCookies(User values)
        {
            var sessionTimeout = Parameters[ParameterId.SessionTimeOut]?.ToString() ?? "60";

            var ticket = new FormsAuthenticationTicket(1,
              values.Username,
              DateTime.Now,
              DateTime.Now.AddMinutes(int.Parse(sessionTimeout)),
              false,
              values.Password,
              FormsAuthentication.FormsCookiePath);

            var ticket1 = new FormsAuthenticationTicket(1,
             "group",
             DateTime.Now,
             DateTime.Now.AddMinutes(int.Parse(sessionTimeout)),
             false,
             values.UserGroup,
             FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);
            var encTicket1 = FormsAuthentication.Encrypt(ticket1); // This is for stored the userGroup

            // Create the cookie.
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            var cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket1);

            // Create the cookie.
            var response = HttpContext.Current.Response;
            response.Cookies.Add(cookie);
            response.Cookies.Add(cookie1);
        }

        /// <summary>
        /// Lis les informations de connexion de l'utilisateur, d'abord depuis la session en cours ensuite 
        /// dans les cookie
        /// </summary>
        /// <returns>L'objet User qui contient les informations de connexion (user et password)</returns>
        public static User ReadCookie()
        {
            var mainUser = new User {Authentifie = false};

            var session = HttpContext.Current.Session;
            mainUser.Username = (string)session["username"];
            mainUser.Password = (string)session["password"];
            mainUser.UserGroup = (string)session["userGroup"];

            if (mainUser.Username == null && mainUser.Password == null)
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    var authCookie = request.Cookies[FormsAuthentication.FormsCookieName];
                    if (authCookie != null)
                    {
                        var ticket = FormsAuthentication.Decrypt(authCookie.Value);

                        //string cookiePath = ticket.CookiePath;
                        //DateTime expiration = ticket.Expiration;
                        //bool expired = ticket.Expired;
                        //bool isPersistent = ticket.IsPersistent;
                        //DateTime issueDate = ticket.IssueDate;
                        if (ticket != null)
                        {
                            mainUser.Username = ticket.Name;
                            mainUser.Password = ticket.UserData;
                        }
                    }

                    var authCookie1 = request.Cookies["group"];
                    if (authCookie1 != null)
                    {
                        var ticket = FormsAuthentication.Decrypt(authCookie1.Value);

                        //string cookiePath = ticket.CookiePath;
                        //DateTime expiration = ticket.Expiration;
                        //bool expired = ticket.Expired;
                        //bool isPersistent = ticket.IsPersistent;
                        //DateTime issueDate = ticket.IssueDate;
                        if (ticket != null)
                        {
                            mainUser.UserGroup = ticket.UserData;
                        }
                        mainUser.Authentifie = true;
                    }
                    //int version = ticket.Version;
                }
                catch (NullReferenceException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (mainUser.Username == null && mainUser.Password == null)
            {
                mainUser.Username = "e";
                mainUser.Password = "e";
                mainUser.Authentifie = false;
            }
            return mainUser;

        }

        /// <summary>
        /// Détruit le cookie de connexion de l'utilsateur et met fin a la connexion
        /// </summary>
        public static void DestroyCookies()
        {
            var request = HttpContext.Current.Request;
            /* Abandon session object to destroy all session variables */
            FormsAuthentication.SignOut();
            var c = request.Cookies[FormsAuthentication.FormsCookieName];
            if (c == null) return;
            c.Expires = DateTime.Now.AddDays(-1);

            var d = request.Cookies["group"];
            if (d == null) return;
            d.Expires = DateTime.Now.AddDays(-1);

            // Update the amended cookie!
            var response = HttpContext.Current.Response;
            response.Cookies.Set(c);
            response.Cookies.Set(d);
        }
    }
}