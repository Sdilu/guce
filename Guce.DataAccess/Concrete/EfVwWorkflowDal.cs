﻿/*=================================================================*
 * Classe: <EfVwWorkflowDal>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour le type complexe VwWorkflow. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete
{
    public class EfVwWorkflowDal : EfEntityRepositoryBase<VwWorkflow>, IVwWorkflowDal
    {
         
    }
}