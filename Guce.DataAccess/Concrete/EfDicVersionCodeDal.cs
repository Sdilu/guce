﻿/*=================================================================*
 * Classe: <EfDicVersionCodeDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implementer le repository pattern pour l'entité DicVersionCode. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfDicVersionCodeDal: EfEntityRepositoryBase<DicVersionCode>, IDicVersionCodeDal
    {
         
    }
}