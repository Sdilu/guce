﻿/*=================================================================*
 * Classe: <EfDicCodeItemDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implementer le repository pattern pour l'entité DicCodeItem. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfDicCodeItemDal : EfEntityRepositoryBase<DicCodeItem>, IDicCodeItemDal
    {
         
    }
}