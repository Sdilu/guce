﻿/*=================================================================*
 * Classe: <EfWfTransitionDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité WfTransition. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using System.Linq;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfWfTransitionDal : EfEntityRepositoryBase<WfTransition>, IWfTransitionDal
    {
        public List<WfTransition> GetFromTransitionsByAction(int actionId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.WfTransitions.Where(t => t.FromWacId == actionId).OrderBy(o => o.Weight).ToList();
            }
        }

        public List<WfTransition> GetToTransitionsByAction(int actionId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.WfTransitions.Where(t => t.ToWacId == actionId).OrderBy(o => o.Weight).ToList();
            }
        }
    }
}