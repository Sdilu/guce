﻿/*=================================================================*
 * Classe: <EfStatPageDal>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité StatPage. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfStatPageDal : EfEntityRepositoryBase<StatPage>, IStatPageDal
    {

    }
}