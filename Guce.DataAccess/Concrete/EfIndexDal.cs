﻿/*=================================================================*
 * Classe: <EfIndexDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité Index. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/
 
using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfIndexDal : EfEntityRepositoryBase<Index>, IIndexDal
    {

    }
}