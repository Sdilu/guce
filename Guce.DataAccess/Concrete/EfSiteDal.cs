﻿/*=================================================================*
 * Classe: <EfSiteDal>
 * Version/date: <2.0.0> <2016.09.21>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité Site. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfSiteDal : EfEntityRepositoryBase<Site>, ISiteDal
    {

    }
}