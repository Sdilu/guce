﻿/*=================================================================*
 * Classe: <EfVwDependantDicCodeDal>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour le type complexe VwDependantDicCode. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete
{
    public class EfVwDependantDicCodeDal : EfEntityRepositoryBase<VwDependantDicCode>, IVwDependantDicCodeDal
    {
         
    }
}