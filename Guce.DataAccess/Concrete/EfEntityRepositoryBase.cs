﻿/*=================================================================*
 * Classe: <EfEntityRepositoryBase>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe est repository pattern pour l'ORM entityframework. ”>
 * Specificities: <“ Cette classe est une classe de pattern ”>
 *
 * Authors:   <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Abstract;

namespace Guce.DataAccess.Concrete
{
    public class EfEntityRepositoryBase<TEntity> : IEntityRepository<TEntity>
         where TEntity : class, IEntity, new()
    {
        
        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                return context.Set<TEntity>().SingleOrDefault(filter);
            }
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null,
              Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
              int? page = null, int? pageSize = null)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                IQueryable<TEntity> query = context.Set<TEntity>();
                if (filter != null)
                {
                    query = query.Where(filter);
                }
                
                if (orderBy != null)
                {
                    query = orderBy(query);
                }

                if (page != null && pageSize != null)
                {
                    query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
                }
                
                return query.ToList();
            }
        }

        public void Add(TEntity entity)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public void Update(TEntity entity)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var updatedEntity = context.Entry(entity);
                updatedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(TEntity entity)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var deletedEntity = context.Entry(entity);
                deletedEntity.State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}