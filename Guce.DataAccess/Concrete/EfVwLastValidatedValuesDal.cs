﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Guce.DataAccess.Abstract;
using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Concrete
{
    public class EfVwLastValidatedValuesDal : EfEntityRepositoryBase<VwLastValidatedValues>, IVwLastValidatedValuesDal
    {
    }
}
