﻿/*=================================================================*
 * Classe: <EfUserGroupDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette classe permet d'implémenter le repository pattern pour l'entité UserGroup. ”>
 * Specificities: <“ - ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Linq;
using Guce.DataAccess.Abstract;
using Guce.DataAccess.Concrete.Context;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Concrete
{
    public class EfUserGroupDal : EfEntityRepositoryBase<UserGroup>, IUserGroupDal
    {
        public bool IsAllowExecuteAction(string username, int grpId)
        {
            using (var context = new GuceContext(GuceContextConnexion.Initialize()))
            {
                var userGroups = context.UserGroups.ToList();
                return userGroups.Any(ug => ug.GrpId == grpId && ug.Username.Equals(username));
            }
        }
    }
}