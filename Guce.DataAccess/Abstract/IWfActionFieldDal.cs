﻿/*=================================================================*
 * Interface: <IWfActionFieldDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: WfActionField :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IWfActionFieldDal: IEntityRepository<WfActionField>
    {
        List<WfActionField> GetActionFieldsByAction(int actionId);
    }
}