﻿/*=================================================================*
 * Interface: <IBusinessRuleDal>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: BusinessRule :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IBusinessRuleDal : IEntityRepository<BusinessRule>
    {
         
    }
}