﻿/*=================================================================*
 * Interface: <IUserDal>
 * Version/date: <2.0.0> <2016.10.21>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: User :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IUserDal: IEntityRepository<User>
    {
        
    }
}