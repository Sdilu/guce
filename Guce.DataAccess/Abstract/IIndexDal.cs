﻿/*=================================================================*
 * Interface: <IIndexDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: Index :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IIndexDal: IEntityRepository<Index>
    {
    
    }
}