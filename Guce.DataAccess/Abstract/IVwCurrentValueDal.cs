﻿/*=================================================================*
 * Interface: <IVwCurrentValueDal>
 * Version/date: <2.0.0> <2016.07.11>
 *
 * Description: <” Cette interface est le referentiel pour le type complexe :: VwCurrentValue :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Abstract
{
    public interface IVwCurrentValueDal : IEntityRepository<VwCurrentValue>
    {
         
    }
}