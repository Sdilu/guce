﻿/*=================================================================*
 * Interface: <IVwAllVariableValueDal>
 * Version/date: <2.0.0> <2016.08.08>
 *
 * Description: <” Cette interface est le referentiel pour le type complexe :: VwAllVariableValue :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.ComplexType;

namespace Guce.DataAccess.Abstract
{
    public interface IVwAllVariableValueDal : IEntityRepository<VwAllVariableValue>
    {
         
    }
}