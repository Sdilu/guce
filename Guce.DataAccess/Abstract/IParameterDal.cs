﻿/*=================================================================*
 * Interface: <IParameterDal>
 * Version/date: <2.0.0> <2016.08.30>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: Parameter :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IParameterDal: IEntityRepository<Parameter>
    {
        string GetValue(string id, string username);
        Parameter GetParameter(string id, string username);
        List<Parameter> GetParameters(string username);
    }
}