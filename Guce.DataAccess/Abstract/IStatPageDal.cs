﻿/*=================================================================*
 * Interface: <IStatPageDal>
 * Version/date: <2.0.0> <2017.01.13>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: StatPage :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IStatPageDal: IEntityRepository<StatPage>
    {

    }
}