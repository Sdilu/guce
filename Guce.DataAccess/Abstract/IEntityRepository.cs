﻿/*=================================================================*
 * Interface: <IEntityRepository>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface est le referentiel des interfaces. Toutes classes qui implementent cette derniere,
 * implementent directement toutes ses methodes. Elle implemente les methodes de bases pour une entité. ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Guce.Entities.Abstract;

namespace Guce.DataAccess.Abstract
{
    public interface IEntityRepository<T> where T : class, IEntity, new()
    {
        T Get(Expression<Func<T, bool>> filter);

        List<T> GetList(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            int? page = null, int? pageSize = null);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}