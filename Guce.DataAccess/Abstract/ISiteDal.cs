﻿/*=================================================================*
 * Interface: <ISiteDal>
 * Version/date: <2.0.0> <2016.09.21>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: Site :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface ISiteDal: IEntityRepository<Site>
    {

    }
}