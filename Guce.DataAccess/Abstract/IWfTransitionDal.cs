﻿/*=================================================================*
 * Interface: <IWfTransitionDal>
 * Version/date: <2.0.0> <2016.07.07>
 *
 * Description: <” Cette interface est le referentiel pour l'entité :: WfTransition :: ”>
 * Specificities: <“ Cette classe est un repository pattern ”>
 *
 * Authors:  <Steve> <DILU>
 * Copyright: GUCE/ESSOR, all rights reserved
 *
 *================================================================*/

using System.Collections.Generic;
using Guce.Entities.Concrete;

namespace Guce.DataAccess.Abstract
{
    public interface IWfTransitionDal: IEntityRepository<WfTransition>
    {
        List<WfTransition> GetFromTransitionsByAction(int actionId);
        List<WfTransition> GetToTransitionsByAction(int actionId);
    }
}